require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('message', require('./components/message.vue').default);
Vue.component('notification', require('./components/Notification.vue'));

const app = new Vue({
    el: '#app',
    data: {
        message: '',
        chat : {
            messages: []
        },
        notifications : []
    },
    methods: {
    	send : function(){
    		if( this.message.length != 0){
                this.chat.messages.push({ message: this.message, from: 'replies' });
                
                axios.post('/send', {
                    message: this.message,
                    transaction_id: $('input[name=transaction_id]').val()
                })
                .then(response => {
                    console.log(response.data);

                    this.message = '';
                })
                .catch(error => {
                    console.log(error);
                });
            }
        }
    },
    mounted(){
        Echo.private('chat')
        .listen('ChatEvent', (e) => {
            if( $('input[name=UserId]').val() != e.user.id){
                this.chat.messages.push({ message: e.message, from: 'replies' });
            }else{
                this.chat.messages.push({ message: e.message, from: 'sent' });
            }
        });
    },
    //     created(){
    //         axios.get('/vendor/notifications').then(response=>{
    //             this.notifications =response.data;
    //         });
    //         var userId= $('meta[name="userid"]').attr('content');
    //         Echo.private('App.User.' + userId)
    //         .notification((notification) => {
    //         this.notifications.push(notification);
    //     });
    // }
    created(){
        axios.get('/notifications').then(response=>{
            this.notifications = response.data;
        });
        var userId= $('meta[name="userId"]').attr('content');
        Echo.private('App.User.' + userId)
        .notification((notification) => {
            this.notifications.push(notification);
            console.log(notification.type);
        });
    }
});