@extends('layouts/base')
@section('addstyle')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css"/>
@endsection
@section('content')
<div class="job-posting-form container">
	@if(Session::has('profileRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('profileRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('addressRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('addressRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('success'))
		    <div class="alert alert-success">
		      <p style="color:green;">{{Session::get('success')}}</p>
		      @php Session::forget('success'); @endphp
		    </div>
	@endif
	
	<div class="form-fields col-md-12">
	<form role="form" method="POST" action="">
			{!! csrf_field() !!}
		<div class="vfform  venues" id="venues" >
				<div class="title">
				<h3>Job For TourGuide</h3>
				</div>
			<div class="feildcont">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6 ">
								<label >Title<em>*</em></label>
								<input type="text" name="title" placeholder="Title">
								<div class="clearfix"></div>
		                        @if($errors->has('title'))
		                            <span class="text-danger">
		                                {{ ucwords($errors->first('title')) }}
		                            </span>
		                        @endif
							</div>
							
						</div>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-6 ">
								<label >Number of people? <em>*</em></label>
								<input type="number" name="people" placeholder="0">
								<div class="clearfix"></div>
		                        @if($errors->has('people'))
		                            <span class="text-danger">
		                                {{ $errors->first('people') }}
		                            </span>
		                        @endif
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-2 ">
								<label class="filter-heading">From</label>
								<select name="fromcountry" id="countryfrom"  onchange="fgetstate(this)">
									<option value="">country</option>
										@foreach($country as $key=>$val)
									<option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
										@endforeach
								</select>
								<div class="clearfix"></div>
		                        @if($errors->has('fromcountry'))
		                            <span class="text-danger">
		                                {{ $errors->first('fromcountry') }}
		                            </span>
		                        @endif
							</div>
							<div class="col-md-2 ">
								<label>&nbsp;</label>
								<select name="fromstate" id="fstate"  onchange="fgetcity(this)">
									<option value="">State</option>
								</select>
								<div class="clearfix"></div>
		                      
							</div>
							<div class="col-md-2 ">
								<label>&nbsp;</label>
								<select name="fromcity"  id="fcity">
									<option value="">City</option>
								</select>
								<div class="clearfix"></div>
		                       
							</div>
							<div class="col-md-2 ">
								<label class="filter-heading">To</label>
								<select name="tocountry" id="countrys" onchange="tgetstates(this)">
									<option value="">Country</option>
										@foreach($country as $key=>$val)
									<option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
										@endforeach
								</select>
								<div class="clearfix"></div>
		                        @if($errors->has('tocountry'))
		                            <span class="text-danger">
		                                {{ $errors->first('tocountry') }}
		                            </span>
		                        @endif
							</div>
							<div class="col-md-2 ">
								<label> &nbsp;</label>
								<select name="tostate" id="states"  onchange="tgetcitys(this)">
									<option value="">State</option>
								</select>
								<div class="clearfix"></div>
		                       
							</div>
							<div class="col-md-2 ">
								<label>&nbsp;</label>
								<select name="tocity"  id="citys">
									<option value="">City</option>
								</select>
								<div class="clearfix"></div>
		                       
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<label>Date & Time</label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-6">
										<label>Start </label>
										<div class="input-group date" id="datePicker">
											<input type="text" name="fromdate" value="">
											<span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
										</div>
										<div class="clearfix"></div>
										@if($errors->has('fromdate'))
										<span class="text-danger">{{ $errors->first('fromdate') }}</span>
										@endif 
									</div>
									<div class="col-md-6">
										<label>End </label>
										<div class="input-group date " id="enddatePicker">
											<input type="text" name="todate" value="">
											<span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
										</div>
										<div class="clearfix"></div>
										@if($errors->has('todate'))
										<span class="text-danger">{{ $errors->first('todate') }}</span>
										@endif
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
				<div class="text-center">
					
					<button type="submit" id="" class="button solid ">Next</button>
					
				</div>		
			</div>
		</div>
	</form>
</div>
		

@endsection
@section('addjavascript')	
	<script type="text/javascript" src="{{asset('js/tourGuideProfile.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/tourGuide.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/address.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
@endsection