@extends('../layouts/base')
  @section('content')
<div class="freelancer-list-container job-posting-form container panel-group">
	<div class="row">
		<div class="vfform">
			@if(Session::has('profileRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('profileRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('addressRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('addressRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('success'))
		    <div class="alert alert-success">
		      <p style="color:green;">{{Session::get('success')}}</p>
		      @php Session::forget('success'); @endphp
		    </div>
		@endif
			<div class="search-freelancer-filter heading col-sm-12">
				
				<form class="search-form freelancer-search-form" method="post" id="myForm" name="searchForm" >
				
					<div class="freelancer-search-form-wrapper col-sm-12">
						<div class="row">
							<input type="text" class="q-sf" placeholder="Search for freelancers" name="filter_country" value="{{$addedcountry}}" readonly="">
							<select class="dq-sf" id="filterstate" name="filter_state">
								<option value="">Select State</option>
								<?php
					                foreach ($state as $key => $value) {
					                  ?>
					                  <option name="state" value="{{$value['id']}}">{{$value['name']}}</option>
					                <?php
					                }
					                ?>
							</option> 
						</select>
						<select class="dq-sf" id="cityfilter" name="filter_city">
								<option value="">
							</option> 
						</select>
							<input type="text" class="q-sf" placeholder="Search by name" name="filter_name" value="<?php echo isset($data['search_params']['filter_name']) ? $data['search_params']['filter_name'] : ''; ?>">
							<button class="button solid st-btn" type="submit"><i class="icon" data-icon=""></i></button>
							{{-- <button id="search_guide_btn" class="button solid" type="button"><i class="fa fa-filter" aria-hidden="true"></i>&nbsp;Filters</button> --}}
						</div>
					</div>
					
					<div class="filters-div col-sm-12 hide">
							<div class="col-sm-2">
							<label class="filter-heading">Gender</label>
							<ul>
								<li>
									<input type="radio" name="filter_gender" value="f"<?php echo isset($data['search_params']['filter_gender']) && $data['search_params']['filter_gender'] == 'f' ? 'checked' : ''; ?>>&nbsp;
									<span>Female</span>
								</li>
								<li>
									<input type="radio" name="filter_gender" value="m"<?php echo isset($data['search_params']['filter_gender']) && $data['search_params']['filter_gender'] == 'm' ? 'checked' : ''; ?>>&nbsp;
									<span>Male</span>
								</li>
								<li>
									<input type="radio" name="filter_gender" value=""<?php echo isset($data['search_params']['filter_gender']) && $data['search_params']['filter_gender'] == '' ? 'checked' : ''; ?>>&nbsp;
									<span>Friends</span>
								</li>
							</ul>
							</div>
							<div class="col-sm-3">
							<label class="filter-heading">Status</label>
							<ul>
								<li>
									<input type="radio" name="filter_status" value="0"<?php echo isset($data['search_params']['filter_status']) && $data['search_params']['filter_status'] == 'citizen' ? 'checked' : ''; ?>>&nbsp;
									<span>citizen</span>
								</li>
								<li>
									<input type="radio" name="filter_status" value="1"<?php echo isset($data['search_params']['filter_status']) && $data['search_params']['filter_status'] == 'lawfulResident' ? 'checked' : ''; ?>>&nbsp;
									<span>Lawful Resident</span>
								</li>
								<li>
									<input type="radio" name="filter_status" value="2"<?php echo isset($data['search_params']['filter_status']) && $data['search_params']['filter_status'] == 'workPermit' ? 'checked' : ''; ?>>&nbsp;
									<span>Work Permit</span>
								</li>
								<li>
									<input type="radio" >&nbsp;
									<span>All</span>
								</li>
							</ul>
						</div>
						<div class="col-sm-3 filter-category-wrap">
							
							<label class="filter-heading">Language</label>
							<select name="filter_language" class="form-control">
								<option value="">Language</option>
							</select>
							<label class="filter-heading">Activities</label>
							<select name="filter_activities" id="filtActivity" class="form-control">
					             <option  value="">Select Activity</option>
								<?php
								$activity = App\TourGuide::$activies;
								foreach ($activity as $key => $value) {
									?>
								<option value="<?php echo $key; ?>" <?php echo isset($data['search_params']['filter_activity']) && $data['search_params']['filter_activity'] == $key ? 'selected' : ''; ?>><?php echo $value; ?></option>
								<?php
								}
								?>
								<option value=""></option>
							</select>

							<br>
						</div>

						<div class="col-sm-4">
							<label class="filter-heading">Job Success</label>
							<ul>
								<li>
									<input type="radio" name="filter_job_success" value="" <?php echo isset($data['search_params']['filter_job_success']) && $data['search_params']['filter_job_success'] != '' ? '' : 'checked'; ?>>&nbsp;
									<span>Any job success</span>
								</li>
								<li>
									<input type="radio" name="filter_job_success" value="80" <?php echo isset($data['search_params']['filter_job_success']) && $data['search_params']['filter_job_success'] == '80' ? 'checked' : ''; ?>>&nbsp;
									<span>80&#37; &#38; up</span>
								</li>
								<li>
									<input type="radio" name="filter_job_success" value="90" <?php echo isset($data['search_params']['filter_job_success']) && $data['search_params']['filter_job_success'] == '90' ? 'checked' : ''; ?>>&nbsp;
									<span>90&#37; &#38; up</span>
								</li>
							</ul>
						</div>

						<div class="col-sm-12 filter-close-wrap">
							<button class="button default" id="close_filter" type="button">Close filters</button>
							<button class="button solid" id="getformdata" type="submit">Apply filters</button>
						</div>
					</div>
				</form>
			</div>

			<div class="panel-body feildcont field freelancer-listing" id="tgtmplate">
				<?php
				if (sizeof($data['all_tourguide']) > 0)
				{
					foreach ($data['all_tourguide'] as $keyAF => $valueAF)
					{
					?>
						<div class="col-sm-12 freelancer-details searchtourguidefilter">
							<div class="col-sm-3">
								<div class="imagetr">
									<?php
	                                        if ($valueAF->path != NULL) { ?>
									<img src="/<?php echo $valueAF->path; ?>" class="img-responsive img-circle" >
									<?php
	                                }else{
	                                    ?>
									<img src="/images/no-user.png" class="img-responsive img-circle">
									 <?php
	                                    }
	                                    ?>
								</div>
							</div>
							<div class="col-sm-7">
								<p><a href="/tourguide/{{$valueAF->id}}/info"><?php if($valueAF->firstName != ""){
										echo $valueAF->firstName . " " . $valueAF->lastName;
									}else{
										echo  $valueAF->username;
									} ?></a></p>
								<p><?php echo substr($valueAF->business_desc, 0, 115); ?>...</p>
								<div class="freelancer-earnings-div">

									<span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerHour . " / hr"; ?></span>
									<!-- <span><i class="fa fa-usd" aria-hidden="true"></i>lorem earned</span> -->
									<span>
										
										<?php
											$barPercent = 0;
											if(array_key_exists($valueAF->id, $stars)){
												echo $stars[$valueAF->id]['success_percent'] . "% Job Success";
												$barPercent = $stars[$valueAF->id]['success_percent'];
											}else{
												echo 0 . "% Job Success";

											}
										?>
										
										<div class="job-success-progress"><div class="progress-complete" style="width: <?php if($barPercent != 0){echo $barPercent.'%';}else{echo '0';} ?>%" data-toggle="tooltip" data-placement="top" title="{{ $barPercent.'% success job' }}"></div><div class="progress-pending" style="width: <?php if($barPercent != 0){echo 100 - $barPercent.'%';}else{echo '0';} ?>%"></div></div>
									</span>
									<?php
									if ($valueAF->address_country != '')
									{
									?>
										<span><i class="fa fa-map-marker"></i><?php echo $valueAF->address_country; ?></span>
									<?php
									}
									?>
								</div>
							</div>
							<div class="col-sm-2">
								<input type="button" name="invite_to_job" class="button solid invite-to-job pull-right" onclick="initiate_invite('<?php echo $valueAF->id; ?>');" value="Invite To Guide">
							</div>
						</div>
					<?php
					}
					}
					else
					{
					?>
					<div class="list-empty">
						<h3 class="text-center">No Tour Guide found based on your search criteria.</h3>
					</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>

<div id="invitation_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="send_invitation_form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<span class="modal-title">Invite <a href="javascript:void(0);" class="invitation_rcvr_name"></a> to apply regarding</span>
				</div>
				<div class="modal-body">
					<div class="col-sm-12 form-group">
						<div class="row">
							<div class="col-sm-1">
								<div class="row userimgappend">
									<img src="/images/no-user.png" class="img-responsive img-circle">
								</div>
							</div>
							<div class="col-sm-11">
								<span><a href="javascript:void(0);" class="invitation_rcvr_name"></a></span><br>
								<span></span>
							</div>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<div class="row">
							<label>Message</label>
							<input type="hidden" name="invitation_rcvr_id" class="invitation_rcvr_id">
							<textarea class="form-control" placeholder="Write your message here..." name="invitation_message"></textarea>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<div class="row">
							<?php
							if (sizeof($data['vendor_all_jobs']) > 0)
							{
							?>
								<label>Choose a job</label>
								<select class="form-control" name="invitation_jobs" id="invitation_jobs">
									<?php
									foreach ($data['vendor_all_jobs'] as $keyVAJ => $valueVAJ)
									{
									?>
										<option value="<?php echo $valueVAJ->id; ?>"><?php echo $valueVAJ->title; ?></option>
									<?php
									}
									?>
								</select>
							<?php
							}
							?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<?php
					if (sizeof($data['vendor_all_jobs']) > 0)
					{
					?>
						<input type="button" id="send_job_invite_btn" class="btn btn-block btn-success" onclick="send_invitation();" value="Send Invitation">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<?php
					}
					?>
					<a href="javascript:void(0);" class="text-center btn-block"></a>
					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('addjavascript')	
	<script type="text/javascript" src="{{asset('js/tourGuideProfile.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/tourGuide.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/address.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

	<script type="text/template" id="tgListTmpl">     
    <% _.each(data,function(v,i,l){ %>
			<div class="col-sm-12 freelancer-details searchtourguidefilter">
							<div class="col-sm-3">
								<img src="/<%= v.path %>" class="img-responsive">
							</div>
							<div class="col-sm-6">
								<p><% if(v.firstName != ""){ %>
									<a href="/tourguide/<%= v.user_id %>/info"> <%= v.firstName %> <%=v.lastName %></a>
								<%	}else{ %>
								<a href="/tourguide/<%= v.user_id %>/info"> <%= v.username %></a>
									<% } %></p>
								<p><%= v.business_desc %>...</p>
								<div class="freelancer-earnings-div">

									<span><i class="fa fa-usd" aria-hidden="true"></i><%= v.ratePerHour %> / hr</span>
									<!-- <span><i class="fa fa-usd" aria-hidden="true"></i>lorem earned</span> -->
									<span><%= v.success_rate %> % Job Success<div class="job-success-progress"><div class="progress-complete" style="width: <%= v.success_rate %>%"></div><div class="progress-pending" style="width:"  100 - <%= v.success_rate %> %"></div></div></span>
									
								</div>
								<span><i class="fa fa-map-marker"></i><%= v.address_country %></span>
							</div>
							<div class="col-sm-3">
								<input type="button" name="invite_to_job" class="button solid invite-to-job pull-right" onclick="initiate_invite(<%= v.user_id %>);" value="Invite To Guide">
							</div>
						</div>
					
		   
					<!-- <div class="list-empty">
						<h3 class="text-center">No Tour Guide found based on your search criteria.</h3>
					</div> -->

    <% }) %>

</script>
@endsection



						
				