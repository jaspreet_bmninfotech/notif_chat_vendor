@extends('layouts/base')
  @section('content')

			<!-- Banner -->
      <div class="maindasboard">
        
        <section id="banner">
          <div class="banner_cover">
            <div class="banner_content_placeholder">
              <h1 class="banner_content_h1">Find a tourguide to show you arround.</h1>
              <!-- <p>From house painting to personal training, we bring you the right pros for every project on your list.</p> -->
              <form id="search-arear" method="post">
                {!! csrf_field() !!}
                <select class="src-q" name="country">
                
                  <option value="">Select Country</option>
              <?php
                foreach ($category as $key => $value) {
                  ?>
                  <option name="country" value="{{$value['name']}}">{{$value['name']}}</option>
                <?php
                }
                ?>
                </select>
                <button class="search-btn">Find Locals</button>
              </form>
            </div>
            <!-- <ul class="actions">
              <li><a href="#" class="button special">Get Started</a></li>
              <li><a href="#" class="button">Plan an Event</a></li>
            </ul> -->
          </div>
        </section>

      <!-- Main -->
        
     
        <section class="box special features" style="background-color: #fff;">
          <section id="" class="">
            <h2 style="font-weight: bold;">How it works</h2>
            <div class="tfeatures-row">
              <section>
                  <img src="{{asset('/images/icons/search.png')}}" class="hiw-icon" alt="Find vendor">
                  <h3>Find</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </section>
               <section>
                  <img src="{{asset('/images/icons/arrange.png')}}" class="hiw-icon" alt="Find vendor">
                  <h3>Arrange</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </section>
               <section>
                  <img src="{{asset('/images/icons/find_user.png')}}" class="hiw-icon" alt="Find vendor">
                  <h3>Explore</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </section>
            </div>
          </section>
        </section>
        <section class="box special features" style="background-color: #f5f5f5; padding: 2em 0 7em 0;">
          <section id="" class="container">
            <h2 style="font-weight: bold;">Explore Your Favourite Place With Our Coolest Tour Guide</h2>
              <div class="section group group-sect category-sect">
                <div class="tourguidefindvendor" >
                    <table class="table vf-saved-job" id="vendorlist">
                      
                      <tbody class="">
                        
                      </tbody>
                    </table>
                </div>
              </div>
              <a class="see-btn" href="javascript:void(0)" id="show-mores" >Show More</a>
          </section>
        </section>
    <!-- CTA -->
      <section id="cta">

        <h4 style="font-weight: bold; font-size: 2em;">Don't have time to search around? Fine, create a trip <br> Get offers from local tour guides</h4>
        @php
          @$utype = Auth::user()->type;  
          $utypecheck = Auth::guard('web')->check();
        @endphp
        <?php
          if ($utype == "vn") {
        ?>
          <a href="{{route('vendor.search.tourguide')}}" class="button">Create A Trip</a>
        <?php
          }
         ?>
         <?php
          if ($utype == "cn") {
        ?>
          <a href="{{route('client.search.tourguide')}}" class="button">Create A Trip</a>
        <?php
          }
         ?>
         <?php
          if ($utypecheck == false) {
        ?>
          <a href="{{route('login')}}" class="button">Create A Trip</a>
        <?php
          }
         ?>
      </section>
      <!-- Footer -->
      </div>

 @endsection
 @section('addjavascript')

<script type="text/javascript" src="/js/home.js"></script>
<script type="text/javascript" src="/js/tourGuide.js"></script>
<script type="text/template" id="tourguideListTmpl">     
    <% _.each(data,function(v,i,l){ %> 
      <tr class="bottom-border tourguidesearch">
        <td width="150px";>
            <span><img src="/<%= v.path %>" width="100%"; ></span>
        </td>
        <td class="tourguidelistcenter">
            <h4><a href="/tourguide/<%= v.id %>/info"> <%= v.username %></a></h4>
            <span><%= v.description %></span>
            <h4>Job Success:<%= v.job_success_rate %></h4>
        </td>
        <td>
            <h5><%= v.address_id %></h5>
        <% if(v.isHourly == 1){ %>
        <span>$<%= v.minRate %>k - <%= v.maxRate %>K/Hr</span>
        <% }else{ %>
        <span>$<%= v.budget %>&nbsp;K</span>
        <% } %> 
      </td> 
    </tr>
    <% }) %>

</script>
 @endsection
