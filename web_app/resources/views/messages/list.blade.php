@extends('../layouts/base')
  	@section('content')
	<div class="container messages-page">
		<div class="row">
			<?php
			if (sizeof($data['all_messages']) > 0)
			{
			?>
				<div class="col-sm-3 left-pane">
					<div class="message-filter">
						<input class="iInput iSearch" placeholder="Search" name="search" type="text">
						<select class="iInput">
							<option>All Recent</option>
							<option>Read</option>
							<option>Unread</option>
							<option>Archived</option>
						</select>
					</div>

					<div class="message-brief-wrap">
					<?php
					foreach ($data['all_messages'] as $keyAM => $valueAM)
					{
					?>
						<div class="message-brief">
							<a href="javascript:void(0);" onclick="get_message_thread('<?php echo base64_encode($valueAM->message_id); ?>', this);">
								<div class="col-sm-3">
									<div class="row">
										<img src="/images/no-user.png" class="img-responsive img-circle">
									</div>
								</div>
								<div class="col-sm-9">
									<p><strong id="left_pn_clnt_nm"><?php echo $valueAM->sender_name ? $valueAM->sender_name : $valueAM->sender_username; ?></strong></p>
									<span><?php echo $valueAM->job_name != '' ? substr($valueAM->job_name, 0, 24) . '...' : ''; ?></span>
									<span><?php echo substr($valueAM->latest_message, 0, 24) . '...'; ?></span>
								</div>
							</a>
						</div>
					<?php
					}
					?>
					</div>
				</div>

				<div class="col-sm-9 message-details right-pane">
					<div class="row">
						<div class="message-detail-pane">
							<div class="message-header">
								<div class="col-sm-10">
									<div class="row">
										<h3 class="msg_hdng"></h3>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="row">
										<button type="button" class="btn btn-primary btn-sm pull-right">View Contract</button>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<p class="last_msg_dt"></p>
							</div>
							
							<div class="row">
								<div class="col-sm-12 messages-list-wrap">
									<p class="text-center">Loading messages...</p>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<textarea rows="2" name="send_message_txt" id="send_message_txt" class="form-control" placeholder="Enter you message here..."></textarea>
								<input type="hidden" name="hid_parent_msg" id="hid_parent_msg" class="form-control">
							</div>
							<div class="col-sm-12 form-group">
								<button class="btn btn-primary pull-right" type="button" onclick="send_message();">Send</button>
							</div>
						</div>
					</div>
				</div>
			<?php
			}
			else
			{
			?>
				<div class="text-center alert alert-info">
					<br>
					<h4>No messages yet.</h4>
					<br>
				</div>
			<?php
			}
			?>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.message-brief-wrap .message-brief:first-child>a').click();

			setInterval(function(){
				$(".message-brief.selected a").click();
			}, 3000);
		});

		function send_message()
		{
			if ($("#send_message_txt").val() != '' && $("#hid_parent_msg").val() != '')
			{
				$.ajax({
					url: "/message/send",
					dataType: 'json',
					data:{'_token': $("#csrf-token").val(),
						'msg_txt': $("#send_message_txt").val(),
						'parent_id': $('#hid_parent_msg').val()},
					method: 'POST',
					success: function(retJson)
					{
						$("#no_msg_thread_span").remove();
						var thread_html = '<p class="text-center">Message failed. Try Again.</p>';
						if (retJson.status)
						{
							thread_html = '<div class="message-wrap">\
										<div class="col-sm-1">\
											<div class="row">\
												<img src="/images/no-user.png" class="img-responsive img-circle">\
											</div>\
										</div>\
										<div class="col-sm-9">\
											<span><strong>' + retJson.message.sender_name + '</strong></span>\
											<p>' + retJson.message.message + '</p>\
										</div>\
										<div class="col-sm-2">\
											<span>' + retJson.message.message_time + '</span>\
										</div>\
									</div>';

							$("#send_message_txt").val('');
						}

						$('.messages-list-wrap').append(thread_html);
					}
				});
			}
		}

		function get_message_thread(id, ele)
		{
			$(".msg_hdng").html('');
			$('#hid_parent_msg').val(id);

			$('.message-brief.selected').removeClass('selected');
			$(ele).parents('.message-brief').addClass('selected');

			$(".msg_hdng").html($(ele).find('#left_pn_clnt_nm').html());

			$.ajax({
				url: "/message/" + id,
				dataType: 'json',
				method: 'GET',
				success: function(retJson)
				{
					var thread_html = '<p id="no_msg_thread_span" class="text-center">No messages found for this thread.</p>';
					if (retJson.messages)
					{
						var sndr_nm = retJson.messages.sender_uname;
						if (retJson.messages.sender_fname != null)
						{
							sndr_nm = retJson.messages.sender_fname + ' ' + retJson.messages.sender_lname;
						}

						thread_html = '<div class="message-wrap">\
								<div class="col-sm-1">\
									<div class="row">\
										<img src="/images/no-user.png" class="img-responsive img-circle">\
									</div>\
								</div>\
								<div class="col-sm-9">\
									<span><strong>' + sndr_nm + '</strong></span>\
									<p>' + retJson.messages.message + '</p>\
								</div>\
								<div class="col-sm-2">\
									<span>' + retJson.messages.message_time + '</span>\
								</div>\
							</div>';

						for (var i = 0; i < retJson.messages.thread.length; i++)
						{
							if (i == retJson.messages.thread.length-1)
							{
								$(".last_msg_dt").html(retJson.messages.thread[i]['message_date']);
							}

							var sndr_nm = retJson.messages.thread[i].sender_uname;
							if (retJson.messages.thread[i].sender_fname != null)
							{
								sndr_nm = retJson.messages.thread[i].sender_fname + ' ' + retJson.messages.thread[i].sender_lname;
							}

							thread_html += '<div class="message-wrap">\
								<div class="col-sm-1">\
									<div class="row">\
										<img src="/images/no-user.png" class="img-responsive img-circle">\
									</div>\
								</div>\
								<div class="col-sm-9">\
									<span><strong>' + sndr_nm + '</strong></span>\
									<p>' + retJson.messages.thread[i].message + '</p>\
								</div>\
								<div class="col-sm-2">\
									<span>' + retJson.messages.thread[i].message_time + '</span>\
								</div>\
							</div>';
						}
					}
					$('.messages-list-wrap').html(thread_html);
				}
			});
		}
	</script>
@endsection