@extends('layouts/base')
  @section('content')

			<!-- Banner -->
      <div class="maindasboard">
        
        <section id="banner">
          <div class="banner_cover">
            <div class="banner_content_placeholder">
              <h1 class="banner_content_h1">Find and hire like you got it.</h1>
              <h3>Find and hire the best vendor from all over the world. From renting to making your dream event come true, we got you.</h3>
              <!-- <p>From house painting to personal training, we bring you the right pros for every project on your list.</p> -->
              <form id="search-arear">
                <input type="search" name="search" class="src-q" placeholder="What do you need?" />
                <button class="search-btn">Get Started</button>
              </form>
            </div>
            <!-- <ul class="actions">
              <li><a href="#" class="button special">Get Started</a></li>
              <li><a href="#" class="button">Plan an Event</a></li>
            </ul> -->
          </div>
        </section>

      <!-- Main -->
        <section id="main" class="container">
          <section class="box special" style="position: relative; z-index: 9;">
            <h2 style="font-weight: bold; color: #333;">Take the first step</h2>
            <header class="">
              <div class="section group">
                <div class="col span_1_of_5 span_01" style="border-bottom:4px solid #55efc4;">
                <a href="">Vendors</a>
                </div>
                <div class="col span_1_of_5 span_11" style="border-bottom:4px solid #ffeaa7;">
                <a href="">Pro Vendors</a>
                </div>
                <div class="col span_1_of_5 span_21" style="border-bottom: 4px solid #fab1a0;">
                <a href="">Tour Guides</a>
                </div>
                <div class="col span_1_of_5 span_31" style="border-bottom: 4px solid #74b9ff;">
                <a href="">Model</a>
                </div>
                <div class="col span_1_of_5 span_41" style="border-bottom: 4px solid #a29bfe;">
                <a href="">Renting</a>
                </div>
              </div>
            </header>
            <span class="image featured"><img src="images/pic01.jpg" alt="" /></span>
          </section>
      </section>
      <section>
        <section id="" class="container">
            <section class="" style="text-align: center; padding: 0 0 7em 0;">
              <h2 style="font-weight: bold; color: #333;">Work with someone perfect for your team</h2>
              <div class="section group group-sect category-sect">
                <?php
                foreach ($category as $key => $value) {
                  ?>
                <div class="col span_1_of_4">
                {{$value['name']}}
                </div>
                <?php
                }
                ?>
              </div>

              <a class="see-btn" href="javascript:void(0)" onclick="getincategory()">See All Categories</a>
            </section>
          </section>
        </section>
        <section class="box special features" style="background-color: #fff;">
          <section id="" class="container">
            <h2 style="font-weight: bold;">How it works</h2>
            <div class="features-row">
              <section>
                  <img src="{{asset('/images/icons/FindTransparent.png')}}" class="hiw-icon" alt="Find vendor">
                  <h3>Find</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </section>
              <section>
                <img src="{{asset('/images/icons/Hire_Transparent.png')}}" class="hiw-icon" alt="Hire vendor">
                <h3>Hire</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </section>
            </div>
            <div class="features-row">
              <section>
                <img src="{{asset('/images/icons/Work_Transparent.png')}}" class="hiw-icon" alt="Work">
                <h3>Work</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </section>
              <section>
                <img src="{{asset('/images/icons/Transparent_Pay.png')}}" class="hiw-icon" alt="Pay vendor">
                <h3>Pay</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </section>
            </div>
          </section>
        </section>
        <section class="box special features" style="background-color: #f5f5f5; padding: 2em 0 7em 0;">
          <section id="" class="container">
            <h2 style="font-weight: bold;">Rent just what you need</h2>
              <div class="section group group-sect category-sect">
                <div class="col span_1_of_4">
                Speakers
                </div>
                <div class="col span_1_of_4">
                Drum
                </div>
                <div class="col span_1_of_4">
                Chairs
                </div>
                <div class="col span_1_of_4">
                Book
                </div>
              </div>
              <a class="see-btn" href="javascript:void(0)" onclick="getincategory()">More to rent</a>
          </section>
        </section>
        <section class="box special features" id="job-bidding" style="text-align: left;">
          <section id="" class="container">
            <h3>Browse Jobs by Categories</h3>
            <ul>
              <li>
                <a href="#">Award Ceremony</a>
              </li>
              <li>
                <a href="#">Birthday</a>
              </li>
              <li>
                <a href="#">Board Meeting</a>
              </li>
              <li>
                <a href="#">Business Dinner</a>
              </li>
              <li>
                <a href="#">Conference</a>
              </li>
              <li>
                <a href="#">Convention</a>
              </li>
              <li>
                <a href="#">Corporate Enetertainment</a>
              </li>
              <li>
                <a href="#">Executive Retreat</a>
              </li>
              <li>
                <a href="#">Family Event</a>
              </li>
              <li>
                <a href="#">Golf Event</a>
              </li>
              <li>
                <a href="#">Incentive Event</a>
              </li>
              <li>
                <a href="#">Incentive Travel</a>
              </li>
              <li>
                <a href="#">Meeting</a>
              </li>
              <li>
                <a href="#">Seminar</a>
              </li>
              <li>
                <a href="#">Team Building Event</a>
              </li>
              <li>
                <a href="#">Trade Show</a>
              </li>
              <li>
                <a href="#">Press Conference</a>
              </li>
              <li>
                <a href="#">Networking Event</a>
              </li>
              <li>
                <a href="#">Opening Ceremony</a>
              </li>
              <li>
                <a href="#">Product Launches</a>
              </li>
              <li>
                <a href="#">Theme Party</a>
              </li>
              <li>
                <a href="#">VIP Event</a>
              </li>
              <li>
                <a href="#">Trade Fair</a>
              </li>
              <li>
                <a href="#">Shareholder Meeting</a>
              </li>
              <li>
                <a href="#">Wedding</a>
              </li>
              <li>
                <a href="#">Wedding Anniversary</a>
              </li>
            </ul>
          </section>
        </section>
    <!-- CTA -->
      <section id="cta">

        <h2 style="font-weight: bold; font-size: 3em;">Build your team online</h2>
        <a href="" class="button">Get Started</a>
      </section>
      <!-- Footer -->
      </div>

 @endsection
 @section('addjavascript')

<script type="text/javascript" src="/js/home.js"></script>
 @endsection
