@extends('layouts/base')
    @section('addstyle')
        <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
        <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="{{asset('plugin/jQuery-File-Upload-9.19.1/css/jquery.fileupload.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('plugin/jQuery-File-Upload-9.19.1/css/style.css')}}">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css">
    @endsection
    @section('content')
    <div class="container client-content">
        <h2 class="job-title padding_bottom2"> Edit a Job </h2>
        <form action="{{ route('client.job.edit', ['id' => $job['id']])}}" name="editForm" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="jobId" value="{{$job['id']}}">
            <div id="job_description" class="panel-group">
             <div class="vfform">   
                <div class="heading">
                    <h3> Describe the Job </h3>
                </div>
                <div class="panel-body feildcont field">
                    <div class="row">
                      <div class="col-md-6">
                        <label> Event Type </label>
                        <select class="" name="eventtype">
                            <option value="">select event type</option>
                            <option value="pt" {{ ($job['eventType']=='pt')?'selected':''}}>part time</option>
                            <option value="ft" {{ ($job['eventType']=='ft')?'selected':''}}>full time</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>What are you looking for?</label>
                        <select class="form-select-field" name="category_id" onchange="getSubcategories(this)" id="category_id">
                            <option value="">Select</option>
                            @foreach($cat as $key=>$val)
                            <option value="{{$val->id}}" data-id="{{$val->id}}" {{ ($job['category_id']==$val->id)?'selected':''}}>{{$val->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('category_id'))
                            <span class="text-danger">
                                 {{ $errors->first('category_id') }}
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Specific category</label>
                        <select class="" name="subcategory_id" id ="subcategory">
                            <option value="">Select</option>
                        </select>
                            @if(isset($job->subcategory_id) && $job->subcategory_id != '')
                               <input type="hidden" name="subcategoryidhidden" id="sub_id" value="{{$job->subcategory_id}}">
                            @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6"> 
                        <label>How many  people will be  attending?</label>
                        <span><input type="number" placeholder="100" class="" name="peopleAttending" value="{{$job['peopleAttending']}}">
                        </span> 
                      </div>
                    </div>
                    <div class="row">
                        
                      <div class="col-md-6">
                        <label> Name your job posting </label>
                        <input name="name" placeholder="Please Type Your Job Title" type="text"  class="" value="{{$job['name']}}">
                        <div class="clearfix"></div>
                        @if($errors->has('name'))           
                            <span class="text-danger">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                      </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label> Describe the work to be done </label>
                            <textarea rows="8" max-length="5000" placeholder="Please type your job description here." class="" name="description" >{{$job['description']}}</textarea>
                            <div class="clearfix"></div>
                            @if($errors->has('description'))                          
                                <span class="text-danger">
                                    {{ $errors->first('description') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5">
                            <div class="progress-extended">&nbsp;</div>
                                    <div class="imagePresentation">
                                        <div class="">
                                                
                                         <div class="clearfix"></div>
                                            <span class="button solid fileinput-button">
                                                <i class="fa fa-plus"></i>
                                                <span>Upload Images...</span>
                                                <input type="file" name="files[]" id="postupload" multiple>
                                            </span><br/>
                                            <div id="jobprogress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                        </div>
                                                @if (session('status'))
                                                    <div class="alert alert-danger">
                                                        {{ session('status') }}
                                                    </div>
                                                @endif
                                            <p class="margin_defaullt">Maximum five files are allowed and each must not exceed 2mb</p>
                                        </div>
                                    </div>
                        </div>
                        <div class="col-md-7">
                            <div id="viewimage" class="viewimg">
                           </div>
                           <div id="profilesuploadappend" class="profileimage-inline">
                            <?php if ($profileattach == NULL) {
                                    ?>
                                    <p></p>
                                    <?php
                                } else {
                                  
                            foreach ($profileattach as $profileimg => $value) {
                                ?>
                                <div class="profileimagesf vieweditimg">
                                <i data-id="{{$value->id}}" class="fa fa-times" onclick="removejobimg({{$value->id}},{{$value->job_id}},'{{$value->name}}',this)"></i><img src="/{{$value->path}}" width="100px">
                                </div>
                                <?php
                                }
                            }
                          ?></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label> What type of project do you have? </label>
                                <div class="">
                                        <input type="radio" name="term" value="0" checked="checked" class="jobinput" {{ ($job['isOneTime']==1) ? 'checked':'0'}}>
                                        One-time project                                
                                </div>
                                <div class="">
                                        <input type="radio" name="term" value="1" class="jobinput" 
                                        {{ ($job['isOngoing']==1)?'checked':'0'}}>
                                        <span>
                                        Ongoing project
                                    </span>
                                </div>
                                <div class=" ">
                                    <span>
                                        <input type="radio" name="term" value="2" class="jobinput" {{ ($job['isOneTime']==0)&&($job['isOngoing']==0) ? 'checked':''}}>
                                        I am not sure
                                    </span>
                                </div>
                            
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-4">
                                <label> TimeZone </label>
                                    <select class="form-select-field" name="timeZone" id="timezone">
                                        <option>Time Zone</option>   
                                        @foreach($timezone as $key=>$val)
                                        <option value="{{$val->id}}" {{ ($job['timeZone']==$val->id)?'selected':''}}>(GMT{{$val->offset}})  {{$val->value}}</option>
                                        @endforeach
                                       
                                    </select>
                            </div>
                            <div class="col-md-4 ">
                                <label>End Date</label>
        
                               <div class="input-group date" id="datePicker">
                                <input type="text" class="" name="startDateTime[]" required="required" value="{{date('Y-m-d', strtotime($job['startDateTime']))}}" />
                                <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                    @if($errors->has('startDateTime'))           
                                    <span class="text-danger">
                                        {{ $errors->first('startDateTime') }}
                                    </span>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            @if($errors->has('startDateTime'))                          
                                <span class="text-danger">
                                    {{ $errors->first('startDateTime') }}
                                </span>
                            @endif
                            </div>
                        
                            <div class="col-md-4">
                                <label>End Time</label>
                                 <div class="input-group bootstrap-timepicker-component" id="timePicker">
                                    <input type="text" name="startDateTime[]" class="timepicker-default input-small" required="required" value="{{date('H:i:s', strtotime($job['startDateTime']))}}">
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    @if($errors->has('startDateTime'))           
                                    <span class="text-danger">
                                        {{ $errors->first('startDateTime') }}
                                    </span>
                                @endif
                                </div>
  
                            </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
              </div><!-- vfform -->  
            </div>
            <div id="job_rate" class="vfform panel-group">
                <div class="heading">
                    <h3> Rate and Availability </h3>
                </div>
                <div class="panel-body feildcont field">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label> How would you like to pay? </label>
                            <div>
                                <span>
                                    <input type="radio" name="contracttype" value="hourly" class="jobinput" {{ ($job['isHourly']===1) ? 'checked':''}}>
                                    Hourly
                                </span>
                            </div>
                            <div class="">
                                <span>
                                    <input type="radio" name="contracttype" value="fixed" class="jobinput" {{ ($job['budget']!=0.00) ? 'checked':''}}>
                                    Fixed
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row fixed">
                        <div class="col-sm-6 col-md-3">
                            <label> Budget </label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text"  name="budget" value="{{$job['budget']}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row hourly hidden">
                        <div class="col-md-6">
                            <div class="col-sm-6 col-md-6">
                                <label> Min Rate </label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text"  name="minrate" value="{{$job['minRate']}}">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <label>Max Rate</label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text"  name="maxrate" value="{{$job['maxRate']}}">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div id="job_rate" class="panel-group vfform">
                <div class="heading">
                    <h3> Vendor Prefference </h3>
                </div>
                <div class="panel-body feildcont field">
                    <div class="row">
                        <div class="col-md-6">
                            <label> Do you want vendors to find and apply to your job? </label>
                            <div>
                                @foreach($vendorPreffer as $k=>$v)  
                                <span>  
                                <input type="checkbox" name="vendorPreffer[]"  class="jobinput" value="1" {{($v==1) ?'checked':''}} >
                                Only vendors i have invited
                                </span>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Do you have a vendor in mind?</label>
                            <input type="text" placeholder="Start typing the vendor's name" name="typeahead" id="typeahead" onSelect= "function(item)" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Job Success</label>
                            <select class="form-select-field" name="jobSuccess">
                                <option value="" selected disabled>-Select-</option>
                                <option {{($job['jobSuccess']=='Any job success')?'selected':''}}>Any job success</option>
                                <option {{($job['jobSuccess']=='90% job success and up')?'selected':''}}>90% job success and up</option>
                                <option {{($job['jobSuccess']=='80% job success and up')?'selected':''}}>80% job success and up</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-6">Location</label>
                            <select class="form-select-field" name="addressInfo[]" id="country" onchange="getstate(this)">
                                <option>Select Country</option>          
                                @foreach($country as $key=>$val)
                                    <option value="{{$val->id}}"  data-id="{{$val->id}}" {{(@$addressedit[1]==$val->id)?'selected':''}}>{{$val->name}}</option>
                                @endforeach       
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label></label>
                            <select class="form-select-field" name="addressInfo[]" id="state" onchange="getcity(this)">
                                <option value="">State</option>
                            </select>
                            <input type="hidden" name="stateedit" id ="stateedit" value="{{@$addressedit[2]}}">
                        </div>
                    
                        <div class="col-md-6">
                            <label></label>
                            <select class="form-select-field " name="addressInfo[]" id="city">
                                <option value="">City</option>
                            </select>
                             <input type="hidden" name="cityedit" id ="cityedit" value="{{@$addressedit[3]}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Language</label>
                                <select class="form-select-field" name="language">
                                    
                                <option>Select language</option>
                                     @foreach($language as $k=>$v)
                                        <option value="{{$v->name}}" {{ ($job['language']== $v->name)?'selected':''}}>{{$v->name}}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="col-md-6">
                            <label>Can travel</label>
                                <select class="form-select-field" name="canTravel">
                                    <option value="yes" @if(($job['canTravel']=='1')?'selected':'')@endif>yes can travel</option>
                                    <option value="No" {{($job['canTravel']=='0')?'selected':''}}>no</option>      
                                </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="button solid" value="Update"> 
                <a href="javascript:void(0);" onclick="close_job( {{$job['id']}} );" class="button default2">
                    <i class="fa fa-times fa-lg"></i>&nbsp; Close</a>
            </div>
        </form>
    </div>
    @endsection
    @section('addjavascript')
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
    <script src="{{asset('js/client-job.js')}}"></script>
    <script src="{{asset('js/client.js')}}"></script>
    <script src="{{asset('js/address.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script src ="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js"></script>
    @endsection