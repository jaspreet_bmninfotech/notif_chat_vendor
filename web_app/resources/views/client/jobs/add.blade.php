@extends('layouts/base')
    @section('addstyle')
        <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
        <link rel="stylesheet" type="text/css" href="{{asset('plugin/jQuery-File-Upload-9.19.1/css/jquery.fileupload.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugin/jQuery-File-Upload-9.19.1/css/style.css')}}">
    
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css">
    @endsection
    @section('content')
    <div class="container client-content">
       
    @if(Session::has('message'))
                <div class="alert alert-danger">
                <p style="">{{Session::get('message')}}</p>
                @php Session::forget('message'); @endphp
                </div>
    @endif 
        <form action="{{ route('client.job.add')}}" name="postForm" method="POST"
        enctype="multipart/form-data">
            {{ csrf_field() }}
            <div id="job_description" class="panel-group">
             <div class="vfform">   
                <div class="heading">
                    <h3> Describe the Job </h3>
                </div>
                <div class="panel-body feildcont field">
                    <div class="row">
                      <div class="col-md-6">
                        <label> Event Type </label>
                        <select class="" name="eventtype">
                            <option value="">select event type</option>
                            <option value="pt">Part Time</option>
                            <option value="ft">Full Time</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>What are you looking for?<em> * </em></label>
                        <select class="form-select-field" name="category_id" onchange="getSubcategories(this)">
                            <option value="">Select</option>
                        @foreach($cat as $key=>$val)
                            <option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
                        @endforeach
                        </select>
                        @if($errors->has('category_id'))
                            <span class="text-danger">
                                 {{ $errors->first('category_id') }}
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Specific category</label>
                        <select class="" name="subcategory_id" id ="subcategory" onchange="getSubSubcategory(this)">
                            <option value="">Select</option>
                       
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Specific SubCategory</label>
                        <select class="" name="sub_subcategoryId" id ="sub_subcategory">
                            <option value="">Select</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6"> 
                        <label>How many  people will be  attending?</label>
                        <span><input type="number" placeholder="100" class="" name="peopleAttending"></span> 
                      </div>
                    </div>
                    <div class="row">
                        
                      <div class="col-md-6">
                        <label> Name your job posting<em> * </em> </label>
                        <input name="name" placeholder="Please Type Your Job Title" type="text"  class="">
                        <div class="clearfix"></div>
                        @if($errors->has('name'))           
                            <span class="text-danger">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label> Describe the work to be done<em> * </em> </label>
                            <textarea rows="6" maxlength="500" placeholder="Please type your job description here." class="" name="description"></textarea>
                            <div class="clearfix"></div>
                            @if($errors->has('description'))                          
                                <span class="text-danger">
                                    {{ $errors->first('description') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                           {{-- <div class="progress-extended">&nbsp;</div>--}}
                                    <div class="imagePresentation">
                                        <div class="">
                                        <div id="jobviewimg" class="viewimg"> 
                                        </div><br/>
                                         <div class="clearfix"></div>
                                         <div class="errorappnd">
                                            
                                         </div>
                                            <span class="button solid fileinput-button">
                                                <i class="fa fa-plus"></i>
                                                <span>Upload Images...</span>
                                                <input type="file" name="files[]" id="jobPostfile" multiple>
                                            </span><br/>
                                                @if (session('status'))
                                                    <div class="alert alert-danger">
                                                        {{ session('status') }}
                                                    </div>
                                                @endif
                                            <p class="margin_bottom1 maxfilesize">Maximum five files are allowed and each must not exceed 2mb</p> 
                                            
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label> What type of project do you have? </label>
                                <div class="">
                                        <input type="radio" name="term" value="0" checked="checked" class="jobinput">
                                        One-time project                                
                                </div>
                                <div class="">
                                    <span>
                                        <input type="radio" name="term" value="1" class="jobinput">Ongoing project
                                    </span>
                                </div>
                                <div class="">
                                    <span>
                                        <input type="radio" name="term" value="2" class="jobinput">I am not sure
                                    </span>
                                </div>
                        </div>
                    </div>
                    <div class="row">

                            <div class="col-md-4">
                                <label> TimeZone </label>
                                    <select class="form-select-field" name="timeZone" id="timezone">
                                        <option>Time Zone</option>
                                        @foreach($timezone as $key=>$val)
                                        <option value="{{$val->id}}">(GMT{{$val->offset}})  {{$val->value}}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="col-md-4 ">
                                <label>End Date<em> * </em></label>
                               <div class="input-group date" id="datePicker">
                                <input type="text" class="" name="startDateTime[]" required="required" />
                                <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                    @if($errors->has('startDateTime'))           
                                    <span class="text-danger">
                                        {{ $errors->first('startDateTime') }}
                                    </span>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            @if($errors->has('startDateTime'))                          
                                <span class="text-danger">
                                    {{ $errors->first('startDateTime') }}
                                </span>
                            @endif
                            </div>
                            <div class="col-md-4">
                                <label>End Time</label>
                                 <div class="input-group bootstrap-timepicker-component timePicker" id="timePicker">
                                    <input type="text" name="startDateTime[]" class="timepicker-default input-small" required="required">
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    @if($errors->has('startDateTime'))           
                                    <span class="text-danger">
                                        {{ $errors->first('startDateTime') }}
                                    </span>
                                @endif
                                </div>
                            </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
              </div><!-- vfform -->  
            </div>
            <div id="job_rate" class=" panel-group">
                <div class="vfform">
                    <div class="heading">
                        <h3> Rate and Availability </h3>
                    </div>
                    <div class="panel-body feildcont field">
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label> How would you like to pay? </label>
                                <div>
                                    <span>
                                        <input type="radio" name="contracttype" value="hourly" class="jobinput">
                                        Hourly
                                    </span>
                                </div>
                                <div class="">
                                    <span>
                                        <input type="radio" name="contracttype" value="fixed"  class="jobinput">
                                        Fixed
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row fixed">
                            <div class="col-sm-6 col-md-3">
                                <label> Budget </label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="number"  name="budget">
                                </div>
                                 @if($errors->has('budget'))
                                    <span class="text-danger">
                                         {{ $errors->first('budget') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row hourly hidden">
                            
                                <div class="col-sm-6 col-md-3">
                                    <label> Min Rate </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="number"  name="minrate">
                                    </div>
                                    @if($errors->has('minrate'))
                                        <span class="text-danger">
                                             {{ $errors->first('minrate') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <label>Max Rate</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="number"  name="maxrate">
                                    </div>
                                    @if($errors->has('maxrate'))
                                        <span class="text-danger">
                                             {{ $errors->first('maxrate') }}
                                        </span>
                                    @endif
                                </div>
                            
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="job_rate" class="panel-group vfform">
                <div class="heading">
                    <h3> Vendor Prefference </h3>
                </div>
                <div class="panel-body feildcont field">
                    <div class="row">
                        <div class="col-md-6">
                            <label> Do you want vendors to find and apply to your job? </label>
                            <div>
                                <span>    
                                <input type="checkbox" name="vendorPreffer[]"  class="jobinput" value="1">
                                Only vendors i have invited
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Do you have a vendor in mind?</label>
                            <input type="text" placeholder="Start typing the vendor's name" name="typeahead" id="typeahead" onSelect= "function(item)" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Job Success</label>
                            <select class="form-select-field" name="jobSuccess">
                                <option value="" selected disabled>-Select-</option>
                                <option>Any job success</option>
                                <option>90% job success and up</option>
                                <option>80% job success and up</option>
                            </select>
                        </div>
                    
                        
                        <div class="col-md-6">
                            <label class="col-md-6">Location</label>
                            <select class="form-select-field" name="addressInfo[]" id="country" onchange="getstate(this)">
                                <option>Select Country</option>
                                            @foreach($country as $key=>$val)
                                <option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
                                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label></label>
                            <select class="form-select-field" name="addressInfo[]" id="state" onchange="getcity(this)">
                                <option value="">State</option>
                            </select>
                        </div>
                    
                        <div class="col-md-6">
                            <label></label>
                            <select class="form-select-field " name="addressInfo[]" id="city">
                                <option value="">City</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Language</label>
                                <select class="form-select-field" name="language">
                                    <option>Select language</option>
                                     @foreach($language as $k=>$v)
                                        <option value="{{$v->name}}">{{$v->name}}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="col-md-6">
                            <label>Can travel</label>
                                <select class="form-select-field" name="canTravel">
                                    <option value="yes">yes can travel</option>
                                    <option value="No">no</option>                                    
                                </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="button solid" value="Post a Job">
                <input type="reset" class="button default" value="Reset">
            </div>
        </form>
    </div>
    @endsection
    @section('addjavascript')
<script type="text/javascript">
    $(document).change(function(){
      var count = $("#jobviewimg > img").length
      if (count  == 5) {
      $('#jobviewimg').empty();
      }

      
    });
    document.getElementById('jobPostfile').onchange = function(e){
        if (e.target.files.length < 6) {
     console.log($("#jobviewimg > img").length);
     for (var i = 0; i < e.target.files.length;i++) {
        if (e.target.files[i].size <= 2000000) {
      
    // $.each(e.target.files, function (index, file) {
    var image = e.target.files[i];
    loadImage(image, function(img){
     document.querySelector("#jobviewimg").appendChild(img);
     },{
      maxWidth:100,
      maxHeight:100
     });
     }else{
            $('.errorappnd').html('');
            $('.errorappnd').append('<p class="text-danger margin-zero" style="">File size should be 2mb</p>');
            }   
     }
    }else{
      $('.errorappnd').html('');
      $('.errorappnd').append('<p class="text-danger margin-zero">Maximum five files are allowed</p>');
    }
    };
    </script>
    <script src="{{asset('js/client.js')}}"></script>
    <script src="{{asset('js/address.js')}}"></script>
   <!--  <script src="{{asset('plugin/Easy-Timezone-Picker-with-jQuery-Moment-js-Timezones/dist/timezones.full.js')}}"></script> -->
    <script>

        // $('#timezone').timezones();
        // $('#timezone').onchange(function(){
        //    var time =$(this).children(':selected').html();
        //    alert(time); 
        // });
    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src ="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js"></script>
    @endsection