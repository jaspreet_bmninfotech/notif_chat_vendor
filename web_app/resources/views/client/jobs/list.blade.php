@extends('../layouts/base')
@section('addstyle')
<link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
@endsection
@section('content')
<div class="container client-content job-listing">
    <div class="panel-group vfform">
        <div class="heading ">
            <h2 class="pull-left">&nbsp;Postings </h2>
            <a href="{{route('client.job.add')}}" class="pull-right button solid  button-margin1 margin_right3">Post a New Job</a>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            @if(count($jobs)==0)
            <div class="text-center top-bot-margin1">You do not not have open Job</div>
            @else
            <div class="stcode_title1 job">
                <table >
                    <thead>
                        <tr>
                            <th width="30%" class="padding_bottom2">Job Title</th>
                            <th width="12%" class="padding_bottom2">Bids</th>
                            <th width="11%" class="padding_bottom2">Hires</th>
                            <th width="9%" class="padding_bottom2">Offers</th>
                            <th width="9%" class="padding_bottom2">Status</th>
                            <th width="14%" class="padding_bottom2">Action</th>
                        </tr>
                    </thead>
                    <tbody class="job">
                        @foreach($jobs as $job)
                        <tr class="trjoblist bottom-line">
                            <td>
                                <h2>
                                   <span class="text">
                                   <a href="/client/job/details/{{$job['id']}}">{{$job['name']}}</a>
                                   </span>
                                </h2>
                                <span>
                                    <div class="details">
                                      @if ($job['isHourly'] == '0')
                                      <h5 class="budget">
                                        <label> Budget: </label>
                                        <span>$ {{ $job['budget'] }}</span>
                                      </h5>
                                      @else
                                        <h5 class="budget">
                                        <label> Rate: </label>
                                        <span>$ {{ $job['minRate'] }} ~ {{ $job['maxRate'] }}</span> 
                                        </h5>
                                      @endif
                                    </div>
                                </span>
                                <span>Posted at : {{ date('m/d/Y', strtotime($job['created_at'])) }}</span>
                            </td>
                            <td>{{$job['countBid'] }}</td>
                            <td>{{$job['countHires'] }}</td>
                            <td>{{$job['countInvites'] }}</td>
                            <td>
                            <p>
                               @if ($job['isOngoing'] == '1')
                               Ongoing
                               @elseif ($job['isOneTime'] == '1')
                               One-time
                               @endif
                            </p>
                            </td>
                            <td>
                                <span>
                                   <div class="btn-groups">
                                      <a href="{{ route('client.job.edit', ['id' => $job['id']]) }}" class="button solid">
                                      <i class="fa fa-edit fa-lg"></i>&nbsp; Edit
                                      </a>&nbsp;<span>
                                      <a href="javascript:void(0);" onclick="close_job( {{$job['id']}} );" class="button default2">
                                      <i class="fa fa-times fa-lg"></i>&nbsp; Close
                                      </a></span>
                                   </div>
                                   <br/>
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="paginate push-right">  
                   {{ $pjob->render() }}
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
@section('addjavascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
<script src="{{asset('js/client-job.js')}}"></script>
@endsection