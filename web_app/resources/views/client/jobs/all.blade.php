@extends('layouts/base')
    @section('addstyle')
        <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
        <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
    @endsection
    @section('content')
    <div class="container client-content job-listing">
        <h2 class="job-title padding_bottom2"> My Jobs </h2>
        <div class="panel-group">
          <div class="vfform">
            
                <div class=" heading">
                    <h3>&nbsp;Open Jobs </h3>
                </div>
            
            <div class="panel-body">
            @if(count($openedJobs)==0)
               <div class="text-center top-bot-margin1">You currently do not have any open job</div>
            @endif
            @foreach($openedJobs as $job)
                <div class="stcode_title1 job">
                    <h3>
                        <span class="text">
                            <strong><a href="#">{{$job->name}}</a></strong>
                        </span>
                   </h3>
                    <div class="details"> 
                        <p class="description">{{ $job->description }}</p>
                        <h5 class="budget">
                            <label> Budget: </label>
                            <span>$ {{ $job->budget }}</span>
                        </h5>
                    </div>
                    <div class="btn-groups">
                        <a href="{{ route('client.job.edit', ['id' => $job->id]) }}" class="button default2">
                            <i class="fa fa-edit fa-lg"></i>&nbsp; Edit
                        </a>&nbsp;
                        <a href="javascript:void(0);" onclick="close_job( <?php echo $job->id ;?>);" class="button defaultclose">
                            <i class="fa fa-times fa-lg"></i>&nbsp; Close
                        </a>
                    </div>
                    <hr></hr>
                </div>
            @endforeach
            <div class="paginate push-right">  
                    {{ $openedJobs->render() }}
            </div>
            </div>
          </div>
        </div>
        <div class="panel-group">
            <div class="vfform">
            <div class="heading">
                <h3>&nbsp;Closed Jobs </h3>
            </div>
            <div class="panel-body ">
            @if(count($closedJobs)==0)
                  <div class="text-center top-bot-margin1">You currently do not have any closed job</div>
            @endif
            @foreach($closedJobs as $job)
                <div class="stcode_title1 job">
                    <h3>
                        
                        <span class="text">
                            <strong>{{$job->name}}</strong>
                        </span>
                    </h3>
                    <div class="details"> 
                        <p class="description">{{ $job->description }}</p>
                        <h5 class="budget">
                            <label> Budget: </label>
                            <span>$ {{ $job->budget }}</span>
                        </h5>
                    </div>
                    <div class="btn-groups">
                        <a href="{{ route('client.job.reopen', ['id' => $job->id]) }}" class="button default2">
                            <i class="fa fa-edit fa-lg"></i>&nbsp; Reopen
                        </a>
                    </div>
                    <hr></hr>
                </div>
            @endforeach
                <div class="paginate push-right">  
                    {{ $closedJobs->render() }}
            </div>
            </div>
        </div>
        </div>
    </div>
    @endsection
    @section('addjavascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
    <script src="{{asset('js/client-job.js')}}"></script>
    @endsection