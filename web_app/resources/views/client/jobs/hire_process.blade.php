@extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
    @endsection
    @section('content')
@if(isset($data['user_details']))
    <?php
        $user_name = $data['user_details']->userName;
        if($data['user_details']->firstName != "")
        {
            $user_name = $data['user_details']->firstName . " " . $data['user_details']->lastName;
        }
    ?>
    @if($data['hire']['id'])
        @if (Session::has('noCustomer'))
            <div class="modal fade" id="myModal{{$data['hire']['id']}}" role="dialog">
            
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Payment of ${{$data['hire']['rate']}}</h4>
                    </div>
                    <div class="modal-body PaymentForm">
                        <!-- <form action="/action_page.php"> -->
                            <div class="form-group">
                                <label for="card_number">Card Number</label>
                                <input type="text" class="form-control" value="4242424242424242" name="card_number" id="card_number">
                                <input type="hidden" class="form-control" value="{{ (Session::has('hireId')) ? Session::get('hireId') :0 }}" name="hireId">
                                <input type="hidden" class="form-control" value="{{$data['hire']['rate']}}" name="hireRate">

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cvv">CVV</label>
                                        <input type="text" placeholder="CVV" value="314" name="cvv" class="form-control" id="cvv">
                                    </div>
                                </div>
                                <div class="col-md-4">  
                                    <div class="form-group">
                                        <label for="exipryMonth">Expire Month</label>
                                        <input type="text" placeholder="MM" value="3" name="expireMonth" class="form-control" id="exipryMonth">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="expireYear">Expire Year</label>
                                        <input type="text" placeholder="YYYY" value="2019" name="expireYear" class="form-control" id="expireYear">
                                    </div>
                                </div>
                            </div>
                            <div class="card-error"></div>
                            <button class="btn btn-success approve_fund_button" transaction-id="{{ (Session::has('hireId')) ? Session::get('hireId') :0 }}">Accept</button><img src="{{asset('images/sppin.gif')}}" class="form-loader" style="left: 80px;bottom: 0;">
                            <div class="circle-loader" style="display: none;">
                                <div class="checkmark draw"></div>
                            </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
        @if (Session::has('noCustomer'))
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.modal').modal('show');
                });
            </script>
        @endif    
        @endif
    @endif    
    <div class="container client-content hire-content">
        <div class="col-xs-12">
            @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{hire_flash_success_msg}}.
                </div>
            @endif

            @if (Session::has('hire_flash_err_msg'))
                <div class="alert alert-warning text-center">
                    <strong>Great!</strong> {{Session::get('hire_flash_err_msg')}}.
                </div>
            @endif
            <div class="col-xs-12">
                <div class="errorpay text-align"></div>
            </div>
            <div class="job-detail-wrapper hirecont">
                <div id="view_job_post_div" class="tab-pane fade in active">
                        <form action="{{url('client/hire-confirm')}}/{{$data['job_details']->id}}/{{$data['user_details']->id}}" method="POST">
                    <div class="col-xs-9">
                            <div class="form-group">
                                <b>Contract Title</b>
                                <input type="hidden" name="jobID" value="{{$data['job_details']->id}}">
                                <p class="margin_bottom2"><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->name}}</a></p>
                            </div>
                             <h3>Details</h3>
                            <div class="form-group">
                                <p class="margin_defaullt"><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->description}}&nbsp;(#{{$data['job_details']->id}})</a></p>
                            </div>

                            <?php
                            if ($data['job_details']->isHourly != '0')
                            {
                            ?>
                                <div class="form-group">
                                    <p><b>Hourly Rate :</b>
                                    <span>${{$data['job_details']->minRate}}/hr</span></p>
                                </div>
                            <?php
                            }
                            else
                            {
                            ?>
                                <div class="form-group">
                                    <p><b>Rate :</b>
                                    <span>${{$data['job_details']->budget}}</span></p>
                                </div>
                            <?php
                            }
                            ?>

                        </div>
                            <div class="col-xs-3 hire_user_div">
                                <div class="row">
                                    <div class="col-xs-12" id="client_img" style="height: 200px;">
                                        <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png' ?>" class="img-responsive img-circle" style="border: 1px solid #5fcf80;width: 100%; height: 100%;">
                                    </div>
                                    <br>
                                    <div class="text-align" >
                                        <br>    
                                        <h2 class="au_name">{{$user_name}}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label><b>Work Description</b></label>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="bid_id" value="{{$data['bid_id']}}">
                                    <textarea name="work_desc" class="form-control" rows="10">{{$data['job_details']->description}}</textarea>
                                </div>
                                
                                <div class="form-group">
                                    <input type="checkbox" name="hire_tnc" id="hire_tnc">&nbsp;<span>Yes, I understand and agree to the <a href="javascript:void(0);">Vendor Forest Terms of Service</a>, including the <a href="javascript:void(0);">User Agreement</a> and <a href="javascript:void(0);">Privacy Policy</a>.</span>
                                </div>

                                <div class="form-group text-align">
                                    <button type="submit" class="button solid">Hire {{$user_name}}</button>
                                    <a href="{{ route('client.job') }}" class="button default2 btn_padd">Cancel</a>
                                    @if (Session::has('successTransaction'))
                                        <div class="alert alert-warning text-center">
                                            <strong>Great!</strong> {{Session::get('successTransaction')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@elseif(isset($data['team_details']))
    <?php
    $team_name = $data['team_details']->name;
    $tId = $data['team_details']->id;
    ?>
    <!-- here out errors -->

    <div class="container client-content hire-content">
        <div class="col-xs-12">
            @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{hire_flash_success_msg}}.
                </div>
            @endif

            @if (Session::has('hire_flash_err_msg'))
                <div class="alert alert-warning text-center">
                    <strong>Great!</strong> {{hire_flash_err_msg}}.
                </div>
            @endif
            <div class="col-xs-12">
                <div class="errorpay text-align"></div>
            </div>
            <div class="job-detail-wrapper  hirecont">

                <div id="view_job_post_div" class="tab-pane fade in active">
                    <form action="{{url('client/team/hire-confirm')}}/{{$data['job_details']->id}}/{{$data['team_details']->id}}" method="POST">
                 <div class="col-xs-9">
                    <div class="form-group">
                        <h2>Contract Title:-</h2>
                        <h4><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->name}}</a></h4>
                    </div>
                    <h3>Details</h3>
                            <div class="form-group">
                              
                                <input type="hidden" name="teamjobID" value="{{$data['job_details']->id}}">
                                <p><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->description}}&nbsp;(#{{$data['job_details']->id}})</a></p>
                            </div>

                            <?php
                            if ($data['job_details']->isHourly!= '0')
                            {
                            ?>
                                <div class="form-group">
                                    <label>Hourly Rate</label>
                                    <p>{{$data['job_details']->minRate}}/hr</p>
                                </div>
                            <?php
                            }
                            else
                            {
                            ?>
                                <div class="form-group">
                                    <label>Rate: <strong>{{$data['job_details']->budget}}</strong></label>
                                </div>
                            <?php
                            }
                            ?>

                            </div>
                            <div class="col-xs-3 hire_user_div">
                                <div class="">
                                    <div class="col-xs-12" style="height: 200px;">
                                        <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png' ?>" class="img-responsive img-circle" style="border: 1px solid #5fcf80;width: 100%; height: 100%;">
                                    </div>
                                    <br>
                                    <div class="text-align" >
                                    <br>    
                                        <b><h2 class="au_name">{{$team_name}}</h2></b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <h4>Work Description</h4>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="bid_id" value="{{$data['bid_id']}}">
                                   
                                    <textarea name="work_desc" class="form-control" rows="10">{{$data['job_details']->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="hire_tnc" id="hire_tnc">&nbsp;<span>Yes, I understand and agree to the <a href="javascript:void(0);">Vendor Forest Terms of Service</a>, including the <a href="javascript:void(0);">User Agreement</a> and <a href="javascript:void(0);">Privacy Policy</a>.</span>
                                </div>
                                <div class="form-group text-align" >
                                    <button type="submit" class="button solid" data-id="{{$tId}}">Hire {{$team_name}}</button>
                                    <a href="{{ route('client.job') }}" class="btn-default button solid cancel_btn hirecan">Cancel</a>
                                     
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endif
@endsection

    @section('addjavascript')
        <script src="{{asset('js/client.js')}}"></script>
        <script type="text/javascript">
            $(document).on('click' , '.approve_fund_button' , function(e){
                e.preventDefault();
                    $('.form-loader').fadeIn(200);
                    $('.approve_fund_button').prop('disabled',true);
                    var hireRate = $('input[name=hireRate]').val();
                    var jobId = $('input[name=jobID]').val();
                   
                    var hireId = $('input[name = hireId]').val();
                    var card_number = $('input[name = card_number]').val();
                    var cvv = $('input[name = cvv]').val();
                    var expireMonth = $('input[name = expireMonth]').val();
                    var expireYear = $('input[name = expireYear]').val();
                    var token = $('input[name=_token]').val();
                $.ajax({
                    url : '{{route("fund.funded")}}',
                    type : "POST",
                    data : {_token : token , card_number : card_number , cvv : cvv , expireMonth : expireMonth , expireYear: expireYear,hireRate : hireRate ,hireId :hireId},
                    success : function(data){
                        $('.circle-loader').hide();
                        if(data === "true"){
                            $('.form-loader').fadeOut(200);
                            $('.modal-header .close').click();
                            $('.modal').modal('hide');
                            $('.errorpay').append('<p class="alert-success">Great! Transaction successfull</p>');
                            window.location.href = "/client/job/details/"+jobId;
                        }else {
                            
                            alert("something went wrong");
                            $('.errorpay').append('<p class="alert-warning">Error Occur While Transaction</p>');
                        }
                    },
                    errorsor : function(xhr, status, error){
                        $('.card-error').html("<span style='color:red'>"+xhr.responseJSON.message+"</span>");
                        $('.circle-loader').hide();
                    }
                });
            });

        </script>
    @endsection