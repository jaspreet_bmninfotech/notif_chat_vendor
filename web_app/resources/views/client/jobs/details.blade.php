@extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
    @endsection
    @section('content')
    <div class="container client-content">
        @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{Session::get('hire_flash_success_msg')}}.
                </div>
            @endif
        @if (Session::has('hire_flash_err_msg'))
            <div class="alert alert-warning text-center">
                <strong>Great!</strong> {{Session::get('hire_flash_err_msg')}}.
            </div>
        @endif
        <div class="job-detail-wrapper padding-right">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#view_job_post_div">View Job Post</a></li>
                <li><a data-toggle="tab" href="#review_proposals_div">Review Proposals</a></li>
                <li><a data-toggle="tab" href="#team_proposals_div">Team Proposals</a></li>
                <li><a data-toggle="tab" href="#hire_div">Hire</a></li>
            </ul>

            <div class="tab-content col-xs-9">
                <div id="view_job_post_div" class="tab-pane fade in active">
                    <div class="col-xs-12">
                        
                        <div class="margin_bottom1">
                           
                            <h2 class="job_head">{{$data['job_details']->name}}</h2>
                                <small><b>Posted at : </b> 
                                @if( \Carbon\Carbon::parse($data['job_details']->created_at)->diffInMonths(\Carbon\Carbon::now()) <= 10 )
                                    {{ \Carbon\Carbon::parse($data['job_details']->created_at)->diffForHumans() }}
                                @else
                                    {{ \Carbon\Carbon::parse($data['job_details']->created_at)->format('m/d/Y') }}
                                @endif
                            </small><br/>
                        </div>
                        <div>
                            <b>Category</b>:-
                            <span>{{$data['job_details']->category_name}}</span>
                        </div>
                         <p class="margin_bottom1">
                            <b>Sub Category:-</b>
                            <span>{{ @$data['subcat']->subcat }}</span>
                        </p>
                        <div class="row">
                            <div class="col-xs-6 jbdtl_div">
                                <p class="margin_bottom1">{{$data['job_details']->description}}</p>
                                <div><b>Event Type :&nbsp;</b><span><?php echo $data['job_details']->eventType == 'pt' ? 'Part Time' : 'Full Time'; ?></span></div>
                              
                                <?php
                                if (!$data['job_details']->isOneTime)
                                {
                                    $project_type = 'One-Time';
                                }
                                elseif ($data['job_details']->isOneTime)
                                {
                                    $project_type = 'On-Going';
                                }
                                elseif ($data['job_details']->isOneTime == 2)
                                {
                                    $project_type = 'Not Sure';
                                }
                                ?>

                                <div><b>People Attending :&nbsp;</b><span><?php echo $data['job_details']->peopleAttending ? $data['job_details']->peopleAttending : '0' ?></span></div>
                                <div><b>Project Type :&nbsp;</b><span>{{$project_type}}</span></div>
                            </div>
                            <div class="col-xs-6 job_length_div">
                                <div class="col-md-6 job-requirement">
                                    <div>
                                            <?php
                                    if ($data['job_details']->isHourly==0) {
                                        ?>
                                            <h2 class="job_budget">${{$data['job_details']->budget}}</h2>
                                            <p class=""><b>Fixed Price</b></p>
                                            <?php
                                    }else{
                                        ?>
                                            <h2 class="job_budget">${{$data['job_details']->minRate}} ~ ${{$data['job_details']->maxRate}}</h3>
                                            <p class="">Hourly</p>
                                        <?php
                                        }   
                                            ?>
                                    </div>
                                    <div>                                   
                                        <p class="">{{ date("m/d/Y", strtotime($data['job_details']->startDateTime)) }}</p>
                                        <h3><b>Start Date</b></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                      
                    </div>
                </div>

                <div id="review_proposals_div" class="tab-pane fade">
                    <div class="panel-body feildcont field freelancer-listing">
                        <?php
                        if (sizeof($data['job_proposals']) > 0)
                        {
                            foreach ($data['job_proposals'] as $keyAF => $valueAF)
                            {
                            ?>
                                <div class="col-sm-12 freelancer-details forp row">
                                    <div class="row">
                                        <div class="col-sm-1 "> 

                                            <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png'  ?>" class="img-responsive img-circle clt_img">

                                        </div>
                                        <div class="col-sm-9">
                                            <p>
                                                {{$valueAF->name}}
                                            </p>
                                            <p><?php echo substr($valueAF->business_desc, 0, 115); ?>...</p>
                                            <div class="freelancer-earnings-div">
                                                <span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerHour . " / hr"; ?></span>
                                                <span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerProject . " Fixed"; ?></span>
                                                <span>
                                                    @if(isset($totalstars))
                                                    {{ $totalstars[$valueAF->id]['success_percent'] }}% Job Success
                                                    <div class="job-success-progress">
                                                        <div class="progress-complete" style="float:left;width: <?php echo $totalstars[$valueAF->id]['success_percent'] ?>%" data-toggle="tooltip" data-placement="top" title="{{ $totalstars[$valueAF->id]['success_percent'].'% success job' }}">
                                                        </div>
                                                        <div class="progress-pending" style="float:left;width: <?php echo 100-$totalstars[$valueAF->id]['success_percent'] ?>%">
                                                        </div>
                                                    </div>
                                                     @else
                                                    <?php echo 0 . "% Job Success";?>
                                                    <div class="job-success-progress">
                                                        <div class="progress-complete" style="float:left;width:<?php echo '0';?>%" data-toggle="tooltip" data-placement="top" title="">
                                                        </div>
                                                        <div class="progress-pending" style="float:left;width: <?php  echo '100';?>%">
                                                        </div>
                                                    </div>
                                                    @endif
                                                </span>
                                                <?php
                                                if ($valueAF->address_country != '')
                                                {
                                                ?>
                                                    <span> <i class="fa fa-map-marker"></i><?php echo $valueAF->address_country; ?></span>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php 
                                            if($data['countvendors'] < 5)
                                            {
                                        ?>
                                        <div class="col-sm-2">
                                            <a href="{{url('client/hire')}}/<?php echo $valueAF->job_id . '/' . $valueAF->user_id . '/' . $valueAF->bid_id; ?>" name="hire_btn" class="button solid invite-to-job pull-right">Hire</a>
                                        </div>
                                        <?php }else{
                                            ?>
                                            <div class="col-sm-2"></div>
                                            <?php } ?>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        else
                        {
                        ?>
                            <div class="list-empty">
                                <h3 class="text-center">No freelancer found based on your search criteria.</h3>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div id="team_proposals_div" class="tab-pane fade">
                    <div class="panel-body feildcont field freelancer-listing">
                        <?php
                        if (sizeof($data['job_proposals']) > 0)
                        {
                            foreach ($data['job_proposals'] as $keyAF => $valueAF)
                            {
                                if($valueAF->team_id != NULL )
                                {
                            ?>
                                <div class="col-sm-12 freelancer-details forp row">
                                    <div class="row">
                                        <div class="col-sm-1">
                                            <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png'  ?>" class="img-responsive img-circle">
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="margin_bottom1">
                                                {{$valueAF->name}}
                                                
                                            </p>
                                            <p><?php echo substr($valueAF->business_desc, 0, 115); ?>...</p>
                                            <div class="freelancer-earnings-div">
                                                <span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerHour . " / hr"; ?></span>
                                                <span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerProject . " Fixed"; ?></span>
                                                
                                                <span>
                                                    @if(isset($totalstars))
                                                    {{ $totalstars[$valueAF->id]['success_percent'] }}% Job Success
                                                    <div class="job-success-progress">
                                                        <div class="progress-complete" style="float:left;width: <?php echo $totalstars[$valueAF->id]['success_percent'] ?>%" data-toggle="tooltip" data-placement="top" title="{{ $totalstars[$valueAF->id]['success_percent'].'% success job' }}">
                                                        </div>
                                                        <div class="progress-pending" style="float:left;width: <?php echo 100-$totalstars[$valueAF->id]['success_percent'] ?>%">
                                                        </div>
                                                    </div>
                                                     @else
                                                    <?php echo 0 . "% Job Success";?>
                                                    <div class="job-success-progress">
                                                        <div class="progress-complete" style="float:left;width:<?php echo '0';?>%" data-toggle="tooltip" data-placement="top" title="">
                                                        </div>
                                                        <div class="progress-pending" style="float:left;width: <?php  echo '100';?>%">
                                                        </div>
                                                    </div>
                                                    @endif
                                                </span>
                                                <?php
                                                if ($valueAF->address_country != '')
                                                {
                                                ?>
                                                    <span><i class="fa fa-map-marker"></i><?php echo $valueAF->address_country; ?></span>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                       <?php 
                                            if($data['countteam'] <= 1)
                                            {
                                        ?>
                                        <div class="col-sm-2">
                                            <a href="{{url('client/team/hire')}}/<?php echo $valueAF->job_id . '/' . $valueAF->team_id . '/' . $valueAF->bid_id; ?>" name="hire_btn" class="button solid invite-to-job pull-right">Hire</a>
                                        </div>
                                   
                                    <?php }else{
                                            ?>
                                            <div class="col-sm-2"></div>
                                    <?php } ?>
                                    </div>
                                </div>
                            <?php
                            }
                            else
                            {
                                ?>
                                <div class="list-empty">
                                    <h3 class="text-center">No freelancer found based on your search criteria.</h3>
                                </div>
                                 <?php
                                 }
                                ?>
                         <?php   
                        }
                        }
                        else
                        {
                        ?>
                            <div class="list-empty">
                                <h3 class="text-center">No freelancer found based on your search criteria.</h3>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div id="hire_div" class="tab-pane fade">
                    <?php
                    if (sizeof($data['all_hires']) > 0)
                    {
                        foreach ($data['all_hires'] as $keyAH => $valueAH)
                        {
                    ?>
                            <div class="col-sm-12 freelancer-details forp row">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png' ?>" class="img-responsive img-circle">
                                    </div>
                                    <div class="col-sm-11">
                                        <p class="margin_bottom">
                                            <?php
                                            if($valueAH->firstName != "")
                                            {
                                                echo $valueAH->firstName . " " . $valueAH->lastName;
                                            }
                                            else
                                            {
                                                echo $valueAH->userName;
                                            }
                                            ?>
                                        </p>
                                        <div class="freelancer-earnings-div">
                                            <span><?php echo $valueAH->job_type == '1' ? 'Hourly' : 'Fixed'; ?>&nbsp;Job @ <i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAH->rate; $valueAH->job_type == '0' ? '/hr' : ''; ?></span>
                                            <span>Current Status:
                                                {{$data['status_hires']->where('status_int', $valueAH->status)->first()->name}}
                                            </span>
                                            <span>Hired on: <?php echo date("d M, Y", strtotime($valueAH->created_at)); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    }
                    else
                    {
                    ?>  <div class="list-empty">
                            <p class="text-center">No Hires Yet</p>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="right-pane col-xs-3">
                <ul>
                    <li><a href="{{url('client/job/edit')}}/{{$data['job_details']->id}}" class="button solid "><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;Edit Posting &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a href="javascript:void(0);" onclick="close_job({{$data['job_details']->id}})"  rel="" class="button default"><i class="fa fa-times"></i>&nbsp;Remove Posting</a></li>
                </ul>
            </div>
        </div>
    </div>
    @endsection
    @section('addjavascript')
    <script src="{{asset('js/client.js')}}"></script>
    <script src="{{asset('js/client-job.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
    @endsection