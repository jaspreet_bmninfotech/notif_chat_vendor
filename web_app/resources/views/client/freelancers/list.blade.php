@extends('../layouts/base')
@section('content')
<div class="freelancer-list-container job-posting-form container ">
	
		<div class="vfform panel-group">
			<div class="search-freelancer-filter heading col-sm-12">
				<form class="search-form freelancer-search-form" name="searchForm" action="http://127.0.0.1:8000/client/freelancer">
					<div class="freelancer-search-form-wrapper col-sm-6">
						<div class="row">
							<input type="text" class="q-s" placeholder="Search for freelancers" name="filter_name" value="<?php echo isset($data['search_params']['filter_name']) ? $data['search_params']['filter_name'] : ''; ?>">
							<button id="search_freelancers_btn" class="button solid" type="button"><i class="fa fa-filter" aria-hidden="true"></i>&nbsp;Filters</button>
							<button class="s-btn" type="submit"><i class="icon" data-icon=""></i></button>
						</div>
					</div>
					
					<div class="filters-div col-sm-12 hide">
						<div class="col-sm-4 filter-category-wrap">
							<label class="filter-heading">Category</label>
							<select name="filter_category" class="form-control">
								<option value="">Any Category</option>
								<?php
								foreach ($data['all_categories'] as $keyAC => $valueAC)
								{
								?>
									<option value="<?php echo $valueAC['id']; ?>" <?php echo isset($data['search_params']['filter_category']) && $data['search_params']['filter_category'] == $valueAC['id'] ? 'selected' : ''; ?>><?php echo $valueAC['name']; ?></option>
								<?php
								}
								?>
							</select>
							<br>
							<label>Location</label>
							<select name="filter_location" class="form-control">
								<option value="">Any Country</option>
								<?php
								foreach ($data['all_countries'] as $keyACS => $valueACS)
								{
								?>
									<option value="<?php echo $valueACS['name']; ?>" <?php echo isset($data['search_params']['filter_location']) && $data['search_params']['filter_location'] == $valueACS['name'] ? 'selected' : ''; ?>><?php echo $valueACS['name']; ?></option>
								<?php
								}
								?>
							</select>
						</div>

						<div class="col-sm-3">
							<label class="filter-heading">Hourly Rate</label>
							<ul>
								<li>
									<input type="radio" name="filter_hourly_rate" value="" <?php echo isset($data['search_params']['filter_hourly_rate']) && $data['search_params']['filter_hourly_rate'] != '' ? '' : 'checked'; ?>>&nbsp;
									<span>Any hourly rate</span>
								</li>
								<li>
									<input type="radio" name="filter_hourly_rate" value="-10" <?php echo isset($data['search_params']['filter_hourly_rate']) && $data['search_params']['filter_hourly_rate'] == '-10' ? 'checked' : ''; ?>>&nbsp;
									<span>&#36;10 &#38; below</span>
								</li>
								<li>
									<input type="radio" name="filter_hourly_rate" value="10-29" <?php echo isset($data['search_params']['filter_hourly_rate']) && $data['search_params']['filter_hourly_rate'] == '10-29' ? 'checked' : ''; ?>>&nbsp;
									<span>&#36;10 - &#36;30</span>
								</li>
								<li>
									<input type="radio" name="filter_hourly_rate" value="30-59" <?php echo isset($data['search_params']['filter_hourly_rate']) && $data['search_params']['filter_hourly_rate'] == '30-59' ? 'checked' : ''; ?>>&nbsp;
									<span>&#36;30 - &#36;60</span>
								</li>
								<li>
									<input type="radio" name="filter_hourly_rate" value="60+" <?php echo isset($data['search_params']['filter_hourly_rate']) && $data['search_params']['filter_hourly_rate'] == '60+' ? 'checked' : ''; ?>>&nbsp;
									<span>&#36;60 &#38; above</span>
								</li>
							</ul>
						</div>

						<div class="col-sm-3">
							<label class="filter-heading">Job Success</label>
							<ul>
								<li>
									<input type="radio" name="filter_job_success" value="" <?php echo isset($data['search_params']['filter_job_success']) && $data['search_params']['filter_job_success'] != '' ? '' : 'checked'; ?>>&nbsp;
									<span>Any job success</span>
								</li>
								<li>
									<input type="radio" name="filter_job_success" value="80" <?php echo isset($data['search_params']['filter_job_success']) && $data['search_params']['filter_job_success'] == '80' ? 'checked' : ''; ?>>&nbsp;
									<span>80&#37; &#38; up</span>
								</li>
								<li>
									<input type="radio" name="filter_job_success" value="90" <?php echo isset($data['search_params']['filter_job_success']) && $data['search_params']['filter_job_success'] == '90' ? 'checked' : ''; ?>>&nbsp;
									<span>90&#37; &#38; up</span>
								</li>
							</ul>
						</div>

						<div class="col-sm-12 filter-close-wrap">
							<button class="button default" id="close_filters" type="button">Close filters</button>
							<button class="button solid" type="submit">Apply filters</button>
						</div>
					</div>
				</form>
			</div>

			<div class="panel-body feildcont field freelancer-listing">
				<?php
				if (sizeof($data['all_freelancers']) > 0)
				{

					foreach ($data['all_freelancers'] as $keyAF => $valueAF)
					{
					?>
						<div class="col-sm-12 freelancer-details">
							<div class="col-sm-1">
								<img src="/<?php echo ($data['attach'][$keyAF] != null) ?  $data['attach'][$keyAF]:'images/no-user.png'  ?>" class="img-responsive img-circle vendimg">
								
							</div>
							<div class="col-sm-9 showinvite">
								<span class="uname"><?php if($valueAF->firstName != ""){
										echo $valueAF->firstName . " " . $valueAF->lastName;
									}else{
										echo $valueAF->username;
									} ?></span>
								<span class="success_msg alert alert-info hidden"></span>
								<span class="error_msg alert alert-danger hidden" ></span>
								<p class="margin_defaullt"><?php echo substr($valueAF->business_desc, 0, 115); ?>...</p>
								<div class="freelancer-earnings-div">

									<span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerHour . " / hr"; ?></span>
									<!-- <span><i class="fa fa-usd" aria-hidden="true"></i>lorem earned</span> -->
									<span>
										<?php
											$barPercent = 0;
											if(array_key_exists($valueAF->id, $data['success_rate'])){
												echo $data['success_rate'][$valueAF->id]['success_percent'] . "% Job Success";
												$barPercent = $data['success_rate'][$valueAF->id]['success_percent'];
											}else{
												echo '0'."% Job Success";

											}
										?>
										
										<div class="job-success-progress"><div class="progress-complete" style="width: <?php if($barPercent != 0){echo $barPercent.'%';}else{echo '0';} ?>%" data-toggle="tooltip" data-placement="top" title="{{ $barPercent.'% success job' }}"></div><div class="progress-pending" style="width: <?php if($barPercent != 0){echo 100 - $barPercent.'%';}else{echo '0';} ?>%"></div></div>
									</span>
									<?php
									if ($valueAF->address_country != '')
									{
									?>
										<span><i class="fa fa-map-marker"></i><?php echo $valueAF->address_country; ?></span>
									<?php
									}
									?>
								</div>
							</div>
							<div class="col-sm-2">
								<input type="button" id="button_{{$keyAF}}" name="invite_to_job" class="button solid invite-to-job pull-right" onclick="initiate_invite('<?php echo base64_encode($valueAF->id); ?>',this);" value="Invite To Job">
							</div>
						
						</div>
						
					<?php
					}
				}
				else
				{
				?>
					<div class="list-empty">
						<h3 class="text-center">No freelancer found based on your search criteria.</h3>
					</div>
				<?php
				}
				?>
			</div>
		</div>
	
</div>

					               	
<div id="invitation_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="send_invitation_form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<span class="modal-title">Invite <a href="javascript:void(0);" class="invitation_rcvr_name">Flaana Singh</a> to apply regarding</span>
				</div>
				<div class="modal-body">
					<div class="col-sm-12 form-group">
						<div class="row">
							<div class="col-sm-1">
								<div class="row" id="vend-img" >
									<img src="/images/no-user.png" class="img-responsive img-circle">
								</div>
							</div>
							<div class="col-sm-11">
								<span><a href="javascript:void(0);" class="invitation_rcvr_name">Flaana Singh</a></span><br>
								<span></span>
							</div>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<div class="row">
							<label>Message</label>
							<input type="hidden" name="invitation_rcvr_id" class="invitation_rcvr_id">
							<textarea class="form-control" placeholder="Write your message here..." name="invitation_message"></textarea>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<div class="row">
							<?php
							if (sizeof($data['vendor_all_jobs']) > 0)
							{
							?>
								<label>Choose a job</label>
								<select class="form-control" name="invitation_jobs" id="invitation_jobs">
									
									<?php
									foreach ($data['vendor_all_jobs'] as $keyVAJ => $valueVAJ)
									{
									?>
										<option value="<?php echo $valueVAJ['id']; ?>" ><?php echo $valueVAJ['name']; ?></option>
									<?php
									}
									?>
								</select>
							<?php
							}
							?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<?php
					if (sizeof($data['vendor_all_jobs']) > 0)
					{
					?>
						<input type="button" id="send_job_invite_btn" class="btn btn-block btn-success" onclick="send_invitation(this);" value="Send Invitation">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<?php
					}
					?>
					
					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
				</div>
			</form>

		</div>
	</div>
</div>
@endsection