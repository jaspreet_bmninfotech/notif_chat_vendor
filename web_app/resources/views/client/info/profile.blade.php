@extends('layouts/base')
@section('addstyle')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css"/>
<link rel="stylesheet" href="{{asset('plugin/intl-tel-input-master/build/css/intlTelInput.css')}}">
@endsection
@section('content')
<div class="job-posting-form container">
	@if(Session::has('profileRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('profileRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('addressRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('addressRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('success'))
		    <div class="alert alert-success ">
		      <p style="color:green;">{{Session::get('success')}}</p>
		      @php Session::forget('success'); @endphp
		    </div>
		@endif
	<div class="col-md-3">
		<div class="sidebar_widget">
			<div class="sidebar_title"><h4>Personal Information</h4>
			</div>
			<ul class="arrows_list1">		
				<li><a href="profile"><i class="fa fa-angle-right"></i> Profile</a></li>
				<li><a href="{{route('client.profile.changePassword')}}"><i class="fa fa-angle-right"></i> Change password</a></li>
				<!-- <li><a href="business"><i class="fa fa-angle-right"></i> Business</a></li>
				<li><a href="payment"><i class="fa fa-angle-right"></i> Payment</a></li> -->
			</ul>	
		</div>
	</div>
	<div class="form-fields col-md-9">
		<div class="vfform">
			<div class="title">
				<h3>Profile</h3>
			</div>
			<div class="feildcont fieldpro">
				<form role="form" method="POST" id="clientprofile" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="row">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-6">
									<label>First Name <em>*</em></label>
									<input type="text" name="firstName" value="{{$data['firstName']}}">
									
									<span class="firstName_error text-danger"></span>
									
								</div>
								<div class="col-md-6 last">
									 <label>Last Name <em>*</em></label>
									<input type="text" name="lastName" value="{{$data['lastName']}}">
									
									<span class="lastName_error text-danger"></span>
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Username <em>*</em></label>
									<input type="text" name="username" value="{{$data['username']}}" readonly/>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Email <em>*</em></label>
									<input type="text" name="email" value="{{$data['email']}}" readonly/>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<label>Ph no. <em>*</em></label>
										<div class="row" style="margin-bottom: 0px;">
											<div class="col-md-3">
												<input type="tel" id="demo" placeholder="" name="dialCode" value="{{ (empty($data['countryDialcode']) ? null :$data['countryDialcode']  )}}">
												
												<span class="countrycode_error text-danger"></span>
												
											</div>
											<div class="col-md-9 ">
												<input type="text" name="phone" value="{{$data['phone']}}" min="10" maxlength ="12">
												
												<span class="phone_error text-danger"></span>
												   
											</div>
										</div>
	 
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Gender</label>
									<div class="radiobut">
										<input class="one" type="radio" name="gender" value="M" {{ ($data['gender']=='M') ? 'checked':''}} >
										<span class="onelb">Male</span>
										<input class="two" type="radio" name="gender" value="F" {{ ($data['gender']=='F') ? 'checked':''}}>
										<span class="onelb">Female</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Date of Birth</label>
									<div class="input-group date" id="datePicker">
										<input type="text" name="dob" value="{{ $data['dob'] }}">
										<span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
										@if($errors->has('dob'))
										<span class="text-danger">{{ $errors->first('dob') }}</span>
										@endif 
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row ">
								<div class="col-md-12">
									<div id="cprofilefiles" class="profile">
										<?php
										if ($attachment != NULL) {
											?>
										<img src="/{{$attachment->path}}" class="profile" id="profileimage">
										<?php
										}else{
											?>
										<img src="/images/no-user.png" class="profile" id="profileimage">
										<?php
										}
										?>
								</div>
								</div>
								<div class="col-md-12">
									<span class=" button solid btnupload fileinput-button">
								        <i class="glyphicon glyphicon-plus"></i>
								        <span>Choose Profiles...</span>
								        <input id="cprofileupload" type="file" name="file"  multiple>
								        <input type="hidden" id="fileattachment" name="hid">
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="text-center">
					<button type="submit" id="createprofile" class="button solid ">Save Profile</button>
					</div>
				</form>
			</div>
		</div>
	<!-- Profile end-->
	<!-- Mailing address start -->
		<div class="vfform">
			<div class="title">
				<h3>Mailing Address</h3>
			</div>
			<div class="feildcont fieldpro">
				<form role="form" method="POST" action="{{route('client.profile.mailing')}}" autocomplete="off">
					{!! csrf_field() !!}
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<label>Street Address <em>*</em></label>
									<input type="text" name="address">
									<div class="clearfix"></div>
			                        @if($errors->has('address'))
			                            <span class="text-danger">
			                                {{ $errors->first('address') }}
			                            </span>
			                        @endif
								</div>
							</div>
							<div class="row">		
								<div class="col-md-6">
									<label>Country <em>*</em></label>
									<select name="country" id="country" onchange="getstate(this)">
										<option value="">Country</option>
											@foreach($country as $key=>$val)
										<option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
											@endforeach
									</select>
									<div class="clearfix"></div>
			                        @if($errors->has('country'))
			                           <span class="text-danger">
			                                {{ $errors->first('country') }}
			                            </span>
			                        @endif
								</div>
								<div class="col-md-6 last">
									<label>State <em>*</em></label>
									<select name="state" id="state" onchange="getcity(this)">
											
									</select>
									<div class="clearfix"></div>
			                        @if($errors->has('state'))
			                            <span class="text-danger">
			                                {{ $errors->first('state') }}
			                            </span>
			                        @endif
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>City <em>*</em></label>
									<select name="city" id="city">
											<option>City</option>
									</select>
									<div class="clearfix"></div>
			                        @if($errors->has('city'))
			                            <span class="text-danger">
			                                {{ $errors->first('city') }}
			                            </span>
			                        @endif
								</div>	
								<div class="col-md-6">
									<label>Postal Code <em>*</em></label>
									<input type="text" name="postalCode">
									<div class="clearfix"></div>
			                        @if($errors->has('postalCode'))
			                            <span class="text-danger">
			                                {{ $errors->first('postalCode') }}
			                            </span>
			                        @endif
								</div>
							</div>
						</div>
					</div>
					<div class="text-center">
						<button type="submit" class="button solid">Save Address</button>
					</div>
				</form>
			</div>
		</div>
	<!-- Mailing address end -->						
	</div>
</div>

@endsection
@section('addjavascript')	
	<script type="text/javascript" src="{{asset('js/clientProfile.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/client.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/address.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
	<script src="{{ asset('plugin/intl-tel-input-master/build/js/intlTelInput.js') }}"></script>
	<script type="text/javascript">
	
		$("#demo").intlTelInput();
		
		var code=$('.country-list  .country').find('.dial-code').html();
		$('input[type=tel]').val(code);

		
		$("#demo").intlTelInput("loadUtils", "lib/libphonenumber/build/utils.js");
		$("#mobile-number").intlTelInput();
				$('.country-list .country').each(function(e, v){
					if($(this).find('.dial-code').html() == '<?php echo $data['countryDialcode']; ?>'){
						$(this).find('country').addClass('preferred highlight active');
					}
				});
				$('input[name = dialCode]').val($('.selected-flag').attr('title').split(':')[1]);
				$('.country-list li').click(function(){
					$('input[name = dialCode]').val($(this).find('.dial-code').html());
				});
</script>


@endsection