@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
	<div class="row">
		<div class="vendor-job-list form-fields panel-group">
			<div class="vfform margin-zero" id="">
				<div class="title">
					<h3>CONTRACTS</h3>
				</div>
				<div class="">
					<div class="col-md-6">
						<h4>Title</h4>
					</div>
					<div class="col-md-4">
						<h4>Rate</h4>
					</div>
					<div class="col-md-2">
						<h4>Detail</h4>
					</div>
				</div>
				<?php
				if($vendors->isEmpty()) {
					?>
				 <div>
					<div class="col-md-12">
						<div class="row">
							<div class="vf-bids">
								<div class="col-md-12 text-center ">
									<h3>Currently you have no Contracts</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				 }else{ ?>
			@foreach($vendors as $hires)
				<div>
					<div class="col-md-12">
						<div class="row">
							<div class="vf-bids">
								<div class="col-md-6 ">
									<div class="bid-detail">
										<h3>{{$hires->name}}</h3>
										<span> {{date("m/d/Y", strtotime($hires->created_at))}}</span>
									</div>
								</div>
								<div class="col-md-4 ">
								<?php if ($hires->isHourly == 0) {
									?>
									<div class="bid-detail">
										  ${{$hires->rate}}
									</div>
								<?php
								}else{
									?>
									<div class="bid-detail">
										 ${{$hires->rate}}/hr
									</div>
									<?php
								}
								?>
								</div>
								<div class="col-md-2 ">
									<div class="bid-detail">
										<a href="{{route('client.contract.detail',['id' => base64_encode($hires->hire_id)])}}"><button class="button solid">view contract</button></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			<?php }?>
			</div>
		</div>
	</div>
	
</div>
<div class="job-posting-form container">
	<div class="row">
		<div class="vendor-job-list form-fields panel-group">
			<div class="vfform margin-zero" id="">
				<div class="title">
					<h3>TEAM CONTRACTS</h3>
				</div>
				<div class="">
					<div class="col-md-4">
						<h4>Title</h4>
					</div>
					<div class="col-md-4">
						<h4>Rate</h4>
					</div>
					<div class="col-md-2">
						<h4>Members</h4>
					</div>
					<div class="col-md-2">
						<h4>Detail</h4>
					</div>
				</div>
				<?php
				if($team->isEmpty()) {
					?>
				 <div>
					<div class="col-md-12">
						<div class="row">
							<div class="vf-bids">
								<div class="col-md-12 text-center ">
									<h3>Currently you have no Contracts</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				 }else{ ?>
			@foreach($team as $hires)
				<div>
					<div class="col-md-12">
						<div class="row">
							<div class="vf-bids">
								<div class="col-md-4 ">
									<div class="bid-detail">
										<h3>{{$hires->name}}</h3>
										<span>  {{date("m/d/Y", strtotime($hires->created_at))}}</span>
									</div>
								</div>
								<div class="col-md-4 ">
								<?php if ($hires->isHourly == 0) {
									?>
									<div class="bid-detail">
										  ${{$hires->rate}}
									</div>
								<?php
								}else{
									?>
									<div class="bid-detail">
										 ${{$hires->rate}}/hr
									</div>
									<?php
								}
								?>
								</div>
									<div class="col-md-2 ">
										<div class="bid-detail">
											{{count($members) > 0 ? count($members): 0}}
										</div>
									</div>
								<div class="col-md-2 ">
									<div class="bid-detail">
										<a href="{{route('client.contract.teamdetail',['id' => base64_encode($hires->hire_id),'tid'=>base64_encode($hires->teamid)])}}"><button class="button solid">Request For Fund</button></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			<?php }?>
			</div>
		</div>
	</div>
	
</div>
		

  @endsection