@extends('../layouts/base')
@section('content')
<style type="text/css">
  	.notifications{
  		background: #fff;
  		padding: 10px;
  		margin-top: 10px
  	}

	.circle-loader {
		margin: 0 0 30px 10px;
		border: 2px solid rgba(0, 0, 0, 0.2);
		border-left-color: #5cb85c;
		animation-name: loader-spin;
		animation-duration: 1s;
		animation-iteration-count: infinite;
		animation-timing-function: linear;
		position: relative;
		display: inline-block;
		vertical-align: top;
	}

	.circle-loader,
	.circle-loader:after {
		border-radius: 50%;
		width: 8em;
		height: 8em;
	}

	.load-complete {
		-webkit-animation: none;
		animation: none;
		border-color: #5cb85c;
		transition: border 500ms ease-out;
	}

	.checkmark {
		display: none;

		&.draw:after {
			animation-duration: 800ms;
			animation-timing-function: ease;
			animation-name: checkmark;
			transform: scaleX(-1) rotate(135deg);
		}

		&:after {
			opacity: 1;
			height: 6em;
			width: 2em;
			transform-origin: left top;
			border-right: 2px solid #5cb85c;
			border-top: 2px solid #5cb85c;
			content: '';
			left: 13%;
			top: 60%;
			position: absolute;
	  	}
	}

	@keyframes loader-spin {
		0% {
			transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
		}
	}

	@keyframes checkmark {
		0% {
			height: 0;
			width: 0;
			opacity: 1;
		}
		20% {
			height: 0;
			width: 20%;
			opacity: 1;
		}
		40% {
			height: 20%;
			width: 20%;
			opacity: 1;
		}
		100% {
			height: 20%;
			width: 20%;
			opacity: 1;
		}
	}
  	</style>
		<div class="container job-deatils-page">
			<h2>{{$data['name']}}</h2>
			<div class="row">
				<div class="col-md-8">
					<div class="vf-wrap job-deatils">
                        <div class="col-md-4 border-right">
                            <label>Agreed</label>
                                <div>
                                    <h1 >$<span class="total">{{$hires->rate}}</span></h1>
                                </div>
                        </div>
                        <div class="col-md-4 border-right">
                            <label>Paid</label>
                                <div>
                                    <h1 >$<span class="paidTill">{{$totalPaid}}</span></h1>
                                </div>
                        </div>
                        <div class="col-md-4 ">
                            <label>Pending Amount</label>
                                <div>
                                    <h1 >$<span class="pending">{{$pendingAmount}}</span></h1>
                                </div>
                        </div>
                        {{-- <div class="col-md-3">
                            <label>Remaining</label>
                                <div>
                                    <h1 >$<span class="remaining">{{ (($hires->rate - $totalPaid)-$pendingAmount) }}</span></h1>
                                </div>
                        </div> --}}


                    </div>
                    @if($jobTransaction['approve_release'] == null) 
							<button class="btn btn-success releaseFunds">Release Funds</button>
						@else
							<span>Payment released</span>
						@endif
					{{-- <div class="notifications">
						@if($jobTransaction)
							<table class="table table-bordered" style="background: #fff">
								<thead>
									<tr>
									    <th>Amount</th>
									    <th>Fund Requested</th>
									    <th>Fund Approved</th>
									    <th>Request Release</th>
									    <th>Approve Release</th>
									    <th>Status</th>
									</tr>
								</thead>
								<tbody>

									@foreach($jobTransaction as $k => $v)
									<tr>

										<td >%{{$v->amount}} <span style="display: none;" class="transaction_amount">{{$v->amount}}</span></td>
										<td > {{$v->request_fund}} </td>
										<td class="approve_fund"> {{$v->approve_fund}} </td>
										<td> {{$v->request_release}} </td>
										<td class="approve_release"> {{$v->approve_release}} </td>
										@if( $v->request_fund != null && $v->approve_fund == null )
										<!-- <td><button class="btn btn-success approve_fund_button" transaction-id="{{$v->id}}">Accept</button></td> -->
										<div class="modal fade" id="myModal{{$v->id}}" role="dialog">

									    	<div class="modal-dialog">
										      	<div class="modal-content">
										        	<div class="modal-header" style="text-align: center;">
										          		<button type="button" class="close" data-dismiss="modal">&times;</button>
										          		<h4 class="modal-title"> Payment of ${{$v->amount}}</h4>
										        	</div>
										        	<div class="modal-body PaymentForm">
										          		<!-- <form action="/action_page.php"> -->
														  	<div class="form-group">
															    <label for="card_number">Card Number</label>
															    <input type="text" class="form-control" value="42424242" name="card_number" id="card_number">
															    <input type="hidden" class="form-control" value="{{$v->id}}" name="transaction_id">
															    <input type="hidden" class="form-control" value="{{$v->amount}}" name="transaction_amount">
														  	</div>
														  	<div class="row">
														  		<div class="col-md-4">
																  	<div class="form-group">
																	    <label for="cvv">CVV</label>
																	    <input type="text" placeholder="CVV" value="314" name="cvv" class="form-control" id="cvv">
																  	</div>
														  		</div>
														  		<div class="col-md-4">	
																  	<div class="form-group">
																	    <label for="exipryMonth">Expire Month</label>
																	    <input type="text" placeholder="MM" value="3" name="expireMonth" class="form-control" id="exipryMonth">
																  	</div>
														  		</div>
														  		<div class="col-md-4">
																  	<div class="form-group">
																	    <label for="expireYear">Expire Year</label>
																	    <input type="text" placeholder="YYYY" value="2019" name="expireYear" class="form-control" id="expireYear">
																  	</div>
														  		</div>
														  	</div>
														  	<div class="card-error"></div>
											        		<button class="btn btn-success approve_fund_button" transaction-id="{{$v->id}}">Accept</button>
											        		<div class="circle-loader" style="display: none;">
															  	<div class="checkmark draw"></div>
															</div>
														<!-- </form> -->
									        		</div>
										      	</div>
									    	</div>
									  	</div>
									  	@if($stripeCard)
						  					<td><button type="button" class="btn btn-success hasCustomer" transaction-id="{{$v->id}}" >Accept</button></td>
									  	@else
						  					<td><button type="button" class="btn btn-success " transaction-id="{{$v->id}}" data-toggle="modal" data-target="#myModal{{$v->id}}">Accept</button></td>
					  					@endif
										@elseif( $v->request_fund != null && $v->approve_fund != null  && $v->request_release != null && $v->approve_release == null)
											<td><button class="btn btn-success release_fund_button" transaction-id="{{$v->id}}" >Accept</button></td>
										@else
											<td><button class="btn btn-success " transaction-id="{{$v->id}}" disabled="disabled">Accept</button></td>
										@endif
									</tr>
									@endforeach
								</tbody>
							</table>
						@endif 
						<!-- Trigger the modal with a button -->

						  <!-- Modal -->
						  	
						@if($jobTransaction)
							<div class="show-alert"></div>
							<table class="table table-bordered">
								<thead>
									<tr>
									    <th>Notification</th>
									    <th>Action</th>
									</tr>
								</thead>
								<tbody>

									@foreach($jobTransaction as $k => $v)
									<tr>
										<td>{{$v->getUser->firstName}} Request for fund of $ {{$v->amount}} </td>

										@if($v->approve_fund != null)
											<td><button class="btn btn-success" transaction-id="{{$v->id}}">Approved <span class="fa fa-check"></span></button></td>
										@else
											<td><button class="btn btn-success approve_fund" transaction-id="{{$v->id}}">Approve</button></td>
										@endif
									</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>--}}
				</div>
	                    <input type="hidden" name="jobTransaction" value="{{ $jobTransaction['id'] }}">

				<div class="col-md-4 client-details">
					<div class="row">
						<div class="job-detail-panel bottom-margin">
							<div class="job-detail-panel rightbar-padding ">
								<h3>Client</h3>
							</div>
							<div class="rightbar-padding contract-sidebar">
								<div class="col-md-4">
									<img src="{{asset($profile_image)}}">
								</div>
								<div class="col-md-8">
									<label class="">{{$client['username']}}</label>
									<p class="">{{$bussiness['name']}}</p>
								</div>
							</div>
							<br clear="all">
							<div class="job-detail-panel rightbar-padding">
								<button class="button solid ">Mesage</button>
							</div>
						</div>
						<div class="job-detail-panel bottom-margin">
							<div class="job-detail-panel rightbar-padding ">
								<h3>Start Chat</h3>
							</div>
							<div class="rightbar-padding text-center">
								
							</div>
							<div class="job-detail-panel rightbar-padding">
								<?php if($hires['status']==1 || $hasfeedback==NULL){
									?>
								<a href="{{route('client.contract.closed', ['id' => $hires->id])}}"><button class="button solid ">End contract</button></a>
								<?php	
								}else{
									?>
								<a href="{{route('client.contract')}}"><button class="button solid ">Closed</button></a>
								<?php
									}
								?>
								@if($hires['status'] == 1)
									<a href="{{ route('chat',$jobTransaction['id']) }}"><button class="button solid ">Start Chat</button></a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<script type="text/javascript">
			$(document).ready(function(){
            
				var d = new Date();

				var curr_date = d.getDate();

				var curr_month = d.getMonth();

				var curr_year = d.getFullYear();

				$(document).on('click' , '.approve_fund_button' , function(e){
					e.preventDefault();
						$('.circle-loader').show();
						var transaction_id = $('input[name = transaction_id]').val();
						var card_number = $('input[name = card_number]').val();
						var cvv = $('input[name = cvv]').val();
						var expireMonth = $('input[name = expireMonth]').val();
						var expireYear = $('input[name = expireYear]').val();
						var token = $('input[name=_token]').val();

					$.ajax({
						url : '{{route("fund.funded")}}',
						type : "POST",
						data : {_token : token , transaction_id : transaction_id , card_number : card_number , cvv : cvv , expireMonth : expireMonth , expireYear: expireYear},
						success : function(data){
							$('.circle-loader').hide();
							if(data === "true"){
								$('.circle-loader').toggleClass('load-complete');
								$('.modal').modal('hide');

								$('button[transaction-id = '+transaction_id+']').attr("disabled", "disabled");
								$('button[transaction-id = '+transaction_id+']').removeClass("approve_fund_button");

								if($('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_fund').html().trim() == ""){
									$('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_fund').html(curr_year + "-" + curr_month + "-" + curr_date +' '+ d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
								}else{
									$('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_release').html(curr_year + "-" + curr_month + "-" + curr_date +' '+ d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
								}
								location.reload();
							}else{
								
								alert("spmething went wrong");
							}
						},
						error : function(xhr, status, error){
							$('.card-error').html("<span style='color:red'>"+xhr.responseJSON.message+"</span>");
							$('.circle-loader').hide();
						}
					});
				});
				$(document).on('click' , '.releaseFunds' , function(e){
					e.preventDefault();
					var transaction_id = $('input[name=jobTransaction]').val();
					var token = $('input[name=_token]').val();

						$.ajax({
							url : '{{route("fund.funded")}}',
							type : "POST",
							data : {_token : token , releaseFunds: 'true' , transactId: transaction_id},
							success : function(data){
								console.log(data);

								if(data === "true"){
									alert("payment Sent");
								}else{
									$('.modal').modal('show');
								}
							}
						});
				});
				$(document).on('click' , '.hasCustomer' , function(e){
					e.preventDefault();
					var transaction_id = $(this).attr('transaction-id');

					var token = $('input[name=_token]').val();

						$.ajax({
							url : '{{route("fund.funded")}}',
							type : "POST",
							data : {_token : token , hasCustomer: 'true' , transactId: transaction_id},
							success : function(data){
								console.log(data);

								if(data === "true"){
									$('button[transaction-id = '+transaction_id+']').attr("disabled", "disabled");
									$('button[transaction-id = '+transaction_id+']').removeClass("release_fund_button");

									if($('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_fund').html().trim() == ""){
										$('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_fund').html(curr_year + "-" + curr_month + "-" + curr_date +' '+ d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
									}else{
										$('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_release').html(curr_year + "-" + curr_month + "-" + curr_date +' '+ d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
									}
									// $('.remaining').html(newRemaining);
									// $('.paidTill').html(paid);
									// location.reload();
								}else{

									$('.modal').modal('show');								}
							}
						});
				});
				$(document).on('click' , '.release_fund_button' , function(e){
					e.preventDefault();
					var transaction_id = $(this).attr('transaction-id');
					var token = $('input[name=_token]').val();
						$.ajax({
							url : '{{route("fund.funded")}}',
							type : "POST",
							data : {_token : token , tranId : transaction_id},
							success : function(data){
								if(data === "true"){
									$('button[transaction-id = '+transaction_id+']').attr("disabled", "disabled");
									$('button[transaction-id = '+transaction_id+']').removeClass("release_fund_button");

									if($('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_fund').html().trim() == ""){
										$('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_fund').html(curr_year + "-" + curr_month + "-" + curr_date +' '+ d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
									}else{
										$('button[transaction-id = '+transaction_id+']').parents('tr').find('.approve_release').html(curr_year + "-" + curr_month + "-" + curr_date +' '+ d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
									}
									var newRemaining =$('.pending').html();
									
		 							var newtotalPaid =$('.paidTill').html();
		 							
		 							var p= parseFloat(newtotalPaid) + parseFloat(newRemaining);
									$('.paidTill').html(p);
									// var newremain =$('.remaining').html();
									// 	var remainupdate = newremain - newRemaining;
									// 	$('.pending').html(remainupdate);
									// location.reload();
								}else{
									alert("something went wrong");
								}
							}
						});
				});
			});
		</script>
@endsection