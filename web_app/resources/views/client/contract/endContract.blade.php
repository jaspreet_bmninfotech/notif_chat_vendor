@extends('../layouts/base')
  @section('content')

    <link rel="stylesheet" type="text/css" href="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/src/css/star-rating-svg.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/demo/css/demo.css')}}">
		<div class="container job-deatils-page ">
			<form method="post">
				{!! csrf_field() !!}
				<h2>{{$job['name']}}</h2>
				<h4>Share your experience! Your honest feedback provides helpful information to both the client.</h4>
				<div class="row">
					<div class="col-md-12">
						<div class="job-detail-panel">
							<div class="vf-wrap job-deatils">
								<div class="job-heading">
									<div class="">
										<div class="col-md-1">
											<div class="loudicon">
											<i class="fa fa-bullhorn" aria-hidden="true" ></i> 
												
											</div>
										</div>
										<div class="col-md-11">
											 <h3>  Public Feedback</h3>
											 <p>Shared on your client's profile only after they've left feedback for you.<a href="">learn more</a> </p>
										</div>
									</div>
									<br clear="all">
									<div class="top-bottom-margin stars-total">
										<h4>Feedback to Vendor</h4>
											<div class="skill jq-stars" data-rating="0.1"></div><label>Skills</label>
											<div class="quality jq-stars" data-rating="0.1"></div><label>Quality of Requirements</label>
											<div class="availability jq-stars" data-rating="0.1"></div><label>Availability</label>
											<div class="deadlines jq-stars" data-rating="0.1"></div><label>Set Reasonable Deadlines</label>
											<div class="communication jq-stars" data-rating="0.1"></div><label>Communication</label>
											<div class="cooperation jq-stars" data-rating="0.1"></div><label>Cooperation</label>
											<input type="hidden" name="" id="skillhiddenid" value="0">
											
											
										<h4>Total Score:<span id="total"></span></h4>
									</div>
									<div class="vf-textarea-halfsize">
										<p>Share your experience with this client:</p>
										@if($errors->has('review'))
									<span class="text-danger">{{ $errors->first('review') }}</span>
									@endif
										<textarea name="review"></textarea>
										<p>See an <a href="">example of appropriate feedback</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="topbottom-margin">
					<button class=" button solid">End Contract</button>
					<a href="{{route('client.contract.detail', ['id' => $contract->id])}}"><button type="button" class=" button default">Cancel</button></a>
				</div>
			</form>
		</div>
@endsection
@section('addjavascript')
<script src="{{asset('js/rating.js')}}"></script>
<script src="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/src/jquery.star-rating-svg.js')}}"></script>

@endsection