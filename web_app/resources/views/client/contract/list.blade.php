@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
	<div class="row">
		<div class="vendor-job-list form-fields panel-group">
			<div class="vfform margin-zero" id="">
				<div class="title">
					<h3>CONTRACTS</h3>
				</div>
				@if($data->isEmpty())

				 <div>
					<div class="col-md-12">
						<div class="row">
								<div class="top-bot-margin1 text-center">
									<h3>Currently you have no Contract</h3>
								</div>
						</div>
					</div>
				</div>
				@else
				<div class="">
					<div class="col-md-5">
						<h4>Title</h4>
					</div>
					<div class="col-md-4">
						<h4>Rate</h4>
					</div>
					<div class="col-md-3">
						<h4>Detail</h4>
					</div>
				</div>
			@foreach ($data as $hires)
				<div>
					<div class="col-md-12">
						<div class="row">
							<div class="vf-bids">
								<div class="col-md-5 ">
									<div class="bid-detail">
										<h3>{{$hires->name}}</h3>
										<span>{{date("m/d/Y", strtotime($hires->created_at))}}</span>
									</div>
								</div>
								<div class="col-md-4 ">
								@if($hires->isHourly == 1)
									<div class="bid-detail">
										${{$hires->rate}}/hr
									</div>
								@else

									<div class="bid-detail">
										${{$hires->rate}}/fixed
									</div>
									@endif
								</div>
								<div class="col-md-3 ">
									<div class="bid-detail">
										<a href="{{route('client.contract.contractlist',['id' => base64_encode($hires->id)])}}"><button class="button solid">View Job Contracts	</button></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			@endif
			</div>
		</div>
	</div>
	
</div>
		

  @endsection