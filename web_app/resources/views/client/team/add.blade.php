@extends('layouts/base')
@section('addstyle')
@endsection
@section('content')
<style type="text/css">
	.new_team_append{
		display: none;
	}
</style>
<div class="job-posting-form container">
	<div class="col-md-6">
		@if(Session::has('success'))
		    <div class="alert alert-success">
		      <p style="color:green;">{{Session::get('success')}}</p>
		      @php Session::forget('success'); @endphp
		    </div>
		@endif
		
	</div>
	<div class="form-fields col-md-12">
		
				{!! csrf_field() !!}
			<div class="vfform" >
					<div class="title">
					<h3>Search Team</h3>
					</div>
				<div class="feildcont">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6 ">
									<label >Team Name<em>*</em></label>
									<input type="text" name="cname"  placeholder="enter team name">
									<div class="clearfix"></div>
			                        @if($errors->has('name'))
			                            <span class="text-danger">
			                                {{ $errors->first('name') }}
			                            </span>
			                        @endif
								</div>
								
							</div>
							<div class="clearfix"></div>
							
							<div class="row">
								<div class="col-md-2 ">
									<label class="filter-heading">Location</label>
									<select name="country" id="country"  onchange="fgetstate(this)">
										<option value="">country</option>
											@foreach($data['all_countries'] as $key=>$val)
										<option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
											@endforeach
									</select>
									<div class="clearfix"></div>
			                        @if($errors->has('country'))
			                            <span class="text-danger">
			                                {{ $errors->first('country') }}
			                            </span>
			                        @endif
								</div>
								<div class="col-md-2 ">
									<label>&nbsp;</label>
									<select name="state" id="fstate"  onchange="fgetcity(this)">
										<option value="">State</option>
									</select>
									<div class="clearfix"></div>
			                        @if($errors->has('state'))
			                            <span class="text-danger">
			                                {{ $errors->first('state') }}
			                            </span>
			                        @endif
								</div>
								<div class="col-md-2 ">
									<label>&nbsp;</label>
									<select name="city"  id="fcity">
										<option value="">City</option>
									</select>
									<div class="clearfix"></div>
			                        @if($errors->has('city'))
			                            <span class="text-danger">
			                                {{ $errors->first('city') }}
			                            </span>
			                        @endif
								</div>
								
							</div>
						</div>
					</div>
					<div class="text-center">
						<a href="javascript:;" id="searchteam" class="button solid ">Search</a>
					</div>		
				</div>
			</div>
			<div class="appended_team">
				<div class="vfform new_team_append" >
					<div class="feildcont">
						<div class="col-sm-12 freelancer-details">
								<div class="col-sm-1 col-xs-6" id="appended_teams">
									<img src="https://avatars1.githubusercontent.com/u/5931623?v=4" class="img-responsive img-circle">
								</div>
								<div class="col-sm-9">
									<div class="teamname">
										<span class="name"></span><br>
										<span class="created_at"></span>
										<br>
										<span class="tmember"></span>
									</div>
									<div class="freelancer-earnings-div">
									</div>
								</div>
								<div class="col-sm-2">
									<input type="button" name="invite_team" class="button solid invite-team pull-right " onclick="initiate_invite(this);" value="Hire" data-id="">
								</div>
							</div>
					</div>
				</div>
			</div>
			<div class="search_team_result">
			</div>
			<div class="teamerrorshow" style="display:none;"><h4 class="teamerror text-center"></h4></div>
		
	</div>
</div>
<div id="invitation_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="send_invitation_form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<span class="modal-title">Invite <a href="javascript:void(0);" class="invitation_rcvr_name">Flaana Singh</a> to apply regarding</span>
				</div>
				<div class="modal-body">
					<div class="col-sm-12 form-group">
						<div class="row">
							<div class="col-sm-1">
								<div class="row">
									<?php
                                        if ($attachment != NULL) {
                                        	foreach ($attachment as $key => $value) {
                                        	}
                                            ?>
                                            <img src="/<?php echo $value->path ?>" class="img-responsive img-circle">
										<?php
                                        }else{
                                            ?>
									<img src="https://tctechcrunch2011.files.wordpress.com/2011/07/twitter_newbird_boxed_blueonwhite.png" class="img-responsive img-circle">
									 <?php
                                        }
                                        ?>
								</div>
							</div>
							<div class="col-sm-11">
								<span><a href="javascript:void(0);" class="invitation_rcvr_name">Flaana Singh</a></span><br>
								<span class="invitation_uname">Android Developer</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<div class="row">
							<label>Message</label>
							<input type="hidden" name="invitation_rcvr_id" class="invitation_rcvr_id">
							<input type="hidden" name="invitation_uid" class="invitation_uid">

							<textarea class="form-control" placeholder="Write your message here..." name="invitation_message"></textarea>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<?php
							if (sizeof($data['vendor_all_job']) > 0)
							{
							?>
								<label>Choose a job</label>
								<select class="form-control" name="invitation_jobs" id="invitation_jobs">	
									<?php
									foreach ($data['vendor_all_job'] as $keyVAJ => $valueVAJ)
									{
									?>
										<option value="<?php echo $valueVAJ['id']; ?>" ><?php echo $valueVAJ['name']; ?></option>
									<?php
									}
									?>
								</select>
							<?php
							}
							?>
					</div>
				</div>
					<div class="modal-footer">
						<?php
						if (sizeof($data['vendor_all_job']) > 0)
						{
						?>
							<input type="button" id="send_job_invite_btn" class="btn btn-block btn-success" onclick="send_invitation();" value="Send Invitation">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<?php
						}
						?>
						<a href="javascript:void(0);" class="text-center btn-block">Create a new job</a>
						<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
					</div>
				
			</form>
		</div>
	</div>
</div>	

@endsection
@section('addjavascript')
	<script type="text/javascript" src="{{asset('js/tourGuide.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/address.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/team.js')}}"></script>	
	<script type="text/javascript" src="{{asset('js/tourGuideProfile.js')}}"></script>
	
@endsection