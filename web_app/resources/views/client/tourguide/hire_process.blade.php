@extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
    @endsection
    @section('content')
    <?php
    $user_name = $data['user_details']->userName;
    if($data['user_details']->firstName != "")
    {
        $user_name = $data['user_details']->firstName . " " . $data['user_details']->lastName;
    }
    ?>
    <!-- here out errors -->

    <div class="container client-content hire-content">
        <div class="col-xs-12">
            @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{hire_flash_success_msg}}.
                </div>
            @endif

            @if (Session::has('hire_flash_err_msg'))
                <div class="alert alert-warning text-center">
                    <strong>Great!</strong> {{hire_flash_err_msg}}.
                </div>
            @endif
            <div class="col-xs-6 hire_user_div">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png' ?>" class="img-responsive img-circle">
                    </div>
                    
                    <div class="col-xs-10">
                        <h2>{{$user_name}}</h2>
                    </div>
                </div>
            </div>

            <div class="job-detail-wrapper">
                <div id="view_job_post_div" class="tab-pane fade in active">
                    <h3>Contract Details</h3>
                    <div class="col-xs-12">
                        <form action="{{url('/client/confirm-hire')}}/{{$data['job_details']->id}}/{{$data['user_details']->id}}" method="GET">
                            <div class="form-group">
                                <p><label>Related Job Posting</label></p>
                                <p><a href="<?php echo '/client/tourguide/'. $data['job_details']->id.'/detail'; ?>">{{$data['job_details']->title}}&nbsp;(#{{$data['job_details']->id}})</a></p>
                            </div>

                            <div class="form-group">
                                <p><label>Contract Title</label></p>
                                <p><a href="<?php echo '/client/tourguide/'. $data['job_details']->id.'/detail'; ?>">{{$data['job_details']->title}}</a></p>
                            </div>
                            

                            
                            <div class="form-group">
                                 <input type="hidden" name="tgi_id" value="{{$data['tgi_id']}}">
                                <input type="checkbox" name="hire_tnc" id="hire_tnc">&nbsp;<span>Yes, I understand and agree to the <a href="javascript:void(0);">Vendor Forest Terms of Service</a>, including the <a href="javascript:void(0);">User Agreement</a> and <a href="javascript:void(0);">Privacy Policy</a>.</span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button solid">Hire {{$user_name}}</button>
                                <a href="{{ route('client.tourguide.jobs') }}" class="btn-default button solid cancel_btn">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('addjavascript')
        <script src="{{asset('js/client.js')}}"></script>
        <script src="{{asset('js/notification.js')}}"></script>
    @endsection