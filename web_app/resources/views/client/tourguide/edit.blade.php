@extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
    @endsection
    @section('content')
    <div class="container client-content">
        @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{Session::get('hire_flash_success_msg')}}.
                </div>
            @endif

        @if (Session::has('hire_flash_err_msg'))
            <div class="alert alert-warning text-center">
                <strong>Great!</strong> {{Session::get('hire_flash_err_msg')}}.
            </div>
        @endif
        <div class="job-detail-wrapper">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#view_job_post_div">Edit Job Post</a></li>
                <!-- <li><a data-toggle="tab" href="#hire_div">Hire</a></li> -->
            </ul>

            <div class="tab-content col-xs-12">
                <div id="view_job_post_div" class="tab-pane fade in active">
                <form method="post">
                    {!! csrf_field() !!}
                    <h3>Tour Guide Job Details</h3>
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <span>Title</span>:
                            <input type="text" name="title" class="form-control" value="{{$data['job_details']->title}}">
                            <p><span>Total People</span>:&nbsp;&nbsp;&nbsp;<input type="number" name="totalpeople" class="form-control" value="{{$data['job_details']->totalpeople}}"> 
                            <h5><small>
                        </small></h5>
                        </div>
                        <div class="col-xs-6 jbdtl_div">
                            <p>Status:&nbsp;<span><?php if ($data['job_details']->status == '1') {
                             ?> 
                             <span>Open</span>
                             <?php  
                            }else{
                                ?>
                             <span>Close</span>  
                            <?php
                            }
                            ?></span></p>
                            <p>Posted at:&nbsp;<span>{{date("m/d/y", strtotime($data['job_details']->created_at))}}</span></p>
                        </div>
                        <div class="col-xs-6 job_length_div">
                            <p>From Date:&nbsp;<input type="date" name="fromdate" class="form-control" value="{{$data['job_details']->fromdatetime}}"></p>
                            <p>To Date:&nbsp;<input type="date" name="todate" class="form-control" value="{{$data['job_details']->todatetime}}"></p>
                        </div>
                    </div>
                        <div class="col-xs-12" ><hr style="margin:15px 10px 10px 10px;"></div>
                    <div class="col-xs-12" style="text-align: center;">
                    <button class="button button solid" >Update</button>
                    </div>
            </form>
                </div>
               
                <div id="hire_div" class="tab-pane fade">
                    <?php
                    if (sizeof($data['all_hires']) > 0)
                    {
                        foreach ($data['all_hires'] as $keyAH => $valueAH)
                        {
                    ?>
                            <div class="col-sm-12 freelancer-details row">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img src="/{{$valueAH->path}}" class="img-responsive img-circle">
                                    </div>
                                    <div class="col-sm-11">
                                        <p>
                                            <?php
                                            if($valueAH->firstName != "")
                                            {
                                                echo $valueAH->firstName . " " . $valueAH->lastName;
                                            }
                                            else
                                            {
                                                echo $valueAH->userName;
                                            }
                                            ?>
                                        </p>
                                        <div class="freelancer-earnings-div">
                                            
                                            <span>Current Status: <?php echo $valueAH->status ? 'Hired' : 'Closed'; ?></span>
                                            <span>Hired on: <?php echo date("d M, Y", strtotime($valueAH->created_at)); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    }
                    else
                    {
                    ?>
                        <p class="text-center">No Hires Yet</p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <!-- <div class="right-pane col-xs-2">
                
            </div> -->
        </div>
    </div>
    @endsection

    @section('addjavascript')
    <script src="{{asset('js/client.js')}}"></script>

    <script type="text/javascript">
        function close_job(ele)
        {
            if(confirm('Are you sure?'))
            {
                $(ele).attr('href', $(ele).attr('rel'));
                $(ele).click();
            }

            return false;
        }
    </script>

    @endsection