@extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
    @endsection
    @section('content')
    <div class="container client-content">
        @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{Session::get('hire_flash_success_msg')}}.
                </div>
            @endif

        @if (Session::has('hire_flash_err_msg'))
            <div class="alert alert-warning text-center">
                <strong>Great!</strong> {{Session::get('hire_flash_err_msg')}}.
            </div>
        @endif
        <div class="job-detail-wrapper">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#view_job_post_div">View Job Post</a></li>
                <li><a data-toggle="tab" href="#review_proposals_div">Review Proposals</a></li>
                <li><a data-toggle="tab" href="#hire_div">Hire</a></li>
            </ul>

            <div class="tab-content col-xs-9">
                <div id="view_job_post_div" class="tab-pane fade in active">
                    <h3>Tour Guide Job Details</h3>
                    <div class="col-xs-12">
                        <div class="col-md-9">
                            <h5><span>{{$data['job_details']->title}}</span></h5>
                            <p><span>Total People</span>:&nbsp;&nbsp;&nbsp;<small>{{$data['job_details']->totalpeople}}</small> 
                            <h5><small>
                        </small></h5>
                        </div>
                        <div class="col-xs-6 jbdtl_div">
                            <p>Status:&nbsp;<span><?php if ($data['job_details']->status == '1') {
                             ?> 
                             <span>Open</span>
                             <?php  
                            }else{
                                ?>
                             <span>Close</span>  
                            <?php
                            }
                            ?></span></p>
                            <p>Posted at:&nbsp;<span>{{date("m/d/Y", strtotime($data['job_details']->created_at))}}</span></p>
                        </div>
                        <div class="col-xs-6 job_length_div">
                            <p>From Date:&nbsp;<span>{{date("m/d/Y", strtotime($data['job_details']->fromdatetime))}}</span></p>
                            <p>To Date:&nbsp;<span>{{date("m/d/Y", strtotime($data['job_details']->todatetime))}}</span></p>
                        </div>

                            </div>
                            </div>
                <div id="review_proposals_div" class="tab-pane fade">
                    <div class="panel-body feildcont field freelancer-listing">
                        <?php
                        if (sizeof($data['job_proposals']) > 0)
                        {
                            if($data['hire']['hasexist']==null)
                            {
                            foreach ($data['job_proposals'] as $keyAF => $valueAF)
                            {
                            ?>  
                                <div class="col-sm-12 freelancer-details row">
                                    <div class="row">
                                        <div class="col-sm-1">
                                            <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png'  ?>" class="img-responsive img-circle clt_img">
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="margin_bottom1">
                                                <?php
                                                if($valueAF->firstName != "")
                                                {
                                                    echo $valueAF->firstName . " " . $valueAF->lastName;
                                                }
                                                else
                                                {
                                                    echo $valueAF->userName;
                                                }
                                                ?>
                                            </p>
                                            <p class="margin_bottom2"><?php echo substr($valueAF->business_desc, 0, 115); ?>...</p>
                                            <div class="freelancer-earnings-div">
                                                <span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerHour . " / hr"; ?></span>
                                               <span>
                                                    @if(isset($totalstars))
                                                    {{ $totalstars[$valueAF->id]['success_percent'] }}% Job Success
                                                    <div class="job-success-progress">
                                                        <div class="progress-complete" style="float:left;width: <?php echo $totalstars[$valueAF->id]['success_percent'] ?>%" data-toggle="tooltip" data-placement="top" title="{{ $totalstars[$valueAF->id]['success_percent'].'% success job' }}">
                                                        </div>
                                                        <div class="progress-pending" style="float:left;width: <?php echo 100-$totalstars[$valueAF->id]['success_percent'] ?>%">
                                                        </div>
                                                    </div>
                                                     @else
                                                    <?php echo 0 . "% Job Success";?>
                                                    <div class="job-success-progress">
                                                        <div class="progress-complete" style="float:left;width:<?php echo '0';?>%" data-toggle="tooltip" data-placement="top" title="">
                                                        </div>
                                                        <div class="progress-pending" style="float:left;width: <?php  echo '100';?>%">
                                                        </div>
                                                    </div>
                                                    @endif
                                                </span>
                                                <!-- <span>96% Job Success<div class="job-success-progress"><div class="progress-complete"></div><div class="progress-pending"></div></div></span> -->
                                                <?php
                                                if ($valueAF->address_country != '')
                                                {
                                                ?>
                                                    <span><i class="fa fa-map-marker"></i><?php echo $valueAF->address_country; ?></span>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                            
                                        <div class="col-sm-2">
                                            <a href="{{url('client/hires')}}/<?php echo $valueAF->tourguide_job_id . '/' . $valueAF->user_id . '/' . $valueAF->tgi_id; ?>" name="hire_btn" class="button solid invite-to-job pull-right">Hire</a>
                                        </div>

                                    </div>
                                </div>
                            <?php
                            }
                           }
                           else{
                            ?>
                            <div class="list-empty">
                                <h3 class="text-center">No Proposal yet.</h3>
                            </div>
                            <?php
                           } 
                        }
                        else
                        {
                            
                        ?>
                            <div class="list-empty">
                                <h3 class="text-center">No Proposal yet.</h3>
                            </div>
                        <?php
                        }
                    
                        ?>
                    </div>
                </div>
                <div id="hire_div" class="tab-pane fade">
                    <?php
                    if (sizeof($data['all_hires']) > 0)
                    {
                        foreach ($data['all_hires'] as $keyAH => $valueAH)
                        {
                    ?>
                            <div class="col-sm-12 freelancer-details row">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img src="/{{$valueAH->path}}" class="img-responsive img-circle">
                                    </div>
                                    <div class="col-sm-11">
                                        <p class="margin_bottom">
                                            <?php
                                            if($valueAH->firstName != "")
                                            {
                                                echo $valueAH->firstName . " " . $valueAH->lastName;
                                            }
                                            else
                                            {
                                                echo $valueAH->userName;
                                            }
                                            ?>
                                        </p>
                                        <div class="freelancer-earnings-div">
                                            
                                            <span>Current Status: <?php echo $valueAH->status ? 'Hired' : 'Closed'; ?></span>
                                            <span>Hired on: <?php echo date("d M, Y", strtotime($valueAH->created_at)); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    }
                    else
                    {
                    ?><div class="list-empty">
                        <h3 class="text-center">No Hires Yet</h3>
                    </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <div class="right-pane col-xs-3">
                <ul>
                    <li><a href="{{route('client.tourguide.edit',['id'=> $data['job_details']->id])}}" class="button button solid"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit Post&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a href="javascript:void(0);" onclick="client_remove_post( {{$data['job_details']->id}} );" rel="" class="button button-default"><i class="fa fa-times"></i>&nbsp;Remove Post</a></li>
                </ul>
            </div>
        </div>
    </div>
    @endsection
    @section('addjavascript')
    <script src="{{asset('js/client.js')}}"></script>
    <script src="{{asset('js/tourguide-job.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
    @endsection