@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-md-6 div-center">
					@if(Session::has('success'))
			            <div class="alert alert-success ">
			              <p style="color:green; text-align: center;">{{Session::get('success')}}</p>
			              @php Session::forget('success'); @endphp
			            </div>
			            @endif
			            @if(Session::has('passerror'))
			            <div class="alert alert-danger ">
			              <p style="text-align: center;">{{Session::get('passerror')}}</p>
			              @php Session::forget('passerror'); @endphp
			            </div>
            		@endif 
			
						<div class="vendor-job-list col-md-12">
							<div class="vfform" id="save-job">
								<div class="title">
									<h3>Change your Password</h3>
								</div>
								 
								<form role="form" method="POST" >
									{!! csrf_field() !!}
									<div class="feildcont">
										<div class="row">
											<div class="col-md-12">
												<div class="row">
													<div class="col-md-12">
														<label>Old Password <em>*</em></label>
														<input type="password" name="oldPassword" value="" />
														@if($errors->has('oldPassword'))
														<span class="text-danger">{{ $errors->first('oldPassword') }}</span>
														@endif 
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<label>New Password<em>*</em></label>
														<input type="password" name="newPassword" value="" />
														@if($errors->has('newPassword'))
														<span class="text-danger">{{ $errors->first('newPassword') }}</span>
														@endif 
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<label>Confirm New-Password<em>*</em></label>
														<div class="row">
															<div class="col-md-12 ">
																<input type="password" name="confirmNewPassword" id="confirmNewPassword" value="">
															</div>
														</div>
														@if($errors->has('confirmNewPassword'))
														<span class="text-danger">{{ $errors->first('confirmNewPassword') }}</span>
														@endif     
													</div>
												</div>
												
											</div>
										</div>
										<div class="text-center">
											<button class="button solid reg-btn">Save</button>
										</div>
									</div>
								</form>
								
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('addjavascript')	
	<script type="text/javascript" src="{{asset('js/tourGuide.js')}}"></script>
@endsection