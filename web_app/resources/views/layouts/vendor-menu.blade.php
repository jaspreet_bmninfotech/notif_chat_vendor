 
	<ul>
		<li>
        <form class="tab-search-form">
	          <input type="search" placeholder="What do you need" class="tab-search"/>
	          <button class="tab-search-btn field-srh">Find</button>
        </form>
      </li>
		<li>
			<a href="" class="icon fa-angle-down" ><i class="icon" data-icon="&#xe175;"></i>Jobs</a>
			<ul class="">
				<li><a href="{{route('vendor.dashboard')}}">Search Jobs</a></li>
				<li><a href="{{route('vendor.bids')}}">View Proposals</a></li>
				<li><a href="{{route('vendor.contract')}}">All Contracts</a></li>
				<li><a href="{{route('vendor.search.tourguide')}}">Job To Tour Guide</a></li>
				<li><a href="{{route('searchtourguide')}}">Search Tour Guide</a></li>
				<li><a href="{{route('vendor.tourguide.jobs')}}">Tour Guide Jobs</a></li>
				<li><a href="{{route('vendor.invites.list')}}">Invite Jobs</a></li>
				<li class="dropdown"><a href="{{route('vendor.hire.list')}}">Hire Proposal</a></li>
				<li class="dropdown"><a href="{{route('vendor.team.add')}}">New Team</a></li>
				<li class="dropdown"><a href="{{route('vendor.team.teaminvite')}}">Team List</a></li>
			</ul>
		</li>
		<li><a href="{{ route('messages') }}"><i class="icon" data-icon="&#xe175;"></i>Messages</a></li>
		<!-- <li>
			<a href="{{route('vendor.profile')}}">
				<i class="icon" data-icon="&#xe175;"></i>
			Profile
			</a>
		</li> -->
		<!-- <notification :userid="{{Auth::user()->id}}" :unreads="{{auth()->user()->unreadNotifications}}"></notification> -->
		 <li class="dropdown notif-menu" >
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-bell-o" style="font-size:20px"></i>
            <span class="badge text-sm">{{count(auth()->user()->unreadNotifications)}}</span>.
        </a>
        <ul class="submenu  notification" style="width:20%; ">
             @if(count(auth()->user()->unreadNotifications) > 0)
            <li class="dropdown ">
                <div class="notif-head">
                <h3 class=""  style="display:inline! important; ">Notification</h3>
               <a href="{{route('markall')}}" class="notif-pull-right" style="display:inline! important;float:right;" id="markall">Mark All As Read</a>
            </div>
            </li>	<?php
            			$i=0;
            		?>
                    @foreach(auth()->user()->unreadNotifications as $notification)
                    	<?php if($i==5) break;
                    	?>
                        @include('notifications.'.snake_case(class_basename($notification->type)))
                        <?php
                        	$i++;
                        ?>
                    @endforeach 
                @else
                    <li class="dropdown" id="no-notif">
                        <div class="row">
                             <div class="text-center">
                                No Notification
                            </div>
                            <div class="clearfix"></div> 
                        </div> 
                    </li>
                @endif
                 @if(count(auth()->user()->unreadNotifications) >2)
                <li class=" notif-width">
                    <a href="{{ route('vendor.all.notification') }}" class="seeAll">See All</a>
                </li>
                @endif
        </ul>  
    </li>
	</ul>
	<!-- <ul class="link-pull-left nav">
	</ul> -->
	<ul class="">
	    <li class="dropdown">
	       	        <a href="" class="dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	            <i class="icon fa-angle-down"></i>
	            <i class="fa fa-user user-fa-icon"></i>
	            {{ Auth::user()->username}}
	            <?php
	            $user_id =Auth::user()->id;
	            ?>
	        </a>
	        <ul class="submenu-margin ">
	            <li class="dropdown"><a href="{{route('vendor.information',$user_id)}}">View Profile</a></li>
	            <li class="dropdown"><a href="{{route('vendor.profile')}}">Settings</a></li>
	             @if(Auth::User())
	            <li class="dropdown">
	              <a href="{{URL::to('/logout')}}">
	                <i class="icon" data-icon="&#xe0a0;"></i>Logout</a>
	            </li>  
	            @endif 
	        </ul>
	    </li>
	</ul>
	