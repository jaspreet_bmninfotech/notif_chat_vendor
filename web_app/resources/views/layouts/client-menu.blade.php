<ul >
    <li >
        <a href="{{ route('client.job') }}">
            <i class="icon" data-icon="&#xe175;"></i>
            Jobs
        </a>
        <ul >
            <li ><a href="{{ route('client.job') }}">My Jobs</a></li>
            <li ><a href="{{ route('client.job.all') }}">All Job Postings</a></li>
            <li ><a href="{{ route('client.contract') }}">All Contracts</a></li>
            <li ><a href="{{ route('client.job.add') }}">Post a Job</a></li>
            <li ><a href="{{route('client.search.tourguide')}}">Trip To Tour Guide</a></li>
            <li><a href="{{route('searchtourguide')}}">Search Tour Guide</a></li>
            <li ><a href="{{route('client.tourguide.jobs')}}">Tour Guide Jobs</a></li>
            <li ><a href="{{URL::to('/client/team')}}">Team Search</a></li>
            <li ><a href="{{URL::to('/dispute-apply')}}">Apply Dispute</a></li>
        </ul>
    </li>
    <li >
        <a href="{{ route('client.freelancer') }}">
            <i class="icon" data-icon="&#xe175;"></i>
            Vendors
        </a>
        <ul >
            <li ><a href="{{ route('client.freelancer.my', ['client_id' => Auth::User()->id ]) }}">My Vendors</a></li>
            <li ><a href="{{ route('client.freelancer') }}">Find Vendors</a></li>
        </ul>
    </li>
    <li><a href="{{ route('messages') }}"><i class="icon" data-icon="&#xe175;"></i>Messages</a></li>
    <!-- <notification :userid="{{Auth::user()->id}}" :unreads="{{auth()->user()->unreadNotifications}}"></notification> -->
    <li class="dropdown" >
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-bell-o" style="font-size:20px"></i>
            <span class="badge text-sm">{{count(auth()->user()->unreadNotifications)}}</span>.
        </a>
        <ul class="submenu  notification" style="width:20%; ">
             @if(count(auth()->user()->unreadNotifications) > 0)
            <li class="dropdown notif-width">
                <div class="notif-head">
                <h3 class=""  style="display:inline! important; ">Notification</h3>
                <a href="{{route('markall')}}" class="notif-pull-right" style="display:inline! important;float:right;" id="markall">Mark All As Read</a>
            </div>
            </li>
                    @foreach(auth()->user()->unreadNotifications as $notification)
                        @include('notifications.'.snake_case(class_basename($notification->type)))
                    @endforeach 
                @else
                    <li class="dropdown" id="no-notif">
                        <div class="row">
                             <div class="text-center">
                                No Notification
                            </div>
                            <div class="clearfix"></div> 
                        </div> 
                    </li>
                @endif
                 @if(count(auth()->user()->unreadNotifications) >2)
                <li class=" notif-width">
                    <a href="{{ route('client.all.notification') }}" class="seeAll">See All</a>
                </li>
                @endif
        </ul>
        
    </li>
    
<!-- </ul>

<ul class="link-pull-right nav"> -->
    <li class="dropdown">
       
        <a href="" class="dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-chevron-down"></i>
            {{ Auth::user()->username}}
        </a>
       
        <ul class="submenu-margin ">
            <li ><a href="{{route('client.profile')}}">Profile</a></li>
           
             @if(Auth::User())
            <li >
              <a href="{{URL::to('/logout')}}">
                <i class="icon" data-icon="&#xe0a0;"></i>Logout</a>
            </li>  
            @endif 
        </ul>
    </li>
</ul>