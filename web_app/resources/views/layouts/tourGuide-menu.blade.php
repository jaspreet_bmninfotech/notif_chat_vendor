    <ul>
        <li>
        <a href="{{ route('tourguide.job') }}">
            <i class="icon" data-icon="&#xe175;"></i>Jobs</a>
            </li>
            
            {{-- <li><a href="{{route('tourguide.job')}}">My Jobs</a></li> --}}
    </li>
        <li><a href="{{ route('messages') }}"><i class="icon" data-icon="&#xe175;"></i>Messages</a></li>
        <!-- <li>
            <a href="{{route('vendor.profile')}}">
                <i class="icon" data-icon="&#xe175;"></i>
            Profile
            </a>
        </li> -->
       <li class="dropdown" >
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-bell-o" style="font-size:20px"></i>
            <span class="badge text-sm">{{count(auth()->user()->unreadNotifications)}}</span>.
        </a>
        <ul class="submenu dropdown-menu notification" style="width:300px; ">
             @if(count(auth()->user()->unreadNotifications) > 0)
            <li class="dropdown notif-width">
                <div class="notif-head">
                <h3 class=""  style="display:inline! important; ">Notification</h3>
               <a href="{{route('markall')}}" class="notif-pull-right" style="display:inline! important;float:right;" id="markall">Mark All As Read</a>
            </div>
            </li>
                    @foreach(auth()->user()->unreadNotifications as $notification)
                        @include('notifications.'.snake_case(class_basename($notification->type)))
                    @endforeach 
                @else
                    <li class="dropdown" id="no-notif">
                        <div class="row">
                             <div class="text-center">
                                No Notification
                            </div>
                            <div class="clearfix"></div> 
                        </div> 
                    </li>
                @endif
                 @if(count(auth()->user()->unreadNotifications) >2)
                <li class=" notif-width">
                    <a href="{{ route('tourguide.all.notification') }}" class="seeAll">See All</a>
                </li>
                @endif
        </ul>
        
    </li>
</ul>
<!-- <ul class="link-pull-left nav">
</ul> -->
<ul class="">
    <li class="dropdown">
        <a href="" class="dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-chevron-down"></i>
            {{ Auth::user()->username}}
             <?php
                $user_id =Auth::user()->id;
                ?>
            </a>
         <ul class="submenu-margin ">
            <li class="dropdown"><a href="{{route('tourguide.information',$user_id)}}">View Profile</a></li>
            <li class="dropdown"><a href="{{route('tourguide.profile')}}">Profile</a></li>   
             @if(Auth::User())
                <li class="">
              <a href="{{URL::to('/logout')}}">
                <i class="icon" data-icon="&#xe0a0;"></i>Logout</a>
            </li>  
            @endif 
        </ul>
    </li>
</ul>
    