<!DOCTYPE html>
<html>
  <head>
    <title>VendorForest</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <meta name="userid" content="{{Auth::check() ? Auth::user()->id : ''}}"/>
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="{{asset('css/main.css')}}" /> -->
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  

  <link rel="https://vendorforest-vendorforest.c9users.io/public/css/shortcut icon" type="image/png" href="" />
  <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic'
    rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
    rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>

  <!-- ######### CSS STYLES ######### -->

  <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/reset.css')}}" type="text/css" />

  <!-- font awesome icons -->
  <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">

  <!-- simple line icons -->
  <link rel="stylesheet" type="text/css" href="{{asset('css/simpleline-icons/simple-line-icons.css')}}" media="screen" />

  <!-- et linefont icons -->
  <link rel="stylesheet" href="{{asset('css/et-linefont/etlinefont.css')}}">

  <!-- animations -->
  <link href="{{asset('js/animations/css/animations.min.css')}}" rel="stylesheet" type="text/css" media="all" />

  <!-- responsive devices styles -->
  <link rel="stylesheet" media="screen" href="{{asset('css/responsive-leyouts.css')}}" type="text/css" />

  <!-- shortcodes -->
  <link rel="stylesheet" media="screen" href="{{asset('css/shortcodes.css')}}" type="text/css" />

  <!-- mega menu -->
  <link href="{{asset('js/mainmenu/menu.css')}}" rel="stylesheet">

  <!-- MasterSlider -->
  <link rel="stylesheet" href="{{asset('js/masterslider/style/masterslider.css')}}" />
  <link rel="stylesheet" href="{{asset('js/masterslider/skins/default/style.css')}}" />

  <!-- cubeportfolio -->
  <link rel="stylesheet" type="text/css" href="{{asset('js/cubeportfolio/cubeportfolio.min.css')}}">

  <!-- owl carousel -->
  <link href="{{asset('js/carouselowl/owl.transitions.css')}}" rel="stylesheet">
  <link href="{{asset('js/carouselowl/owl.carousel.css')}}" rel="stylesheet">
  <link href="{{asset('js/progressbar/ui.progress-bar.css')}}" rel="stylesheet">

  <!-- tabs 2 -->
   <link rel="stylesheet" href="{{asset('css/main.css') }}" />

  <link href="{{asset('js/tabs2/tabacc.css')}}" rel="stylesheet" />
  <link href="{{asset('js/tabs2/detached.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('css/styles.css')}}">
  <link href="{{asset('css/index.css')}}" rel="stylesheet" />
  <link href="{{asset('css/howitworks.css')}}" rel="stylesheet" />
  <link href="{{asset('css/responsive.css')}}" rel="stylesheet" />
  <link href="{{asset('/plugin/jQuery-File-Upload-9.19.1/css/jquery.fileupload.css')}}" rel="stylesheet" />
  <link href="{{asset('/plugin/jQuery-File-Upload-9.19.1/css/jquery.fileupload-ui.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{asset('css/dashboard.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{asset('css/job-post-form.css')}}" type="text/css" />
  <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
  @section('addstyle')
  @show

  <script type="text/javascript">
    var BASE_URL = "{{url('/')}}";
  </script>
 
</head>
  <body class="landing">
    <div id="page-wrapper">

    
    @section ('header')
        @include ('layouts/vf-header')
        @show
    

    <script type="text/javascript" src="{{asset('js/universal/jquery.js')}}"></script>
    <div class="vf-container">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
      @yield('content')
    </div>

    @include ("layouts/vf-footer")
    </div>
      <script src="{{asset('js/jquery.min.js')}}"></script>
      <script src="{{asset('js/jquery.dropotron.min.js')}}"></script>
      <script src="{{asset('js/jquery.scrollgress.min.js')}}"></script>
      <script src="{{asset('js/skel.min.js') }}"></script>
      <script src="{{asset('js/util.js')}}"></script>
      <script src="{{asset('js/main.js')}}"></script>
      

    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
    <script type="text/javascript" src="{{asset('plugin/jQuery-File-Upload-9.19.1/js/vendor/jquery.ui.widget.js')}}"></script>
    <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <script type="text/javascript" src="{{asset('plugin/jQuery-File-Upload-9.19.1/js/jquery.fileupload.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugin/jQuery-File-Upload-9.19.1/js/jquery.iframe-transport.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugin/jQuery-File-Upload-9.19.1/js/jquery.fileupload-process.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugin/jQuery-File-Upload-9.19.1/js/jquery.fileupload-image.js')}}"></script>

    
    

    
    <script type="text/javascript" src="{{asset('plugin/typeahead/bootstrap-typeahead.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugin/typeahead/bootstrap-typeahead.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugin/jQuery-File-Upload-9.19.1/js/main.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('plugin/underscore/underscore.js')}}"></script>
    
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <!-- The basic File Upload plugin -->
    
    <script src="{{ asset('js/footer.js') }}"></script>
    <script>
  (function($) {
    $("#header__icon").click(function(e){
      e.preventDefault();
      $("body").toggleClass("width__sidebar");
    });
    $("#site__cash").click(function(e){
      $("body").removeClass("width__sidebar");
    })
  })(jQuery);
</script>
 
<script type="text/javascript" src="{{asset('js/progressbar/progress.js')}}"></script>  
<script src="{{asset('js/animations/js/animations.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/mainmenu/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/mainmenu/customeUI.js')}}"></script> 
<script src="{{asset('js/masterslider/masterslider.min.js')}}"></script>
<script type="text/javascript">
(function($) {
 "use strict";
  var slider = new MasterSlider();
  // adds Arrows navigation control to the slider.
  slider.control('arrows');
  slider.control('bullets');
  
  slider.setup('masterslider' , {
     width:1400,    // slider standard width
     height:680,   // slider standard height
     space:0,
     speed:45,
     layout:'fullwidth',
     loop:true,
     preload:0,
     autoplay:true,
     view:"parallaxMask"
  });
})(jQuery);
</script>
      <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script type="text/javascript" src="{{asset('js/scrolltotop/totop.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mainmenu/sticky.js') }}"></script>
<script type="text/javascript" src="{{asset('js/mainmenu/modernizr.custom.75180.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cubeportfolio/jquery.cubeportfolio.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cubeportfolio/main.js') }}"></script>
<script src="{{asset('js/tabs2/index.js') }}"></script>
<script src="{{asset('js/freelancers.js')}}"></script>
<script src="{{asset('js/notif.js')}}"></script>
<script>
$('.accordion, .tabs').TabsAccordion({
  hashWatch: true,
  pauseMedia: true,
  responsiveSwitch: 'tablist',
  saveState: sessionStorage,
});
</script>
<script src="{{asset('js/aninum/jquery.animateNumber.min.js')}} "></script>
<script src="{{asset('js/carouselowl/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{asset('js/universal/custom.js') }}"></script>
<script type="text/javascript" src="{{asset('js/notification.js')}} "></script>
 
    @section('addjavascript')
    @show
</body>
</html>