@extends ('layouts/base')
  @section('content')
    
    <div class="container">
      <section class="vf-login">
        <div class="vf-standar-div">
          <div class="loginbox">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <p style="color:green;">{{Session::get('success')}}</p>
              @php Session::forget('success'); @endphp
            </div>
            @endif @if ($errors->any())
            <div class="alert alert-danger user-errors">
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            <h2>Forget your password</h2>
            <form class="form-input" autocomplete="off" method="POST">
              {{ csrf_field() }}
              <table class="form-table">
                <tr>
                  <td>
                    <input type="email" class="input-field" name="email" id="email" placeholder="Enter your email" autocomplete="off">
                    <input type="hidden" name="token" value="{{$token}}">
                  </td>
                  <td>
                    <input type="password" name="password">
                  </td>
                  <td>
                    <input type="confirmPassword" name="confirmPassword">
                  </td>
                </tr>
                  <td  class="text-center">
                    <button class="login-btn button solid" type="submit">Submit</button>
                    <br><br>
                  </td>
              </table>
            </form>
        </div>
      </section>
    </div>
  @endsection