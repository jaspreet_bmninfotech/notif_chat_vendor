@extends('layouts/base')
  @section('content')
    @if(Session::has('success'))
    <div class="alert alert-success">
      <p style="color:green;">{{Session::get('success')}}</p>
      @php Session::forget('success'); @endphp
    </div>
    @endif @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="container">
      <section class="vf-login">
        <div class="vf-standar-div">
          <div class="loginbox">
            <h2>SIGN UP</h2>
            <form class="form-input" autocomplete="off" method="POST" onsubmit="return ValidationEvent();" action="{{ route('register')}}">
              {{ csrf_field() }}
              <table class="form-table register-form">
                <tr>
                  <td>
                    <input type="text" class="input-field clearfix" id="username" name="username" placeholder="Enter UserName" autocomplete="off">
                     <span id="checkuser" class="text-danger"></span>
                  </td>
                </tr>
                <tr>
                  <p>
                    @if($errors->has('username'))
                    <span class="text-danger">{{ $errors->first('username') }}</span>
                    @endif
                  </p>
                </tr>
                <tr>
                  <td>
                    <input type="email" class="input-field clearfix" id="email" name="email" placeholder="Email..." autocomplete="off">
                    <span id="checkemail" class="text-danger"></span>
                  </td>
                </tr>
                <tr>
                  <p>
                    @if($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                  </p>
                 </tr>
                <tr>
                  <td>
                    <input type="password" class="input-field clearfix" id="password" name="password" placeholder="Password..." autocomplete="off">
                    <span id="checkpass" class="text-danger"></span>
                  </td>
                </tr>
                <tr>
                  <p>
                    @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                  </p>
                </tr>
                <tr>
                  <td>
                    <input type="password" class="input-field clearfix" id="cpassword" name="confirmpassword" placeholder="Comfirm Password..."
                      autocomplete="off">
                      <span id="checkcpass" class="text-danger"></span>
                  </td>
                </tr>
                <tr>
                  <p>
                    @if ($errors->has('confirmpassword'))
                    <span class="text-danger">{{ $errors->first('confirmpassword') }}</span>
                    @endif
                  </p>
                </tr>
                <tr>
                  <td>
                    <h4>I work as</h4>
                  </td>
                </tr>
                <tr>
                  <td>
                    <select name="accounttype" class="input-field">
                      <option value="vn">Vendor</option>
                      <option value="cn">Client</option>
                      <option value="tg">Tour Guide</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <br>
                    <button class=" button solid reg-btn" type="submit" onsubmit="RegisterForm">Register</button>
                  </td>
                </tr>
              </table>
            </form>
            <div class="text-center signupbox">
              <div class="line-heading txt-gray">Already a member ?</div>
              <a class="button txt-gray default width-sm hidden-xs" href="/auth/login">
                  Login
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
    
  @endsection
  @section('addjavascript')
    <script src="{{asset('js/register.js')}}"></script>
    @endsection