@extends ('layouts/base')
  @section('content')
    
    <div class="container">
      <section class="vf-login">
        <div class="vf-standar-div">
          <div class="loginbox">
            @if('status')
            <div class="alert alert-success">
              <p style="color:green;">{{'status'}}</p>
             
            </div>
            @endif @if ($errors->any())
            <div class="alert alert-danger user-errors">
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            <h2>Forget your password</h2>
            <form class="form-input" autocomplete="off" method="POST">
              {{ csrf_field() }}
              <table class="form-table">
                <tr>
                  <td>
                    <input type="email" class="input-field" name="email" id="email" placeholder="Enter your email" autocomplete="off">
                  </td>
                </tr>
                  <td  class="text-center">
                    <button class="login-btn button solid" type="submit">Submit</button>
                    <br><br>
                  </td>
              </table>
            </form>
        </div>
      </section>
    </div>
  @endsection