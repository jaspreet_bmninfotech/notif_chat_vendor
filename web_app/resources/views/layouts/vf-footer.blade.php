<!-- <footer id="footer">
<ul class="icons">
	<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
	<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
	<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
	<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
	<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
	<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
</ul>
<ul class="copyright">
	<li>&copy; 2018. All rights reserved.</li>
</ul>
</footer> -->
		</div>

		<div class="full-footer">
			<div id="ft-ft-wrap">
				<div class="container">
					<div class="footer-grid">
						<div class="tp-col tp-col--12 tp-col--lg-2">
							<div class="full-footer__item__title">
								<div class="full-footer__item__text">Company</div>
							</div>
							<div class="full-footer__item__links">
								<ul>
									<li><a href="/about" class="full-footer__item__link">About</a></li>
									<li><a href="/careers" class="full-footer__item__link">Careers</a></li>
									<li><a href="/press" class="full-footer__item__link">Press</a></li>
									<li><a href="/blog" class="full-footer__item__link">Blog</a></li>
								</ul>
							</div>
						</div>
						<div class="tp-col tp-col--12 tp-col--lg-2">
							<div class="full-footer__item__title">
								<div class="full-footer__item__text">Customers</div>
							</div>
							<div class="full-footer__item__links">
								<ul>
									<li><a href="/how-it-works" class="full-footer__item__link">How it works</a></li>
									<li><a href="/safety" class="full-footer__item__link">Safety</a></li>
									<li><a href="https://thumbtack.app.link/lA0xBQCnNG?redirect_url=https%3A%2F%2Fwww.thumbtack.com" class="full-footer__item__link">iPhone app</a></li>
									<li><a href="https://thumbtack.app.link/ZQup5oJnNG?redirect_url=https%3A%2F%2Fwww.thumbtack.com" class="full-footer__item__link">Android app</a></li>
									<li><a href="/near-me" class="full-footer__item__link">Services Near Me</a></li>
									<li><a href="/prices" class="full-footer__item__link">Cost Estimates</a></li>
									<li><a href="/how-to" class="full-footer__item__link">How To Pages</a></li>
									<li><a href="/guides" class="full-footer__item__link">Thumbtack Guides</a></li>

								</ul>
							</div>
						</div>					
					</div>
				</div>
			</div>
			<div class="ft-legal">

			</div>
		</div>

		<!-- Scripts -->


	</body>
</html>

		

		
			