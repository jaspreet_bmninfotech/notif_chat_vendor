<li class="dropdown notiftext">
   <a href="javascript:;" data-href="/client/contract/{{base64_encode($notification->data['content']['id'])}}/detail" data-notif-id="{{$notification->id}}" data-operation="{{ $notification->data['notif'] }}">
    <div class="row">
    	<div class="col-md-12 col_black notifybarssetting">
         <span>{{ ucwords($notification->data['senduser'])}}</span> <span>{{ucwords($notification->data['notif']) }} 
         	{{$notification->data['content']['job']['name']}}</b></span>
         	<span><strong>{{date('F j,Y',strtotime($notification->created_at))}}</strong></span> 
     	</div>
     	<div class="clearfix"></div>
    </div>
</a>
</li>
