@extends('../layouts/base')
  	@section('content')

  	
	<div class="job-posting-form container">
		<div class="row">
			<div class="vendor-job-list form-fields ">
				<div class="vfform panel-group" id="">
					<div class="heading">
						<h3>Notification</h3>
					</div>
					<div class="panel-body">
						@if($data==NULL)
							<div>
								<div class="col-md-12">
									<div class="row">
										
											<div class="top-bot-margin1 text-center ">
												Currently you have no Notification
											</div>
										
									</div>
								</div>
							</div>
						@else
						@foreach ($data as $notif)
							<div>
								<div class="col-md-12">
									<div class="row">
										<div class="vf-bids">
											<div class="col-md-6 ">
												<div class="bid-detail">
													@if($notif['type'] == "App\Notifications\VendorInviteForJob")
													<a href="javascript:;" data-href = "/vendor/invites" data-notif-id="{{$notif['id']}}" data-operation="{{ $notif['message'] }}"><span>{{$notif['name']}}</span>&nbsp;<span>{{$notif['message']}} {{$notif['job']}}</span> </a>
													@elseif($notif['type'] == "App\Notifications\ClientBidNotify")
													<a href="javascript:;" data-href = "/client/job/details/{{$notif['jobId']}}" data-notif-id="{{$notif['id']}}" data-operation="{{ $notif['message'] }}"><span>{{$notif['name']}}</span>&nbsp;<span>{{$notif['message']}} {{$notif['job']}}</span></a>
													@elseif($notif['type'] == "App\Notifications\DeclineClientJob")
													<a href="javascript:;" data-href = "/client/job/details/{{$notif['jobId']}}" data-notif-id="{{$notif['id']}}" data-operation="{{ $notif['message'] }}"><span>{{$notif['name']}}</span>&nbsp;<span>{{$notif['message']}} {{$notif['job']}}</span></a>
													@elseif($notif['type'] == "App\Notifications\DeclineInviteTeam")
													<a href="javascript:;" data-href = "/vendor/team/view/team/{{$notif['teamId']}}" data-notif-id="{{$notif['id']}}" data-operation="{{ $notif['message'] }}"><span>{{$notif['name']}}</span>&nbsp;<span>{{$notif['message']}} {{$notif['job']}}</span></a>
													@elseif($notif['type'] == "App\Notifications\EndClientContracts")
													<a href="javascript:;" data-href = "/client/contract/{{base64_encode($notif['contentId'])}}/detail" data-notif-id="{{$notif['id']}}" data-operation="{{ $notif['message'] }}"><span>{{$notif['name']}}</span>&nbsp;<span>{{$notif['message']}} {{$notif['job']}}</span></a>
													@endif
												</div>
											</div>
											<div class="col-md-3 ">
												<div class="bid-detail">
													
												</div>
											

												<div class="bid-detail">
												</div>
												
											</div>
											<div class="col-md-3 ">
												<div class="bid-detail showdate">
												{{date('F j, Y g:i a',strtotime($notif['date']))}} 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>	
  @endsection