<li class="dropdown notiftext">
   <a href="javascript:;" data-href="/vendor/team/invite" data-notif-id="{{$notification->id}}" data-operation="{{ $notification->data['notif'] }}">
    <div class="row">
    	<div class="col-md-12 col_black notifybarssetting">
         <span>{{ucwords($notification->data['notif']) }} {{ ucwords($notification->data['senduser'])}} 
         	</span>
         	<span><strong>{{date('F j,Y',strtotime($notification->created_at))}}</strong></span> 
     	</div>
     	<div class="clearfix"></div>
    </div>
</a>
</li>
