@extends('layouts/base')
@section('content')
<div class="job-posting-form container">
	@if(Session::has('bussinessRedirected'))
	 <div class="alert alert-danger">
	      <p style="">{{Session::get('bussinessRedirected')}}</p>
	    </div>
	@endif
	@if(Session::has('success'))
	    <div class="alert alert-success">
	      <p style="color:green;">{{Session::get('success')}}</p>
	      @php Session::forget('success'); @endphp
	    </div>
	@endif
	<div class="col-md-3">
		@include('vendor/info/sidenav')
	</div>
	
    <div class="form-fields col-md-9">
	  	<form  method="POST" autocomplete="off" action ="{{route('vendor.business.add')}}"  enctype="multipart/form-data">
			<div class="vfform">
				<div class="title">
					<h3>Business Information</h3>
				</div>
				<div class="feildcont">
						{!! csrf_field() !!}
						<div class="row">
							<div class="col-md-6 dropdown">
								<label>Business Category <em>*</em></label>
								<select name="businesscategory" id="category" onchange="getSubcategories(this)">
									<option value="">- Select -</option>
									@foreach($category as $key=>$val)
										<option value="{{$val->id}}_{{$val->shortCode}}" data-id="{{$val->id}}">{{$val->name}}</option>
									@endforeach
								</select>
								<div class="clearfix"></div>
								@if($errors->has('businesscategory'))
									<span class="text-danger">{{ $errors->first('businesscategory') }}</span>
								@endif
							</div>
							<div class="col-md-6 last">
								<label>Can you travel to work to on event? <em>*</em></label>
								<select name="cantravel">
									<option>- Select -</option>
									<option value="yes">YES</option>
									<option value="no">NO</option>
								</select>
								<div class="clearfix"></div>
								@if($errors->has('cantravel'))
									<span class="text-danger">{{ $errors->first('cantravel') }}</span>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Business Name<em>*</em></label>
								<input type="text" name="businessname" id="name">
								@if($errors->has('businessname'))
									<span class="text-danger">{{ $errors->first('businessname') }}</span>
								@endif
							</div>
						</div>
				</div>
			</div>
			<!-- Business Profile end-->
			<!-- Measurment start -->
			<div class="vfform bussiness model" id="model" ">
				<div class="title">
					<h3>Measurment</h3>
				</div>
				<div class="feildcont">
						<div class="row">
							<div class="col-md-3">
								<label>Unit Measurement</label>	
							</div>
							<div class="col-md-6 radiobut">
								<input class="one" type="radio" name="unitMesurement" value="imperial" checked="">
								<span class="onelb">Imperial</span>
								<input class="two" type="radio" name="unitMesurement" value="metric">
								<span class="onelb">Metric</span>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Height <em>*</em></label>
								<select name="height">
									<option value="0">- Select -</option>
									<option value="usa">USA</option>
								</select>
							</div>
							<div class="col-md-6 last">
								<label>Hair Color <em>*</em></label>
								<select name="haircolor">
									<option value="0">- Select -</option>
									<option value="usa">USA</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Eye Color <em>*</em></label>
								<select name="eyecolor">
									<option value="0">- Select -</option>
									<option value="usa">USA</option>
								</select>
							</div>
							<div class="col-md-6 last">
								<label>Weight<em>*</em></label>
								<select name="weight">
									<option value="0">- Select -</option>
									<option value="usa">USA</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Bust <em>*</em></label>
								<select name="bust">
									<option value="0">- Select -</option>
									<option value="usa">USA</option>
								</select>
							</div>
							<div class="col-md-6 last">
								<label>Hips <em>*</em></label>
								<select name="hips">
									<option value="0">- Select -</option>
									<option value="usa">USA</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Shoe Size <em>*</em></label>
								<select name="shoesize">
									<option value="0">- Select -</option>
									<option value="usa">USA</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 radiobut">
								<label>Are you still in school?&nbsp;&nbsp;</label>
								<input class="one" type="radio" name="inschool" value="yes" checked="">
								<span class="onelb">Yes</span>
								<input class="two" type="radio" name="inschool" value="no">
								<span class="onelb">No</span>
							</div>
							<div class="col-md-6">
								<label>Grade</label>
								<input class="two" type="text" name="grade">
							</div>
						</div>
						<div class=" row">
							<div class="col-md-3">
								<h3>Type Of Model</h3>
							</div>
						</div>
						<div class="row measurement">
							<div class="col-md-12">
								
								<div class="col-md-4">
									<ul>
										<li><input type="checkbox" class="push-left" name="modelingtype[]" value ="femailmodeling"><label for="">Femail Modeling</label></li>
										<li><input type="checkbox" class="push-left" name="modelingtype[]" value ="malemodeling"><label for="">Male Modeling</label></li>
										<li><input type="checkbox" class="push-left" name="modelingtype[]" value ="babymodeling"><label for="">Baby Modeling</label></li>	
									</ul>
								</div>
								<div class="col-md-4">
									<ul>
										<li><input type="checkbox" class="push-left" name="modelingtype[]" value ="teenmodeling"><label for="">Teen Modeling</label></li>
										<li><input type="checkbox" class="push-left" name="modelingtype[]" value ="starpetmodeling"><label for="">Star Pet Modeling</label></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="row" id="modelsImages">
							
						</div>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12">
								<div class="progress-extended">&nbsp;</div>
							        <div class="imagePresentation">
										<div class="col-md-6">
										    <span class="button solid fileinput-button">
										        <i class="glyphicon glyphicon-plus"></i>
										        <span>Upload Your Images...</span>
										        <input type="file" name="files[]" id="file" multiple>
										    </span><br/>
										        @if (session('status'))
												    <div class="alert alert-danger">
												        {{ session('status') }}
												    </div>
												@endif
										    <p>Maximum five files are allowed and each must not exceed 2mb</p>
										    <br>
										    <br>
										    <!-- The global progress bar -->
										    <!-- The container for the uploaded files -->
										    
										    <br>
										</div>
							        	<div class="col-md-6">
							        	<div id="viewimg" class="viewimg">
										</div>
										<div class="profileimage-inline">
											<?php if ($businessattach == NULL) {
												?>
												<p></p>
												<?php
											} else {
												foreach ($businessattach as $businessimg => $value) {
												?>
											<div class="profileimagesf">
											<i class="fa fa-times" onclick="removeBusinessimg({{$value->attachment_id}},{{$value->business_id}},{{$value->path}})"></i><img src="/{{$value->path}}" width="100px">
											</div>
											<?php
											}
											}
											?>
										</div>
							        	</div>
						<div class="clearfix"></div>
							        </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 ">
								<label>If you have super power what would it be?</label>
								<textarea class="superpower" name="superpower"></textarea>
							</div>
							<div class="col-md-6 ">
								<label>What famous person do you look like?</label>
								<textarea class="famousperson" name="famousperson"></textarea>
							</div>
							<div class="col-md-12">
								<label>What other talent do you have?</label>
								<textarea class="talent" name="talent"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="margin_bottom2"></div>
						<div class="row">
							<div class="col-md-12 measurement">
								<ul>
									<li><input type="checkbox" class="push-left" name="check">
										<label>I have read and accept the (Application Process Rules)and the (Privacy Policy)</label>
									</li>
								</ul>
							</div>
						</div>				
				</div>
			</div>
			<!-- Measurment end -->		
			<!--Venue Info Start-->
			<div class="vfform bussiness venues" id="venues" >
				<div class="title">
				<h3>Venue Info</h3>
				</div>
				<div class="feildcont">
						<div class="row">
							<div class="col-md-12">
								<h3>Venue Location <em>*</em></h3>
								<input type="text" name="streetaddress" placeholder="Street Address">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 last">
								<select name="country" id="country" onchange="getstate(this)">
									<option value="">Country</option>
									@foreach($country as $key=>$val)
									<option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-6">
								<select name="state" id="state" onchange="getcity(this)">
									<option value="">State</option>
									
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<select name="city" id="city">
									<option value="">City</option>
									<!-- <option value="1">Moga</option> -->
								</select>
							</div>
							<div class="col-md-6 last">
								<input type="text" name="postalCode" placeholder="postalCode">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Maximum Capacity <em>*</em></label>
								<input type="number" name="maximumCapacity" >
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="margin_bottom2"></div>
						<div class="row">
							<div class="col-md-12 venue-info">
								<h3 class="">Venue Type</h3>
								
								<div class="col-md-4">
									<ul class="left_list">
										<li><input type="checkbox" value="garden" name="venueType[]" class="push-left"><label for="" >Garden</label></li>
										<li><input type="checkbox" value="bathroom" name="venueType[]" class="push-left"><label for="">Bathroom</label></li>
										<li><input type="checkbox" value="born" name="venueType[]" class="push-left"><label for="">Born</label></li>
										<li><input type="checkbox" value="form" name="venueType[]" class="push-left"><label for="">Form/Ranch</label></li>
										<li><input type="checkbox" value="historic" name="venueType[]" class="push-left"><label for="">Historic</label></li>
										<li><input type="checkbox" value="banquet" name="venueType[]" class="push-left"><label for="">Banquet Hall</label></li>
										<li><input type="checkbox" value="waterfront" name="venueType[]" class="push-left"><label for="">WaterFront</label></li>
									</ul>
								</div>
								<div class="col-md-4">
									<ul class="left_list">
										<li><input type="checkbox" value="Country/ Golf Club" name="venueType[]" class="push-left"><label for="">Country/ Golf Club</label></li>
										<li><input type="checkbox" value="Winery" name="venueType[]" class="push-left"><label for="">Winery</label></li>
										<li><input type="checkbox" value="Hotel" name="venueType[]" class="push-left"><label for="">Hotel</label></li>
										<li><input type="checkbox" value="Monsion" name="venueType[]" class="push-left"><label for="">Monsion</label></li>
										<li><input type="checkbox" value="Beach" name="venueType[]" class="push-left"><label for="">Beach</label></li>
										<li><input type="checkbox" value="park" name="venueType[]" class="push-left"><label for="">park</label></li>
										<li><input type="checkbox" value="Religious" name="venueType[]" class="push-left"><label for="">Religious</label></li>
									</ul>
								</div>
								<div class="col-md-4">
									<ul class="left_list">
										<li><input type="checkbox" value="Composition/Cobin" name="venueType[]" class="push-left"><label for="">Composition/Cobin</label></li>
										<li><input type="checkbox" value="Inn/B&D" name="venueType[]" class="push-left"><label for="">Inn/B&D</label></li>
										<li><input type="checkbox" value ="Museum" name="venueType[]" class="push-left"><label for="">Museum</label></li>	
									</ul>
								</div>
							
							</div>
						</div>
						<div class="row venue-info">
							<div class="col-md-12">
								<h3>Venue Settings</h3>
								
									<div class="col-md-4">
										<ul class="left_list">
											<li><input type="checkbox" value="Indoor" name="venueSetting[]" class="push-left"><label for="">Indoor</label></li>
											<li><input type="checkbox" value="Uncovered Outdoor" name="venueSetting[]" class="push-left"><label for="">Uncovered Outdoor</label></li>
											<li><input type="checkbox" value="Covered Outdoor" name="venueSetting[]" class="push-left"><label for="">Covered Outdoor</label></li>
											
										</ul>
									</div>
								
							</div>
						</div>	
						<div class="clearfix"></div>
						<div class="row venue-info">
							<div class="col-md-12">
								<h3>Venue Activities</h3>
								<div class="row">
									<div class="col-md-4">
										<ul class="left_list">
											<li><input type="checkbox" value="Get ready rooms" name="venueActivities[]" class="push-left"><label for="">Get ready rooms</label></li>
											<li><input type="checkbox" value="Event Rentails"  name="venueActivities[]" class="push-left"><label for="">Event Rentails</label></li>
											<li><input type="checkbox" value="Outside Vendors"  name="venueActivities[]" class="push-left"><label for="">Outside Vendors</label></li>
											<li><input type="checkbox" value="Bar Services"  name="venueActivities[]" class="push-left"><label for="">Bar Services</label></li>
											<li><input type="checkbox" value="Clean Up"  name="venueActivities[]" class="push-left"><label for="">Clean Up</label></li>
											<li><input type="checkbox" value="Event Planer"  name="venueActivities[]" class="push-left"><label for="">Event Planer</label></li>	
										</ul>
									</div>
									<div class="col-md-4">
										<ul>
											<li><input type="checkbox" value="Wifi" name="venueActivities[]" class="push-left"><label for="">Wifi</label></li>
											<li><input type="checkbox" value="Pet Friendly" name="venueActivities[]" class="push-left"><label for="">Pet Friendly</label></li>
											<li><input type="checkbox" value="Accomodation" name="venueActivities[]" class="push-left"><label for="">Accomodation</label></li>
											<li><input type="checkbox" value="Liability insurrence" name="venueActivities[]" class="push-left"><label for="">Liability insurrence</label></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
				</div>
			</div>
			<!--Venue Info End->
			<!- Guide Info Start-->
			<div class="vfform bussiness guide" id="guide">
				<div class="title">
					<h3>Guide Info</h3>
				</div>
				<div class="feildcont">
					
						<div class="row">
							<div class="col-md-12 last">
								<label>Country of residence <em>*</em></label>
								<select name="countryOfResidence">
									@foreach($country as $key=>$val)
									<option value="{{$val->name}}" data-id="{{$val->id}}">{{$val->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div><br/>
							<div class="col-md-12 radiobut">
								<label>Are you a lawfull resident?</label>
								<input class="one" type="radio" name="lawfull" value="yes" checked="">
								<span class="onelb">YES</span>
								<input class="two" type="radio" name="lawfull" value="no">
								<span class="onelb">NO</span>
							</div><br/>
							<div class="col-md-12">
								<label>How many language do you speak? <em>*</em></label>
								<input type="number" name="language">
							</div>
							<div class="col-md-12">
								<label>If more then one language <em>*</em></label>
								<select>
									<option value="">select your language</option>
									@foreach($language as $k=>$v)
									<option value="{{$v->name}}">{{$v->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="clearfix"></div>
							<div class="margin_bottom2"></div>
							<div class="col-md-12">
								<label>Where would you like to guide people?</label><br/>
							</div>
							<div class="row guide-info">
								<div class="col-md-8">
									<div class="col-md-4">
										<ul class="left_list">
											<li><input type="radio" class="push-left" value="Country" name="guidepeople[]" checked=""><label for=""> Country</label></li>
											<li><input type="radio" class="push-left" value="State" name="guidepeople[]"><label for="">State</label></li>
										</ul>
									</div>
									<div class="col-md-4">
										<ul>
											<li><input type="radio" class="push-left" value="City" name="guidepeople[]"><label for="">City</label></li>
											<li><input type="radio" class="push-left" value="Town" name="guidepeople[]"><label for="">Town</label></li>
										</ul>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>					
							<div class="col-md-12">
								<label>In a scale of 0 to 10(0 being very bad and 10 being verey good) how wel do you know the (place) <em>*</em></label>
								<input type="number" name="scale">
							</div>
							<div class="col-md-12 ">
								<div class="progress-extended">&nbsp;</div>
							        <div class="imagePresentation">
							        	<div class="col-md-6">
										    <span class="button solid fileinput-button">
										        <i class="glyphicon glyphicon-plus"></i>
										        <span>Upload Your Images...</span>
										        <input type="file" name="files[]" id="guidefile" multiple>
										    </span><br/>
										        @if (session('status'))
												    <div class="alert alert-danger">
												        {{ session('status') }}
												    </div>
												@endif
												<div id="progress" class="progress">
										        <div class="progress-bar progress-bar-success"></div>
										    	</div>
										    <p>Max files are allowed and each must not exceed 2mb</p>
										    <br>
										    <br>
										    <!-- The global progress bar -->
										    <!-- The container for the uploaded files -->
										    
										    <br>
										</div>
							        	<div class="col-md-6">
								        	<div id="viewguideimg" class="viewimg">
								        	</div>
								        	<div class="profileimage-inline">
											<?php if ($businessattach == NULL) {
												?>
												<p></p>
												<?php
											} else {
												foreach ($businessattach as $businessimg => $value) {
												?>
											<div class="profileimagesf">
											<i class="fa fa-times" onclick="removeBusinessimg({{$value->attachment_id}},{{$value->business_id}},'{{$value->path}}','{{$value->name}}')"></i><img src="/{{$value->path}}" width="100px">
											</div>
											<?php
											}
											}
											?>
										</div>
							        	</div>
							        </div>
							</div>
						</div>
				</div>
			</div>
			<!-- Guide Info End-->
			<!-- Bussinss Info else part-->
			<div class="vfform bussiness" id ="other">
				<div class="title">
					<h3>Information</h3>
				</div>
				<div class="feildcont">			
						<div class="row">
							<div class="col-md-12 last">

								<label>Specific Category <em>*</em></label>
								<select id ="subcategory" name="subcategory">
									<option value="">select your category</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>How Would like to describe your bussiness?<em>*</em></label>
								<textarea id="description" name="description"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>How Did You hear about us? <em>*</em></label>
								<select name="hearaboutus">
									<option value="">select</option>
									<option value="internet">Internet</option>
                                	<option value="friends">Friends</option>
								</select>
							</div>
						</div>
					
							<div class="clearfix"></div>
				</div>
			</div>
			<!-- Bussiness Info End else part-->
			<div class="text-center">
				<button type="submit" class="button solid">Submit</button>
			</div>
			<br><br>
	 	</form>	
	</div>
</div>
@endsection
@section('addjavascript')
	<script type="text/javascript" src="{{asset('js/bussiness.js')}}"></script>
	

@endsection