@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
    <div class="row">
        <div class="vendor-job-list form-fields panel-group">
            <div class="vfform margin-zero" id="">
                <div class="title">
                    <h3>Vendor Invite Jobs</h3>
                </div>
               <?php
                if($invite->isEmpty()) {
                    ?>
                 <div>
                    
                    <div class="col-md-12">
                        <div class="row">
                            <div class="top-bot-margin1">
                                <div class="col-md-12 text-center ">
                                    <h3>Currently you have no invites</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                 }else{ ?>
                <div class="">
                    <div class="col-md-3">
                        <h4>Title</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Status</h4>
                    </div>
                    <div class="col-md-3">
                        <h4>Invite Status</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>SENT AT</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>ACTION</h4>
                    </div>
                </div>
            
                 @foreach ($invite as $jobs)
                 <form method="POST">
                <div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="vf-bids">
                               <div class="col-md-3 ">
                                    <div class="bid-detail">
                                        {{$jobs->name}}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="bid-detail">
                                        <?php
                                        if ($jobs->status == '1') {
                                              ?>
                                        <p>Open</p>
                                        <?php
                                        } elseif ($jobs->status == '0') {
                                            ?>
                                        <p>Close</p>
                                            <?php
                                        } elseif ($jobs->status == '2') {
                                            ?>
                                        <p>Expired</p>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3 ">
                                    <div class="bid-detail">
                                        <?php
                                        if ($jobs->clientStatus == '2') {
                                              ?>
                                        <p>Pending</p>
                                        <?php
                                        } elseif ($jobs->clientStatus == '1') {
                                            ?>
                                        <p>Accepted</p>
                                            <?php
                                        } elseif ($jobs->clientStatus == '0') {
                                            ?>
                                        <p>Declined</p>

                                            <?php
                                        }
                                        ?>
                                        
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="bid-detail">
                                        {{date("m/d/Y", strtotime($jobs->created_at))}}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="bid-detail"> 
                                         <?php
                                        if ($jobs->clientStatus == '0'){
                                            ?>
                                            <p>Declined</p>
                                        <?php
                                        } elseif ($jobs->clientStatus == '1'){
                                        ?>
                                        <p>Accepted</p>
                                        <?php
                                        }elseif ($jobs->clientStatus == '2'){
                                            ?>
                                        <a href="/vendor/job/{{$jobs->job_id}}/details" class="button solid">Accept</a>
                                        <a href="javascript:;" class="button default" onclick="declined({{$jobs->job_id}})">Decline </a>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </form> 
                @endforeach
                 <div class="paginate push-right">  
                                {{ $invite->render() }}
                    </div>
            <?php }?>
            </div>
        </div>
    </div>  
</div>
<div class="job-posting-form container">
    <div class="row">
        <div class="vendor-job-list form-fields panel-group">
            <div class="vfform margin-zero" id="">
                <div class="title">
                    <h3>Team Invites</h3>
                </div>
               <?php
                if($teaminvite->isEmpty()) {
                    ?>
                 <div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="top-bot-margin1">
                                <div class="col-md-12 text-center ">
                                    <h3>Currently you have no invites</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                 }else{ ?>
                <div class="">
                    <div class="col-md-2">
                        <h4>Title</h4>
                    </div>
                     <div class="col-md-2">
                        <h4>Team</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Status</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>Invite Status</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>SENT AT</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>ACTION</h4>
                    </div>
                </div>

                 @foreach ($teaminvite as $jobs)

                 <form method="POST">
                <div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="vf-bids">
                               <div class="col-md-2 ">
                                    <div class="bid-detail">
                                        {{$jobs->name}}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="bid-detail">
                                        {{$jobs->tname}}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="bid-detail">
                                        <?php
                                        if ($jobs->status == '1') {
                                              ?>
                                        <p>Open</p>
                                        <?php
                                        } elseif ($jobs->status == '0') {
                                            ?>
                                        <p>Close</p>
                                            <?php
                                        } elseif ($jobs->status == '2') {
                                            ?>
                                        <p>Expired</p>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="bid-detail">
                                        <?php
                                        if ($jobs->clientStatus == '2') {
                                              ?>
                                        <p>Pending</p>
                                        <?php
                                        } elseif ($jobs->clientStatus == '1') {
                                            ?>
                                        <p>Accepted</p>
                                            <?php
                                        } elseif ($jobs->clientStatus == '0') {
                                            ?>
                                        <p>Declined</p>

                                            <?php
                                        }
                                        ?>
                                        
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="bid-detail">
                                        {{date("m/d/Y", strtotime($jobs->created_at))}}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="bid-detail"> 
                                         <?php
                                        if ($jobs->clientStatus == '0'){
                                            ?>
                                            <p>Declined</p>
                                        <?php
                                        } elseif ($jobs->clientStatus == '1'){
                                        ?>
                                        <p>Accepted</p>
                                        <?php
                                        }elseif ($jobs->clientStatus == '2'){
                                            ?>
                                        <a href="/vendor/job/{{$jobs->job_id}}/{{$jobs->tid}}/details" class="button solid">Accept</a>
                                        <a href="javascript:;" class="button default " id="declineteam" data-id ="{{$jobs->tid}}" onclick="declined({{$jobs->job_id}}, this)">Decline </a>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </form> 
                @endforeach
                <div class="paginate push-right">  
                    {{$teaminvite->render()}}
                </div>
            <?php }?>
            </div>
        </div>
    </div>  
</div>
  @endsection
  @section('addjavascript')
    <script src="{{asset('js/vendor-job.js')}}"></script>
    @endsection