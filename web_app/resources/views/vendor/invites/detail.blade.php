@extends('../layouts/base')
  @section('content')
		<div class="container job-deatils-page">
			<div class="row">
				<div class="col-md-8">
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils">
							<div class="job-heading">
								<h2>{{$data->name}}</h2>
								{{-- <label>Editing & proofreading</label> --}}
								<span>&nbsp;posted at {{date("m/d/y", strtotime($data->created_at))}}</span><br/>
							</div>
							<div class="job-description">
								<div class="col-md-8 border-right">
									<div class="job-text">
										<label>Message : </label>
										<span>{{$data->invite_message}}</span><br/>
										
									</div>
									<div class="job-info">
										
										<div>
											
										</div>
									</div>
								</div>
								<div class="col-md-4 job-requirement">
									<div>
									
									</div>
									
									<div>									
										<h3>Status </h3>
										<p><?php
                                        if ($data->status == '1') {
                                              ?>
                                        <p>Open</p>
                                        <?php
                                        } elseif ($data->status == '0') {
                                            ?>
                                        <p>Close</p>
                                            <?php
                                        } elseif ($data->status == '2') {
                                            ?>
                                        <p>Expired</p>

                                            <?php
                                        }
                                        ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 client-details">
					<div class="row">
						<div class="job-detail-panel client-panel">
							<div class="text-center submit-proposal-buttons">
									<?php
                                        if ($data->status == '1') {
                                              ?>
									<a href="/vendor/job/{{$data->id}}/proposal" ><button class="button solid button-margin">Accepted</button></a><br>
									
									<?php
                                        } else {
                                            ?>
							
								 	<a href="/vendor/proposal/"  class="button solid vf-btn">View Proposal</a>
							<?php
							}
							?>
							</div>
							<div>
								<label>About the client  </label>
							</div>
							
							<div>
								<h5> Jobs Posted</h5>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection