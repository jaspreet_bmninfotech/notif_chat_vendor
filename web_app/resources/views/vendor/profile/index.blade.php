@extends('layouts/base')
  @section('content')
<div class="page_title2 page_title2pro">
	<div class="container">
		<h1></h1>
		<div class="pagenation">&nbsp;<a href="">Home</a> <i>/</i> <a href="#">Vendor</a> <i>/</i>Vendor Profile
		</div>
	</div>
</div>  
<div class="container">
	<div class="content_fullwidth">
		<div class="col-md-3">
			<img src="/{{$attachment['path']}}" alt="" class="profile">
		</div><!-- end section -->
		<div class="col-md-6 locfa">
			<h3 class="profhtg">{{ucwords($data['username'])}}</h3>
			<h4><i class="fa fa-map-marker" aria-hidden="true"></i>  
			{{$city['name']}},{{$state['name']}},{{$country['name']}}</h4>
			<p>{{$business['description']}}
			<a href="#" class=""></a>
			</p>
			<table class="table">
				<tbody>
					<tr>
						<td>${{$business['ratePerHour']}}</td>
						<td>${{$business['ratePerProject']}}</td>
						<td>{{$hires}}</td>
					</tr>
					<tr>
						<td>Hourly Rate</td>
						<td>Fixed Rate</td>
						<td>Hire</td>
					</tr>
				</tbody>
			</table>
		</div>
	<!-- end section -->
		<div class="col-md-3 last">

			<div id="progress_bar2" class="ui-progress-bar ui-container">
				
				<div class="ui-progress two" id="profile_job_success" data-succes="{{ isset($totalstars[Auth::user()->id]['success_percent']) ? $totalstars[Auth::user()->id]['success_percent'] : '' }}" style="width: {{ isset($totalstars[Auth::user()->id]['success_percent']) ? $totalstars[Auth::user()->id]['success_percent'] :'0'}}%;">
					<span class="ui-label" style="display: block;"><b class="value">{{ isset($totalstars[Auth::user()->id]['success_percent']) ? $totalstars[Auth::user()->id]['success_percent'] : '0'}}%</b>
					</span>
				</div>
				

			</div>
			<h3 class="center"><b>Job Success</b></h3>
			<?php
			if ($data['id']!= $user_id) {
			 	?>
			<div class="text-align">
				<button class="button solid button-margin vf-btn ">Hire Now</button><br>
				<button class="button default button-margin vf-btn"><i class="fa fa-heart" aria-hidden="true"></i> Save </button><br>
				<button class="button default button-margin vf-btn">Invite to team</button><br>
			</div>
			 	<?php
			 } 
			?>
		</div>
	<!-- end section -->
	</div>
		<!-- <div class="stcode_title4">
				<h3><span class="line"></span><span class="text"><strong>Re-Enter</strong><span></span></span></h3>
			</div>
			<div class="margin_top1"></div><div class="clearfix"></div>
			<div id="owl-demo" class="owl-carousel">
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
				<div class="item"><img src="{{asset('/images/vf-si-boy.png')}}" alt="" /></div>
			</div> -->
	<div class="margin_top1"></div><div class="clearfix"></div>
	<div class="content_fullwidth">
		<div class="">
		<!-- end section -->
			<div class="clearfix"></div>
			<?php if ($category['name'] == "model") {
			?>
			<div class="stcode_title4">
				<h3><span class="line"></span><span class="text"><strong>Model</strong><span></span></span></h3>
			</div>
			<div class="margin_top1"></div><div class="clearfix"></div>
			<div id="owl-demo" class="owl-carousel">
				<?php
				$businessAttachmentsModel = count($businessAttachments);
				for ($i=0; $i < $businessAttachmentsModel  ; $i++) {
				?>
				<div class="item "><img class="lazyOwl scrollimage" src="/{{$businessAttachments[$i]->path}}" alt="" /></div>
				<?php
			}
			?>
			</div>
			<?php
		}
		?><!-- end section -->
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- vendor album start -->
	<?php
		if ($data['id'] == $user_id) {
		 	?>
	<div class="col-md-6">
		<div class="vp-section">
			<div class="row">
				<div class="vfform icon-panel">
					<div class="col-md-3 vf-album-icon icon-brown text-align" id="texticon">
						<i class="fa fa-text-height " aria-hidden="true"></i><br><span class="icon-text">Text</span>
					</div>
					<div class="col-md-3 vf-album-icon icon-red text-align" id="imageicon">
						<i class="fa fa-camera" aria-hidden="true"></i><br><span class="icon-text">Images</span>
					</div>
					<div class="col-md-3 vf-album-icon icon-green text-align" id="videoicon">
						<i class="fa fa-video-camera" aria-hidden="true"></i><br><span class="icon-text">Video</span>
					</div>
					<div class="col-md-3 vf-album-icon icon-blue text-align" id="linkicon">
						<i class="fa fa-link" aria-hidden="true"></i><br><span class="icon-text">Link</span>
					</div>
				</div>
			</div>
			<div class="row">
				<form  class="tumblerform" id="" method="post"  enctype="multipart/form-data" action="">
					{!! csrf_field() !!}
					<div class="vfform" id="album-form" >
						<div class="feildcont" id="textcolumn">
							<div>
								<label></label>
								<input type="text" name="title" placeholder="Title">
								@if($errors->has('title'))
									<span class="text-danger">{{ $errors->first('title') }}</span>
									@endif<br>
							</div><br/>
							<div>
								<label></label>
								<textarea name="message" placeholder="your text here"></textarea>
								@if($errors->has('message'))
									<span class="text-danger">{{ $errors->first('message') }}</span>
									@endif
							</div>
						</div>	
						<div id="imagecolumn">
					<!--- vendor album images start --> 
							<div class="feildcont album-margin">
								<div>
									<label></label>
									<input type="text" name="imagetitle" id="imagetitle" placeholder="Album name">
									@if($errors->has('imagetitle'))
									<span class="text-danger">{{ $errors->first('title') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<input type="text" name="tag" placeholder="Tag">
									@if($errors->has('tag'))
									<span class="text-danger">{{ $errors->first('tag') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<textarea name="imagemessage" id="imagemessage" placeholder="Caption"></textarea>
									@if($errors->has('imagemessage'))
									<span class="text-danger">{{ $errors->first('message') }}</span>
									@endif
								</div>
							</div>	
							<div class="feildcont" >	        
						       <div class="row fileupload-buttonbar">
					                <div class="progress-extended">&nbsp;</div>
							        <div class="imagePresentation">
							        	<div class="">
										    <span class="button solid fileinput-button">
										        <i class="glyphicon glyphicon-plus"></i>
										        <span>Upload Images...</span>
										        <input id="fileupload" type="file" name="files[]" multiple>
										        <input type="hidden" id="fileattachment" name="hid">
										    </span>
										    <br>
										    <br>
										    <!-- The global progress bar -->
										    <div id="progress" class="progress">
										        <div class="progress-bar progress-bar-success"></div>
										    </div>
										    <!-- The container for the uploaded files -->
										    <div id="files" class="files">
										    	
										    </div>
										    
										</div>
							        </div>
						        </div>
						        <!-- The table listing the files available for upload/download -->
						        <!-- <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table> -->
							</div>	
						</div>
				<!-- vendor album images end -->
							
				<!-- vendor album video start -->
						<div id="videocolumn">
							<div class="feildcont album-margin">
								<div>
									<label></label>
									<input type="text" name="videotitle" id="videotitle" placeholder="Album name">
									@if($errors->has('videotitle'))
									<span class="text-danger">{{ $errors->first('title') }}</span>
									@endif<br/>
								</div>
								<div>
									<label></label>
									<input type="text" name="tag" placeholder="Tag">
									<br/>
								</div>
								<div>
									<label></label>
									<textarea name="videomessage" id="videomessage" placeholder="Caption"></textarea>
									@if($errors->has('videomessage'))
									<span class="text-danger">{{ $errors->first('videomessage') }}</span>
									@endif
								</div>
									<div class="feildcont" >
								       <div class="row fileupload-buttonbar">
							                <div class="progress-extended">&nbsp;</div>
									        <div class="imagePresentation">
									        	<div class="">
												    <span class="button solid fileinput-button">
												        <i class="glyphicon glyphicon-plus"></i>
												        <span>Upload Video...</span>
												        <input id="fileuploads" type="file" name="files[]" multiple>
												    </span>
												    <br>
												    <br>
												    <!-- The global progress bar -->
												    <div id="progress" class="progress">
												        <div class="progress-bar progress-bar-success"></div>
												    </div>
												    <!-- The container for the uploaded files -->
												    <div id="preview" class="files">
												    	
												    </div>
												    <div id="errorsvideo" class="">
												    	
												    </div>
												    
												</div>
									        </div>
								        </div>
							        <!-- The table listing the files available for upload/download -->
							        <!-- <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table> -->
								</div>		
							</div>	
						</div>
				<!-- vendor album video end -->

				<!-- vendor album link start -->
					
						<div class="feildcont" id="linkcolumn" >
							<h3>Link</h3>
							<div>
								<label></label>
								<textarea name="link" placeholder="Caption"></textarea>
								@if($errors->has('link'))
									<span class="text-danger">{{ $errors->first('link') }}</span>
									@endif
							</div>
						</div>
			<!-- vendor album link end -->
						<div class="push-right margin-20">
							<button class="button solid" id="createImageInfo">Post</button>
							<button class="button solid" id="createvideoInfo">Post</button>
							<button class="button solid" id="post">Post</button>
						</div>
					</div>	
				</form>

			<!-- vendor images start -->
				<div id="vendorPosts">



				</div>
			<!-- vendor images end -->
			</div>
		</div>
	</div>
		<?php
			 } 
			?>
	<!-- vendor album end -->

	<!-- vendor work history or feedback start-->
	<div class="col-md-6">
		<div class="row">
			<div class="vfform">
				<div class="title feildcont">
					<div class="col-md-8">
						<div class="row">
							<h3>Work History And Feedback</h3>
						</div>
					</div>
					<div class="col-md-4 last">
						<select id="filter" class="filters">
							<option value="0">Select</option>
							<option id="new" class="new" value="new">New First</option>
							<option id="old" class="old" value="old">Oldest First</option>
						</select>
					</div>
				</div>
				<div id="getjobList">
				
				</div>
				<div class="text-align" id="loadMores" >
					<button class="button solid vf-btn bottom-margin" id="loadMore">Load More</button>
				</div>
			</div>
			<!-- <div class="vfform">
				<div class="title feildcont">
					<h3>Availability</h3>
				</div>
				<div class="feildcont">
					<h4>{{$business['name']}}</h4>
					<h5>Oct 2017 - Nov 2017</h5>
				</div>
			</div> -->
			<div class="vfform">
				<div class="title feildcont">
					<h3>Verification</h3>
				</div>
				<div class="feildcont">
					{{-- <label>Phone no.: verified</label> --}}
					<label>Email: verified</label>
				</div>
			</div>	
			<!-- <div class="vfform">
				<div class="title feildcont">
					<h3>Languages</h3>
				</div>
				<div class="feildcont">
					<p>English, French, Swahili. lingala</p>
				</div>
			</div>	 -->	
		</div>
	</div>
	<!-- vendor work history or feedback end-->
</div>
@endsection
@section('addjavascript')

<script type="text/javascript" src="/js/vendor-profile-info.js"></script>
<!-- <script type="text/javascript">
	document.getElementById('file').onchange = function(e){
		var image = e.target.files[0];
		loadImage(image, function(img){
		 console.log(img);
		 document.querySelector("#viewimg").appendChild(img);
		 },{
		 	maxWidth:100,
		 	maxHeight:100
		 });	
		};
		
	
</script> -->
<script type="text/template" id="getjobListTmpl">     
    <% _.each(data,function(v,i,l){ %>
    <% if(v.status == 0){ %>
    
			<div class="feildcont">
				<h3><%= v.name %></h3>
				<span><%= v.created_at %></span>
				<p><%= v.description %></p>
				<div class="five-star">
					 
					
				</div>
				<p><%= v.cover %></p>
			</div>
		
		<br clear="all">
		<% }else{ %>
			<div class="feildcont">
				<h3><%= v.name %></h3>
				<span><%= v.created_at %></span>
				<p><%= v.description %></p>
				<div class="five-star">
					   
				</div>
				<p></p>
			</div>
		</div>
	<% } %>
	    
    <% }) %>
</script>
<script type="text/template" id="loadMoreTmpl">
    
		<button class="button solid vf-btn bottom-margin" id="oldloadMore">Load More</button>
</script>
<!-- The template to display files available for download -->
<script type="text/template" id="textpost">
		<div class="vfform">
			<div class="feildcont" >
				<span class="postellip">
		  		<div class="dropdown elepsisab pull-right">
				 <i class="dropbtn fa fa-ellipsis-v "></i>
				 <div class="dropdown-content">
				   <a href="#" class="btn btn-light btn-lg" data-toggle="modal" data-target="#myModal<%= index %>">Edit</a>
				   <a href="javascript:void(0)" onclick="deletePost(<%= el.id %>)">Delete</a>
				 </div>
				</div>
		  <!-- Modal -->
					  <div class="modal fade" id="myModal<%= index %>" role="dialog">
			  		  <div class="modal-dialog modal-lg">
			  		    <div class="modal-content">
		      		  <div class="modal-header">
		       		   <button type="button" class="close" data-dismiss="modal">&times;</button>
		       		   <h4 class="modal-title">Edit Post</h4>
		       		 </div>
		       		 <div class="modal-body">
		       		 	<div id="imagecolumn">
							<div id="imagecolumnerror">
							</div>
					<!--- vendor album images start --> 
							<div class="feildcont album-margin">
								<div>
									<label></label>
									<input type="text" name="texttitles" value="<%= el.title %>">
									@if($errors->has('imagetitles'))
									<span class="text-danger">{{ $errors->first('titles') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<input type="text" name="tag" placeholder="Tag" >
									@if($errors->has('tag'))
									<span class="text-danger">{{ $errors->first('tag') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<textarea name="textmessages" ><%= el.message %></textarea>
									@if($errors->has('imagemessages'))
									<span class="text-danger">{{ $errors->first('messages') }}</span>
									@endif
								</div>
							</div>	
								
						</div>
		      		    
		      		    
		      		    <a href="javascript:void(0)" class="button solid" onclick="updatetextPost(<%= el.id %>)" >Post</a>
		      		  </div>
		       		
		     		 </div>
		   		 </div>
		 	 </div>

					
				</span>
				<div>
					<h4><%= el.title %></h4>
				</div>
				<div>
					<p><%= el.message %> </p>								
				</div><br>
			</div>
		</div>
</script>	
<script type="text/template" id="imagepost">
<form  class="" id="" method="post"  enctype="multipart/form-data" action="">
		<div class="vfform" >
			<div class="feildcont">
				<span class="postellip">
		  		<div class="dropdown elepsisab pull-right">
				 <i class="dropbtn fa fa-ellipsis-v "></i>
				 <div class="dropdown-content">
				   <a href="javascript:void(0)" class="btn btn-light btn-lg" data-toggle="modal" onclick="editimagepost(<%= index %>)" data-target="#myModal<%= index %>">Edit</a>
				   <% if(el.path == null){ %>
				   <a href="javascript:void(0)" onclick="deletePost(<%= el.id %>)">Delete</a>
					<% }else{ %>
				   <a href="javascript:void(0)" onclick="deletePost(<%= el.post_id %>,<%= el.attachment_id %>,'<%= el.name %>')">Delete</a>
				   <% } %>
				 </div>
				</div>
		  <!-- Modal -->
					  <div class="modal fade" id="myModal<%= index %>" role="dialog">
			  		  <div class="modal-dialog modal-lg">
			  		    <div class="modal-content">
		      		  <div class="modal-header">
		       		   <button type="button" class="close" data-dismiss="modal">&times;</button>
		       		   <h4 class="modal-title">Edit Post</h4>
		       		 </div>
		       		 <div class="modal-body">
		       		 	<div id="imagecolumn">
							<div id="imagecolumnerror">
							</div>
					<!--- vendor album images start --> 
							<div class="feildcont album-margin">
								<div>
									<label></label>
									<input type="text" name="imagetitles" value="<%= el.title %>">
									@if($errors->has('imagetitles'))
									<span class="text-danger">{{ $errors->first('titles') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<input type="text" name="tag" placeholder="Tag" >
									@if($errors->has('tag'))
									<span class="text-danger">{{ $errors->first('tag') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<textarea name="imagemessages" ><%= el.message %></textarea>
									@if($errors->has('imagemessages'))
									<span class="text-danger">{{ $errors->first('messages') }}</span>
									@endif
								</div>
							</div>	
							<div class="feildcont" >
							        
						       <div class="row fileupload-buttonbar">
					                <div class="progress-extended">&nbsp;</div>
							        <div class="imagePresentation">
							        	<div class="">
										    <span class="button solid fileinput-button">
										        <i class="glyphicon glyphicon-plus"></i>
										        <span>Upload Images...</span>
										        <input id="updateimagefileupload<%= index %>" type="file" name="files[]" multiple>
										        <input type="hidden" id="fileattachment" name="hid">
										    </span>
										    <br>
										    <br>
										    <!-- The global progress bar -->
										    <div id="progress" class="progress">
										        <div class="progress-bar progress-bar-success"></div>
										    </div>
										    <!-- The container for the uploaded files -->
										    <div id="ufiles<%= index %>" class="files">
										    	
										    </div>
										    <div class="col-md-12 album-first-images">
											<div class="row viewimg">
												<div class="profileimage-inline">
											<% if(el.path == null){ %>
												<p>Invalid Image</p>
											<% }else{ %>
											<div class="profileimagesf<%= index %>">
											<i class="fa fa-times" onclick="removePostimg(<%= el.post_id %>,<%= el.attachment_id %>,'<%= el.path %>','<%= el.name %>')"></i><img src="/<%= el.path %>" width="100px">
											</div>
											<% } %>
										</div>
											</div>
												
										</div>
										    <br>
										</div>
							        </div>
						        </div>
						        <!-- The table listing the files available for upload/download -->
						        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
							</div>	
						</div>
		      		    
		      		    
		      		    <a href="javascript:void(0)" class="button solid" onclick="updatePost(<%= el.id %>)" >Post</a>
		      		  </div>
		       		
		     		 </div>
		   		 </div>
		 	 </div>

					
				</span>
				<div>
					<h1><%= el.title %></h1>
				</div>
				<div>
					<h2><%= el.message %> </h2>								
				</div><br/>
				<div class="col-md-12 album-first-images">
					<div class="row">
						<% if(el.path == null){ %>
						
							<p>Invalid Image</p>
						<% }else{ %>
						<img src="/<%= el.path %>">
						<% } %>
					</div>
				</div>
				<!-- <div class="col-md-6 album-images">
					<img src="/uploads/vendor/2/<%= el.name %>">
				</div>
				<div class="col-md-6 album-images">	
					<img src="/uploads/vendor/2/<%= el.name %>">
				</div> -->
				<!-- <div class="col-md-6 album-images">
					<img src="/images/vf-si-boy.png">
				</div>
				<div class="col-md-6 album-images">	
					<img src="/images/vf-si-boy.png">
				</div> -->
			</div>
			<!-- <div class="feildcont">
				<div class="col-md-12 album-full-images">
					<img src="/images/people-vendor-128.png">
				</div>
			</div> -->
		</div>
	</form>
</script>
	
<script type="text/template" id="videopost">					
		<div class="vfform">	
			<div class="feildcont" >
				<span class="postellip">
		  		<div class="dropdown elepsisab pull-right">
				 <i class="dropbtn fa fa-ellipsis-v "></i>
				 <div class="dropdown-content">
				   <a href="javascript:void(0)" class="btn btn-light btn-lg" data-toggle="modal" onclick="editpost(<%= index %>)" data-target="#myModal<%= index %>">Edit</a>
				   <a href="javascript:void(0)" onclick="deletePost(<%= el.post_id %>,<%= el.attachment_id %>,'<%= el.name %>')">Delete</a>
				 </div>
				</div>
		  <!-- Modal -->
					  <div class="modal fade" id="myModal<%= index %>" role="dialog">
			  		  <div class="modal-dialog modal-lg">
			  		    <div class="modal-content">
		      		  <div class="modal-header">
		       		   <button type="button" class="close" data-dismiss="modal">&times;</button>
		       		   <h4 class="modal-title">Edit Post</h4>
		       		 </div>
		       		 <div class="modal-body">
		       		 	<div id="imagecolumn">
							<div id="imagecolumnerror">
							</div>
					<!--- vendor album images start --> 
							<div class="feildcont album-margin">
								<div>
									<label></label>
									<input type="text" name="videotitles" value="<%= el.title %>">
									@if($errors->has('videotitles'))
									<span class="text-danger">{{ $errors->first('titles') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<input type="text" name="tag" placeholder="Tag" >
									@if($errors->has('tag'))
									<span class="text-danger">{{ $errors->first('tag') }}</span>
									@endif
								</div>
								<div>
									<label></label>
									<textarea name="videomessages" ><%= el.message %></textarea>
									@if($errors->has('videomessages'))
									<span class="text-danger">{{ $errors->first('messages') }}</span>
									@endif
								</div>
							</div>	
							<div class="feildcont" >
							        
						       <div class="row fileupload-buttonbar">
					                <div class="progress-extended">&nbsp;</div>
							        <div class="imagePresentation">
							        	<div class="">
										    <span class="button solid fileinput-button">
										        <i class="glyphicon glyphicon-plus"></i>
										        <span>Upload Images...</span>
										        <input id="updatevfileupload<%= index %>" type="file" name="files[]" multiple>
										        <input type="hidden" id="fileattachment" name="hid">
										    </span>
										    <br>
										    <br>
										    <!-- The global progress bar -->
										    <div id="progress" class="progress">
										        <div class="progress-bar progress-bar-success"></div>
										    </div>
										    <!-- The container for the uploaded files -->
										    <div id="ufiles<%= index %>" class="files">
										    	
										    </div>
										    <div class="col-md-12 album-first-images">
											<div class="row viewimg">
												<div class="profileimage-inline">
											<% if(el.path == null){ %>
												<p>Invalid video</p>
											<% }else{ %>
											<div class="profileimagesf<%= index %>">
											<i class="fa fa-times" onclick="removePostvideo(<%= el.post_id %>,<%= el.attachment_id %>,'<%= el.path %>','<%= el.name %>')"></i>
												<iframe width="100%" height="50" src="/<%= el.path %>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
											</div>
											<% } %>
										</div>
											</div>
										</div>
										    <br>
										</div>
							        </div>
						        </div>
						        <!-- The table listing the files available for upload/download -->
						        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
							</div>	
						</div>
		      		    
		      		    
		      		    <a href="javascript:void(0)" class="button solid" onclick="updatevideoPost(<%= el.id %>)" >Post</a>
		      		  </div>
		       		
		     		 </div>
		   		 </div>
		 	 </div>

					
				</span>
				<div>
					<h4><%= el.title %></h4>
				</div>
				<div>
					<p><%= el.message %> </p>								
				</div><br/>
				<div class="col-md-12 album-full-images">
					<div class="row">
						<% if(el.path == null){ %>
						<p>Invalid video</p>
						<% }else{ %>
						<iframe width="100%" height="350" src="/<%= el.path %>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
						<% } %>
					</div>
				</div>
			</div>	
		</div>
</script>
<script type="text/template" id="linkpost">		
	<div class="vfform">
		<div class="feildcont" >
			<span class="postellip">
		  		<div class="dropdown elepsisab pull-right">
				 <i class="dropbtn fa fa-ellipsis-v "></i>
				 <div class="dropdown-content">
				   <a href="#" class="btn btn-light btn-lg" data-toggle="modal" data-target="#myModal<%= index %>">Edit</a>
				   <a href="javascript:void(0)" onclick="deletePost(<%= el.id %>)">Delete</a>
				 </div>
				</div>
		  <!-- Modal -->
					  <div class="modal fade" id="myModal<%= index %>" role="dialog">
			  		  <div class="modal-dialog modal-lg">
			  		    <div class="modal-content">
		      		  <div class="modal-header">
		       		   <button type="button" class="close" data-dismiss="modal">&times;</button>
		       		   <h4 class="modal-title">Edit Post</h4>
		       		 </div>
		       		 <div class="modal-body">
		       		 	<div id="imagecolumn">
							<div id="imagecolumnerror">
							</div>
					<!--- vendor album images start --> 
							<div class="feildcont album-margin">
								<div>
									<input type="text" name="links" value="<%= el.title %>">
									@if($errors->has('links'))
									<span class="text-danger">{{ $errors->first('links') }}</span>
									@endif
								</div>
							</div>	
								
						</div>
		      		    
		      		    
		      		    <a href="javascript:void(0)" class="button solid" onclick="updatelinkPost(<%= el.id %>)" >Post</a>
		      		  </div>
		       		
		     		 </div>
		   		 </div>
		 	 </div>

					
				</span>
			<br><div>
				<p class="text-center"><a href="<%= el.title %>"><%= el.title %></a></p>
			</div><br>
		</div>
	</div>
</script>
@endsection