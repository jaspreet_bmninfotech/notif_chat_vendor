@extends('../layouts/base')
@section('addstyle')
   <style>
       /*Colors*/
       /*End Colors*/
       /*Containers*/
       /*EndContainers*/
       /*Common*/


       p {
           margin: 0; }

       .wrapper {
           display: -webkit-box;
           display: -webkit-flex;
           display: -ms-flexbox;
           display: flex;
           -webkit-box-orient: vertical;
           -webkit-box-direction: normal;
           -webkit-flex-direction: column;
           -ms-flex-direction: column;
           flex-direction: column;
           height: 100%;
           min-height: 100vh; }

       .content {
           background: #f2f2f2;
           -webkit-box-flex: 1;
           -webkit-flex: 1 0 auto;
           -ms-flex: 1 0 auto;
           flex: 1 0 auto; }

       .title {
           /*font-size: 1.429em;
           font-weight: 700;*/
           margin: 21px 0 25px;
           width: 100%; }
       .title__offers {
           margin: 0;
           /*text-transform: uppercase;*/
            }

       .offers {
           width: 100%;
           -webkit-border-radius: 3px;
           border-radius: 3px;
           background-color: #ffffff;
           -webkit-box-shadow: 0 2px 10px 0 rgba(201, 201, 201, 0.26);
           box-shadow: 0 2px 10px 0 rgba(201, 201, 201, 0.26);
           border: solid 1px #d2d3d4; }
       .offers__head {
           background-color: #f3f3f3;
           display: -webkit-box;
           display: -webkit-flex;
           display: -ms-flexbox;
           display: flex;
           -webkit-box-align: center;
           -webkit-align-items: center;
           -ms-flex-align: center;
           align-items: center;
           -webkit-box-pack: justify;
           -webkit-justify-content: space-between;
           -ms-flex-pack: justify;
           justify-content: space-between;
           padding: 20px 30px; }
       .offers__active {
           display: block;
           width: 15px;
           height: 15px;
           background: #37a60c;
           -webkit-border-radius: 50%;
           border-radius: 50%;
           border: solid 2px #c7c7c7; }
       .offers__row {
           display: -webkit-box;
           display: -webkit-flex;
           display: -ms-flexbox;
           display: flex;
           -webkit-box-align: stretch;
           -webkit-align-items: stretch;
           -ms-flex-align: stretch;
           align-items: stretch;
           -webkit-box-pack: start;
           -webkit-justify-content: flex-start;
           -ms-flex-pack: start;
           justify-content: flex-start;
           border-bottom: 1px solid #eee; }
       .offers__row:last-child {
           border-bottom: none; }
       .offers__row_head {
           border-bottom: 2px solid #ddd; }
       .offers__cell {
           padding: 10px 0 10px 30px; }
       .offers__cell_small {
           width: 32.5%; }
       .offers__cell_big {
           width: 67.5%; }
       .offers__cell_title {
           /*font-size: 1.286em;
           font-weight: 600;*/
           text-transform: uppercase; }

       .date {
           font-size: 1.286em;
           margin-bottom: 13px;
           width: 100%; }

       .attribute {
           width: 100%;
           font-weight: 300;
           color: rgba(51, 51, 51, 0.87); }

       .subject {
           font-weight: 600;
           font-size: 1.286em;
           margin-bottom: 13px;
           width: 100%; }

       @media (min-width: 1200px) {
           .container_custom {
               max-width: 1000px; } }

       @media (max-width: 575px) {
           .content {
               padding: 0 10px; }
           .offers__head {
               padding: 20px 3%; }
           .offers__row {
               -webkit-flex-wrap: wrap;
               -ms-flex-wrap: wrap;
               flex-wrap: wrap; }
           .offers__row_head {
               display: none; }
           .offers__cell {
               padding-left: 3%; }
           .offers__cell_small {
               width: 100%; }
           .offers__cell_big {
               width: 100%; }
           .attribute_date {
               width: 47%;
               display: inline-block; }
           .date {
               width: 50%;
               display: inline-block;
               margin-bottom: 0; } }

   </style>
@endsection
@section('content')
    <div class="wrapper">
    @if($hires->isNotEmpty())
        <main class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('hire_flash_accept'))
                            <div class="alert alert-success text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{Session::get('hire_flash_accept')}}.
                            </div>
                        @endif
                        @if (Session::has('hire_flash_decline'))
                            <div class="alert alert-warning text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{Session::get('hire_flash_decline')}}.
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <h1 class="title">MY PROPOSALS</h1>
                    <div class="offers">
                        <div class="offers__head">
                            <h3 class="title title__offers">{{$hires->count()}} offers</h3>
                            <span class="offers__active"></span>
                        </div>
                        <div class="offers__body">
                            <div class="offers__row offers__row_head">
                                <div class="offers__cell offers__cell_small offers__cell_title">Received</div>
                                <div class="offers__cell offers__cell_big offers__cell_title">Title</div>
                            </div>
                            @foreach($hires as $hire_offer)
                                <div class="offers__row">
                                    <div class="offers__cell offers__cell_small">
                                        <p class="date"> {{$hire_offer->created_at->format('d,M,Y')}}</p>
                                        <p class="attribute attribute_date">{{$hire_offer->created_at->format('H:i')}}</p>
                                    </div>
                                    <div class="offers__cell offers__cell_big">
                                        <h3 class=""><span class="text"><a href="{{route('vendor.hire.show', ['id' => $hire_offer->id])}}">{{ucfirst($hire_offer->job->name)}}</a></span></h3>
                                        <p class="attribute">{{$hire_offer->job->user->firstName}} {{$hire_offer->job->user->lastName}}</p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </main>
    @else
        <main class="content">
            <div class="container">
                <div class="row">
                    <h1 class="title">MY PROPOSALS</h1>
                    <div class="offers">
                        <div class="offers__head">
                            <h3 class="title title__offers">{{$hires->count()}} offers</h3>
                            <span class="offers__active"></span>
                        </div>
                        <div class="offers__body">
                            <div class="offers__row offers__row_head">
                                <div class="offers__cell offers__cell_small offers__cell_title">Received</div>
                                <div class="offers__cell offers__cell_big offers__cell_title">Title</div>
                            </div>
                            <div class=" text-center">
                                <div class="row">
                                <div class="col-md-9">
                                <h3 class="">No proposals!</h3>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endif
    @if($teamhire->isNotEmpty())
    <main class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('hire_flash_accept'))
                            <div class="alert alert-success text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{Session::get('hire_flash_accept')}}.
                            </div>
                        @endif
                        @if (Session::has('hire_flash_decline'))
                            <div class="alert alert-warning text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{Session::get('hire_flash_decline')}}.
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <h1 class="title">Team Hires</h1>
                    <div class="offers">
                        <div class="offers__head">
                            <h3 class="title title__offers">{{$teamhire->count()}} offers</h3>
                            <span class="offers__active"></span>
                        </div>
                        <div class="offers__body">
                            <div class="offers__row offers__row_head">
                                <div class="offers__cell offers__cell_small offers__cell_title">Received</div>
                                <div class="offers__cell offers__cell_big offers__cell_title">Title</div>
                            </div>
                            @foreach($teamhire as $hire_offer)
                                <div class="offers__row">
                                    <div class="offers__cell offers__cell_small">
                                        <p class="date"> {{$hire_offer->created_at->format('d,M,Y')}}</p>
                                        <p class="attribute attribute_date">{{$hire_offer->created_at->format('H:i')}}</p>
                                    </div>
                                    <div class="offers__cell offers__cell_big">
                                        <h2 class="subject"><span class="text"><a href="{{route('vendor.hire.show', ['id' => $hire_offer->id])}}">{{ucfirst($hire_offer->job->name)}}</a></span></h2>
                                        <p class="attribute">{{$hire_offer->job->user->firstName}} {{$hire_offer->job->user->lastName}}</p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </main>
    @else
        <main class="content">
            <div class="container">
                <div class="row">
                    <h1 class="title">Team Hires</h1>
                    <div class="offers">
                        <div class="offers__head">
                            <h3 class="title title__offers">{{$hires->count()}} offers</h3>
                            <span class="offers__active"></span>
                        </div>
                        <div class="offers__body">
                            {{-- <div class="offers__row offers__row_head">
                                <div class="offers__cell offers__cell_small offers__cell_title">Received</div>
                                <div class="offers__cell offers__cell_big offers__cell_title">Title</div>
                            </div> --}}
                            <div class=" text-center">
                                <div class="row">
                                <div class="col-md-12">
                                <h3 class="">No hire!</h3>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endif
    </div>
@endsection