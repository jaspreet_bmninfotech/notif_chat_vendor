@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
	<div class="row">
		<div class="col-md-3">
			<div class="sidebar_widget">
				<div class="sidebar_title"><h4>Jobs</h4>
				</div>
				<ul class="arrows_list1">
				 <li><a href="{{route('vendor.dashboard')}}"><i class="fa fa-angle-right"></i> Find jobs</a></li>
					<li><a href="{{route('vendor.bids')}}"><i class="fa fa-angle-right"></i> Saved jobs</a></li>
					<li><a href="{{route('vendor.invites.list')}}"><i class="fa fa-angle-right"></i>Invite Jobs</a></li>		
					<!-- <li><a href="profile"><i class="fa fa-angle-right"></i> Find jobs</a></li>
					<li><a href="business"><i class="fa fa-angle-right"></i> Saved jobs</a></li>
					<li><a href=""><i class="fa fa-angle-right"></i> Jobs invite</a></li>
					<li><a href="payment"><i class="fa fa-angle-right"></i> My status</a></li>
					<li><a href="payment"><i class="fa fa-angle-right"></i> Transanction</a></li> -->
				</ul>	
			</div>
		</div>
		<div class="vendor-job-list form-fields panel-group col-md-9">
			<div class="vfform" id="save-job">
				<div class="title">
					<h3>Jobs</h3>
				</div>
				<form role="form"  >
					<table class="table vf-saved-job" id="jobList">
						<thead>
							<tr class="">
								<th width="35%">JOB TITLE</th>
								<th width="25%">RATE</th>
								<th width="20%">ORGANIZER</th>
								<th width="20%">ACTIONS</th>
							</tr>
						</thead>
						<tbody class="">
						</tbody>
					</table>		
				</form>
			</div>
			<span class="text-center job-show-more"><h3 class="bottom-border" id="show-more">Show More Jobs</h3></span>
		</div>
	</div>
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <a href="" class="pull-left" data-dismiss="modal">Back to Jobs</a>
	        <a target="_blank" href="" class="pull-right">Open in new window</a>
	      </div>
	      <div class="modal-body" id="quickjobdetailsdata">
	      
	      </div>
	    </div>

	  </div>
	</div>
</div>
@endsection
@section('addjavascript')
<script type="text/javascript" src="/js/vendor-job.js"></script>
<script type="text/javascript" src="/js/quick-view-jobs.js"></script>
<script type="text/javascript" id="jobsApplieds">
	var jobsApplied = {{($jobsApplied)}};
</script>
<script type="text/template" id="jobListTmpl">     
    <% _.each(data,function(v,i,l){ %> 
	    <tr class="bottom-border">
		    <td>
		        <h3><%= v.name %></h3>
		        <%
		        	var date = new Date(v.created_at);

		        %>
		        <span>Posted at: <%= (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear() %></span>
		    </td>
		    <% if(v.isHourly == 1){ %>
		    <td>Hourly:<%= v.minRate %>k - <%= v.maxRate %>K/Hr</td>
		    <% }else{ %>
		    <td>Fixed:<%= v.budget %>K</td>
		    <% } %> 
		    <td><%= v.username %></td>
		    <td>
		    	<% if(!_.contains(jobsApplied,parseInt(v.id))) {	%>
			        <a href="/vendor/job/<%= v.id %>/details" id="applyToJob" class="button solid">Apply To Job</a>
			        <button type="button" class="button solid" onclick="quickJobDetails(<%= v.id %>)" data-toggle="modal" data-target="#myModal" >Quick View</button>
			        <% }else{ %>
			        <a href="/vendor/proposal/<%= v.id %>/details" id="" class="button solid">View Proposal</a>
			        <% } %>
		    </td>
		</tr>
    <% }) %>

</script>
<script type="text/template" id="quickjobdetailsTmpl">
	<div class="vf-wrap modal-job-deatils" >
		<div class="job-heading" >
			<h2><strong><%= data.name %></strong></h2>
			<%
                  var date = new Date(data.created_at);
            %>
			<span><strong>Posted at:</strong>&nbsp;<%= (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear() %></span>
		</div><br>
		<div class="job-description">
			<div class="col-md-8 border-right">
				<div class="job-text">
					<p><%= data.description %></p>
				</div>
				<div class="job-info">
					<h4><strong>Project Type:</strong>One-time project</h4>
				</div>
			</div>
			<div class="col-md-4 jo-requirement">
				<div class="job_budget">
					<p>$<%= data.budget %></p>
					<h5>Fixed Price</h5>
				</div>
				<div class="job_budget">
				<%
                    var date = new Date(data.startDateTime);

                %>
					<p> <%= (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear() %></p>
					<h5>Start Date</h5>
				</div>
			</div>
		</div>
		
	</div>
	<div class="text-center">
			<a href="/vendor/job/<%=data.id %>/proposal"><button class="button solid button-margin">Place Bid</button></a>
		</div>					
</script>
@endsection