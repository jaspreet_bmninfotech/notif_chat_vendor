@extends('layouts/base')
@section('content')
	
<div class="container job-deatils-page form-center">
	@if(Session::has('success'))
	    <div class="alert alert-success col-md-8">
	      <p style="color:green;">{{Session::get('success')}}</p>
	      @php Session::forget('success'); @endphp
	    </div>
	@endif
	<div class="row">
				<div class="col-md-8">
		<div class="vfform">
			<div class="title">
	                    <h3>Team List</h3>
	                </div>
			@if($team)
					<div class="job-detail-panel teamp">
						@foreach($team as $teams)
							<div class="vf-wrap job-deatils">
								<div class="col-md-9">
									<h1>{{$teams['name']}}</h1>
									<div>
										<span>Members :</span>
										<span>{{$teams['teammember']->count()}}</span>
									</div>
								</div>
								<div class="col-md-3">
									<div class="top-margin">
										<a href="{{ route('vendor.view.team', ['id' => $teams->id ])  }}" class="button solid">View Details</a>
			                            <!-- <a href="javascript:;" class="button default" onclick="declined()">Declined </a> -->
			                    	</div>
								</div>
							</div>
						@endforeach
					</div>
					
			@endif
		</div>
		</div>
	</div>
</div>
@endsection