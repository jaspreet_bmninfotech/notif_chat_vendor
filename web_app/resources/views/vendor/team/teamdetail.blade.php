@extends('layouts/base')
@section('content')
<div class="job-posting-form container">	
	@if(Session::has('success'))
	    <div class="alert alert-success">
	      <p style="color:green;">{{Session::get('success')}}</p>
	      @php Session::forget('success'); @endphp
	    </div>
	@endif
	<div class="form-fields col-md-12">
		<form role="form" method="POST" action="{{route('vendor.team.edit',$team['id'])}}" autocomplete="off">
			{!! csrf_field() !!}
			<div class="vfform">
				<div class="title">
					<h3>Edit {{$team['name']}}</h3>
				</div>
				<div class="feildcont">
						<div class="row">
							<div class="col-md-12">
								<table class="table vf-saved-job" id="teammember">
									<?php
					                if($member->isEmpty()) {
					                    ?>
					                 <div>
					                    <div class="col-md-12">
					                        <div class="row">
					                            <div class="top-bot-margin1">
					                                <div class="col-md-12 text-center ">
					                                    <h3>Currently you have no team</h3>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					                <?php
					                 }else{ ?>
									<thead>
										<tr class="">
											<th width="5%">Sno</th>
											<th width="30%">Name</th>
											<th width="15 %">Status</th>
											<th width="20%">joined At</th>
											<th width="15%">ACTIONS</th>
										</tr>
									</thead>
									<tbody class="">	
									@foreach($member as $key=>$value)
										<tr class="bottom-border" id="team{{$value->id}}">
											<td>{{ $loop->iteration }}</td>
											<td>
												<h3>{{$value->username}}</h3>
											</td>
											 <?php
                                        if ($value->status == '0'){
                                            ?>
                                            <td><p>Declined</p></td>
                                        <?php
                                        } elseif ($value->status == '1'){
                                        ?>
                                       <td> <p>Accepted</p></td>
                                        <?php
                                        }elseif ($value->status == '2'){
                                            ?>
											<td><p>Pending</p></td>
										<?php
                                        }
                                        ?>
											<td>{{date('F j,Y, h:i ',strtotime($value->created_at))}} </td>
											<td><button class="btn btn-danger" onclick="deleteteammember({{$value->id}})"><span class="fa fa-times"></span></button></td>	
										</tr>
										@endforeach
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>
				</div>
			</div>
		@if($member->count() < 5 || $member->count() > 0 )
			<div class="vfform">
				<div class="feildcont">
					<div class="row">
						<div class="col-md-12">

							<h3 class="bottom-margin">Invite Member</h3>
							@php
								$countDiv = (5 - $member->count());
							@endphp
							@if($countDiv <= 5)
								@for ($i = 0; $i < $countDiv; $i++)
								<div class="row">
									<div class="col-md-6">
										<input type="text" placeholder="add name of member"  id="typeahead" onSelect= "function(item)"  name="member[]" >
									</div>
									<div class="col-md-6">
										<textarea name="description" placeholder="description" rows="3"></textarea>
									</div>
								</div>
								@endfor
							@endif
						</div>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" id="editteam" class="button solid" > update member</button>
				</div>
				<br>
			</div>
		@endif
		</form>
	</div>
</div>
@endsection
@section('addjavascript')
<script src="{{asset('js/team.js')}}"></script>
@endsection