@extends('layouts/base')
@section('content')
<div class="job-posting-form container">	
	@if(Session::has('success'))
	    <div class="alert alert-success">
	      <p style="color:green;">{{Session::get('success')}}</p>
	      @php Session::forget('success'); @endphp
	    </div>
	@endif
	<div class="form-fields col-md-7">
		<div class="vfform">
			<div class="title">
				<h3>Create A Team</h3>
			</div>
			<div class="feildcont">
				<form role="form" method="POST" action="{{route('vendor.team.add')}}" autocomplete="off">
					{!! csrf_field() !!}
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<label>Name <em>*</em></label>
									<input type="text" name="name" value="">
									@if($errors->has('name'))
									<span class="text-danger">{{ $errors->first('name') }}</span>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Description<em>*</em></label>
									<input type="text" name="description" value="" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Invite Members</label>
										<div class="row">
											<div class="col-md-6">
												<input type="text" placeholder=""  id="typeahead" onSelect= "function(item)"  name="member[]" data-id="">
											</div>
											<div class="col-md-6 ">
												<input type="text" placeholder=""  id="typeahead" onSelect= "function(item)"  name="member[]" value="" data-id="">
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<input type="text" placeholder=""  id="typeahead" onSelect= "function(item)"  name="member[]" data-id="">
											</div>
											<div class="col-md-6 ">
												<input type="text" placeholder=""  id="typeahead" onSelect= "function(item)"  name="member[]" value="" data-id="">
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<input type="text" placeholder=""  id="typeahead" onSelect= "function(item)"  name="member[]" data-id="">
											</div>
											
										</div>
											     
								</div>
							</div>
						</div>
					</div>
					<div class="text-center">
						<button type="submit" id="teamsave" class="button solid">Save Team</button>
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('addjavascript')
<script src="{{asset('js/team.js')}}"></script>
@endsection