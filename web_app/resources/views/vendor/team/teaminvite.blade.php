@extends('layouts/base')
@section('content')
<div class="container job-deatils-page">	
	@if(Session::has('success'))
	    <div class="alert alert-success">
	      <p style="color:green;">{{Session::get('success')}}</p>
	      @php Session::forget('success'); @endphp
	    </div>
	@endif
	<div class="form-fields col-md-12">
		<form role="form" method="POST">
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-md-12">
					<div class="vfform">
						<div class="title">
				            <h3>Invite for Team</h3>
		                </div>
		                <?php
						if(empty($data)) {
							?>
						<div>
							<div class="vf-bids">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-12 text-center ">
											<h3>Currently you have no team invite</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						 }else{ ?>
            			@foreach($data as $k => $v)
							<div class="job-detail-panel">
								<div class="vf-wrap job-deatils">
									<div class="col-md-12 row">
										<div class="col-md-5">
											<h1 class="namecapitalise">{{$v['name']}}</h1>
											<div>
												<span>Members :</span>
												<span>{{$v['members']}}</span>
											</div>
										</div>
                                    	<div class="col-md-3">
											<div class="top-margin">
												 <?php
	                                        if ($v['memberstatus'] == '0'){
	                                            ?>
	                                            <p>Declined</p>
	                                        <?php
	                                        } elseif ($v['memberstatus'] == '1'){
	                                        ?>
	                                        <p>Accepted</p>
	                                        <?php
	                                        }elseif ($v['memberstatus'] == '2'){
	                                            ?>
										 	<a href="javascript:;" class="button solid button-margin" onclick="accept({{$v['member_id']}})" >Accept</a>
	                                        <a href="javascript:;" class="button default" onclick="declined({{$v['member_id']}})">Declined </a>
	                                        <?php
	                                        }
	                                        ?>
					                    	</div>
										</div>
										<div class="col-md-1 col-sm-1 ">
	                                            <img src="{{asset($v['profile_image'])}}" class="img-responsive img-circle">
	                                    </div>
                                        <div class="col-md-3">
										<h3 class="namecapitalise">{{$v['adminName']}}</h3>
                                        	<div>{{date('F j,Y, h:i ',strtotime($v['created_at']))}}</div>
                                       </div>
                                    </div>
   								</div>
							</div>	
						@endforeach
						<?php }?>
					</div>
				</div>
			</div>	
		</form>
	</div>
</div>
@endsection
@section('addjavascript')
<script src="{{asset('js/team.js')}}"></script>
@endsection