@extends('../layouts/base')
  @section('content')

    <link rel="stylesheet" type="text/css" href="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/src/css/star-rating-svg.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/demo/css/demo.css')}}">
		<div class="container job-deatils-page ">
			<form method="post">
				{!! csrf_field() !!}
				<h2>{{$job['name']}}</h2>
				<h4>Share your experience! Your honest feedback provides helpful information to both the client.</h4>
				<div class="row">
					<div class="col-md-12">
						<div class="job-detail-panel">
							<div class="vf-wrap job-deatils">
								<div class="job-heading">
									<div class="">
										<div class="col-md-1">
											<div class="loudicon">
											<i class="fa fa-bullhorn" aria-hidden="true" ></i> 
												
											</div>
										</div>
										<div class="col-md-11">
											 <h3>Public Feedback</h3>
											 <p>Shared on your client's profile only after they've left feedback for you.<a href="">learn more</a> </p>
										</div>
									</div>
									<br clear="all">
									<div class="top-bottom-margin stars-total">
										<h4>Feedback to Client</h4>
											<div class="skill jq-stars" data-rating="0.1"></div><label>Skills</label>
											<div class="quality jq-stars" data-rating="0.1"></div><label>Quality of Requirements</label>
											<div class="availability jq-stars" data-rating="0.1"></div><label>Availability</label>
											<div class="deadlines jq-stars" data-rating="0.1"></div><label>Set Reasonable Deadlines</label>
											<div class="communication jq-stars" data-rating="0.1"></div><label>Communication</label>
											<div class="cooperation jq-stars" data-rating="0.1"></div><label>Cooperation</label>
											
										<h4>Total Score:<span id="total"></span></h4>
									</div>
									<div class="vf-textarea-halfsize">
										<p>Share your experience with this client:</p>
										@if($errors->has('review'))
									<span class="text-danger">{{ $errors->first('review') }}</span>
									@endif
										<textarea name="review"></textarea>
										<p>See an <a href="" class="feedback_lk"  data-toggle="modal" data-target="#myModal" >example of appropriate feedback</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="topbottom-margin">
					<button class=" button solid">End Contract</button>
					<a href="{{route('vendor.contract.detail', ['id' => $contract->id])}}"><button type="button" class=" button default">Cancel</button></a>
				</div>
			</form>
		</div>
@endsection
@section('addjavascript')
<script type="text/javascript" src="/js/vendor-job.js"></script>
<script type="text/javascript" src="/js/quick-view-jobs.js"></script>
<script type="text/template" id="quickjobdetailsTmpl">
	<div class="vf-wrap modal-job-deatils" >
		<div class="job-heading" >
			<h2><%= data.name %></h2>
			<label>Editing & proofreading</label>
			<span>&nbsp;<%= data.isHourly %> ago</span>
		</div>
		<div class="job-description">
			<div class="col-md-8 border-right">
				<div class="job-text">
					<p><%= data.description %></p><br/>
					<p>Payment:about 10 $(Depends on your capabilities If you have any suggestions. do not be afraid to offer them!)</p><br/>
					<p>Thank you!</p><br/>
				</div>
				<div class="job-info">
					<label>Attachment</label>
					<h3>@Raw food.doc(9.3mb)</h3>
					<h4>Project Type:One-time project</h4>
					<p>You will be asked to answer the following questions when submitting a proposal:</p>
						<li>1. Do you have any suggestion for my job?</li>
				</div>
			</div>
			<div class="col-md-4 jo-requirement">
				<div>
					<h4>$<%= data.maxRate %></h4>
					<p>Fixed Price</p>
				</div>
				<div>
					<h4>Intermediate Level</h4>
					<p>I am looking for a mix of experience and value</p>
				</div>
				<div>									
					<h4><%= data.created_at %></h4>
					<p>Start Date</p>
				</div>
			</div>
		</div>
		
	</div>
	<div class="text-center">
			<a href="/vendor/job/<%=data.id %>/proposal"><button class="button solid button-margin">Place Bid</button></a>
		</div>					
</script>
<script src="{{asset('js/rating.js')}}"></script>
<script src="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/src/jquery.star-rating-svg.js')}}"></script>

@endsection