@extends('../layouts/base')
  @section('content')
 <style>
        .form-group label{
            float: left
        }
        .form-group input{
            width: 14%;
        }
        
        div label {
            width: 22%
        }
        .job-deatils label{
        	width: 100%
        }
        .card{

            display: none;
		    padding: 10px;
		    box-shadow: 0 1px 6px rgba(57,73,76,.35);
		    background: #fff;
		    margin-top: 14px;
        }
        .alert-info{
			width: 11%;
		    position: fixed;
		    right: 0;
		    color: white;
		}
    </style>
    <div class="alert-info"></div>
		<div class="container job-deatils-page">
			<div class="job-heading">
				<h2>{{$data['name']}}</h2>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="job-detail-panel">
                        <div class="vf-wrap job-deatils">
                            <div class="col-md-4 border-right">
                                <label>Agreed</label>
                                    <div>
                                        <h1 class="total">${{$hires->rate}}</h1>
                                    </div>
                            </div>
                            <div class="col-md-4 border-right">
                                <label>Paid</label>
                                    <div>
                                        <h1 class="totalPaid">${{$totalPaid}}</h1>
                                    </div>
                            </div>
                            <div class="col-md-4">
	                            <label>Requested Amount</label>
	                                <div>
	                                    <h1 >$<span class="Requested">{{$pendingAmount}}</span></h1>
	                                </div>
	                        </div>

                            <!-- <div class="col-md-3">
                                <label>Remaining</label>
                                    <div>
                                        <h1 class="totalPending">${{ (($hires->rate - $totalPaid)-$pendingAmount) }}</h1>
                                    </div>
                            </div> -->
                            <input type="hidden" id="pendingAndPAid" value="{{$totalPaid}}">
                        </div>
                        	@if($jobTransaction)
								{{-- <table class="table table-bordered" style="background: #fff">
									<thead>
										<tr>
										    <th>Amount</th>
										    <th>Fund Requested</th>
										    <th>Fund Approved</th>
										    <th>Request Release</th>
										    <th>Approve Release</th>
										    <th>Status</th>
										</tr>
									</thead>
									<tbody>
										@foreach($jobTransaction as $k => $v)
										<tr>
											<td> %{{$v->amount}} </td>
											<td class="fund_requested">  {{$v->request_fund}} </td>
											<td class="approve_fund"> {{$v->approve_fund}} </td>
											<td id="request_release"> {{$v->request_release}} </td>
											<td> {{$v->approve_release}} </td>
											@if($v->approve_fund != null)
												
												<td><button class="btn btn-success {{($v->request_release)?'':'request_release'}}" transaction-id="{{$v->id}}" {{($v->request_release)?'disabled':''}}>Request Release</button></td>
											@else
												<td><button class="btn btn-success release_fund" transaction-id="{{$v->id}}" disabled="disabled">Requested</button></td>
											@endif
										</tr>
										@endforeach
									</tbody>
								</table> --}}
							@endif
	                    </div>

                        <a href="javascript:;" class="btn btn-success releaseRequest" hire_id="{{ $hires['id'] }}">Request to release the funds</a>
	                    <input type="hidden" name="jobTransaction" value="{{ $jobTransaction['id'] }}">
	                    <div class="row">
	                    	<div class="modal fade" id="myModal" role="dialog" style="display: none">
						    	<div class="modal-dialog">
							      	<div class="modal-content">
							        	<div class="modal-header" style="text-align: center;">
							          		<button type="button" class="close" data-dismiss="modal">&times;</button>
							          		<h4 class="modal-title"> Your bank account is not Added, please add bank details to get funds </h4>
							        	</div>
							        	<div class="modal-body PaymentForm">
							          		<!-- <form action="/action_page.php"> -->
							          			<div class="card-error"></div>
											  	<div class="form-group">
												    <label style="width: 100%" for="account_name">Account Name</label>
												    <input style="width: 100%" type="text" class="form-control" value="Sandeep singh" name="account_name" id="account_name">
												    <div class="error"></div>
											  	</div>
											  	<div class="row">
											  		<div class="col-md-4">
													  	<div class="form-group">
														    <label style="width: 100%" for="bank_name">Bank Name</label>
														    <input style="width: 100%" type="text" placeholder="Bank Name" value="test bank name" name="bank_name" class="form-control" id="bank_name">
														    <div class="error"></div>
													  	</div>
											  		</div>
											  		<div class="col-md-4">	
													  	<div class="form-group">
														    <label style="width: 100%" for="routing_number">Routing Number</label>
														    <input style="width: 100%" type="text" placeholder="110000000" value="110000000" name="routing_number" class="form-control" id="routing_number">
														    <div class="error"></div>
													  	</div>
											  		</div>
											  		<div class="col-md-4">
													  	<div class="form-group">
														    <label style="width: 100%" for="account_number">Account Number</label>
														    <input style="width: 100%" type="text" placeholder="Account Number" value="000123456789" name="account_number" class="form-control" id="account_number">
														    <div class="error"></div>
													  	</div>
											  		</div>
											  	</div>
								        		<button class="btn btn-success add_bank_account " transaction-id="" >Add Bank Account</button>
						        		</div>
							      	</div>
						    	</div>
						  	</div>
	                    </div>

                    	<!-- <button class="btn btn-success request_for_fund">Request for fund Payment</button>
					    <div class="card">
					        <div class="card-body">
				            	<input type="hidden" name="vendor" value="{{$vendor->id}}">
				            	<input type="hidden" name="client" value="{{$client->id}}">
				            	<input type="hidden" name="hire" value="{{$hires->id}}">
				                <div class="form-group">
				                    <label for="total">Total -</label>
				                    <input type="text" readonly="readonly" class="form-control" id="total">
				                </div>
				                <div class="form-group">
				                    <label for="funded">Paid / Funded -</label>
				                    <input type="text" readonly="readonly" class="form-control" id="funded">
				                </div>
				                <div class="form-group">
				                    <label for="pending">Pending -</label>
				                    <input type="text" readonly="readonly" class="form-control" id="pending">
				                </div>
				                <div class="form-group">
				                    <label for="requested">Requested -</label>
				                    <input type="text" class="form-control" placeholder="%" id="requested" name="fundRequest" style="float: left;">
				                    <div style="float: left;line-height: 40px;">&nbsp; % </div>
				                    <span class="requestedStatus"><i style="color:green;font-size: 25px;padding: 5px;" class="fa f"></i></span>
				                    <div class="clearfix"></div>
				                </div>
				                <div class="form-group">
				                    <label for="vffees">VF Fees -</label>
				                    <input type="text" readonly="readonly" class="form-control" id="vffees" value="{{$serviceFee}}">
				                </div>
				                <div class="form-group">
				                    <label for="getamount">You Will get -</label>
				                    <input type="text" readonly="readonly" class="form-control" id="getamount">
				                </div>
				                <div class="form-group">
				                    <button class="btn btn-success requestFund">Submit</button>
				                </div>
					        </div>
					    </div> -->
				</div>
				<div class="col-md-4 client-details">
					<div class="row">
						<div class="job-detail-panel bottom-margin">
							<div class="job-detail-panel rightbar-padding ">
								<h3>Client</h3>
							</div>
							<div class="rightbar-padding contract-sidebar">
								<div class="col-md-4">
									<img src="{{asset('images/vf-si-boy.png')}}">
								</div>
								<div class="col-md-8">
									<label class="">{{$client['username']}}</label>
									<p class="">{{$bussiness['name']}}</p>
								</div>
							</div>
							<br clear="all">
							<div class="job-detail-panel rightbar-padding">
								<a href="{{ route('chat',$jobTransaction['id']) }}"><button class="button solid ">Message</button></a>
							</div>
						</div>
						<div class="job-detail-panel bottom-margin">
							<div class="job-detail-panel rightbar-padding ">
								<h3>Staffed By</h3>
							</div>
							<div class="rightbar-padding contract-sidebar">
								<div class="col-md-4">
									<img src="{{asset('images/people-vendor-128.png')}}" >
								</div>
								<div class="col-md-8">
									<label>{{$vendor['username']}}</label>
									
								</div>
							</div>
							<br clear="all">
							<div class="job-detail-panel rightbar-padding">
								<button class="button solid ">Message</button>
							</div>
						</div>
						
						<div class="job-detail-panel bottom-margin">
							<div class="job-detail-panel rightbar-padding ">
								<h3>Contract Details</h3>
							</div>
							<div class="rightbar-padding text-center">
								<?php if($hires['status']==2){
									?>
									<h3>ACTIVE</h3>
								<label>{{date("m/d/Y", strtotime($hires->created_at))}}</label>
									<?php	
								}else if($hires['status']==0){
									?>
									<h3>CLOSED</h3>
								<label>{{date("m/d/Y", strtotime($hires->updated_at))}}</label>
									<?php
								}
								?>
							</div>
							<div class="job-detail-panel rightbar-padding">
								<?php if($hires['status']==1 || $hasfeedback==NULL){
									?>
								<a href="{{route('vendor.contract.closed', ['id' => $hires['id']])}}"><button class="button solid ">End contract</button></a>
								<?php	
								}else{
									?>
								<a href="{{route('vendor.contract')}}"><button class="button solid ">Closed</button></a>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		 $(document).ready(function () {
		 	if($('.total').html().substr(1) == $('.totalPaid').html().substr(1)){
		 		$('.request_for_fund').remove();
		 	}
            $(document).on('click','.request_for_fund', function () {

                var totalAmount = $('.total').html();
                var pendingAndPAid = $('#pendingAndPAid').val();
                var totalPaid = $('.totalPaid').html();
                var Requested = $('.Requested').html();
                var totalPending = $('.totalPending').html();
                $('#total').val(totalAmount);
                $('#funded').val(totalPaid);
                $('#pending').val(totalPending);
                // console.log((+(totalPending.substr(1))- +Requested));
                $('.card').toggle();
            });
            $(document).on('keyup','#requested', function(){

                $('.requestedStatus').html('');
                if($(this).val() <= 100){
                    $('.requestedStatus').html('<i style="color:green;font-size: 25px;padding: 5px;" class="fa fa-check"></i>');
                    var totalAmount = $('.total').html();
                    totalAmount = totalAmount.substr(1);
                    var vffees = $('#vffees').val();
                    if($('#funded').val() == '$0'){
                    	// alert($('#funded').val());
                    	var requestTofund = $(this).val();
                    					
                    	var requested = (totalAmount * requestTofund)/100;
                    	var getvffees = (((totalAmount * requestTofund)/100) *vffees) /100;
                    	var deductvffees = requested - getvffees;

                    	var getsffees=(deductvffees*2.9)/100 + 0.30;
                    	var getAmount = deductvffees - getsffees;
                    }else{
                        var funded = $('#funded').val();
                        funded = funded.substr(1);//3000
                        var restAmount = $('#pending').val();//7000

                        var restvffees =  (((restAmount.substr(1) * $('#requested').val())/100)*$('#vffees').val())/100;//280
                        var requestTofund = $(this).val();//20%
                        var requested = (restAmount.substr(1) * requestTofund)/100;//1400
                        var deductvffees = requested - restvffees;//1120
                       	var getsffees=(deductvffees*2.9)/100 + 0.30;
                        var getAmount = deductvffees-getsffees ;
                    }
                        $('#getamount').val(getAmount);
                }else{
                    $('.requestedStatus').html('<i style="color:red;font-size: 25px;padding: 5px;" class="fa fa-times"></i>');
                    $('#getamount').val('');

                }

            });

            $(document).on('click', '.requestFund', function(e){
            	e.preventDefault();

            	var askedAmount  =  $('.card input[name = fundRequest]').val();
            	var _token = $('input[name=_token]').val();
            	var vendor = $('input[name=vendor]').val();
            	var client = $('input[name=client]').val();
            	var hire = $('input[name=hire]').val();
            	$.ajax({
            		url : '{{ route("fund.request") }}',
            		type : 'post',
            		data : {_token : _token , requestFund : askedAmount , vendor : vendor , client : client , hire : hire},
            		success : function(data){
            			console.log(data);
						$('.alert-info').html('<div style="background: rgb(92, 184, 92); opacity:0.5;padding: 10px">Request Successfully send</div>')
						setTimeout(function() {
							$('.alert-info').hide();
						}, 3000);
						location.reload();
            		}
            	});
            });
            $(document).on('click', '.request_release', function(e){
            	e.preventDefault();
            	var d = new Date();
				var curr_date = d.getDate();
				var curr_month = d.getMonth();
				var curr_year = d.getFullYear();
				
            	var token = $('input[name=_token]').val();
            	var transactionId = $(this).attr('transaction-id');
            	$.ajax({
            		url : '{{ route("fund.request") }}',
            		type : 'post',
            		data : { _token : token , transactionId : transactionId },
            		success : function(data){
            			console.log(data);
            			if( data == 'noBankAccount'){
            				$('.modal').modal('show');
            				$('.add_bank_account').attr('transaction-id' , transactionId);
            			}
            			if(data){
            				// $('button[transaction-id = '+transactionId+']').removeClass('request_release').attr('disabled','disabled');
	            			$('button[transaction-id = '+transactionId+']').parents('tr').find('#request_release').html(curr_year + "-" + curr_month + "-" + curr_date +' '+ d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
	            			
	            			var newRequested =$('.Requested').html();
		 					var newtotalPaid =$('.totalPaid').html();
		 					var p= parseFloat(newtotalPaid.substr(1)) + parseFloat(newRequested);
		 					alert(p);
		 					$('.totalPaid').html('$'+p);
	            			
            			}

            		}
            	});
            });

            $(document).on('click','.add_bank_account', function(e){
            	e.preventDefault();
            	if( typeof  $('input[name = account_name]').val() == "string" ){
						var account_name = $('input[name = account_name]').val();           			
            		if( typeof  $('input[name = bank_name]').val() == "string" ){
						var bank_name = $('input[name = bank_name]').val();	
            		}else{
            			$('input[name = bank_name]').siblings('.error').html(" <span class='hasError'>Bank Name must be a Name</span>");
            		}
            	}else{
            		$('input[name = account_name]').siblings('.error').html(" <span class='hasError'>Account Name must be a Name</span>");
            	}
            	if( typeof Number($('input[name = routing_number]').val()) == "number"){
	            		var routing_number = $('input[name = routing_number]').val();
	            	if( typeof Number($('input[name = account_number]').val()) == "number"){
	            		var account_number = $('input[name = account_number]').val();
	            	}else{
	            		$('input[name = routing_number]').siblings('.error').html(" <span class='hasError'>Routing Number must be a number</span>");
	            	}
            	}else{
            		$('input[name = account_number]').siblings('.error').html(" <span class='hasError'>Account Number must be a number</span>");
            	}
            	
            	var token = $('input[name=_token]').val();

            	var accountDetails = [];
            	accountDetails.push({account_name : account_name, bank_name : bank_name, routing_number : routing_number, account_number : account_number});
            	$.ajax({
            		url : '{{ route("fund.request") }}',
            		type : 'post',
            		data : { _token : token ,transactionId : $('input[name=jobTransaction]').val(), accountDetails : accountDetails },
            		success : function(data){
            			if(data == 'true'){
	            			$('#myModal').modal('close');
	            			// $('#myModal').find('button.close').click();
            			}

            		},
            		error : function(xhr, status, error){
						$('.card-error').html("<span style='color:red'>"+xhr.responseJSON.message+"</span>");
						$('.circle-loader').hide();
					}
            	});
            });
            $(document).on('click','.releaseRequest',function(e){
            	var hire_id = $(this).attr('hire_id');
            	var token = $('input[name=_token]').val();

            	$.ajax({
            		url : '{{ route("fund.request") }}',
            		type : 'post',
            		data : { _token : token ,transactionId : $('input[name=jobTransaction]').val(),requestFund : 'requestFund', hire_id : hire_id },
            		success : function(data){
            			if(data == 'no bank details'){
            				$('#myModal').modal('show');
            			}
            			alert('Request sent successfully');

            		}
            	});

            });
        });
	</script>
@endsection