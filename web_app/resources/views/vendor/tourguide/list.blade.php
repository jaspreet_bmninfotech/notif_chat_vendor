@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
    <div class="row">
        <div class="vendor-job-list form-fields panel-group">
            <div class="vfform margin-zero" id="">
                <div class="title">
                    <h3>Tour Guide Jobs</h3>
                </div>
                <div class="">
                    <div class="col-md-3">
                        <h4>Title</h4>
                    </div>
                    <div class="col-md-3">
                        <h4>Status</h4>
                    </div>
                    <div class="col-md-4">
                        <h4>SENT</h4>
                    </div>
                    <div class="col-md-2">
                        <h4>ACTION</h4>
                    </div>
                </div>
                <?php
                if(!$data) {
                    ?>
                 <div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="vf-bids">
                                <div class="col-md-12 text-center ">
                                    <h3>Currently you have no bid</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                 }else{ ?>
            @foreach ($data as $jobs)
                <div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="vf-bids">
                                <div class="col-md-3 ">
                                    <div class="bid-detail">
                                        {{$jobs->title}}
                                    </div>
                                </div>
                                <div class="col-md-3 ">
                                    <div class="bid-detail">
                                        <?php
                                        if ($jobs->status == '1') {
                                              ?>
                                        <p>Open</p>
                                        <?php
                                        } elseif ($jobs->status == '0') {
                                            ?>
                                        <p>Close</p>
                                            <?php
                                        } elseif ($jobs->status == '2') {
                                            ?>
                                        <p>Expired</p>

                                            <?php
                                        }
                                        ?>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="bid-detail">
                                        {{date("m/d/y", strtotime($jobs->created_at))}}
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="bid-detail">
                                        <a href="/vendor/tourguide/{{$jobs->id}}/detail"><button class="button solid">View Job Detail  </button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach 
            <div class="paginate push-right">  
                                {{ $data->render() }}
                    </div>  
            <?php }?>
            </div>
        </div>
    </div>
</div>
        

  @endsection