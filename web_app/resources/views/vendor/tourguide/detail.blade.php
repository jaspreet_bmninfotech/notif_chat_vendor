@extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
    @endsection
    @section('content')
    <div class="container client-content">
        @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{Session::get('hire_flash_success_msg')}}.
                </div>
            @endif

        @if (Session::has('hire_flash_err_msg'))
            <div class="alert alert-warning text-center">
                <strong>Great!</strong> {{Session::get('hire_flash_err_msg')}}.
            </div>
        @endif
        <div class="job-detail-wrapper">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#view_job_post_div">View Job Post</a></li>
                <li><a data-toggle="tab" href="#review_proposals_div">Review Proposals</a></li>
                <li><a data-toggle="tab" href="#hire_div">Hire</a></li>
            </ul>

            <div class="tab-content col-xs-9">
                <div id="view_job_post_div" class="tab-pane fade in active">
                    <h2>Tour Guide Job Details</h2>
                    <div class="col-xs-12">
                        <div class="col-md-9">
                            <h5><span>{{$data['job_details']->title}}</span></h5>
                            <p><span>Total People</span>:&nbsp;&nbsp;&nbsp;<small>{{$data['job_details']->totalpeople}}</small> 
                            <h5><small>
                        </small></h5>
                        </div>
                        <div class="col-xs-6 jbdtl_div">
                            <p>Status:&nbsp;<span><?php if ($data['job_details']->status == '1') {
                             ?> 
                             <span>Open</span>
                             <?php  
                            }else{
                                ?>
                             <span>Close</span>  
                            <?php
                            }
                            ?></span></p>
                            <p>Posted at:&nbsp;<span> @if( \Carbon\Carbon::parse($data['job_details']->created_at)->diffInMonths(\Carbon\Carbon::now()) <= 10 )
                                    {{ \Carbon\Carbon::parse($data['job_details']->created_at)->diffForHumans() }}
                                @else
                                    {{ \Carbon\Carbon::parse($data['job_details']->created_at)->format('m/d/Y') }}
                                @endif</span></p>
                        </div>
                        <div class="col-xs-6 job_length_div">
                            <p>From Date:&nbsp;<span>{{date("m/d/Y", strtotime($data['job_details']->fromdatetime))}}</span></p>
                            <p>To Date:&nbsp;<span>{{date("m/d/Y", strtotime($data['job_details']->todatetime))}}</span></p>
                        </div>
                        
                        <div class="col-xs-12"><hr></div>

                        <!-- <div class="row">
                            <div class="col-md-6">
                                <h5>Preffered Qualifications</h5>
                                <p>Job Success Score:&nbsp;<span>At least 90%</span></p>
                                <p>Include Rising Talent:&nbsp;<span>Yes</span></p>
                            </div>
                            <div class="col-md-6">
                                <h5>Activity on this Job</h5>
                                <p>Bids:&nbsp;<span>{{$data['total_bids']}}</span></p>
                            </div>
                        </div> -->
                    </div>
                </div>
                
                <div id="review_proposals_div" class="tab-pane fade">
                    <div class="panel-body feildcont field freelancer-listing">
                        <?php
                        if (sizeof($data['job_proposals']) > 0)
                        {
                            foreach ($data['job_proposals'] as $keyAF => $valueAF)
                            {
                            ?>
                                <div class="col-sm-12 freelancer-details row">
                                    <div class="row">
                                        <div class="col-sm-1">
                                            <img src="https://avatars1.githubusercontent.com/u/5931623?v=4" class="img-responsive img-circle">
                                        </div>
                                        <div class="col-sm-9">
                                            <p>
                                                <?php
                                                if($valueAF->firstName != "")
                                                {
                                                    echo $valueAF->firstName . " " . $valueAF->lastName;
                                                }
                                                else
                                                {
                                                    echo $valueAF->userName;
                                                }
                                                ?>
                                            </p>
                                            <p><?php echo substr($valueAF->business_desc, 0, 115); ?>...</p>
                                            <div class="freelancer-earnings-div">
                                                <span><i class="fa fa-usd" aria-hidden="true"></i><?php echo $valueAF->ratePerHour . " / hr"; ?></span>
                                                <span><i class="fa fa-usd" aria-hidden="true"></i>lorem earned</span>
                                                <span>96% Job Success<div class="job-success-progress"><div class="progress-complete"></div><div class="progress-pending"></div></div></span>
                                                <?php
                                                if ($valueAF->address_country != '')
                                                {
                                                ?>
                                                    <span><i class="fa fa-map-marker"></i><?php echo $valueAF->address_country; ?></span>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="{{url('vendor/hire')}}/<?php echo $valueAF->tourguide_job_id . '/' . $valueAF->user_id . '/' . $valueAF->tgi_id; ?>" name="hire_btn" class="button solid invite-to-job pull-right">Hire</a>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        else
                        {
                        ?>
                            <div class="list-empty">
                                <h3 class="text-center">No tourguide found based on your search criteria.</h3>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div id="hire_div" class="tab-pane fade">
                    <?php
                    if (sizeof($data['all_hires']) > 0)
                    {
                        foreach ($data['all_hires'] as $keyAH => $valueAH)
                        {
                    ?>
                            <div class="col-sm-12 freelancer-details row">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <img src="/{{$valueAH->path}}" class="img-responsive img-circle">
                                    </div>
                                    <div class="col-sm-11">
                                        <p>
                                            <?php
                                            if($valueAH->firstName != "")
                                            {
                                                echo $valueAH->firstName . " " . $valueAH->lastName;
                                            }
                                            else
                                            {
                                                echo $valueAH->userName;
                                            }
                                            ?>
                                        </p>
                                        <div class="freelancer-earnings-div">
                                            
                                            <span>Current Status: <?php echo $valueAH->status ? 'Hired' : 'Closed'; ?></span>
                                            <span>Hired on: <?php echo date("d M, Y", strtotime($valueAH->created_at)); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    }
                    else
                    {
                    ?>
                        <p class="text-center">No Hires Yet</p>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <div class="right-pane col-xs-3">
                <ul>
                    <li><a href="{{route('vendor.tourguide.edit',['id'=> $data['job_details']->id])}}" class="button solid"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit Posting &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a href="javascript:void(0);" onclick="vendor_remove_post( {{$data['job_details']->id}} );" rel="" class="button default"><i class="fa fa-times"></i>Remove Posting</a></li>
                </ul>
            </div>
        </div>
    </div>
    @endsection

    @section('addjavascript')
    <script src="{{asset('js/client.js')}}"></script>
    <script src="{{asset('js/tourguide-job.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>

    @endsection