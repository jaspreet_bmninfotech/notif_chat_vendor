@extends('../layouts/base')
  @section('content')

		<div class="container job-deatils-page">
			<div class="row">

				<?php
				
			if (isset($data))
			{
			?>
				<div class="col-md-8">
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils">
							<div class="job-heading">
								<h2>{{$data['name']}}</h2>
								
								<small><b>Posted at</b> : @if( \Carbon\Carbon::parse($data['created_at'])->diffInMonths(\Carbon\Carbon::now()) <= 10 )
                                    {{ \Carbon\Carbon::parse($data['created_at'])->diffForHumans() }}
                                @else
                                    {{ \Carbon\Carbon::parse($data['created_at'])->format('m/d/Y') }}
                                @endif</small><br/>
								<div>
									<b>Category</b>:-
									<span>{{$category['name']}}</span>
								</div>
								<div>
								<b>Sub Category:-</b>
								<span>{{$subcategory['name']}}</span>
							</div>
							</div><br/>
							<div class="job-description">
								<div class="col-md-8 border-right">
									<div class="job-text">
										<p class="margin_bottom1">{{$data['description']}} </p>
										
										<p class="margin_bottom1"></p>
									</div>
									<div class="job-info">
										<div>
											<?php
								if($data['isOneTime']===0) {
									?>
									<p><b>Project Type</b> : One-time project</p>
									<?php
								}else{
									?>
									<p><b>Project Type</b> : Not One-time project</p>
								<?php
								}	
								?>
										</div>
									</div>
								</div>
								<div class="col-md-4 job-requirement">
									<div>
										<?php
								if ($data['isHourly']==0) {
									?>
										<h2 class="job_budget">${{$data['budget']}}</h2>
										<p>Fixed Price</p>
										<?php
								}else{
									?>
										<h2 class="job_budget">$ {{ $data['minRate'] }} ~ {{ $data['maxRate'] }} </h3>
										<p>Hourly</p>
									<?php
									}	
										?>
									</div>
									<div>
										<h2 class="job_budget">{{ date("m/d/Y", strtotime($data['startDateTime'])) }}</h2>
										<p>Start Date</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 client-details">
					<div class="row">
						<div class="job-detail-panel client-panel">
							<div class="text-center submit-proposal-buttons">
							<?php
								$job_id = $data['id'];
								if ($hasdata['user_id'] != $user_id) {
									if ($hasdata['job_id'] != $job_id) {
							?>
									<?php if(@$tid==NULL) { ?>
									
									<a href="/vendor/job/{{$data['id']}}/proposal"><button class="button solid button-margin">Submit a Proposal</button></a><br>
									<?php }
									else{ ?>
									<a href="/vendor/job/{{$data['id']}}/{{$tid}}/proposal"><button class="button solid button-margin">Submit a Proposal</button></a><br>
									<?php } ?>
							<?php
								}
							}else{

								?>
								 	<a href="/vendor/proposal/{{$data['id']}}/details"><button class="button solid vf-btn">View Proposal</button></a>

								<?php
							}
							?>
							</div>
							<div>
								<b>About the client</b>  
							</div>
							 
							<div class="profileimg">
								<img src="/{{$profileimg->profileimg[0]['path']}}">
								<span></span>
							</div>
							<div>
								<h2>{{$profileimg['username']}}</h2>
							</div>	
							<div class="five-star stars-total">
								<div class="showstar  jq-stars" style="padding: 0px;" data-rating="{{$feedback}}" live-rating="{{$feedback}}"></div>
									<span> ({{$feedback}})  </span>
									<span> 0 reviews</span>
							</div>
							<div class="">
								<h5>{{$country['name']}}</h5>
								<!-- <p>50% Hire Rate,1 Open Job</p> -->
							</div>
							<div class="">
								<h5>{{$totalJobs}} Jobs Posted</h5>
								<!-- <p>50% Hire Rate,1 Open Job</p> -->
							</div>
							<div class="">
								<h5>${{$cJobs_id['maxRate']}}/Hr</h5>
								<!-- <p>50% Hire Rate,1 Open Job</p> -->
							</div>
							<div class="">
								@if($totalspend!="")
								 @if(strlen((string)$totalspend) > 3)
									<h5>${{ substr($totalspend , 0 , -3) }}K + Total Spent</h5>
								@else
									<h5>${{$totalspend}} + Total Spent</h5>
								@endif
								@endif
								<span>{{count($data['hires']) > 0 ? count($data['hires']) : 0}} Hires</span>	<span>,{{ count($activevendor) ? count($activevendor) : 0 }} Active User</span>
							</div>
							<div class="">
								<h5>{{$cJobs_id['jobSuccess']}}</h5>
								<!-- <p>50% Hire Rate,1 Open Job</p> -->
							</div>
						</div>
					</div>
				</div>
			<?php
			}
			else
			{
			?>
				<div class="text-center alert alert-info">
					<br>
					<h4>{{$mes}}</h4>
					<br>
				</div>
			<?php
			}
			?>
			</div>
		</div>
@endsection
@section('addjavascript')
	{{-- <script type="text/javascript" src="{{asset('js/vendorProposal.js')}}"></script> --}}
	<script src="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/src/jquery.star-rating-svg.js')}}"></script>
		<script type="text/javascript">
			$(".showstar").starRating({
    			initialRating: 0.0,
    			starSize: 22,
    			strokeColor : '#5fcf80',
    			useGradient :true,
    			starGradient :	{start: '#5fcf80', end: '#5fcf80'}
  			});
  			$(".showstar").starRating('getRating');
  			$('.showstar').starRating('setReadOnly', true);
		</script>
		
@endsection