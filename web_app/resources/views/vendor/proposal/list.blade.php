	@extends('../layouts/base')
	 @section('addstyle')
	 	<link rel="stylesheet" type="text/css" href="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/src/css/star-rating-svg.css')}}">
    	<link rel="stylesheet" type="text/css" href="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/demo/css/demo.css')}}">
    	
	 @endsection
  @section('content')
  	 
		<div class="container job-deatils-page">
			<div class="row">
				<div class="col-md-8">
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils detail-box">
							<div class="job-heading">
								<h1>{{$data['name']}}</h1>
								<p class="margin_bottom2">
								<span>Posted at : @if( \Carbon\Carbon::parse($data['created_at'])->diffInMonths(\Carbon\Carbon::now()) <= 10 )
                                    {{ \Carbon\Carbon::parse($data['created_at'])->diffForHumans() }}
                                @else
                                    {{ \Carbon\Carbon::parse($data['created_at'])->format('m/d/Y') }}
                                @endif </span></p>
								<h2>Details</h2>
							</div>
							<div class="job-description">
								<div class="col-md-8">
									<div class="job-text">
										<p>{{$data['description']}} </p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils detail-box">
							<div class="job-heading">
								<h2>Cover Letter</h2>
							</div>
							<div class="job-description">
								<div class="col-md-8">
									<div class="job-text">
										<p>{{$data['cover']}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 client-details">
					<div class="row">
						<div class="job-detail-panel client-panel proposal-detail">
							<div>
								<p class="margin_defaullt">About the client </p>
							</div>
							{{-- <div class=" submit-proposal-buttons">
								<a href="/vendor/job/{{$data['id']}}/bid"><button class="button solid button-margin">Propose Diffrent Terms</button></a><br>
								<p>Withdraw Proposal</p>
							</div> --}}
							<div class=" submit-proposal-buttons col-md-9">
								<span>
									@if(sizeof($totalstars) > 0)
                                    {{ $totalstars[0]['success_percent'] }}% Job Success
                                    <div class="job-success-progress">
                                        <div class="progress-complete" style="float:left;width: <?php echo $totalstars[0]['success_percent'] ?>%" data-toggle="tooltip" data-placement="top" title="{{ $totalstars[0]['success_percent'].'% success job' }}">
                                        </div>
                                        <div class="progress-pending" style="float:left;width: <?php echo 100-$totalstars[0]['success_percent'] ?>%">
                                        </div>
                                    </div>
                                    @else
                                    <?php echo 0 . "% Job Success";?>
                                    <div class="job-success-progress">
                                        <div class="progress-complete" style="padding:0 0 8px 0;float:left;width:<?php echo '0';?>%" data-toggle="tooltip" data-placement="top" title="">
                                        </div>
                                        <div class="progress-pending" style="padding:0 0 8px 0;float:left;width: <?php  echo '100';?>%">
                                        </div>
                                    </div>
                                    @endif
                                </span>
							</div><div class="clearfix"></div>
							<div class="five-star stars-total">
								<div class="showstar jq-stars" style="padding: 0px;" data-rating="{{$feedback}}" live-rating="{{$feedback}}"></div>
									<span> ({{$feedback}})  </span>
									<span> 0 reviews</span>
							</div>
							<div>
								<h5>{{$data['country_name']}}</h5>
								<span class="margin_bottom1">{{$data['state_name']}}  {{ date('g:i a', strtotime($data['created_at'])) }}</span>
							</div>
							<div>
								<h5>{{$totaljobs}} jobs posted</h5>

								<span>{{count($openedjob) >0 ? count($openedjob) : '0'}} Open job</span>
							</div>
	
							<div>
								<h5>60 Hours billed</h5>
							</div>
							
							<div>
								@if($totalspend!="")
								 @if(strlen((string)$totalspend) > 3)
									<h5>${{ substr($totalspend , 0 , -3) }}K + Total Spent</h5>
								@else
									<h5>${{$totalspend}} + Total Spent</h5>
								@endif
								@endif
								<span>{{count($data['hires']) > 0 ? count($data['hires']) : 0}} Hires</span>	<span>,{{ count($activevendor) ? count($activevendor) : 0 }} Active User</span>
							</div>
							<div>
								@if ($data['isHourly'] == '0')
								<h5>${{$data['budget']}}.00/hr Fixed Rate</h5>      
									@else
                                        <h5>${{$data['isHourly']}}.00/hr Avg Hourly Rate</h5>
                                    @endif
							</div>
							<div>
								<h5>Activity for this job </h5>	
								<span>{{$getprops}} proposals</span>
							</div>
							<div>
								{{-- <p>0 interviews</p> --}}
								<span>Members since {{ date('M d, Y', strtotime($data['udate'])) }}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
@section('addjavascript')
	<script type="text/javascript" src="{{asset('js/vendorProposal.js')}}"></script>
	<script src="{{asset('plugin/SVG-Based-Star-Rating-Plugin-For-jQuery-star-rating-svg-js/src/jquery.star-rating-svg.js')}}"></script>
		<script type="text/javascript">
			$(".showstar").starRating({
    			initialRating: 0.0,
    			starSize: 22,
    			strokeColor : '#5fcf80',
    			useGradient :true,
    			starGradient :	{start: '#5fcf80', end: '#5fcf80'}
  			});
  			$(".showstar").starRating('getRating');
  			$('.showstar').starRating('setReadOnly', true);
		</script>
		
@endsection