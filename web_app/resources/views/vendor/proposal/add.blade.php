@extends('../layouts/base')
@section('content')
<div class="job-posting-form container">
	@if(Session::has('message'))
	    <div class="alert alert-info">
	      <p style="">{{Session::get('message')}}</p>
	      @php Session::forget('message'); @endphp
	    </div>
	@endif
	<?php
	if(isset($data))
	{
	?>
	<form role="form" method="post" >
					{!! csrf_field() !!}
					<input type="hidden" name="jobId" value="{{$jobId}}">
					<input type="hidden" name="tid" value="{{$tid}}">

			<div class="vendor-job-list form-fields panel-group ">
				<div class="vfform" id="save-job">
					<div class="title">
						<h3>Job Details</h3>
					</div>

					<div class="vf-wrap addproposal-margin " >
						<div class="col-md-12">
							<div class="row">
								<div class="job-heading" >
									<h3>{{$data['name']}}</h3>
								</div>
							</div>
						</div>
						<div class="job-description">
							<div class="col-md-8 border-right">
									<div class="row">
								<div class="job-text bottom-margin">
										<p>{{$data['description']}}</p><br/>
									</div>
								</div>
							</div>
							<div class="col-md-4 jo-requirement bottom-margin">
								<div>
									<?php
								if ($data['isHourly']==0) {
									?>
										<p>Fixed Price : ${{$data['budget']}}</p>
										<?php
								}else{
									?>
										<p>Hourly : $ {{$data['minRate']}} ~ {{$data['maxRate']}} </p>
									<?php
									}	
										?>
								</div>
								<div>									
									<h4></h4>
									<p>Start Date : {{ date("m/d/Y", strtotime($data['startDateTime'])) }}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="vendor-job-list form-fields panel-group ">
				<div class="vfform" id="">
					<div class="title">
					<h3>Terms</h3>
					</div>
					<div class="row">
						<div class="feildcont">
							<div class=" vf-saved-job vf-terms" >
								<div class="bottom-border parpo">
									<div class="col-md-8">
										<h3>Bid</h3>
										<span class="spand">Total Amount the client will see on your proposal. </span>
									</div>
									<div class="col-md-4">
										<div class="col-md-2">
											<span class="vf-dollor-sign">$</span> 
										</div>
										<div class="col-md-10">
											<input type="number" class="" name="bidPrice" id="bidPrice" value="">
										@if($errors->has('bidPrice'))
												<span class="text-danger">{{ $errors->first('bidPrice') }}</span><br/>
										@endif
											<div class="print-error-msg">
												<ul>
												</ul>	
											</div>
										</div>
									</div>
								</div>
								<div class="bottom-border parpo ">
									<div class="col-md-8">
										<h3>VendorForest Service Fee &nbsp;&nbsp;<span class="default-color expthis" data-toggle="popover" title="Service Fee" data-content="This is vendorforest services fee.">Explain this</span></h3>
									</div>
									<div class="col-md-4">
										<div class="col-md-2">
											<span class=" vf-dollor-sign">$</span> 
										</div>
										<div class="col-md-8">
											<input type="number" name="serviceFee" id="serviceFee" class="" value="{{$comission['value']}}" readonly/>
											
										@if($errors->has('serviceFee'))
											<span class="text-danger">{{ $errors->first('serviceFee') }}</span>
										@endif
										</div>
										<div class="col-md-2">
											<span class="vf-percentage-sign">%</span> 
										</div>
									</div>
								</div>
								<div class="bottom-border parpo">
									<div class="col-md-8">
										<h3>You'll be paid</h3>
										<span class="spand">The estimated amount you'll receive after service fees. </span>
									</div>
									<div class="col-md-4">
										<div class="col-md-2">
											<span class=" vf-dollor-sign">$</span> 
										</div>
										<div class="col-md-10">
											<input type="number" name="payment" id="payment" class="" value="" readonly/>
										@if($errors->has('payment'))
											<span class="text-danger">{{ $errors->first('payment') }}</span>
										@endif
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		
		<div class="vendor-job-list form-fields panel-group ">
				<div class="vfform">
					<div class="title">
						<h3>Additional Information</h3>
					</div>
					<div class="">
						<div class="addproposal-margin vf-textarea-halfsize">
							<label>Cover Letter</label>
							<textarea name="cover"></textarea><br/>
						@if($errors->has('cover'))
							<span class="text-danger">{{ $errors->first('cover') }}</span><br/>
						@endif
						<div class="col-md-2">
							<br/>
							<label>Attachment(optional)</label>
							<span class="button solid fileinput-button">
						        <i class="glyphicon glyphicon-plus"></i>
						        <span>Add files...</span>
						        <input id="coverfileupload" type="file" name="files[]" >
						        <input type="hidden" id="fileattachment" name="hid">
						    </span>
			            </div>
			               <div class="col-md-10"> 
			                
			                <!-- The container for the uploaded files -->
				                <div id="coverfiles" class="files coverfile">
				                  <div class="imagePreview">
				                  </div>
				                  <div class="profileimage-inline">
				                  
				                	</div>
				                	<div class="coveri">
                               		 	<i data-id="" class="fa fa-times" id="removecoverimg"></i>
                                	</div>
				                </div>
	  						    <div id="progress" class="progress" style="display: none;width:285px">
	  						        <div class="progress-bar progress-bar-success"></div>
	  						   </div>
			                </div>
						</div>
						</div>
					<br clear="all">
					<div class="text-center bottom-margin rel row">
							<img src="{{asset('images/sppin.gif')}}" class="form-loader">
							<button class="button solid vf-btn" id="submitProposal">Submit Proposal</button> 
							{{-- <button class="button default vf-btn ">Cancel</button> --}}
							<div class="message"></div>
					</div>
				</div>
				
		</div>
	</form>
		<?php
			}
			else
			{
			?>
				<div class="text-center alert alert-info">
					<br>
					<h4>{{$mes}}</h4>
					<br>
				</div>
			<?php
			}
			?>
</div>
@endsection
@section('addjavascript')
<script type="text/javascript" src="/js/vendorProposal.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>

@endsection