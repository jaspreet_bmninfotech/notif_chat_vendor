@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
	<div class="vendor-job-list form-fields">
		<div class="panel-group">
			<div class="vfform " id="">
				<div class="title">
					<h3>PROPOSALS</h3>
				</div>
				<div class="clearfix"></div>
				<div class="panel-body">
					<?php
					if(empty($data)) {
						?>
					 
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12 text-center top-bot-margin1">
									<h3>Currently you have no bid</h3>
								</div>
							</div>
						</div>
					
					<?php
					 }else{ ?>
					<div class="">
						<div class="col-md-4">
							<h4>SENT</h4>
						</div>
						<div class="col-md-6">
							<h4>JOBS</h4>
						</div>
						<div class="col-md-2">
							<h4>ACTION</h4>
						</div>
					</div>
				<div class="">
					<div class="col-md-4">
						<h4>SENT</h4>
					</div>
					<div class="col-md-6">
						<h4>JOBS</h4>
					</div>
					<div class="col-md-2">
						<h4>ACTION</h4>
					</div>
				</div>
					@foreach ($data as $bid)
						<div>
							<div class="col-md-12">
								<div class="row">
									<div class="vf-bids">
										<div class="col-md-4 ">
											<div class="bid-detail">
												{{date('m/d/Y', strtotime($bid['created_at']))}}
											</div>
										</div>
										<div class="col-md-6 ">
											<div class="bid-detail">
												{{$bid['name']}}
											</div>
										</div>
										<div class="col-md-2 ">
											<div class="bid-detail">
												<a href="/vendor/job/{{$bid['id']}}/details"><button class="button solid">View Job Detail	</button></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
		

  @endsection