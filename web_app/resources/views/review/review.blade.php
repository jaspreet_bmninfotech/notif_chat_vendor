@extends('../layouts/base')
  @section('content')
  	<div class="container client-content job-listing">   
        <div class="panel-group">
          <div class="vfform">
            
                <div class=" heading">
                    <h3>&nbsp; Jobs Title </h3>
                </div>
            <div class="panel-body">
                <div class="stcode_title1 job">
                    <h3>
                        <span class="text">
                            <strong><a href="#"></a></strong>
                        </span>
                   </h3>
                    <div class="details"> 
                        <p class="description"></p>
                        <h5 class="budget">
                            <label> Budget: </label>
                            <span></span>
                        </h5>
                    </div>
                    
                </div>
            
            </div>
          </div>
        </div>
		<div class="container">
			<div class="row" style="margin-top:40px;">
				<div class="col-md-6">
			    	<div class="well well-sm">
			            <div class="text-right">
			                <a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>
			            </div>
			        
			            <div class="row" id="post-review-box" style="display:none;">
			                <div class="col-md-12">
			                    <form accept-charset="UTF-8" action="" method="post">
			                        <input id="ratings-hidden" name="rating" type="hidden"> 
			                        <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>
			        
			                        <div class="text-right">
			                            <div class="stars starrr" data-rating="0"></div>
			                            <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
			                            <i class="fa fa-remove"></i> Cancel</a>
			                            <button class="btn btn-success btn-lg" type="submit">Save</button>
			                        </div>
			                    </form>
			                </div>
			            </div>
			          
			        </div>   
				</div>
			</div>
		</div>
	</div>
  @endsection
   @section('addjavascript')
   		<script src="{{asset('js/review.js')}}"></script>
   @endsection