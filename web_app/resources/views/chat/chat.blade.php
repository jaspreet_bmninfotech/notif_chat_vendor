@extends('layouts/base')
@section('content')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/chat.css') }}">
	<style type="text/css">
		.wrap {
			min-height: 50px
		}
	</style>
	@php
		$urlTransaction = explode('/' , $_SERVER['REQUEST_URI'])[2];
	@endphp
<div class="container">
	<div id="frame" class="cframe">
		<input type="hidden" name="user_id" id="{{ Auth::user()->id }}">
		<input type="hidden" name="transaction_id" value="{{$id}}">
		<input type="hidden" name="urlTransaction" value="{{ $urlTransaction }}">
		<div id="sidepanel">
			<div id="profile">
				<div class="wrap">
					@if (Auth::user()->profilePath != "")
					<img id="profile-img" src="{{ asset(Auth::user()->profilePath) }}" class="online" alt="" />
					@endif
					<p>{{ Auth::user()->username }}</p>
					<i class="fa fa-chevron-down expand-button" aria-hidden="true"></i>
					<div id="status-options">
						<ul>
							<li id="status-online" class="active"><span class="status-circle"></span> <p>Online</p></li>
							<li id="status-away"><span class="status-circle"></span> <p>Away</p></li>
							<li id="status-busy"><span class="status-circle"></span> <p>Busy</p></li>
							<li id="status-offline"><span class="status-circle"></span> <p>Offline</p></li>
						</ul>
					</div>
					<div id="expanded">
						<label for="twitter"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></label>
						<input name="twitter" type="text" value="mikeross" />
						<label for="twitter"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></label>
						<input name="twitter" type="text" value="ross81" />
						<label for="twitter"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></label>
						<input name="twitter" type="text" value="mike.ross" />
					</div>
				</div>
			</div>
{{-- 			<div id="search">
				<label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
				<input type="text" placeholder="Search contacts..." />
			</div>
 --}}			<div id="contacts">
				<ul>
					@if($Job)					
						@if(App\Attachment::select('path')->where('id', Auth::user()->profile_image)->first() != null)
							@foreach($Job as $k => $v)
								<li class="contact contact_{{$v->id}}" data-jobTransaction="{{ $v->id }}">
									<div class="wrap" data-route="{{ route('get.user.chat' , $v->id)}}">
											@if( $v->hires->user_id == Auth::user()->id )
												<img src="{{ asset(App\Attachment::select('path')->where('id', Auth::user()->profile_image)->first()->path) }}" alt="" />
											@else
												<img src="{{ asset(App\Attachment::select('path')->where('id', Auth::user()->profile_image)->first()->path) }}" alt="" />
											@endif
										<div class="meta">
											<span class="contact-status busy"></span>
											<p class="name">{{ $v->hires->job->name }}</p>
											<!-- <p class="preview"></p> -->
										</div>
									</div>
								</li>
							@endforeach
						@endif
					@endif
				</ul>
			</div>
		</div>
		<div class="content" id="app">
			 	<div class="contact-profile">
					<img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
					<p style="color: black;">Harvey Specter</p>
					{{-- <div class="social-media">
						<i class="fa fa-facebook" aria-hidden="true"></i>
						<i class="fa fa-twitter" aria-hidden="true"></i>
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</div>  --}}
				</div> 

			<div class="messages">
				<!-- <ul v-chat-scroll="{always: false}">
					<li v-for="messages in chat.messages" :key="messages.id">@{{ messages }}</li>
				</ul> -->
				<ul>
					@foreach($prevChat as $k => $message)
						<li class="{{ ((int)$message->sender == (int)Auth::user()->id)  ? 'replies' : 'sent'}}" >
							<img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
							<p> {{$message->message}}</p>
						</li>
					@endforeach
					<li v-for="messages in chat.messages" :key="messages.id" :class="[ messages.from ]" >
						<img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
						<p>@{{ messages.message }}</p>
					</li>
				</ul>
			</div>
			<div class="message-input">
				<div class="wrap">
				<input type="text" placeholder="Write your message and press enter ..." name="message" v-model="message" @keyup.enter="send"/>
				<button class="submit">Sent<i class="fa fa-paper-plane" aria-hidden="true"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>

	
@endsection
@section('addjavascript')
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{asset('js/universal/jquery.js')}}"></script>
<script type="text/javascript">
	var url = window.location;
	var hrefArray = url.href.split('/');
	var transaction_id = hrefArray[hrefArray.length-1];

	$('.contact_'+transaction_id).addClass('active');

	$(document).ready(function(){
		$(document).on('click','.contact ',function(){
			var href = $(this).find('.wrap').attr('data-route')
			window.location.href = href; 
		});
	});
</script>
@endsection