@extends('layouts/base')
@section('content')
  	<section class="browshome">
  		<div class="container ">
  			<div class="row">
  				<div class="col-md-10">
  					<h1 class="title-head">Browse category for {{ucwords($name['name'])}}</h1>
				</div>
  			</div>
  			<div class="row">
  				@if( !empty($data) )
  				@php
  					$existCategory = [];
  				@endphp
					@foreach($data as $k => $value)
						<div class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 200 }'>
						  <div class="grid-item"></div>
						  <div class="grid-item"></div>
			  				<div class="col-md-4 col-sm-6">
			  					<section>
			  						<h4 class="topbottom-margin category_head">{{$value['name']}}</h4>
			  						<ul class="unstyled">
									@foreach($value['childcategory'] as $ky => $val)
				  						<li>{{$val['name']}}</li>
									@endforeach
			  						</ul>
			  					</section>
							</div>
						</div>
					@endforeach
				@endif
  			</div>
  		</div>
  	</section>
@endsection
@section('addjavascript')
<script src="{{asset('plugin/masonry-master/dist/masonry.pkgd.min.js')}}"></script>
<script>
	$('.grid').masonry({
  // options...
  itemSelector: '.grid-item',
  columnWidth: 200
});
</script>
       
@endsection
