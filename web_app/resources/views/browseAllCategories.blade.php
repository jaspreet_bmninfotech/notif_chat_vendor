@extends('layouts/base')
  @section('content')
  	<section>
	  		<div class="container ">
	  			<div class="row">
	  				<div class="col-md-10">
	  					<h1 class="title-head">Browse all category</h1>
					</div>
	  			</div>
  			<div class="row">
  				@if( !empty($data) )
  				@php
  					$existCategory = [];
  				@endphp
					@foreach($data as $k=> $value)
						@foreach($value as $key => $val)

		  				<div class="col-md-4 col-sm-6">
			  					<section>
		  						<h4 class="topbottom-margin category_head">{{$key}}</h4>
			  						<ul class="unstyled">
		  							@foreach($val['sub_sub_category'] as $ke => $valu)
			  							@foreach($valu as $keys => $subCate)
			  								<li>{{ $subCate['name'] }}</li>
				  							@endforeach
		  							@endforeach
			  						</ul>
			  					</section>
							</div>
						@endforeach
					@endforeach
				@endif
		  			</div>
	  		</div>
  	</section>
  @endsection