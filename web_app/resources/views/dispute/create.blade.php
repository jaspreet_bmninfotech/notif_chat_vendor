@extends('layouts.base')

@section('content')
 @if(Session::has('success'))
		  <div class="alert alert-success">
			      <p style="color:green;">{{Session::get('success')}}</p>
				@php
		        Session::forget('success');
		    @endphp
	    </div>
	  @endif
<div class="col-md-1"> </div>
<div class="form-fields col-md-10">
		<div class="vfform">
			<div class="title">
				<h3>Dispute Form </h3>
			</div>
			<div class="feildcont">
				<form role="form" method="POST" action="{{route('dispute.store')}}" autocomplete="off">
						{!! csrf_field() !!}
					<div class="row">
						<div class="col-md-1"> </div>
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-6">
									<label>Subject<em>*</em></label>
									{{-- <input type="text" name="firstName" value=""> --}}
									<input type="text" placeholder="Subject"  name="subject">
									@if($errors->has('subject'))
									<span class="text-danger">{{ $errors->first('subject') }}</span>
									@endif
								</div>
								<div class="col-md-6 last">
									 <label>Related to <em>*</em></label>
									{!! Form::select('category_id',[null=>'Please Select']+ $category, null, [ 'class'=>'form-select-field'] )!!}
									@if($errors->has('category_id'))
									<span class="text-danger">{{ $errors->first('category_id') }}</span>
									@endif
								</div>
							</div>
						</div><br clear="both">
						<div class="col-md-1"> </div>
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-6">
									<label>Job<em>*</em></label>
									{!! Form::select('job_id',[null=>'Please Select']+$job, null, [  'class'=>'form-select-field disputeJob'] )!!}
									@if($errors->has('job_id'))
									<span class="text-danger">{{ $errors->first('job_id') }}</span>
									@endif
								</div>
								<div class="col-md-6 last">
									 <label>Description <em>*</em></label>
									{!! Form::textarea('content', null,  ['rows'=>"4",  'cols'=>"50" , 'class'=>'form-select-field'] )!!}
									@if($errors->has('content'))
									<span class="text-danger">{{ $errors->first('content') }}</span>
									@endif
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>Vendor<em>*</em></label>
									{!! Form::select('vendors',[], null, [  'class'=>'form-select-field','id' =>'vendors'] )!!}
								</div>
							</div>
						</div>
					</div>

					<div class="text-center">
						<button type="submit" id="teamsave" class="button solid">Submit</button>
					</div>
			</form>
		</div>
	</div>
</div>
<div style="clear: both;"> </div>








  {{-- <div class="job-posting-form container">
	<div class="form-fields">
		<form class="form-table"  autocomplete="off" method="post"  action = "{{ route('dispute.store')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<section class=" left animate fadeInLeft section-design margin_bottom10 ">
					<div class="">
							<div class="color-main-bg border-heading-div">
								<h1>Dispute Form </h1>
							</div>
							<div class="">
								<div class="posting-form-fields">
									<div>
										<label>Subject</label>
									</div>
									<input type="text" placeholder="Subject" required="required" class="form-select-field" name="subject">
								</div>

								<div class="posting-form-fields">
									<div>
										<label>Related to </label>
									</div>
										{!! Form::select('category_id', $category, null, [ 'required'=>"required" , 'class'=>'form-select-field'] )!!}
								</div>
								<div class="posting-form-fields">
									<div>
										<label>Job </label>
									</div>
										{!! Form::select('job_id', $job, null, ['required'=>"required" ,  'class'=>'form-select-field'] )!!}
								</div>

								
								<div class="posting-form-fields">
									<div>
										<label>Description </label>
									</div>
										{!! Form::textarea('content', null,  ['rows'=>"14", 'required'=>"required" , 'cols'=>"50" , 'class'=>'form-select-field'] )!!}
								</div>

								<div class="posting-form-fields">
									<div>
										<label> </label>
									</div>
										{!! Form::submit( )!!}
								</div>
							</div>
					</div>
			</section>
		</form>
	</div>
</div>  --}}



@endsection 

@section('addjavascript')
    <script src="{{asset('js/dispute.js')}}"></script>
@endsection