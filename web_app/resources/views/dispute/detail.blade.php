@extends('layouts.base')

@section('content')
@php

// dd($data->toArray());
$except = ['dispute_verdicts', 'comment' ,'id' , 'status_id', 'category_id','job_id', 'client_id', 'vendor_id', 'vendor_id', 'tour_guide_id', 'agent_id', 'updated_at'];
@endphp
<h1></h1>

<style>
  
.dispute-detail label span{
  margin-left: 50px;
}
</style>

<div class="col-md-2"> </div>
<div class="form-fields col-md-8">
    <div class="vfform">
      <div class="title">
        <h3>Dispute Details </h3>
      </div>
      <div class="feildcont">
            
          <div class="row">
            <div class="col-md-1"> </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-6 dispute-detail ">
                  <label>Subject:  <span>{{$data['subject']}}</span> </label>
                  
                </div>
                <div class="col-md-6 dispute-detail ">
                   <label>Category:  <span>{{$data['category']['name']}} </span> </label>
                 <label for=""> </label>
                </div>

                 <div class="col-md-6 dispute-detail ">
                   <label>Job:  <span>{{$data['job']['name']}} </span> </label>
                 <label for=""> </label>
                </div>

                <div class="col-md-6 dispute-detail ">
                   <label>Status:  <span>{{$data['status']['name']}} </span> </label>
                 <label for=""> </label>
                </div>

                 <div class="col-md-6 dispute-detail ">
                   <label>Description :  <span>{{$data['content']}} </span> </label>
                 <label for=""> </label>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

 <div class="container client-content job-listing">
        <h2 class="page-title">Detail of Dispute</h2>

         <h2 class="page-title"> <a href="{{route('dispute.index')}}"><< Back to Dispute list</a></h2>
        <div class="panel-group">
            <div class="panel-heading">
               @if($data->dispute_apply_by == Auth::user()->type )

               <h2 class="page-title">Applied Dispute </h2>
                      {{Auth::user()->username}}  Verse  
                      @if(!empty($data['client_id']))
                      {{username($data['client_id'])}}
                    @elseif(!empty($data['tour_guide_id']))
                      {{username($data['tour_guide_id'])}}
                    @elseif(!empty($data['vendor_id']))
                      {{username($data['vendor_id'])}}
                    @endif
                @else
                <h2 class="page-title">Aganist Dispute </h2>
                @if(!empty($data['client_id']))
                      {{username($data['client_id'])}}
                    @elseif(!empty($data['tour_guide_id']))
                      {{username($data['tour_guide_id'])}}
                    @elseif(!empty($data['vendor_id']))
                      {{username($data['vendor_id'])}}
                    @endif
                        Verse 
                    {{Auth::user()->username}}    

                @endif
            </div>






            <div class="panel-body">
             @foreach(array_except($data->toArray(), $except ) as $key => $val)

             @if($key == 'dispute_verdicts')
             <h3>   Final Verdict of Dispute </h3>

              @foreach($val as $verdic_key => $verdict_val)
                <h5>{{$loop->iteration  }} {{ $verdict_val['content']}}   </h5>
              @endforeach
                @continue
             @endif
                <div class="stcode_title1 job">
                    <div class="details"> 
                        <p class="description"></p>
                     
                        <h5 class="budget">
                            <label> {{ucwords(str_replace('_' , ' ' , $key )) }}  </label>
                               @if(!is_array($val))
                                <span>{{$val}}</span>
                              @else
                                  {{$val['name']}}
                                       
                              @endif
                        </h5>
                    </div>
                </div>
            @endforeach
            </div>
        </div>

        <h3> Comments </h3>
<!-- PAGE CONTENT WRAPPER -->
            @foreach(collect($data['comment'])->where('to', Auth::id())->toArray() as $ckey => $cVal)

            @if(empty($cVal['to_agent']))
              <span> {{agent_user_name($cVal['user_id']) }} </span>  <h5 style="margin-left: 50px">  {{$loop->iteration  }} {{$cVal['content']}}</h5>
            @else
              <h5> {{$loop->iteration  }} {{$cVal['content']}}</h5>

            @endif
            @endforeach
        <div class="page-content-wrap">
             {!! Form::open(['route'=>'tickets.comment'    ]) !!}
                        <input type="hidden"  name="type"  value="dispute">
                        <input type="hidden" name="ticket_id" value="{{$data['id']}}">
                        <input type="hidden" name="user_id" value="1">
                        <input type="hidden" name="to_agent" value="1">
                        @if(Auth::user()->type == 'vn')
                            <input type="hidden" name="to" value="{{$data['vendor_id']}}">
                        @elseif(Auth::user()->type == 'cn')
                            <input type="hidden" name="to" value="{{$data['client_id']}}">
                        @endif
                        <textarea name="content" id="" cols="30" rows="10"> text here   </textarea>
                      {!! Form::submit('send message') !!}
            {!! Form::close() !!}

        </div>

        <h2> Final Verdict </h2>


             <h3>   Final Verdict of Dispute </h3>

              @foreach($data['dispute_verdicts'] as $verdic_key => $verdict_val)
                <h5>{{$loop->iteration  }} {{ $verdict_val['content']}}   </h5>
              @endforeach
</div>

@endsection 