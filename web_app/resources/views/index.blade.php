@extends('layouts/base')
  @section('addstyle')
 <link rel="stylesheet" href="{{asset('css/home.css')}}" type="text/css" />
  @endsection
  @section('content')
			<!-- Banner -->
				<section id="banner">
          <div class="banner_cover">
            <div class="banner_content_placeholder">
              <h1 class="banner_content_h1">Find and hire like a boss.</h1>
    					<h3 style="color: #32325d; font-size: 1.3em;">Find and hire the best vendor from all over the world. From renting to making your dream event come true, we got you.</h3>
    					<!-- <p>From house painting to personal training, we bring you the right pros for every project on your list.</p> -->
    					<form id="search-arear">
    						<input type="search" name="search" class="src-q" placeholder="What do you need?" />
    						<button class="search-btn">Get Started</button>
    					</form>
            </div>
  					<!-- <ul class="actions">
  						<li><a href="#" class="button special">Get Started</a></li>
  						<li><a href="#" class="button">Plan an Event</a></li>
  					</ul> -->
          </div>
				</section>
        <section id="services-box" class="s_box1">
          <div class="container medium_cont">
            <ul>
              @foreach ($homecategory as $key => $value)
              @if ($value != '' && $value != null)
                <li>
                  <a href="{{route('home.category',$value['id'])}}">
                    <span style="background-image: url(http://127.0.0.1:8000/icons/service_icon/home.svg);background-size: cover;background-repeat: no-repeat;width: 50px;height: 40px; display: inline-block; margin-top: .8em;"></span>
                    <span style="font-weight: normal;line-height: 17px;font-size: .59091rem;display: block; font-size: 13px;">{{$value['name']}}</span>
                  </a>
                </li>
              @endif
              @endforeach
              <li>
                <a href="">
                  <span style="background-image: url(http://127.0.0.1:8000/icons/service_icon/Travel_Agents.svg);background-size: cover;background-repeat: no-repeat;width: 40px;height: 40px; display: inline-block; margin-top: .8em;"></span>
                  <span style="font-weight: normal;font-size: .59091rem;display: block; font-size: 13px;">Tour Guides</span>
                </a>
              </li>
              <li>
                <a href="">
                  <span style="background-image: url(http://127.0.0.1:8000/icons/service_icon/Rentals.svg);background-size: cover;background-repeat: no-repeat;width: 40px;height: 40px; display: inline-block; margin-top: .8em;"></span>
                  <span style="font-weight: normal;font-size: .59091rem;display: block; font-size: 13px;">Rentals</span>
                </a>
              </li>
            </ul>
          </div>
        </section>
			<!-- Main -->
      <section>
        <section id="" class="container">
            <section class="" style="text-align: center; padding: 30px 0 7em 0;">
              <h2 style="font-weight: bold; color: #32325d;margin:2.3em 0 0 0;">Work with someone perfect for your team</h2>
              <ul class="tabs">
            		<li class="tab-link current" data-tab="tab-1">Home Improvement</li>
            		<li class="tab-link" data-tab="tab-2">Events</li>
            		<li class="tab-link" data-tab="tab-3">Weddings</li>
            		<li class="tab-link" data-tab="tab-4">Wellness</li>
            	</ul>

            	<div id="tab-1" class="tab-content current">
                <div class="section group group-sect category-sect">
                  <?php
                  // foreach ($category as $key => $value) {
                    ?>
                  <div class="col span_1_of_4">
                    <a href="">
                      <span>
                        <span style="background-image: url(http://127.0.0.1:8000/icons/TransparentIcons/);background-size: cover;background-repeat: no-repeat;width: 50px;height: 50px; margin:2em 0 0 0;display: inline-block;"></span>
                        <span style="font-weight: normal;font-size: .59091rem;display: block;text-transform: uppercase;font-size: 13px;"></span>
                      </span>
                    </a>
                  </div>
                  <?php
                  // }
                  ?>
                </div>
                <a class="see-btn" href="{{URL::to('/freelancer-categories')}}" onclick="getincategory()">See All Categories</a>
            	</div>
              <div id="tab-2" class="tab-content">
                <div class="section group group-sect category-sect">
                  <h1>Event tabs</h1>
                </div>
              </div>
              <div id="tab-3" class="tab-content">
                <div class="section group group-sect category-sect">
                  <h1>Wedding tabs</h1>
                </div>
              </div>
              <div id="tab-4" class="tab-content">
                <div class="section group group-sect category-sect">
                  <h1>Wellnes tabs</h1>
                </div>
              </div>
            </section>
          </section>
        </section>
				<section class="box special features" style="background-color: #fff; padding: 1em 0; border-top:1px solid #e6eaef; border-bottom:1px solid #e6eaef;">
          <section id="" class="container">
  					<h2 style="font-weight: bold; color: #32325d; padding: 1em 0;">How it works</h2>
            <div class="hw_wrp">
              <div class="hw_tp_dv">
                <img src="/icons/bigsvgs/post_big.svg" class="hiw-icon" alt="Find vendor">
                <div class="hw_tp_dv_ct">
                  <div style="clear: both; float: left;">
                    <h4>Post</h4>
                    <img src="/icons/smallsvgs/static/post.svg" class="hiw-icon" alt="Find vendor">
                  </div>
                  <div style="clear: both; padding: 2em 0 0 0;">
                    <p>
                      ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                </div>
              </div>
              <div class="hw_bp_dv">
                <img src="/icons/bigsvgs/bid_big.svg" class="hiw-icon" alt="Find vendor">
                <div class="hw_bp_dv_ct">
                  <div style="clear: both; float: left;">
                    <img src="/icons/smallsvgs/static/bid.svg" class="hiw-icon" alt="Find vendor">
                    <h4>Bid</h4>
                  </div>
                  <div style="clear: both; padding: 2em 0 0 0;">
                    <p>
                      ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                </div>
              </div>
              <div class="hw_tp_dv">
                <img src="/icons/bigsvgs/work_big.svg" class="hiw-icon" alt="Find vendor">
                <div class="hw_tp_dv_ct">
                  <div style="clear: both; float: left;">
                    <h4>Work</h4>
                    <img src="/icons/smallsvgs/static/work.svg" class="hiw-icon" alt="Find vendor">
                  </div>
                  <div style="clear: both; padding: 2em 0 0 0;">
                    <p>
                       ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                </div>
              </div>
              <div class="hw_bp_dv">
                <img src="/icons/bigsvgs/getpaid_big.svg" class="hiw-icon" alt="Find vendor">
                <div class="hw_bp_dv_ct">
                  <div style="clear: both; float: left;">
                    <img src="/icons/smallsvgs/static/getpaid.svg" class="hiw-icon" alt="Find vendor">
                    <h4>Get Paid</h4>
                  </div>
                  <div style="clear: both; padding: 2em 0 0 0;">
                    <p>
                      ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
				</section>
				<section class="special features" style="background-color: transparent; padding: 2em 0 7em 0; text-align: center;">
          <section id="" class="container">
  					<h2 style="font-weight: bold; color: #32325d;">Find what to rent</h2>
              <div class="section group group-sect category-sect rent-div">
                <div class="col span_1_of_4">
                  <a href="">
                    <span>
                      <span style="background-image: url(http://127.0.0.1:8000/icons/RentIcons/MusicInstruments.svg);background-size: cover;background-repeat: no-repeat;width: 80px;height: 80px;line-height: 0; margin:1em 0 0 0;display: inline-block;"></span>
                      <span style="font-weight: normal; line-height: 0; font-size: .59091rem;display: inline-flex;
    margin-left: -110px;text-transform: uppercase;font-size: 13px;">Music Instruments</span>
                    </span>
                  </a>
                </div>
                <div class="col span_1_of_4">
                  <a href="">
                    <span>
                      <span style="background-image: url(http://127.0.0.1:8000/icons/RentIcons/Furniture.svg);background-size: cover;background-repeat: no-repeat;width: 80px;height: 80px;line-height: 0; margin:1em 0 0 0;display: inline-block;"></span>
                      <span style="font-weight: normal; line-height: 0; font-size: .59091rem;display: inline-flex;
    margin-left: -76px;text-transform: uppercase;font-size: 13px;">Furniture</span>
                    </span>
                  </a>
                </div>
                <div class="col span_1_of_4">
                  <a href="">
                    <span>
                      <span style="background-image: url(http://127.0.0.1:8000/icons/RentIcons/Garage.svg);background-size: cover;background-repeat: no-repeat;width: 80px;height: 80px;line-height: 0; margin:1em 0 0 0;display: inline-block;"></span>
                      <span style="font-weight: normal; line-height: 0; font-size: .59091rem;display: inline-flex;
    margin-left: -62px;text-transform: uppercase;font-size: 13px;">Garage</span>
                    </span>
                  </a>
                </div>
                <div class="col span_1_of_4">
                  <a href="">
                    <span>
                      <span style="background-image: url(http://127.0.0.1:8000/icons/RentIcons/Books.svg);background-size: cover;background-repeat: no-repeat;width: 80px;height: 80px;line-height: 0; margin:1em 0 0 0;display: inline-block;"></span>
                      <span style="font-weight: normal; line-height: 0; font-size: .59091rem;display: inline-flex;
    margin-left: -50px;text-transform: uppercase;font-size: 13px;">Books</span>
                    </span>
                  </a>
                </div>
              </div>
              <a class="see-btn" href="javascript:void(0)" onclick="getincategory()">More to rent</a>
          </section>
				</section>
        <!-- CTA -->
          <section id="cta">

            <h2 style="font-weight: bold; font-size: 3em;">Build your team online</h2>
              <a href ="{{ route('login') . '?action=' . 'team' }}" class="button" >Get Started</a>
          </section>
          <section class="special features" style="background-color: transparent; padding: 2em 0 7em 0; text-align: center;">
            <section id="" class="container">
    					<h2 style="font-weight: bold; color: #32325d;">Professional Models</h2>
                <div class="section group group-sect category-sect">
                  <div class="col span_1_of_4 model-div" style="position: relative; height: 20em; overflow: hidden; background-image: url('http://www.kabibimag.com/wp-content/uploads/2017/07/Sira-Kante-4-kabibi-365x500.jpg'); background-size: cover; background-position: center; background-repeat: no-repeat;">
                    <a href="">
                      <div class="model_info_age">
                        19<small>yrs</small>
                      </div>
                      <div class="model_info_div">
                        <h3>Joel Nguanire</h3>
                        <h4>United States, Florida</h4>
                      </div>
                    </a>
                  </div>
                  <div class="col span_1_of_4" style="position: relative; height: 20em; overflow:hidden;  background-image: url('https://i.pinimg.com/736x/fc/54/77/fc5477c579d689ce2d1af0a1b7493828--black-hairstyles-african-hairstyles.jpg'); background-size: cover; background-position: center; background-repeat: no-repeat;">
                    <a href="">
                      <div class="model_info_age">
                        27<small>yrs</small>
                      </div>
                      <div class="model_info_div">
                        <h3>Gracia Boskovic</h3>
                        <h4>United States, Florida</h4>
                      </div>
                    </a>
                  </div>
                  <div class="col span_1_of_4" style="position: relative; height: 20em; overflow:hidden;  background-image: url('https://qph.fs.quoracdn.net/main-qimg-272df88b27e1bc5cb18f00a7b8de6011-c'); background-size: cover; background-position: center; background-repeat: no-repeat;">
                    <a href="">
                      <div class="model_info_age">
                        31<small>yrs</small>
                      </div>
                      <div class="model_info_div">
                        <h3>Davis Gustavo</h3>
                        <h4>France, Paris</h4>
                      </div>
                    </a>
                  </div>
                  <div class="col span_1_of_4" style="position: relative; height: 20em; overflow:hidden;  background-image: url('https://static01.nyt.com/images/2018/02/08/t-magazine/08tmag-models-slide-9EPC/08tmag-models-slide-9EPC-articleLarge.jpg?quality=75&auto=webp'); background-size: cover; background-position: center; background-repeat: no-repeat;">
                    <a href="">
                      <div class="model_info_age">
                        17<small>yrs</small>
                      </div>
                      <div class="model_info_div">
                        <h3>Demy de Vries</h3>
                        <h4>United States, California</h4>
                      </div>
                    </a>
                  </div>
                </div>
                <a class="see-btn" href="javascript:void(0)" onclick="getincategory()">More models</a>
            </section>
  				</section>
        <section class="special features" id="job-bidding" style="text-align: left; background-color: #fff; padding: 2em 0; border-top:1px solid #e6eaef; border-bottom:1px solid #e6eaef;">
          <section id="" class="container">
            <h3 style="font-weight: bold; color:#32325d;">Browse Jobs by Categories</h3>
            <ul>
              <li>
                <a href="#">Award Ceremony</a>
              </li>
              <li>
                <a href="#">Birthday</a>
              </li>
              <li>
                <a href="#">Board Meeting</a>
              </li>
              <li>
                <a href="#">Business Dinner</a>
              </li>
              <li>
                <a href="#">Conference</a>
              </li>
              <li>
                <a href="#">Convention</a>
              </li>
              <li>
                <a href="#">Corporate Enetertainment</a>
              </li>
              <li>
                <a href="#">Executive Retreat</a>
              </li>
              <li>
                <a href="#">Family Event</a>
              </li>
              <li>
                <a href="#">Golf Event</a>
              </li>
              <li>
                <a href="#">Incentive Event</a>
              </li>
              <li>
                <a href="#">Incentive Travel</a>
              </li>
              <li>
                <a href="#">Meeting</a>
              </li>
              <li>
                <a href="#">Seminar</a>
              </li>
              <li>
                <a href="#">Team Building Event</a>
              </li>
              <li>
                <a href="#">Trade Show</a>
              </li>
              <li>
                <a href="#">Press Conference</a>
              </li>
              <li>
                <a href="#">Networking Event</a>
              </li>
              <li>
                <a href="#">Opening Ceremony</a>
              </li>
              <li>
                <a href="#">Product Launches</a>
              </li>
              <li>
                <a href="#">Theme Party</a>
              </li>
              <li>
                <a href="#">VIP Event</a>
              </li>
              <li>
                <a href="#">Trade Fair</a>
              </li>
              <li>
                <a href="#">Shareholder Meeting</a>
              </li>
              <li>
                <a href="#">Wedding</a>
              </li>
              <li>
                <a href="#">Wedding Anniversary</a>
              </li>
            </ul>
          </section>
        </section>
        <section class="special features trip_area">
          <section id="" class="container">
            <div style="text-align: center; clear: both; margin: 3em 0 7em 0;">
              <h2 style="font-weight: bold; color: #32325d;">Planning a trip? It's easy, try it:</h2>
              <h4 class="trip_steps"><p style="color: #10ac84;">1</p> Create a trip <p style="color: #ff9f43;">2</p> Get offers <p style="color: #ee5253;">3</p>Hire the one you like</h4>
              <a class="button-trip" href="{{route('login') . '?action=' . 'trip'}}">Create a trip</a>
            </div>
            <div class="card-container">
              <div class="lft-dt">
                <div class="left-dt-avt">
                  Avatar
                </div>
              </div>
              <div class="rgt-dt">
                <div class="rgt-dt-header">
                  <div class="rgt-dt-header-left">
                    <h3>Gerard Kasemba</h3>
                    <h4>United State, Boston MA</h4>
                  </div>
                  <div class="rgt-dt-header-price">
                    $100/h
                  </div>
                </div>
                <div class="rgt-dt-header-detals">
                  We have some great news for you!  We have received a settlement offer on one of your accounts.
                  Please Click Here to review the details of the settlement.  Keep in mind,
                </div>
                <div class="rgt-dt-dt">
                  <div class="rgt-dt-rate">
                    <h3 class="rgt-dt-rate-title">Rate</h3>
                    <span class="rgt-dt-rate-icons">
                      &#9734;
                      &#9733;
                      &#9733;
                      &#9733;
                      &#9733;
                    </span>
                  </div>
                  <div class="rgt-dt-review">
                    <h3 class="rgt-dt-review-title">Review</h3>
                    <span class="rgt-dt-review-icons">11k</span>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
            <div class="card-container">
              <div class="lft-dt">
                <div class="left-dt-avt">
                  Avatar
                </div>
              </div>
              <div class="rgt-dt">
                <div class="rgt-dt-header">
                  <div class="rgt-dt-header-left">
                    <h3>Gerard Kasemba</h3>
                    <h4>United State, Boston MA</h4>
                  </div>
                  <div class="rgt-dt-header-price">
                    $100/h
                  </div>
                </div>
                <div class="rgt-dt-header-detals">
                  We have some great news for you!  We have received a settlement offer on one of your accounts.
                  Please Click Here to review the details of the settlement.  Keep in mind,
                </div>
                <div class="rgt-dt-dt">
                  <div class="rgt-dt-rate">
                    <h3 class="rgt-dt-rate-title">Rate</h3>
                    <span class="rgt-dt-rate-icons">
                      &#9734;
                      &#9733;
                      &#9733;
                      &#9733;
                      &#9733;
                    </span>
                  </div>
                  <div class="rgt-dt-review">
                    <h3 class="rgt-dt-review-title">Review</h3>
                    <span class="rgt-dt-review-icons">11k</span>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
            <!-- <div class="section group">
            	<div class="col span_1_of_3">

            	</div>
            </div> -->
            <div style="clear: both; text-align: center;">
              <a class="see-btn" href="javascript:void(0)" onclick="getincategory()">More tour guides</a>
            </div>
          </sectipn>
        </section>
			<!-- Footer -->
      <script>
      $(document).ready(function(){

      	$('ul.tabs li').click(function(){
      		var tab_id = $(this).attr('data-tab');

      		$('ul.tabs li').removeClass('current');
      		$('.tab-content').removeClass('current');

      		$(this).addClass('current');
      		$("#"+tab_id).addClass('current');
      	})

      })
      </script>
 @endsection
 @section('addjavascript')

<!-- <script type="text/javascript" src="/js/home.js"></script> -->
 @endsection
