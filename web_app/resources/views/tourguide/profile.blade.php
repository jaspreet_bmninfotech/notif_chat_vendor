@extends('layouts/base')
@section('addstyle')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css"/>
<link rel="stylesheet" href="{{asset('plugin/intl-tel-input-master/build/css/intlTelInput.css')}}">
@endsection
@section('content')
<div class="job-posting-form container">
		@if(Session::has('profileRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('profileRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('addressRedirected'))
		 <div class="alert alert-danger">
		      <p style="">{{Session::get('addressRedirected')}}</p>
		    </div>
		@endif
		@if(Session::has('success'))
		    <div class="alert alert-success">
		      <p style="color:green;">{{Session::get('success')}}</p>
		      @php Session::forget('success'); @endphp
		    </div>
		@endif
	<div class="col-md-3">	
		@include('tourguide/sidenav')
	</div>
	<div class="form-fields col-md-9">
		<form role="form" method="POST" action="{{route('tourguide.profile.update')}}" id="form" class="al_rad">
			{!! csrf_field() !!}
			<div class="vfform">
				<div class="title">
					<h3>Profile</h3>
				</div>
				<div class="feildcont">
				
					<div class="row">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-6">
									<label>First Name <em>*</em></label>
									<input type="text" name="firstName" value="{{$data['firstName']}}">
									@if($errors->has('firstName'))
									<span class="text-danger">{{ $errors->first('firstName') }}</span>
									@endif
								</div>
								<div class="col-md-6 last">
									 <label>Last Name <em>*</em></label>
									<input type="text" name="lastName" value="{{$data['lastName']}}">
									@if($errors->has('lastName'))
									<span class="text-danger">{{ $errors->first('lastName') }}</span>
									@endif
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<label>Username <em>*</em></label>
									<input type="text" name="username" value="{{$data['username']}}" readonly/>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Email <em>*</em></label>
									<input type="text" name="email" value="{{$data['email']}}" readonly/>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<label>Phone No. <em>*</em></label>
										<div class="row">
											<div class="col-md-3">
													<input type="tel" id="demo" placeholder="" name="dialCode" value="{{ (empty($data['countryDialcode']) ? null :$data['countryDialcode'] )}}">
											</div>
											<div class="col-md-9 ">
												<input type="text" name="phone" value="{{$data['phone']}}" min="10" maxlength ="12">
												@if($errors->has('phone'))
												<span class="text-danger">{{ $errors->first('phone') }}</span>
												@endif     
											</div>
										</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Date of Birth</label>
									<div class="input-group date" id="datePicker">
										<input type="text" name="dob" value="{{$data['dob']}}">
										<span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
										@if($errors->has('dob'))
										<span class="text-danger">{{ $errors->first('dob') }}</span>
										@endif 
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row ">
								<div class="col-md-12">
									<div id="tprofilefiles" class="profile">

										@if($attachment!=NULL)
										<img src="/{{$attachment->path}}" class="profile" id="profileimage">
                                        @else
                                             <img src="/images/vf-inms.png" class="profile" id="profileimage">
                                        @endif
									</div>
								</div>
								<div class="col-md-12">
									<span class=" button solid btnupload fileinput-button">
								        <i class="glyphicon glyphicon-plus"></i>
								        <span>Upload Image...</span>
								        <input id="tprofileupload" type="file" name="file"  multiple>
								        <input type="hidden" id="fileattachment" name="hid">
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row ">	
						<div class="col-md-2 radioleft">
							<label>Gender</label>
								<ul class="left_list">
									<li><input class="push-left" type="radio" name="gender" value="M">
									<span class="">Male</span></li>
									<li><input class="push-left " type="radio" name="gender" value="F">
									<span class="">Female</span></li>
								</ul>
						</div>
						<div class="col-md-10 radioseries">
							<label>Race</label>
							<ul class="race">	
								@foreach($race as $key=>$racevalue)
								<li><input class="push-left" type="radio" name="race" value="{{$key}}">
								<span class="">{{$racevalue}}</span></li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
	<!-- Profile end-->
		<div class="vfform  venues" id="venues" >
				<div class="title">
				<h3>Location And Nationality</h3>
				</div>
			<div class="feildcont">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6 ">
								<label>Birth Country <em>*</em></label>
								<select name="birth_country">
									<option value="">Country</option>
									@foreach($country as $key=>$val)
									<option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
									@endforeach
								</select>
								<div class="clearfix"></div>
		                        @if($errors->has('birth_country'))
		                            <span class="text-danger">
		                                {{ $errors->first('birth_country') }}
		                            </span>
		                        @endif
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-6 ">
								<label >Country Of Residence <em>*</em></label>
								<select name="country" id="country" onchange="getstate(this)">
									<option value="">Country</option>
									@foreach($country as $key=>$val)
									<option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
									@endforeach
								</select>
								<div class="clearfix"></div>
		                        @if($errors->has('country'))
		                            <span class="text-danger">
		                                {{ $errors->first('country') }}
		                            </span>
		                        @endif
							</div>
							<div class="col-md-6 last">
								<label>State</label>
								<select name="state" id="state" onchange="getcity(this)">
									<option value="">State</option>
								</select>
								<div class="clearfix"></div>
		                        @if($errors->has('state'))
		                            <span class="text-danger">
		                                {{ $errors->first('state') }}
		                            </span>
		                        @endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>City <em>*</em></label>
								<select name="city" id="city">
									<option>City</option>
								</select>
								<div class="clearfix"></div>
		                        @if($errors->has('city'))
		                            <span class="text-danger">
		                                {{ $errors->first('city') }}
		                            </span>
		                        @endif
							</div>	
							<div class="col-md-6">
								<label>Postal Code</label>
								<input type="text" name="postalCode">
								<div class="clearfix"></div>
		                        @if($errors->has('postalCode'))
		                            <span class="text-danger">
		                                {{ $errors->first('postalCode') }}
		                            </span>
		                        @endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Street Address</label>
								<input type="text" name="address">
								<div class="clearfix"></div>
			                       @if($errors->has('address'))
		                            <span class="text-danger">
	                                	{{ $errors->first('address') }}
			                        </span>
			                        @endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 radioleft">
								<label class="margin_bottom1">Status</label><br>
								<ul class="left_list">
									@foreach($nationality_status as $key=>$val)
									<li><input class="push-left" type="radio" name="nationality_status" value="{{$key}}" >
									<span class="">{{$val}}</span></li>
									@endforeach
								</ul>
							</div>
							<div class="col-md-9">
								<label class="margin_bottom1">Upload a document supporting your claim</label>
								<div class="col-md-5 profprogdocs">
									<span class=" button solid btnupload fileinput-button">
									        <i class="glyphicon glyphicon-plus"></i>
									        <span>Upload files...</span>
									        <input id="docupload" type="file" name="file"  multiple>
									        <input type="hidden" id="fileattachment" name="hid">
									</span>
										<div id="progress" class="progress">
								        	<div class="progress-bar progress-bar-success"></div>
								    	</div>
								        <p>File not exceed 2mb</p>
									
								</div>
								<div class="col-md-7">
									<div id="docfiles" class=" profileimage-inline">
										<div class="profileimagesf">
												<?php if($docfile->path == null) {
													?>
													<p></p>
													<?php
												} else {
													?>
												<i data-id="{{$docfile->attachment_claim}}" class="fa fa-times" onclick="removedocimg({{$docfile->attachment_claim}},'{{$docfile->name}}')"></i><img src="/{{$docfile->path}}" width="100px">
												<?php
												}
												?>
											
										</div>
										
									</div>
								</div>
							</div>
												
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Location and Nationality End -->
		<!--profile start -->
		<div class="vfform  venues" id="venues" >
				<div class="title">
				<h3>Profile Photos</h3>
				</div>
			<div class="feildcont">
				<div class="row">
					<div class="col-md-12">
						<p>Add 5 photos that show others how amazing you are. This is your chance to make an awesome first impression! Remember: A friendly smile makes friends :slightly_smiling_face:
							Important: We reserve our right to change your main photo or reject photos you upload that does not comply with our profile photo guidelines. Learn more.<br/>
							� Your face should be clearly visible in your main photo.<br/>
							� Photos should be natural and bright without effects. Blurry, low-resolution photos may be removed.<br/>
							� Photos should not have text on them.<br/>
							� If other people are in your photos, it should be obvious who you are.<br/>
							� Explicit, sexually suggestive, or aggressive photos are not welcome.<br/>
							� Avoid children and pets in your photos.<br/>
							Use your five photos to leave a good first impression on local hosts and other travelers! Since photos are critical in building trust among our users, we reserve the right to reject photos or profiles that do meet the above-listed guidelines.</p><br/>
							<p>Important: We love all languages, but we kindly ask to fill out this section in English. Please avoid writing brands, company names and links to other sites in your description.</p><br/>
						<div class="row">
							
								<div class="col-md-5 profprogdocs">
									<span class=" button solid btnupload fileinput-button">
									        <i class="glyphicon glyphicon-plus"></i>
									        <span>Upload files...</span>
									        <input id="profilesupload" type="file" name="files[]"  multiple>
									        <input type="hidden" id="fileattachment" name="hid">
									</span>
										<div id="mprofileprogress" class="progress">
								        	<div class="progress-bar progress-bar-success"></div>
								    	</div>
									        <p>File not exceed 2mb</p>
								</div>
								<div class="col-md-7">
									<div id="profilesuploadappend" class="profileimage-inline">
										<?php
										foreach ($profileattach as $profileimg => $value) {
											?>
											<div class="profileimagesf">
											<i data-id="{{$value->id}}" class="fa fa-times" onclick="removeProfileimg({{$value->id}},'{{$value->name}}')"></i><img src="/{{$value->path}}" width="100px">
											</div>
										<?php
										}
										?>
									</div>
								</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--profile End -->
		<div class="vfform">
			<div class="title">
				<h3>Language & Culture</h3>
			</div>
			<div class="feildcont">	
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<label>Fluent Language</label>
									<div class="input-group" id="extend">
									 	@if(count($getlanguage) > 0)
										<div class="input-group text-field1">
											<input type="text" name="guidelanguage[]" value="{{$getlanguage[0]}}">
											<span class="input-group-addon addtext"><a href="#" class="add_field_button">Add</a></span>
										</div>
									 	@endif
										 @foreach($getlanguage as $key=>$val)
                                            @if($key != 0)
                                                <div class="input-group text-field1">
                                                    <input type="text" name="guidelanguage[]" value="{{$val}}">
                                                    <span class="input-group-addon addtext">
                                                        <a href="#" class="remove_field">X</a>
                                                    </span>
                                                </div>
                                            @endif
                                        @endforeach
									</div>
			                        @if($errors->has('guidelanguage'))
			                            <span class="text-danger">
			                                {{ $errors->first('address') }}
			                            </span>
			                        @endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="row">		
								<div class="col-md-12">
									<label>For a scale of 1 to 10 being very well.How well do you know the country</label>
									<div class="radiobut">
										<input class="one" type="radio" name="know_country" value="1" checked="">
										<span class="onelb">1</span>
										<input class="two" type="radio" name="know_country" value="2">
										<span class="onelb">2</span>
										<input class="two" type="radio" name="know_country" value="3">
										<span class="onelb">3</span>
										<input class="two" type="radio" name="know_country" value="4">
										<span class="onelb">4</span>
										<input class="two" type="radio" name="know_country" value="5">
										<span class="onelb">5</span>
										<input class="two" type="radio" name="know_country" value="6">
										<span class="onelb">6</span>
										<input class="two" type="radio" name="know_country" value="7">
										<span class="onelb">7</span>
										<input class="two" type="radio" name="know_country" value="8">
										<span class="onelb">8</span>
										<input class="two" type="radio" name="know_country" value="9">
										<span class="onelb">9</span>
										<input class="two" type="radio" name="know_country" value="10">
										<span class="onelb">10</span>
									</div>
								</div>	
							</div>
						</div>
					</div>
			</div>
		</div>
	<!-- Language and Culture end -->	
		<!-- Activities tourguide start -->
		<div class="vfform">
			<div class="title">
				<h3>Activies</h3>
			</div>
				<div class="feildcont">	
					<div class="row">		
						<div class="col-md-12">
							<label>What Activies do you do with your visitors? </label>
							<div class="touractivities">
								<ul>
									@foreach($activies as $key=>$val)
									<li><input type="checkbox" value="{{$key}}" name="tourguideactivities[]" class="push-left"><label for="" class="margin_defaullt">{{ucwords($val)}}</label></li>
									@endforeach
								</ul>
							</div>					
						</div>
					</div>
				</div>
		</div>
		<!-- Activities tourguide end -->
		<!-- Show you tourguide start -->
		<div class="vfform">
			<div class="title">
				<h3>I Will Show You</h3>
			</div>
				<div class="feildcont">	
					<div class="row">		
						<div class="col-md-12">
							<label>How would  you describe the ideal tour around your city ? Be specific</label><br>
								<textarea name="showyou" placeholder="Where would you take your visitors for a tour" id="showyou"></textarea>	
						</div>
					</div>
				</div>
		</div>
		<!-- Show you tourguide end -->
		<div class="vfform">
			<div class="title">
				<h3>Transportation</h3>
			</div>
			<div class="feildcont">
				
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-3">
								<label>Do you have a car ?</label>	
							</div>
							<div class="col-md-6 radiobut radbutn" >
								<input class="one" type="radio" name="isCars" value="yes">
								<span class="onelb">Yes</span>
								<input class="two" type="radio" name="isCars" value="no" >
								<span class="onelb">No</span>		
							</div>
						</div>
						<div class="row" id="carforTour">
							<div class="col-md-6">
								<label>Will you be using your car for the tour ?</label>	
							</div>
							<div class="col-md-6 radiobut cartour">
								<input class="one" type="radio"   name="carforTour" value="yes" >
								<span class="onelb">Yes</span>
								<input class="two" type="radio"  name="carforTour" value="no">
								<span class="onelb">No</span>	
							</div>
						</div>
						<div id="cardocuments">
							<div class="row" id="cardocs">
								<div class="col-md-4">
									<label>Make</label>	
									<select name="make" id="">
											<option value="">-Select-</option>		
											<option value="10oct">10 OCT</option>		
									</select>
								</div>
								<div class="col-md-4">
									<label>Model</label>
									<select name="model" id="">
											<option value="" >-Select-</option>					
											<option value="2012" >2012</option>					
											<option value="2015" >2015</option>					
									</select>	
								</div>
								<div class="col-md-4">
									<label>Year</label>	
									<select name="year" id="year">
											<option value="">-Select-</option>	
											<option value="2012">2012</option>	
											<option value="1999">1999</option>	
									</select>
								</div>
							</div>
							<div class="row" id="plate_licence_no">
								<div class="col-md-6">
									<label>Plate No.</label>	
									<input type="text" name="plateNo">
								</div>
								<div class="col-md-6">
									<label>Driving Liscence No.</label>
									<input type="text" name="licenceNo">	
								</div>
							</div>
							<div class="row" id="licencecopy">
								<div class="col-md-12">
									<label class="margin_bottom1">Upload a copy of your licence</label>
									<div class="row">
										<div class="col-md-5 profprogdocs">
											<span class=" button solid btnupload fileinput-button">
												        <i class="glyphicon glyphicon-plus"></i>
												        <span>Upload licence...</span>
												        <input id="licenceupload" type="file" name="file"  multiple>
												        <input type="hidden" id="fileattachment" name="hid">
											</span>
											<div id="progresslice" class="progress">
									        	<div class="progress-bar progress-bar-success"></div>
									    	</div>
									        <p>File must not exceed 2mb</p>
										</div>
										<div class="col-md-7">
											<div id="licencefiles" class="viewimg">
											</div>	
												<div class="profileimagesf">
												<?php if ($licencefile->path == NULL) {
													?>
													<p></p>
													<?php
												} else {
													?>
												<i data-id="{{$licencefile->attachment_licence}}" class="fa fa-times" onclick="removelicimg({{$licencefile->attachment_licence}})"></i><img src="/{{$licencefile->path}}" width="100px">
												<?php
												}
												?>
												</div>
										</div>
									</div>
								</div>
							</div>	
							<div class="row" id="isCarinsurance">
								<div class="col-md-5">
									<label>Do you have a car insurance ?</label>	
								</div>
								<div class="col-md-7 radiobut insurance">
									<input class="one" type="radio" name="carinsurance" value="yes" >
									<span class="onelb">Yes</span>
									<input class="two" type="radio" name="carinsurance" value="no">
									<span class="onelb">No</span>	
								</div>
							</div>
							<div class="row" id="insurance_reason">
								<div class="col-md-6">
									<textarea name="insurancereason" cols="10" rows="5" placeholder="reason" id="reason"></textarea>
								</div>
							</div>
							<div class="row" id="insurance_docs">
								<div class="col-md-12">
									<label class="margin_bottom1">Upload a copy of your insurance Document</label>
									<div class="row">
										<div class="col-md-8 loaddoc">
											<div class="col-md-6 profprogdocs">
												<span class=" button solid btnupload fileinput-button">
												        <i class="glyphicon glyphicon-plus"></i>
												        <span>Upload insurance...</span>
												        <input id="insuranceupload" type="file" name="file"  multiple>
												        <input type="hidden" id="fileattachment" name="hid">
												</span>
										        <p>File must not exceed 2mb</p>
											</div>
											<div class="col-md-6">
												<div id="insurancefiles" class="viewimg">
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>	
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
		
		<div class="text-center">
			<button type="submit" id="" class="button solid ">Save Profile</button>
		</div>		
		<br><br>
		</form>			
	</div>
</div>
@endsection
@section('addjavascript')	
	<script type="text/javascript" src="{{asset('js/tourGuideProfile.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/tourGuide.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/address.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
	<script src="{{ asset('plugin/intl-tel-input-master/build/js/intlTelInput.js') }}"></script>
	<script type="text/javascript">
	
		$("#demo").intlTelInput();
		var code=$('.country-list  .country').find('.dial-code').html();
		$('input[type=tel]').val(code);
		$("#demo").intlTelInput("loadUtils", "lib/libphonenumber/build/utils.js");
		$("#mobile-number").intlTelInput();
	
				// $('.country-list .country').each(function(e, v){
				// 	if($(this).find('.dial-code').html() == '<?php echo $data['countryDialcode']; ?>'){
				// 		$(this).find('country').addClass('preferred highlight active');
				// 	}
				// });

					// console.log($("li[data-dial-code = $('.selected-flag').attr('title').split(':')[1] ]").html());
				$('input[name = dialCode]').val($('.selected-flag').attr('title').split(':')[1]);
				$('.country-list li').click(function(){
					$('input[name = dialCode]').val($(this).find('.dial-code').html());
				});
</script>
@endsection
