	<div class="sidebar_widget">
    
	    	<div class="sidebar_title"><h4>Setting</h4></div>
	        
			<ul class="arrows_list1">		
	            <li><a href="{{route('tourguide.profile')}}"><i class="fa fa-angle-right"></i> Profile</a></li>
	            <li><a href="{{route('tourguide.payment')}}"><i class="fa fa-angle-right"></i> Payment</a></li>
	            <li><a href="{{route('tourguide.profiles.changePassword')}}"><i class="fa fa-angle-right"></i> Change password</a></li>
			</ul>
	</div>
	