@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
    <div class="row">
        <div class="vendor-job-list form-fields panel-group">
            <div class="vfform panel-group" id="">
                <div class="title">
                    <h3>Tour Guide Trips</h3>
                </div>
                <div class="panel-body">
                   <?php
                    if($tourguideinvite->isEmpty()) {
                        ?>
                         <div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="top-bot-margin1">
                                        <div class="col-md-12 text-center ">
                                            <h3>Currently you have no Trip</h3>
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    <?php
                     }else{ ?>
                        <div class="">
                            <div class="col-md-3">
                                <h4>Title</h4>
                            </div>
                            <div class="col-md-2">
                                <h4>Status</h4>
                            </div>
                    <div class="col-md-2">
                                <h4>Accepted</h4>
                            </div>
                            <div class="col-md-2">
                                <h4>SENT AT</h4>
                            </div>
                    <div class="col-md-3">
                                <h4>ACTION</h4>
                            </div>
                        </div>
                   
                         @foreach ($tourguideinvite as $jobs)
                            {{ csrf_field() }}
                        <div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="vf-bids">
                                       <div class="col-md-3 ">
                                            <div class="bid-detail">
                                                {{$jobs->title}}
                                            </div>
                                        </div>
                                        <div class="col-md-2 ">
                                            <div class="bid-detail">
                                                <?php
                                                if ($jobs->status == '1') {
                                                      ?>
                                                <p>Open</p>
                                                <?php
                                                } elseif ($jobs->status == '0') {
                                                    ?>
                                                <p>Close</p>
                                                    <?php
                                                } elseif ($jobs->status == '2') {
                                                    ?>
                                                <p>Expired</p>

                                                    <?php
                                                }
                                                ?>
                                                
                                            </div>
                                        </div>
                                <div class="col-md-2 ">
                                            <div class="bid-detail">
                                                <?php
                                                if ($jobs->clientStatus == '2') {
                                                      ?>
                                                <p>Pending</p>
                                                <?php
                                                } elseif ($jobs->clientStatus == '1') {
                                                    ?>
                                                <p>Accepted</p>
                                                    <?php
                                                } elseif ($jobs->clientStatus == '0') {
                                                    ?>
                                                <p>Declined</p>

                                                    <?php
                                                }
                                                ?>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="bid-detail">
                                                {{date("m/d/Y", strtotime($jobs->created_at))}}
                                            </div>
                                        </div>
                                <div class="col-md-3 ">
                                            <div class="bid-detail">
                                                <?php
                                                if ($jobs->clientStatus == '1') {
                                                    ?>
                                                <a href="/tourguide/job/{{$jobs->tourguide_job_id}}/detail" class="button solid">View Job Detail </a>
                                                <?php
                                                } elseif ($jobs->clientStatus == '2'){
                                                ?>
                                                <a href="/tourguide/job/{{$jobs->tourguide_job_id}}/detail" class="button solid">Accept  </a>
                                                <a href="javascript:;" class="button default" onclick="declined({{$jobs->tourguide_job_id}})" >Declined </a>

                                                <?php
                                                }elseif ($jobs->clientStatus == '0'){
                                                    ?>
                                                    <p>Declined</p>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                            <div class="paginate push-right">  
                                    {{ $tourguideinvite->render() }}
                            </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>  
</div>
        

  @endsection
   @section('addjavascript')
    <script src="{{asset('js/tourGuide.js')}}"></script>
    @endsection