@extends('layouts/base')
@section('content')
<div class="job-posting-form container">
	@if(Session::has('paymentRedirected'))
	 <div class="alert alert-danger">
	      <p style="">{{Session::get('paymentRedirected')}}</p>
	    </div>
	@endif
	@if(Session::has('success'))
	    <div class="alert alert-success">
	      <p style="color:green;">{{Session::get('success')}}</p>
	      @php Session::forget('success'); @endphp
	    </div>
	@endif
	<div class="col-md-3">
		@include('tourguide/sidenav')
	</div>
	<div class="form-fields col-md-9">
		<div class="vfform setformlab">
	        <div class="title">
				<h3>Payment Method</h3>  
	        </div> 
	        <div class="feildcont">
	            <form role="form" method="POST" autocomplete="off" action ="{{route('tourguide.payment.save')}}">
	            	{!! csrf_field() !!}
	            	<div class="row ">
            	  			<div class="col-md-3 onecheck">
            	  				<input type="checkbox" name="hasFree" class="push-left" value="1" ><label style="line-height: 1;" for="">Free</label><br>
            	  			</div>
    	  			</div>

    	  			<div class="clearfix"></div>
            	  	<div class="row">
            	  		<div class="col-md-12">

		                    <label>How much do you charge per hour? <em>*</em></label>
	                    </div>
		            		<div class="col-md-3">
		                    	<select name="currencyPerhour">
		                            <option value="usd">USD</option>
		                            <option value="rupee">Rupee</option>
		                        </select>

	                        </div>  
			                <div class="col-md-6 last">
			                    <input type="text" name="rateperHour" value="{{$data['rateperHour']}}" >
			                     @if($errors->has('rateperHour'))
									<span class="text-danger">{{ $errors->first('rateperHour') }}</span>
								@endif
			                </div>
                  	</div>
                  	<div class="clearfix"></div>
                  	<div class="row">
                  		<div class="col-md-12">
		                    <label>What is your fix rate per project? <em>*</em></label>
	                    </div>
		                <div class="col-md-3">
		                    	<select name="currencyPerproject">

		                            <option value="usd">USD</option>
		                            <option value="rupee">Rupee</option>
		                        </select>
		                </div>
		                <div class="col-md-6 last">
		                   	<input type="text" name="rateperProject" value="{{$data['rateperProject']}}">
		                   	@if($errors->has('rateperProject'))
									<span class="text-danger">{{ $errors->first('rateperProject') }}</span>
							@endif
		                </div>
	            	</div>
	            	
	            	<div class="row">
	            		<div class="col-md-12">
		                    <label>Minimum Tour Duration <em>*</em></label>
	                    </div>
		                <div class="col-md-6">
		                    	<select name="minduration">
		                			<option value="">select</option>
				                	@foreach($tourduration as $k=>$v)
			                            <option value="{{$k}}" {{ ($data['minDuration'] == $k) ? "Selected" : "" }}>{{$v}}</option>
			                         @endforeach   
		                        </select>
		                         @if($errors->has('minduration'))
									<span class="text-danger">{{ $errors->first('minduration') }}</span>
								@endif
		                </div>
	            	</div>
    		</div>
		</div>
			<div class="text-center">
				<button type="submit"  class="button solid">Submit</button>
			</div>
		
			<br><br>
	            </form>		
	<!--  form end -->				
	</div>
</div>
@endsection