@extends('../layouts/base')
@section('content')

    <div class="container job-deatils-page">
        <div class="row">
            <div class="vendor-job-list form-fields panel-group">
                <div class="vfform margin-zero" id="">
            <div class="col-md-8">
                <div class="col-md-12">
                    <div class="row">
                        <br>
                        <div class="col-md-12">
                    <div class="col-md-4">
                        <h4>Status:-</h4>
                    </div>
                    <div class="col-md-8">
                        {{$tourguidehire->status == 2 ? "Active" : "Closed" }}
                    </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Contract Title:-</h4>
                            </div>
                            <div class="col-md-8">
                                {{$tourguidehire->tourguidejob->title}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Number of people:-</h4>
                            </div>
                            <div class="col-md-8">
                                {{$tourguidehire->tourguidejob->totalpeople}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Start Date:-</h4>
                            </div>
                            <div class="col-md-8">
                                {{date('m-d-y', strtotime($tourguidehire->tourguidejob->fromdatetime))}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>End Date:-</h4>
                            </div>
                            <div class="col-md-8">
                                {{date('m-d-y', strtotime($tourguidehire->tourguidejob->todatetime))}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>From:-</h4>
                            </div>
                            <div class="col-md-8">
                                <span>{{$from_country_id['name']}}</span><br><span>{{$from_state_id['name']}}</span><br><span>{{$from_city_id['name']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                                <br>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>To:-</h4>
                            </div>
                            <div class="col-md-8">
                                <span>{{$to_country_id['name']}}</span><br><span>{{$to_state_id['name']}}</span><br><span>{{$to_city_id['name']}}</span>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Job Category</h4>
                            </div>
                            <div class="col-md-8">
                               
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Hourly Rate</h4>
                            </div>
                            <div class="col-md-8">
                                description?????
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Offer Date</h4>
                            </div>
                            <div class="col-md-8">
                                description?????
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Hourly Rate</h4>
                                <p>Total amount the client will see</p>
                            </div>
                            <div class="col-md-8 text-right">
                                <h4></h4>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>20% service fee </h4>
                                <a>Explain this</a>
                            </div>
                            <div class="col-md-8 text-right">
                                <h4></h4>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>You will be pais</h4>
                                <p>Estimated amount you will receive</p>
                            </div>
                            <div class="col-md-8 text-right">
                                <h4></h4>
                            </div>

                        </div>
                    </div>
                    <hr />
                    <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <h4>Work Description</h4>
                        </div>
                        <div class="col-md-8" style="white-space:normal">
                            
                        </div>
                    </div> --}}
                    </div>
                </div>
            
            <div class="col-md-4">
                <div class="col-md-12">
                    <br />
                    <div class="profileimg">
                        <img src="/{{$user->profileimg[0]['path']}}">
                        <span></span>
                    </div>
                    <div class="text-center"><h3>{{$user['username']}}</h3></div><br>
                    <div class="col-md-10 col-md-offset-1">
                        <button class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#myModal_AC">Accept Offer</button>
                        <br/>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <button class="btn btn-warning btn-lg btn-block" data-toggle="modal" data-target="#myModal_DC">Decline Offer</button>
                        <br/>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>
    <div id="myModal_AC" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                    <h3>Accept Offer</h3>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <form class="form" action="{{route('tourguide.hire.accept', ['id' => $tourguidehire->id])}}" method="post" id="hire-accept">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <div class="col-md-12">
                                    <h4 for="message-text" class="form-control-label">Message to client:</h4>
                                    <textarea name="message" class="form-control" id="message-text" rows="10" maxlength="5000"></textarea>
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <h4>Agree to Terms</h4>
                                    </div>
                                <div class="col-md-12">
                                    <input type="checkbox" name="check" id="hire_tnc"><span> Yes I agree to al .......</span>
                                </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <button type="button" class="button solid" onclick="validate_hire(this)">Accept</button>
                                        <button type="button" class="btn-default button cancel_btn" data-dismiss="modal" aria-label="Close">
                                           Close
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="myModal_DC" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                    <h3>Decline Offer</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <form class="form" action="{{route('tourguide.hire.decline', ['id' => $tourguidehire->id])}}" method="post" id="hire-decline">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <h4 for="message-text" class="form-control-label">Reason:</h4>
                                        <select class="form-control" name="reason" required id="reason-decline">
                                            <option value="">Select a reason</option>
                                            @if($reasons->isNotEmpty())
                                                @foreach($reasons as $reason)
                                                    <option value="{{$reason->id}}">{{$reason->title}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <h4 for="message-text" class="form-control-label">Message to client (optional)</h4>
                                        <textarea name="message" class="form-control" id="message-text" rows="10" maxlength="5000"></textarea>
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <button type="button" class="button solid" onclick="checkDecline(this)">Decline</button>
                                        <button type="button" class="btn-default button cancel_btn" data-dismiss="modal" aria-label="Close">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('addjavascript')
<script>
    function validate_hire(event) {
        if($(event).closest('form').find('#hire_tnc').is(':checked')) {
            $('#hire-accept').submit();
        }
    }
    function checkDecline(event) {
        var select = $(event).closest('form').find('select#reason-decline');
        if(select.val() == '') {
            select.css('border', '1px solid red');
        }else {
            $('form#hire-decline').submit();
        }
    }
</script>
@endsection