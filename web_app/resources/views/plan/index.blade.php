    @extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/plan.css')}}" type="text/css" />
    @endsection
    @section('content')
    <div class="container client-content">
        
        <div class="row">
            <div class="col-md-12 title">
                @if(Auth::user()->type=='vn')
                <h2>Vendor & Pro Vendor Plan</h2>
                @else
                <h2>Client Plan</h2> 
                @endif
            </div>
        </div>
        <div class="row wrapper plan-wrapper">
            {{--{{ print_r($plans, true) }}--}}
            @foreach($plans as $plan)
                <div class="plan">
                    <div class="pricingtable9 pricingvalue9">
                        <h3>{{ $plan["name"] }}</h3>
                        <strong> ${{ $plan["price"] }} </strong>
                        <b> / month </b> <br> <br>
                        <a href="{{ route('plan.save', $plan["id"]) }}" class="button five"> BUY NOW! </a>
                        <span>
                        @foreach($plan["descriptions"] as $desc)
                           <i class="fa fa-check"></i> {{ $desc }} </br>
                        @endforeach
                    </span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @endsection


