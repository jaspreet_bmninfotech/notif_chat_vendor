@include ("../inc/vf-header")	
<div class="job-posting-container container">
	<section class="job-posting margin-top-bottom">
		<div class="job-posting-top-bottom job-posting-bottom-line">
			<h2>Job Postings</h2>
			<div class="pull-right">
				<input type="text" class="search-input-field btn-input-field-text" name="search" placeholder="Find Posted Jobs">
			   <a href="{{asset('job-post-form')}}"><input type="button" class="btn-job-post btn-input-field-text" name="post_job" value="Post New Job"></a>
			   <input type="hidden" id="hid" value="{{($user_id)}}">
			</div>	
		</div>
		<table >
			<thead>
				<tr>
					<th>Job Title</th>
					<th>Bids</th>
					<th>Messages</th>
					<th>Hires</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody class="job">
				{{-- <tr>
					<td>
						<h3>Job title will be here</h3>
						<span>$1000</span>
						<span>Posted 4 days ago</span>
					</td>
					<td>12</td>
					<td>34</td>
					<td>12</td>
					<td>Public</td>	
				</tr> --}}
			</tbody>
		</table>
		<div class="pagging pull-right ">
			<ul>			
				<li>1</li>
				<li>2</li>
				<li>3</li>
				<li class="lll">4</li>
			</ul>
		</div>
	</section>
	<section class="job-posting margin-top-bottom">
		<div class="job-posting-top-bottom job-posting-bottom-line">
			<h2>Teams</h2>
			<div class="pull-right">
				 <input type="button" class="btn-job-post btn-input-field-text" name="post_job" value="Create New Team">
			</div>	
		</div>
		<table>
			<thead>
				<tr>
					<th>Team Name</th>
					<th>MEMBERS</th>
					<th>MESSAGES</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<h3>The cool kids in the town</h3>
					</td>
					<td>40</td>
					<td>5</td>
				</tr>
			</tbody>
		</table>
		<div class="pagging pull-right job-posting-top-bottom">
			<ul>
				<li>1</li>
				<li>2</li>
				<li>3</li>
				<li>4</li>
			</ul>
		</div>
	</section>
	<section class="job-posting margin-top-bottom">
		<div class="job-posting-top-bottom job-posting-bottom-line">
			<h2>Contracts</h2>
			<div class="pull-right">
				<input type="text" class="search-input-field btn-input-field-text" name="search" placeholder="Search">
			</div>	 
		</div>

		<table>
			<thead>
				<tr>
					<th>VENDOR</th>
					<th>PRICE</th>
					<th>HOURS</th>
					<th>DATE</th>
					<th>TX-ID</th>
					<th>STATUS</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>						
						<img src="images/people-vendor-128.png" width="40px" height="40px"><h3 class="inline">GerardKasemba<br>-photographer</h3>
					</td>
					<td>$10k</td>
					<td>14 H 12min</td>
					<td>March 25 2017</td>
					<td>xxx xxxx xxx</td>
					<td>Completed</td>
				</tr>
				<tr>
					<td>				
						<img src="images/people-vendor-128.png" width="40px" height="40px"><h3 class="inline"> PiamyMoto<br>-Model</h3>
					</td>
					<td>$30H</td>
					<td>...........</td>
					<td>...........</td>
					<td>xxx xxxx xxx</td>
					<td>On Going<br>Completed</td>
				</tr>
			</tbody>
		</table>
		<div class="pagging pull-right job-posting-top-bottom">
			<ul>
				<li>1</li>
				<li>2</li>
				<li>3</li>
				<li>4</li>
			</ul>
		</div>
	</section>
		</div>
	</div>
</div>

<script type="text/javascript">
	
$(function jobposting()
{
	var id= $('#hid').val();
	alert(id);
	$.ajax({
            url : 'dashboard/client/{id}',
            type : 'GET',
            dataType : 'json', 
            data:
            	"id="+id,
            success : function(response)
            {   
             		for(var i in response)
             		var tr="<tr>"+
					"<td>"+
						"<h3>"+response[i]['title']+"</h3>"+
						"<span>"+response$1000+"</span>"+
						"<span>"+Posted+4+days+ago+"</span>"+
					"</td>"+
					"<td>"+response[i]['bids']+"</td>"+
					"<td>"+response[i]['message']+"</td>"+
					"<td>"+response[i]['']+"</td>"+
					"<td>"+Public+"</td>"+	
				"</tr>";	
				$('.job').append(tr);
               		console.log('response');                
            }
    });
});
</script>
@include ("../inc/vf-footer")