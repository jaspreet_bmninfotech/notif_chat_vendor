<?php

namespace Modules\Admin\Http\Controllers\user;
use Illuminate\Http\Request;
use App\User;
use App\State;
use App\City;
use App\Attachment;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Redirect;
use Auth;
use Input;
use Session;
use DB;
use App\Country;
use App\Address;

class UserController extends Controller
{
   public function __construct()
    {
    
        $this->middleware('guest')->except('logout');
    }
  public function index()
  {
     $country= Country::all();
    return view('admin::user.addnewuser',['country'=>$country]);
  } 
  public function state(Request $request,$id)
    {
      $data   =State::select('id','name')->where('country_id',$id)
          ->get();
       return response()->json($data);
    }
     public function city(Request $request,$id)
    {
    $data   = City::select('id','name')->where('state_id',$id)
        ->get();
       return response()->json($data);
    }
  public function create(Request $request)
  {
     if($request->isMethod('post'))
        {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'username' => 'required|string|max:255|unique:admin_user',
            'password' => 'required|min:6',
            'accounttype' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'country' => 'required',
            'state' => 'required',
            'postalCode' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.adduser')->withErrors($validator)->withInput();
            // return redirect()->route('admin.adduser')
            //             ->withErrors($validator)
            //             ->withInput();
        }
          $file = $request->file;  
           if ($file != null) {
                                $fileid = count($file);
                                 $id = Auth::id();
                                $destinationPath = "uploads/admin/user/pages/";
                                   $fileSize = $file->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $file->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $file = $file->move($destinationPath, $fileName);
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $file,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $image_id = $datas->id;
                                    
                                 }
                            
           }else {
            $image_id = null;
           }
         // $data=Input::except(array('_token'));
          // $data =[
          //         'username'          => $request->username,
          //         'type'              => $request->accounttype,
          //         'email'             => $request->email,
          //         'password'          => bcrypt($request->password),
          //         'firstName'     =>$request->firstName,
          //         'lastName'      =>$request->lastName,
          //         'dob'       =>$request->dob,
          //         'profile_image'   =>$image_id,
          //         'gender'      =>$request->gender,
          //         'countryDialcode' => $request->dialCode,
          //         'phone'       => $request->phone
          //     ];
        $address=Null;
        $data=Input::except(array('_token'));
        
         
              
              $user =User::create(['username' => $request->username,
                  'type'              => $request->accounttype,
                  'email'             => $request->email,
                  'password'          => bcrypt($request->password),
                  'firstName'     =>$request->firstName,
                  'lastName'      =>$request->lastName,
                  'dob'       =>$request->dob,
                  'profile_image'   =>$image_id,
                  'gender'      =>$request->gender,
                  'countryDialcode' => $request->dialCode,
                  'phone'       => $request->phone]);
              $user->save();
              $userId=$user->id;
              $address_id =User::where('id',$userId)->select('address_id')->first();   
              $addressid=$address_id->address_id;
            $address = [
                    'country_id'    => $request->get('country'),
                    'state_id'      => $request->get('state'),
                    'city_id'       => $request->get('city'),
                    'postalCode'    => $request->get('postalCode'),
                    'address'       => $request->get('address')
                    ];      
          if($address!= NULL){
              if($addressid    != NULL)
              {
                  $address        =   Address::where('id',$addressid)
                                      ->update($address);   
              }else{
                  $address = Address::create($address);
                  $address->save();
                  $addressId      = $address->id;
                  $address =User::select('address_id')->where('id',$userId)
                                      ->update(['address_id'  =>$addressId
                                              ]);   
              }

          }
          
          Session::flash('success','Successful Profile created');
          return redirect()->route('admin.adduser');
      }
      
  }
  public function list()
  {
    $user =User::all();
    return view('admin::user.userlist',['user'=>$user]);
  }
  public function edit(Request $request,$id)
  {
    
     $user= \DB::table('user')
                            ->leftjoin('address','user.address_id','=','address.id')
                            ->select('user.id as userid','user.*','address.*')
                            ->where('user.id',$id)->first();
    $attachment =\DB::table('attachment')->select('path')->where('id','=',$user->profile_image)->first();                  
    $country= Country::all();
    return view('admin::user.useredit',['user'=>$user,'country'=>$country,'attachment'=>$attachment]);
  }
 
  public function update(Request $request,$id)
  {

    if($request->isMethod('post'))
    {
      $userImageid = User::select('profile_image')->where('id','=',$id)->first();
      $file = Input::file('file');
            
           if ($file != null) {
                                $fileid = count($file);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/user/pages/";
                                   $fileSize = $file->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $file->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $file = $file->move($destinationPath, $fileName);
                                    $datas = Attachment::where('id',$userImageid['profile_image'])->update(
                                                     ['name' => $fileName,
                                                      'path' => $file,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $image_id = $userImageid['profile_image'];
                                    
                                 }
                            
           }else {
            $image_id = $userImageid['profile_image'];
           }
    $address=Null;
        $data=Input::except(array('_token'));
        $rule=array(
                'firstName'=>'Required_with:lastname|Alpha',
                'lastName' =>'Required_with:firstName|Alpha',
                'phone'     =>  'required|min:10|max:12',
                'postalCode'=>  'required',
                'address'   =>  'required'
        );      
        $validator=Validator::make($data, $rule);
        if($validator->fails()) {
            return redirect()->route('admin.user.edit',$id)->withErrors($validator)->withInput();;
        }
          $data =[
                  'username'          => $request->username,
                  'type'              => $request->accounttype,
                  'email'             => $request->email,
                  'password'          => bcrypt($request->password),
                  'firstName'     =>$request->firstName,
                  'lastName'      =>$request->lastName,
                  'profile_image'     =>$image_id,
                  'dob'       =>$request->dob,
                  'gender'      =>$request->gender,
                  'phone'       => $request->phone
              ]; 
              
              $user =User::where('id',$id)->update($data);
              $address_id =User::where('id',$id)->select('address_id')->first();
              $addressid=$address_id->address_id;
               
            $address = [
                    'country_id'    => $request->get('country'),
                    'state_id'      => $request->get('state'),
                    'city_id'       => $request->get('city'),
                    'postalCode'    => $request->get('postalCode'),
                    'address'       => $request->get('address')
                    ];      
          if($address!= NULL){
              if($addressid    != NULL)
              {
                  $address        =   Address::where('id',$addressid)
                                      ->update($address);   
              }else{
                  $address = Address::create($address);
                  $address->save();
                  $addressId      = $address->id;
                  $address =User::select('address_id')->where('id',$userId)
                                      ->update(['address_id'  =>$addressId
                                              ]);   
              }

          }
          
      return redirect()->route('admin.users')->with('success','Successfully User Update');
    }
    return redirect()->route('admin.users')->withInput();
  }
   public function delete($id)
    {
       User::find($id)->delete();
      return redirect()->route('admin.users')->with('success','Successfully User Delete');
    }

}