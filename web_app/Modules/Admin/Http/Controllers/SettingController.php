<?php

namespace Modules\Admin\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Setting;
use App\Category;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Redirect;
use Auth;
use Input;
use session;
use Modules\Admin\SettingRepositories\Home\HomeRepository;
use Modules\Admin\SettingRepositories\Home\HomeInterface as HomeInterface;

class SettingController extends Controller
{
	 public $setting;
	 public $category ; 

   public function __construct(HomeInterface $category,HomeInterface $setting)
   {
       // set the model

       // $obj = new HomeInterface($setting , $category);
       $this->setting = $setting;
       $this->category = $category;
   }
	public function index()
	{
		$setting = Setting::get();
		return view('admin::setting.costperjob',['setting'=>$setting]);
	}
	// public function create(Request $request)
	// {
	// 	if($request->isMethod('get'))
	// 	{
	// 		return view('admin::setting.addcost');
	// 	}
		
	// 	$costvalue =$request->get('costvalue');
	// 	$costdesc =$request->get('description');
	// 	$data =Setting::create([
	// 							'value' =>$costvalue,
	// 							'description'=>$costdesc
	// 							]);
	// 	$data->save();
	// 	return redirect()->route('admin.setting.insert');

	// }
	public function update(Request $request){

		$id = $request->jobId;
		
		$value = $request->value;
		
		$setting =Setting::where('id',$id)->update(['value'=> $value
													
													]);
		return response('success');
	}
	public function home()
	{
		$category = $this->category->all();
		$getcat=$this->setting->show();

		if($getcat !=NULL)
		{
			$ar= array_column($getcat,'id');

	    	if(is_array($ar))
	    	{
				$sresult = implode("-",$ar);
				$nresult = '-'.$sresult;
	    	}
		return view('admin::setting.home',['category'=>$category,'getcat'=>$getcat,'strcat'=>$nresult]);	
		}
		else{
			$getcat="NULL";
			return view('admin::setting.home',['category'=>$category,'getcat'=>$getcat]);
		}
	}
	public function getcategory(Request $request)
	{
		$cat= Category::where('id',$request->Id)->select('name','id')->first();
		return response()->json($cat);
	}
	public function managecategory(Request $request)
	{
		if($request->isMethod('post'))
		{	
			$category = $this->setting->create($request->all());
			if($category==true)
			{
				return Back();
			}
			
		}
	}
	 public function removecategory(Request $request){
            $remove_id= $this->setting->deletecategory($request->r_id);
            if($remove_id==true)
            {
            	return response('success');
            	
            }
            else{
            	return response('error');
            }
        }
	

}