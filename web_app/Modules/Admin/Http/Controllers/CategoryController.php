<?php
namespace Modules\Admin\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Category;
use App\Attachment;
use App\Admin;
use Validator;
use Redirect;
use Auth;
use Input;
use Session;
class CategoryController extends Controller
{
    //<!--Category add--->
    public function insert()
    {
        $category=Category::all();
        return view('admin::category.insert',['category'=>$category]);

    }
     
     //<!--Category Edit-->
    public function edit($id)
    {
      
      $category =Category::find($id);         
      return view('admin::category.index',compact('category')); 
       // $info=DB::table('category')->where('id',$id)->first();          
    }
    
    //<!--Category Update Function-->
    public function update(Request $request)
    {
          $data=Input::except(array('_token'));

          $rule=array(
              'getcatname'=>'required',
              'getcatdesc'=>'required',
          );
          $validator=Validator::make($data,$rule);
          if($validator->passes()) 
          {
          $new=[
            'name'=>$request->getcatname,
            'description'=>$request->getcatdesc
          ];
          $update=Category::where('id',$request->id)->update($new);
          return response()->json(['success'=>$update]);
          }
          return response()->json(['errors' => $validator->errors()]);
    }

      
      //<!--Category Delete-->
    public function delete($id)
    {
       Category::find($id)->delete();
       /*dd($id);
      die();*/
      return redirect()->route('category.list')->with('success','Successfully Category Delete');
    }

      //<!--Category View--->
    public function index()
    {
        $category=Category::where('parent_id',NULL)->get();
        return view('admin::category.index', compact('category'));    
    }
   
     //<!--Category Add Insert Function-->
    public function addinsert(Request $request)
    { 
          $data=Input::except(array('_token'));
          $rule=array(
              'categoryName'=>'required',
              
          );
          $validator=Validator::make($data,$rule);
           if ($validator->passes()) 
          {
           $category = Category::create(array('name'=>$request->categoryName,'description' => $request->description));
           $category->save(); 
           $dataId = $category->id;
           $file = $request->catfile;
           if ($file != null) {
                  $fileid = count($file);
                   // $id = Auth::id();
                  $destinationPath = "uploads/category/icons/";
                     $fileSize = $file->getClientSize();
                      if ($fileSize < 2000000) {
                      $ext = $file->getClientOriginalExtension();
                      $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                      $file = $file->move($destinationPath, $fileName);
                      $datas = Attachment::create(
                                       ['name' => $fileName,
                                        'path' => $file,
                                        'mimeType' => $ext
                                      ]
                                  );
                      $datas->save();
                      $image_id = $datas->id;
                      $iconupdate = Category::where('id',$dataId)->update(['attachment_id'=>$image_id]);
                      
                   }else {
                    return Redirect::back()->withErrors(['errors' => 'file size not accepted']);
                   }
                            
           }
            return redirect()->back()->with(['success'=>'category add successfully','id'=>$dataId]);   
          }
          return Redirect::back()->withErrors(['errors' => $validator->errors()]);   
    }
     public function addicon(Request $request)
    { 

      $file = $request->file('catfile');
      $fileName = $file->getClientOriginalName();
      $fileExtension = $file->getClientOriginalExtension();
      $destination = 'category/icons';
      $addFile = $file->move($destination , $fileName);
          $data=Input::except(array('_token'));
          $rule=array(
              'name'        =>'required',
              
          );
          $validator=Validator::make($data,$rule);
           if ($validator->passes()) 
          {
           $category = Category::create(array('name'=>$request->name,'description' => $request->description));
           $category->save(); 
            return response()->json(['success'=>$category]);   
          }
          return response()->json(['errors' => $validator->errors()]);   
    }
    public function getsubcategory($id)
    {
      $cat= Category::find($id);
      $subcategory= Category::where('parent_id',$id)->select('id','name','description')->get();
      return view('admin::category.subCategoriesList',compact('subcategory','cat'));
    }
    public function addsubcategory(Request $request)
    { 
      $data = $request->except(array('_token'));
      $rule=array(
          'subname' =>'required',    
        );
      $validator=Validator::make($data,$rule);
      if ($validator->passes()) 
      {
        $hasdeleted=Category::where('id',$request->Id)->first();
        if($hasdeleted!=NULL)
        {
          $data =[
            'name'  =>$request->subname,
            'description' =>$request->subdesc,
            'parent_id'   =>$request->Id
          ];
          $subcat= Category::create($data);
          $subcat->save();
          return response()->json(['success'=>$subcat]);
        }
      }
        return response()->json(['errors' => $validator->errors()]);
    }
     public function updatesubcategory(Request $request)
    {
          $data=Input::except(array('_token'));
          $rule=array(
              'name'=>'required',
              
          );
          $validator=Validator::make($data,$rule);
          if($validator->passes()) 
          {
          $new=[
            'name'=>$request->name,
            'description'=>$request->description
          ];
          $update=Category::where('id',$request->id)->update($new);
          return response()->json(['success'=>$update]);
          }
          return response()->json(['errors' => $validator->errors()]);
    }
    public function getnextsubcategory($id)
    {
      $cat= Category::find($id);
      $subcategory= Category::where('parent_id',$id)->select('id','name','description')->get();
      return view('admin::category.nextSubCategory',compact('subcategory','cat'));
    }
     public function addnextsubcategory(Request $request)
    { 
      $data = $request->except(array('_token'));
      $rule=array(
          'subname' =>'required',    
        );
      $validator=Validator::make($data,$rule);
      if ($validator->passes()) 
      {
        $hasdeleted=Category::where('id',$request->Id)->first();
        if($hasdeleted!=NULL)
        {
          $data =[
            'name'  =>$request->subname,
            'description' =>$request->subdesc,
            'parent_id'   =>$request->Id
          ];
          $subcat= Category::create($data);
          $subcat->save();
          return response()->json(['success'=>$subcat]);
        }
      }
        return response()->json(['errors' => $validator->errors()]);
    }
     public function updatenextsubcategory(Request $request)
    {
          $data=Input::except(array('_token'));
          $rule=array(
              'name'=>'required',  
          );
          $validator=Validator::make($data,$rule);
          if($validator->passes()) 
          {
          $new=[
            'name'=>$request->name,
            'description'=>$request->description
          ];
          $update=Category::where('id',$request->id)->update($new);
          return response()->json(['success'=>$update]);
          }
          return response()->json(['errors' => $validator->errors()]);
    }

}


 
     

        
