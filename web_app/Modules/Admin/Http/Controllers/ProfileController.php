<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use App\AdminUser;
use App\Attachment;
use Validator;
use Input;
use Hash;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $user_id = Auth::guard('admin')->id();
        $admin = AdminUser::where('id',$user_id)->first();
        return view('admin::profile.index',['admin'=>$admin]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $user_id = Auth::guard('admin')->id();
        $file = $request->file;
           if ($file != null) {
                  $fileid = count($file);
                   // $id = Auth::id();
                  $destinationPath = "uploads/admin/profile/";
                     $fileSize = $file->getClientSize();
                      if ($fileSize < 2000000) {
                      $ext = $file->getClientOriginalExtension();
                      $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                      $file = $file->move($destinationPath, $fileName);
                      $adminuser = AdminUser::where('id',$user_id)->first();
                      if($adminuser['profile_image'] == "") {
                          $datas = Attachment::create(
                                           ['name' => $fileName,
                                            'path' => $file,
                                            'mimeType' => $ext
                                          ]
                                      );
                          $datas->save();

                      $image_id = $datas->id;
                    }else {
                        $datas = Attachment::where('id',$adminuser['profile_image'])->update(
                                           ['name' => $fileName,
                                            'path' => $file,
                                            'mimeType' => $ext
                                          ]
                                      );
                        $image_id = $adminuser['profile_image'];
                    }
                      $imageupdate = AdminUser::where('id',$adminuser['profile_image'])->update(['profile_image'=>$image_id]);
                      
                        return back()->with(['success'=>'Image Upload Successfully']);   
                   }else {
                    return back()->withErrors(['errors' => 'file size not accepted']);
                   }
                            
           }
                    if($request->username != '' || $request->email != '') {
                        $dataadmin = AdminUser::where('id',$user_id)->update(
                                           ['username' => $request->username,
                                            'email' => $request->email,
                                          ]
                                      );
                    }
                    if($request->firstName != '' || $request->lastName != '' || $request->address != '') {
                        $dataadmin = AdminUser::where('id',$user_id)->update(
                                           ['firstName' => $request->firstName,
                                            'lastName' => $request->lastName,
                                            'address' => $request->address
                                          ]
                                      );
                    }
             return back()->with(['success'=>'Profile Edit Successfully']);   
          //return back()->withErrors(['errors' => 'Image Not Uploaded']);
        //return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function changePassword(Request $request){
        if($request->isMethod('post'))
        {
             $user_id = Auth::guard('admin')->id();
            $selectoldPassword = AdminUser::select('password')->where('id','=',$user_id)->first();
            $oldPassword          = Input::get('oldpassword');
            $newPassword          = Input::get('newpassword');
            $confirmNewPassword   = Input::get('repassword');
            $validator = Validator::make($request->all(),[
                'oldpassword' => 'required',
                'newpassword' =>'required|min:6',
                'repassword' =>'required|min:6',
            ]);
            if ($validator->fails()) {
             return redirect()->route('admin.profile')->with(['errors' => 'your password length should be greater then 6.'])->withInput();
            } 
            if (Hash::check($oldPassword, $selectoldPassword['password'])) {
                if ($newPassword == $confirmNewPassword){           
                    if ($newPassword == $confirmNewPassword) {
                            $password = bcrypt($newPassword);
                            $user = AdminUser::where('id',$user_id)->update(['password' => $password
                                                                       ]);
                        return redirect()->route('admin.profile')->with(['success'=>'Password change Successfully.']);
                    }else{
                        return redirect()->route('admin.profile')->with(['errors' => 'your password does not match.']);
                }
                }else{
                    return redirect()->route('admin.profile')->with(['errors' => 'your confirm Password does not match.']);
                }
            } else{
                    return redirect()->route('admin.profile')->with(['errors' => 'your password does not match.']);
            }
         }
        return redirect()->route('admin.profile');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
