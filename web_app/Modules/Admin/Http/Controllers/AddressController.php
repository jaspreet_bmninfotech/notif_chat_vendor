<?php
namespace Modules\Admin\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Country;
use App\State;
use App\City;
use DB;
use Validator;
use Redirect;
use Auth;
use Input;
use Session;

class AddressController extends Controller
{
	public function index()
	{

		$country = Country::all();
		return view('admin::address.addaddress',compact('country'));
	}
	public function store(Request $request)
	{
		
		 	$data =array();
		 	if(is_array($request->datas)){
		 		foreach ($request->datas as $key => $value) {
		 			$data[$value['key']] = $value['value'];
		 		}
	 		}
			
		$validator = Validator::make($data, [
            'newcountry' => 'required|max:255|alpha',
            'newsortName' => 'required|max:3',
            'newphoneCode' => 'required',
        ]);
		 if ($validator->passes()) 
		 {
		 	if(array_key_exists('newcountry',$data))
		 	{
		 		$country=new Country();
		 		$country->name= $data['newcountry'];
		 		$country->sortName= $data['newsortName'];
		 		$country->phoneCode=$data['newphoneCode'];
		 		$country->save();
		 		 return response()->json(['success'=>$country]);
		 	}
	 	}
	 	 return response()->json(['errors' => $validator->errors()]);
	 	
	 	// elseif(array_key_exists('newstate',$data))
	 	// {
	 	// 	$state=[
	 	// 		'name'=>$data['newstate'],
	 	// 		'country_id'=>$data['country']
	 	// 	];
	 	// 	$savedata=State::create($state);
	 	// 	$savedata->save();
	 	// }
	 	// elseif(array_key_exists('newcity',$data))
	 	// {
	 	// 	$city =new City();
	 	// 	$city->name=$data['newcity'];
	 	// 	$city->state_id=$data['state'];
	 	// 	$city->save();
	 	// }	
	}
	public function countrytable()
	{
		$data = Country::all();
		return view('admin::address.countrydatatable',['data'=>$data]);
	}
	public function statetable($id)
	{	
		$data = State::where('country_id',$id)->select('name','id','country_id')->get();	

		 return view('admin::address.statedatatable',['data'=>$data,'id'=>$id]);
	}
	public function addstate(Request $request)
	{

		$data =[
			'newstate'=>$request->name,
			'_token'	=>$request->_token
			];
		$validator = Validator::make($data, [
            'newstate' => 'required|max:255|alpha'
        ]);
		 if ($validator->passes()) 
		 {
			$hasdeleted=Country::where('id',$request->Id)->first();
			if($hasdeleted!=NULL)
			{

	            $data =[
	            	'name'=>$request->name,
	            	'country_id'=>$request->Id,
	            ];

	          	$state= State::create($data);
	            $state->save();
	            return response()->json(['success'=>$state]);
	        }
        }
        return response()->json(['errors' => $validator->errors()]);
        	// return redirect()->route('admin.address.statetable',$request->Id);
	}
	public function updatecountry(Request $request)
	{

		$data = Country::find($request->id);
	      	$new=[
	      			'getcountry'=>$request->getcountry,
	      			'getsortName'=>$request->getsortName,
	      			'getphoneCode'=>$request->getphoneCode
	      	];
		$validator = Validator::make($new, [
            'getcountry' => 'required|max:255|alpha',
            'getsortName' => 'required|max:3',
            'getphoneCode' => 'required',
        ]);
	 	if ($validator->passes()) 
		{
			$new=[
	      			'name'=>$request->getcountry,
	      			'sortName'=>$request->getsortName,
	      			'phoneCode'=>$request->getphoneCode
	      	];
      	$updata=Country::where('id',$request->id)->update($new);
        return response()->json(['success'=>$updata]);
     	}
     	 return response()->json(['errors' => $validator->errors()]);
	}
	public function delete(Request $request)
	{
		Country::find($request->id)->delete();
        return response()->json();
	}
	public function updatestate(Request $request)
	{
		$data = State::find($request->id);
		$statedata =[
			'editstate'=>$request->name
			];
		$validator = Validator::make($statedata, [
            'editstate' => 'required|max:255|alpha'
        ]);
		if ($validator->passes()) 
		{
      		$new=[
      			'name'=>$request->name
      		];
      		$update=State::where('id',$request->id)->update($new);
        	return response()->json(['success'=>$update]);
    	}
    	return response()->json(['errors' => $validator->errors()]);
	}
	public function deletestate(Request $request)
	{
		State::find($request->id)->delete();
        return response()->json();
	}
	public function citytable($id)
	{
		$data = City::where('state_id',$id)->select('name','id','state_id')->get();	
		 return view('admin::address.citydatatable',['data'=>$data,'id'=>$id]);
	}
	public function addcity(Request $request)
	{
		$citydata =[
			'newcity'=>$request->name,
			'_token'	=>$request->_token
			];
		$validator = Validator::make($citydata, [
            'newcity' => 'required|max:255|alpha'
        ]);
		if ($validator->passes()) 
		{
            $data =[
	            	'name'=>$request->name,
	            	'state_id'=>$request->Id,
            		];
           $city= City::create($data);
            $city->save();
            return response()->json(['success'=>$city]);
        }
        return response()->json(['errors' => $validator->errors()]);
	}
	public function updatecity(Request $request)
	{
		$cityedit =[
			'editcity'=>$request->name
			];
		$validator = Validator::make($cityedit, [
            'editcity' => 'required|max:255|alpha'
        ]);
		if ($validator->passes()) 
		{
	      	$new=[
	      			'name'=>$request->name
	      	];
      		$cityupdate=City::where('id',$request->id)->update($new);
        	return response()->json(['success'=>$cityupdate]);
    	}
    	return response()->json(['errors' => $validator->errors()]);
	}
	public function deletecity(Request $request)
	{
		City::find($request->id)->delete();
        return response()->json();
	}
}
