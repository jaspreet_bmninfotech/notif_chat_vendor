<?php

namespace Modules\Admin\Http\Controllers\role;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\RolePermisson;
use Modules\Admin\Entities\Module;
use Modules\Admin\Entities\Role;

// use App\Helpers\Modules;

class RolePermissonController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        
        $data['permisson'] = Role::where('id',$id)->first();
        if(!empty($data['permisson']['role_permisson'])){
            $data['role_permisson'] = $data['permisson']['role_permisson']->pluck('status','module_id');
        }
        $data['module'] = Module::whereNull('parent')->get();
        return view('admin::role_permisson.permisson', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {        
        foreach ($request['moduleRoute'] as $module_id => $value) {
            if($value == 'on'){
                $status = 1; 
            }else{
                $status = 0;
            }
           RolePermisson::updateOrCreate(['role_id'=> $request->role_id, 'module_id'=>$module_id],['status'=> $status ]);
        }
        return redirect()->route('role.permisson',['role_id' => $request->role_id]);
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
