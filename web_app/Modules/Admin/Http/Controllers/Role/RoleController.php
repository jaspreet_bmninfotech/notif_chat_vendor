<?php

namespace Modules\Admin\Http\Controllers\role;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\Role;
use Input;
use Validator;
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = Role::all();
        return view('admin::role.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $role = new Role();
        $role->name = $request->name;
        $role->save();
        return redirect()->route('role.list');

    
    }
    public function updaterole(Request $request,$id)
    {
        
          $validator = Validator::make($request->all(), [
              'rolename'=>'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('role.list')->withErrors($validator)->withInput();
        }
          
          $update=Role::where('id',$id)->update([
            'name'=>$request->rolename]);
            return redirect()->route('role.list')->with('success','Successfully role Update');
          
    }
    public function delete($id)
    {
       Role::find($id)->delete();
       /*dd($id);
      die();*/
      return back()->with('success','Successfully role Delete');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
