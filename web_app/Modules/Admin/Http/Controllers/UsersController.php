<?php

namespace Modules\Admin\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\AdminUser;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Redirect;
use Auth;
use Input;
use session;
class UsersController extends Controller
{
    //<!--VENDOR LIST-->
 
     public function list()
    {
        $user=User::where('type','vn')->orderBy('id','desc')->get();
        return view('admin::user.vendor.list', compact('user'));    
    }
    
    //<!--VENDOR EDIT-->
    public function edit($id)
    {
      $user =User::find($id);         
      return view('admin::user.vendor.edit',compact('user')); 
       // $info=DB::table('user')->where('id',$id)->first();          
    }
   
    //<!--User Update Function-->
    public function update(Request $request,$id)
    {
         
        $username=Input::get('username');
        $email=Input::get('email');
        $user = User::where('id',$id)->update(['username' => $username,
                                                'email' => $email   
                                                ]);
        return redirect()->route('user.vendor.list')->with('success','Successfully User Update');
    }
     public function vendordelete($id)
    {
       User::find($id)->delete();
      
      return redirect()->route('user.vendor.list')->with('success','Successfully Vendor Delete');
    }

   //<!--CLIENT LIST-->
    public function clientlist()
    {
        $user= User::where('type','cn')->orderBy('id','desc')->get();
        return view('admin::user.client.clientlist', compact('user'));    
    }

    public function clientedit($id)
    {
      $user =User::find($id);         
      return view('admin::user.client.clientedit',compact('user')); 
       // $info=DB::table('user')->where('id',$id)->first();          
    }

    public function clientupdate(Request $request,$id)
    {
         
      $rule=array('password'     => 'required|min:6'
             );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('user.client.list')->withErrors($validator)->with('error','Please fill profile');
            }
        $username=Input::get('username');
        $email=Input::get('email');
        $user = User::where('id',$id)->update(['username' => $username,
                                                'email' => $email
                                                
                                                ]);
        return redirect()->route('user.client.list')->with('success','Successfully User Update');
    }

    public function userdelete($id)
    {
       $user = AdminUser::find($id)->delete();
      return back()->with('success','Successfully Client Delete');
    }
    public function useredit(Request $request,$id)
    {
       $user = AdminUser::where('id',$id)->update(['username'=>$request->rolename,'email'=>$request->roleemail]);
      return back()->with('success','Successfully Admin Update');
    }

     public function delete($id)
    {
       User::find($id)->delete();
      return redirect()->route('user.client.list')->with('success','Successfully Client Delete');
    }
    // public function logout(Request $request)
    // {
    //   dd($request);
    //     Auth::guard('admin')->logout();
    //     $request->session()->flush();
    //     $request->session()->regenerate();
    //     return redirect()->guest(route( 'admin.login' ));
    // }
    

}

