<?php

namespace Modules\Admin\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\AdminUser;
use Modules\Admin\Entities\Role;
use Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    // use RegistersUsers;

    public function showRegistrationForm()
    {
        $roles = Role::whereNotIn('id', [1])->pluck('name','id');
        return view('admin::register.user', compact('roles'));
    } 

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admin_user',
            'username' => 'required|string|max:255|unique:admin_user',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return redirect('admin/register')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        AdminUser::create([
        'name' => $request['name'],
        'username' => $request['username'],
        'email' => $request['email'],
        'password' => Hash::make($request['password']),
        'role_id' => $request['role_id'],
        ]);

        return redirect()->route('admin.admin.users');
    }


    public function admin_users(){
        $user = AdminUser::with('role')->get();
        return view('admin::user.admin.user' , compact('user'));
    }
    public function edit()
    {
        $user =AdminUser::with('role')->find($id);
        dd($user);
        return view('admin::user.admin.useredit', compact('user'));
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
