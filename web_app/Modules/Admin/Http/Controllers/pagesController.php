<?php

namespace Modules\Admin\Http\Controllers;
use App\Pages;
use App\Attachment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
// use Session;
use Response;
use Validator;
use Auth;
use DB;
use Illuminate\Support\Facades\Session;

class pagesController extends Controller
{
     public function index()
    {  
        return view('admin::pages.create');
       
    }
      public function list()
    {
      
        $pages= DB::table('pages')->leftjoin('attachment as atch','atch.id','=','pages.image_id')->select('pages.id','pages.title','pages.description','pages.slug','pages.image_id','pages.created_at','atch.path','atch.name')->where('pages.deleted_at','=',NULL)->orderBy('pages.id', 'desc')->get();
        return view('admin::pages.list', ['pages' =>$pages]);    
    }

    public function create(Request $request){
    	if($request->isMethod('post'))
        {
          if (Pages::where('slug', '=',$request->slug)->exists()) {
            $error = 'Slug already exist';
            return redirect()->route('admin.pages',['error' => $error])->with('success','Successfully Pages Add');
   
        }else
           $userid = Auth::id();
           $file = $request->imageFile;
            
           if ($file != null) {
                                $fileid = count($file);
                                 $id = Auth::id();
                                $destinationPath = "uploads/admin/pages/";
                                   $fileSize = $file->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $file->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $file = $file->move($destinationPath, $fileName);
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $file,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $image_id = $datas->id;
                                    
                                 }
                            
           }else {
            $image_id = null;
           }
         // $data=Input::except(array('_token'));
          $request->validate([
                            'title' => 'required',
                            'description' => 'required',
                            'slug' => 'required',
                            'publishDate' => 'required',
                            ]);
           $data = Pages::create(['title' =>$request->title,
                                  'description' => $request->description,
                                'slug' => $request->slug,
                              'image_id' => $image_id,
                            'publishdate' => $request->publishDate]);

           $data->save();
           
           if ($data == true) {
            return redirect()->route('admin.pages')->with('success','Successfully Pages Add');   
            } else{
              
            return redirect()->route('admin.pages')->with('error','Pages does not Add');   
              
            }
          }
        
    }
    public function checkSlug(Request $request){
      if (Pages::where('slug', '=',$request->slug)->exists()) {
            return 'error';
            }else{
              return 'success';
            }           
    }

    public function edit($id)
    {
      
      $page =Pages::find($id);
      $pages= DB::table('pages')->leftjoin('attachment as atch','atch.id','=','pages.image_id')->select('pages.id','pages.title','pages.description','pages.slug','pages.image_id','pages.created_at','pages.publishdate','atch.path','atch.name')->where('pages.id','=',$id)->first();
      return view('admin::pages.edit',compact('pages')); 
       // $info=DB::table('category')->where('id',$id)->first();          
    }
    
    //<!--Category Update Function-->
    public function update(Request $request,$id)
    {
         if($request->isMethod('post'))
        {
          $pages = Pages::select('image_id')->where('id','=',$id)->first();
          $file = $request->imageFile;
            if ($file != null) {
                                $fileid = count($file);
                                 $userid = Auth::id();
                                $destinationPath = "uploads/admin/pages/".$userid."/";
                                   $fileSize = $file->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $file->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $file = $file->move($destinationPath, $fileName);
          if ($pages['image_id'] != NULL) {
                                    $datas = Attachment::where('id',$pages['image_id'])->update(
                                                     ['name' => $fileName,
                                                      'path' => $file,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $image_id = $pages['image_id'];
          }else{

                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $file,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $image_id = $datas->id;
          }
                                 }
                               }else{
                                $image_id = $pages['image_id'];
                               }

           
          $request->validate(['title' => 'required',
                            'description' => 'required',
                            'slug' => 'required',
                            'publishDate' => 'required',
                            ]);
          $user = Pages::where('id',$id)->update(['title' =>$request->title,
                                                        'description' => $request->description,
                                                      'slug' => $request->slug,
                                                    'image_id' => $image_id,
                                                  'publishdate' => $request->publishDate]);
          return redirect()->route('admin.pages')->with('success','Successfully Page Update');
        }
        return redirect()->route('pages.edit')->withInput();
    }
      
      //<!--Category Delete-->
    public function delete($id)
    {
       Pages::find($id)->delete();
       /*dd($id);
      die();*/
      return redirect()->route('admin.pages')->with('success','Successfully Page Delete');
    }

    public function removePagesimage(Request $request){

        $image_id = $request->image_id;
        $pagesid = $request->id;
        $filename = $request->filename;
        $data = \DB::table('attachment')->where('id','=',$image_id)->delete();
        $datas = \DB::table('pages')->where('id','=',$pagesid)->update(['image_id'=>NULL]);
        unlink(public_path('/uploads/admin/pages/'.$filename));
        return response('success');
    }
    
}
