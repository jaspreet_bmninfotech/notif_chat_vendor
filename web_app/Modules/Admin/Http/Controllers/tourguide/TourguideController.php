<?php

namespace Modules\Admin\Http\Controllers\tourguide;
use Illuminate\Http\Request;
use App\User;
use App\State;
use App\City;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use App\TourGuide;
use App\TourGuideLanguage;
use Redirect;
use Auth;
use Input;
use Session;
use DB;
use App\Country;
use App\Address;
use App\Attachment;

class TourguideController extends Controller
{
    public function index()
    {

        // $attachment =\DB::table('attachment')->select('path')->where('id','=',$data['profile_image'])->first();
        $country= Country::all(); 
        return view('admin::tourguide.add',[
                                            'country'    => $country,
                                            
                                            'race'      => TourGuide::$race,
                                            'nationality_status'=>TourGuide::$nationality_status,
                                            'activies'  =>TourGuide::$activies,                                        
                                            ]);
    } 
    public function createtourguide(Request $request)
    { 
        if($request->isMethod('post'))
        {
            // dd($request->all());
            $file = $request->pfile;
            
           if ($file != null) {
                                $fileid = count($file);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/profile/";
                                   $fileSize = $file->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $file->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $file = $file->move($destinationPath, $fileName);
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $file,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $image_id = $datas->id;
                                    
                                 }
                            
           }else {
            $image_id = null;
           }
        $address=Null;
        $firstName=Input::get('firstName');
        $lastName=Input::get('lastName');
        $username=Input::get('username');
        $password=bcrypt($request->password);
        $email =Input::get('email');
        $gender=$request->gender; 
        $phone=Input::get('phone');
        $dob=Input::get('dob');
        $profile = ['firstName' => $firstName,
                    'lastName' => $lastName,
                    'type' => 'tg',
                    'email'  =>$email,
                    'username'=>$username,
                    'profile_image'=>$image_id,
                    'password'=>$password,
                    'gender' => $gender,
                    'phone' => $phone,
                    'dob' => $dob
                ]; 
             
        $checkuser= User::select('email')->where('email',$email)->first(); 
        if($checkuser['email']==NULL)
        {
            $user=User::create($profile);
            $user->save();
            $user_id = $user->id;
        }
        else{
            return back()->withInput();
        } 
/** claim image**/
        $files = $request->claimfile;
            
           if ($files != null) {
                                $fileid = count($files);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/claim/";
                                   $fileSize = $files->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $files->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($files->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $files = $files->move($destinationPath, $fileName);
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $files,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $claim_id = $datas->id;
                                    
                                 }
                            
           }else {
            $claim_id = null;
           }
/** profile image**/
           $filep = $request->profilefiles   ;
            
           if ($filep != null) {
                                $fileid = count($filep);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/profile/";
                                   $fileSize = $filep->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $filep->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($filep->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $filep = $filep->move($destinationPath, $fileName);
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $filep,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $claim_id = $datas->id;
                                    
                                 }
                            
           }else {
            $claim_id = null;
           }
/** licence image**/
           $filel = $request->licencefiles;
            
           if ($filel != null) {
                                $fileid = count($filel);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/licence/";
                                   $fileSize = $filel->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $filel->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($filel->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $filel = $filel->move($destinationPath, $fileName);
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $filel,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $alicence_id = $datas->id;
                                    
                                 }
                            
           }else {
            $alicence_id = null;
           }
/** insurance image**/
           $filei = $request->insurancefiles;
            
           if ($filei != null) {
                                $fileid = count($filei);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/insurance/";
                                   $fileSize = $filei->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $filei->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($filei->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $filei = $filei->move($destinationPath, $fileName);
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $filei,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $insurance_id = $datas->id;
                                    
                                 }
                            
           }else {
            $insurance_id = null;
           }

        $race=Input::get('race');
        $birth_country=Input::get('birth_country');
        $nationality_status =Input::get('nationality_status');
        $know_country =Input::get('know_country');
        $have_car =$request->get('isCars') == 'yes' ? 1: 0;
        $car_for_tour=$request->get('carforTour') == 'yes' ? 1: 0;
        $make =$request->get('make');
        $model =$request->get('model');
        $year =$request->get('year');
        $plateno=Input::get('plateNo');
        $car_licenceno=Input::get('licenceNo');
        $car_insurance=$request->get('carinsurance')==='yes' ? 1 :0;
        $insurancereason =Input::get('insurancereason');
        $touractivies=Input::get('tourguideactivities');
        $showyou =Input::get('showyou');
        $data = [
                'user_id'       =>$user_id,
                'race'          => $race,
                'birthCountry'     =>$birth_country,
                'nationalityStatus'=>$nationality_status,
                'attachment_claim'=>$claim_id,
                'knowCountry'     =>$know_country,
                'haveCar'         =>$have_car,
                'carForTour'      =>$car_for_tour,
                'carMake'          =>$make,
                'carModel'        =>$model,
                'carYear'         =>$year,
                'carPlate'        =>$plateno,
                'carLicenceno'      =>$car_licenceno ,
                'attachment_licence'=>$alicence_id,
                'carInsurance'    =>$car_insurance,
                'noInsuranceReason' => $insurancereason,
                'carinsurance_attachment_id'=> $insurance_id,
                'showYou'     =>$showyou
                ];
        $data['tourguideActivities']   =serialize($touractivies);   
     
        $address = [
                    'country_id'    => $request->get('country'),
                    'state_id'      => $request->get('state'),
                    'city_id'       => $request->get('city'),
                    'postalCode'    => $request->get('postalCode'),
                    'address'       => $request->get('address')
                    ];      
          if($address!= NULL){
           
                $address = Address::create($address);
                $address->save();
                $addressId      = $address->id;
                $address =User::select('address_id')->where('id',$user_id)
                                    ->update(['address_id'  =>$addressId
                                            ]);   
            }
        
            $hastourguide=TourGuide::where('user_id',$user_id)->first();
            if($hastourguide == NULL){
                $tourguideinfo = TourGuide::create($data);
                $tourguideinfo->save();
                $tourguideinfo_id = $tourguideinfo->id;
            }
            $guidelanguage=$request->get('guidelanguage');
            if($guidelanguage){
                $guidelanguageArr = array();
                TourGuideLanguage::where('tourguideInfo_Id',$tourguideinfo_id)->delete();
                    foreach ($guidelanguage as $key => $value) {
                        $guide=new TourGuideLanguage();
                        $guide->language=$value;
                        $guide->tourguideInfo_Id=$tourguideinfo_id;
                        $guide->save();

                    } 
            }  
            // Session::flash('success','Successful tourguide Created ');
         return redirect()->back()->with('success','Successful tourguide Created');     
    }
    }
    public function show()
   {
       $tourguide =DB::table('tour_guide_info as tour')->leftjoin('user','tour.user_id','=','user.id')
                        ->select('user.*','tour.*')->where('tour.deleted_at','=',NULL)->get(); 
        return view('admin::tourguide.list',['tourguide'=>$tourguide]);
   }
   
    public function edit($id)
    {

      $td=TourGuide::find($id);

      $languages = \DB::table('user')
                                ->leftjoin('tour_guide_info','user.id','=','tour_guide_info.user_id')
                                ->leftjoin('tour_guide_language','tour_guide_info.id','=','tour_guide_language.tourguideInfo_Id')
                                ->select('tour_guide_language.language')
                                ->where('tour_guide_info.id',$id)->get()->toArray(); 
         $getlanguage =array_column($languages ,'language');
        
            $getaddress= \DB::table('user')
                            ->leftjoin('address','user.address_id','=','address.id')
                            ->leftjoin('tour_guide_info','user.id','=','tour_guide_info.user_id')   
                            ->select('user.id as userid','user.gender','user.email','user.firstName','user.lastName','user.profile_image','user.password','user.username','user.phone','user.dob','address.*','tour_guide_info.*')
                            ->where('tour_guide_info.id',$id)->first();
                            
            $setactivies= unserialize($getaddress->tourguideActivities);
            $attachmentu = Attachment::select('id','path','name')->where('id',$getaddress->profile_image)->first();
           
            //$attachmentp = Attachment::select('id','path','name')->where('id',$getaddress->attachment_claim)->first();
            $attachmentc = Attachment::select('id','path','name')->where('id',$getaddress->attachment_claim)->first();
            $attachmentl = Attachment::select('id','path','name')->where('id',$getaddress->attachment_licence)->first();
            $attachmenti = Attachment::select('id','path','name')->where('id',$getaddress->carinsurance_attachment_id)->first();
       $country= Country::all(); 
        return view('admin::tourguide.edit',[
                                          'country'    => $country,
                                          'race'      => TourGuide::$race,
                                          'nationality_status'=>TourGuide::$nationality_status,
                                          'activies'  =>TourGuide::$activies,
                                          'getlanguage'=>$getlanguage,
                                            'data' =>$getaddress,
                                            'profileimage' =>$attachmentu,
                                            'claimimage' =>$attachmentc,
                                            'licenceimage' =>$attachmentl,
                                            'insuranceimage' =>$attachmenti,
                                          'setactivies' => $setactivies                                       
                                          ]);

      
    }
    public function update(Request $request,$id)
    {

      if($request->isMethod('post'))
        {
            $tourguideatach_id = TourGuide::select('user_id','attachment_claim','attachment_licence','carinsurance_attachment_id')->where('id',$id)->first();
            $profile_image_id = User::select('profile_image')->where('id',$tourguideatach_id['user_id'])->first();
          
            $file = $request->edfile;
            
           if ($file != null) {
                $fileid = count($file);
                 // $id = Auth::id();
                $destinationPath = "uploads/admin/tourguide/profile/";
                   $fileSize = $file->getClientSize();
                    if ($fileSize < 2000000) {
                    $ext = $file->getClientOriginalExtension();
                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                    $file = $file->move($destinationPath, $fileName);
                    $datas = Attachment::where('id',$profile_image_id['profile_image'])->update(
                                     ['name' => $fileName,
                                      'path' => $file,
                                      'mimeType' => $ext
                                    ]
                                );
                 }
           }
        $address=Null;
        $firstName=Input::get('firstName');
        $lastName=Input::get('lastName');
        $username=Input::get('username');
      
        $email =Input::get('email');
        $gender=$request->gender; 
        $phone=Input::get('phone');
        $dob=Input::get('dob');
        $profile = ['firstName' => $firstName,
                    'lastName' => $lastName,
                    'email'  =>$email,
                    'username'=>$username,
                    'gender' => $gender,
                    'phone' => $phone,
                    'dob' => $dob
                ]; 

        $getuser= DB::table('tour_guide_info  as tour')->leftjoin('user','user.id','=','tour.user_id')->select('user.id','user.address_id')
        			     ->where('tour.id',$id)->first();
        $user=User::where('id',$getuser->id)->update($profile);
         $files = $request->claimfile;
           if ($files != null) {
                                $fileid = count($files);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/claim/";
                                   $fileSize = $files->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $files->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($files->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $files = $files->move($destinationPath, $fileName);
                                if ($tourguideatach_id['attachment_claim'] == NULL) {
                                  
                                    $datas = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $files,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $datas->save();
                                    $datasID = $datas->id;
                                    $datas = TourGuide::where('user_id',$tourguideatach_id['user_id'])->update(
                                                     ['attachment_claim' => $datasID
                                                    ]
                                                ); 
                                }else {

                                    $datas = Attachment::where('id',$tourguideatach_id['attachment_claim'])->update(
                                                     ['name' => $fileName,
                                                      'path' => $files,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                }
                                    
                                 }
                            
           }
/** profile image**/
           // $filep = $request->pfile;
            
           // if ($filep != null) {
                                
           //                       $id = Auth::id();
           //                      $destinationPath = "uploads/admin/tourguide/profile/";
           //                         $fileSize = $filep->getClientSize();
           //                          if ($fileSize < 2000000) {
           //                          $ext = $filep->getClientOriginalExtension();
           //                          $fileName = substr_replace(str_slug($filep->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
           //                          $filep = $filep->move($destinationPath, $fileName);
           //                          $datas = Attachment::where('id',$profile_image_id['profile_image'])->update(['name' => $fileName, 'path' => $filep,'mimeType' => $ext
           //                        ]);
           //                       }
           //                        }
/** licence image**/
           $filel = $request->licencefiles;
           if ($filel != null) {
                                $fileid = count($filel);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/licence/";
                                   $fileSize = $filel->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $filel->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($filel->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $filel = $filel->move($destinationPath, $fileName);
                                     if ($tourguideatach_id['attachment_licence'] == NULL) {
                                  
                                      $datasl = Attachment::create(
                                                       ['name' => $fileName,
                                                        'path' => $filel,
                                                        'mimeType' => $ext
                                                      ]
                                                  );
                                      $datasl->save();
                                      $dataslID = $datasl->id;
                                    $datas = TourGuide::where('user_id',$tourguideatach_id['user_id'])->update(
                                                     ['attachment_licence' => $dataslID
                                                    ]
                                                ); 
                                }else {
                                    $datasl = Attachment::where('id',$tourguideatach_id['attachment_licence'])->update(
                                                     ['name' => $fileName,
                                                      'path' => $filel,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                  }
                                    
                                 }
                            
           }
/** insurance image**/
           $filei = $request->insurancefiles;
            
           if ($filei != null) {
                                $fileid = count($filei);
                                 // $id = Auth::id();
                                $destinationPath = "uploads/admin/tourguide/insurance/";
                                   $fileSize = $filei->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $filei->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($filei->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $filei = $filei->move($destinationPath, $fileName);
                                    if ($tourguideatach_id['carinsurance_attachment_id'] == NULL) {
                                      $datasl = Attachment::create(
                                                       ['name' => $fileName,
                                                        'path' => $filei,
                                                        'mimeType' => $ext
                                                      ]
                                                  );
                                      $datasl->save();
                                      $dataslID = $datasl->id;
                                    $datas = TourGuide::where('user_id',$tourguideatach_id['user_id'])->update(
                                                     ['carinsurance_attachment_id' => $dataslID
                                                    ]
                                                ); 
                                }else {
                                    $datas = Attachment::where('id',$tourguideatach_id['carinsurance_attachment_id'])->update(
                                                     ['name' => $fileName,
                                                      'path' => $filei,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    }
                                 }
                            
           }           
        $race=Input::get('race');
        $birth_country=Input::get('birth_country');
        $nationality_status =Input::get('nationality_status');
        $know_country =Input::get('know_country');
        $have_car =$request->get('isCars') == 'yes' ? 1: 0;
        $car_for_tour=$request->get('carforTour') == 'yes' ? 1: 0;
        $make =$request->get('make');
        $model =$request->get('model');
        $year =$request->get('year');
        $plateno=Input::get('plateNo');
        $car_licenceno=Input::get('licenceNo');
        $car_insurance=$request->get('carinsurance')==='yes';
        $insurancereason =Input::get('insurancereason');
        $touractivies=Input::get('tourguideactivities');
        $showyou =Input::get('showyou');
        $data = [
                'user_id'   		=>$getuser->id,
                'race'      		=> $race,
                'birthCountry'     =>$birth_country,
                'nationalityStatus'=>$nationality_status,
                'knowCountry'  		=>$know_country,
                'haveCar'      		=>$have_car,
                'carForTour'  		=>$car_for_tour,
                'carMake'     		 =>$make,
                'carModel'     		=>$model,
                'carYear'      		=>$year,
                'carPlate'     		=>$plateno,
                'carLicenceno' 		=>$car_licenceno,
                'carInsurance' 		=>$car_insurance,
                'noInsuranceReason'	=> $insurancereason,
                'showYou'			=>$showyou
                ];
        $data['tourguideActivities']   =serialize($touractivies);  

        $address = [
                    'country_id'    => $request->get('country'),
                    'state_id'      => $request->get('state'),
                    'city_id'       => $request->get('city'),
                    'postalCode'    => $request->get('postalCode'),
                    'address'       => $request->get('address')
                    ];      
       		if($address!= NULL){
          
	            if($getuser->address_id    != NULL)
	            {
	                $address        =   Address::where('id',$getuser->address_id)
	                                    ->update($address);  
	            }
            }
        
        		$hastourguide=TourGuide::where('user_id',$getuser->id)->first();
		        if($hastourguide == NULL){
		            $tourguideinfo = TourGuide::create($data);
		            $tourguideinfo->save();
		            $tourguideinfo_id = $tourguideinfo->id;
		        }
		        else{
		            $tourguideinfo= TourGuide::where('user_id',$getuser->id)->update($data);
		            $tourguideinfo = TourGuide::select('id')->where('user_id',$getuser->id)->first();
		            $tourguideinfo_id=$tourguideinfo->id;
		            
        		}
            	$guidelanguage=$request->get('guidelanguage');
            	if($guidelanguage){
               	 	$guidelanguageArr = array();
                	TourGuideLanguage::where('tourguideInfo_Id',$tourguideinfo_id)->delete();
                    foreach ($guidelanguage as $key => $value)
                     {
                        $guide=new TourGuideLanguage();
                        $guide->language=$value;
                        $guide->tourguideInfo_Id=$tourguideinfo_id;
                        $guide->save();

                    } 
            	}
            	return redirect()->back()->with('success','Successful tourguide updated'); 
        }  
    }
    public function delete($id)
    {
    	 TourGuide::find($id)->delete();
      return redirect()->route('admin.tourguide.all')->with('success','Successfully tourguide Delete');
    }

    public function removetourguideimage(Request $request){

        $claim_id = $request->claim_id;
        $licence_id = $request->licence_id;
        $insur_id = $request->insur_id;
        $userid = $request->id;
        $filename = $request->filename;
        if ($claim_id != 'null') {
            $atchfield = 'attachment_claim';
            $image_id = $claim_id;
        unlink(public_path('/uploads/admin/tourguide/claim/'.$filename));
        }
         if ($licence_id != 'null') {
             $atchfield = 'attachment_licence';
            $image_id = $licence_id;
        unlink(public_path('/uploads/admin/tourguide/licence/'.$filename));
        }
         if ($insur_id != 'null') {
             $atchfield = 'carinsurance_attachment_id';
            $image_id = $insur_id;
        unlink(public_path('/uploads/admin/tourguide/insurance/'.$filename));
        }
        
        $data = \DB::table('attachment')->where('id','=',$image_id)->delete();
        $datas = \DB::table('tour_guide_info')->where('id','=',$userid)->update([$atchfield => NULL]);
        
        return response('success');
    }
}