<?php

namespace Modules\Admin\Http\Controllers\Dispute;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\User;
use App\Admin_User;
use App\Dispute;
use Modules\Admin\Entities\Ticket\TicketitCategory as Category;
use Modules\Admin\Entities\Ticket\TicketitCategoryUser as Category_user;
use Modules\Admin\Entities\Ticket\TicketitComment as Comment;
use Modules\Admin\Entities\Ticket\TicketitStatus as status_list;
use Modules\Admin\Entities\Dispute\DisputeVerdicts;

class AdminDisputeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(admin_role_id()==2){
            $id = admin_user_id();
            $data = Dispute::where('agent_id', $id)->get();
        }elseif(admin_role_id()==1){
                $data = Dispute::all();
        }
        return view('admin::dispute.index', compact('data') );
    }
 /**
     * Display a listing of the resource.
     * @return Response
     */

 public function dispute_verdict(Request $request){
    
        Dispute::where('id', $request->id)->update(['status_id'=>2 ]);
        $verdict  =  new DisputeVerdicts();
        $verdict->fill($request->all());
        $verdict->save();
        return redirect()->route('admin.dispute.index');
 }
 

  public function status_change($id, $status_id)
    {
        Dispute::where('id', $id)->update(['status_id'=>$status_id ]);
        return back();
    }

    public function detail($id)
    {

        $data = Dispute::with('dispute_verdicts', 'category', 'job',  'comment')->where('id', $id)->first();  
        $status_list = status_list::status_list();
        return view('admin::dispute.detail', compact('data', 'status_list') );
    }

   
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
