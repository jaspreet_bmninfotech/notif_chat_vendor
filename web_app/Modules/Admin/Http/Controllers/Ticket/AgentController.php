<?php

namespace Modules\Admin\Http\Controllers\Ticket;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\AdminUser;
use Modules\Admin\Entities\Ticket\TicketitCategory;
use Modules\Admin\Entities\Ticket\TicketitCategoryUser;



class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $category =  TicketitCategory::where('status', 1)->pluck('name','id');

        $data = AdminUser::with('ticket_agent_category')->where('role_id', 2)->get();
        return view('admin::ticket.agent.index', compact('data', 'category' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }/**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function agent_category(Request $request)
    {
        TicketitCategoryUser::where(['user_id'=>$request['agent_id']])->delete();
        foreach ($request['category'] as $key => $value) {
               $cat_user = new TicketitCategoryUser();
               $cat_user->user_id = $request['agent_id'];
               $cat_user->category_id = $value;
               $cat_user->save();
        }
        return back();
    }

public function agent_remove($id){
    TicketitCategoryUser::where(['user_id'=>$id])->delete();
    AdminUser::where('id', $id)->update(['role_id'=> 0]);
    return back();
}

public function agent_create(Request $request){

    if($request->isMethod('post')){
        AdminUser::whereIn('id',$request['user_id'])->update(['role_id'=>2]);
    }
    $users =  AdminUser::whereNotIn('role_id',[1])->get();
    return view('admin::ticket.agent.create_agent', compact('users'));

}
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
