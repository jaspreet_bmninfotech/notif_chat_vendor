<?php

namespace Modules\Admin\Http\Controllers\Ticket;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Modules\Admin\Entities\Ticketit;
use Modules\Admin\Entities\Ticket\TicketitPriority;
use App\User;
use App\AdminUser;
use Modules\Admin\Entities\Ticket\TicketitCategory as Category;
use Modules\Admin\Entities\Ticket\TicketitCategoryUser as Category_user;
use Modules\Admin\Entities\Ticket\TicketitComment as Comment;
use Modules\Admin\Entities\Ticket\TicketitStatus as status_list;


class TicketController extends Controller
{


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($status=null)
    {
        if(admin_role_id()==2){
            $id = Auth::guard('admin')->id();

            if($status){
                $data = Ticketit::with(['priority','agent','status_name','category_name'])->where('status_id',2 )->Where('agent_id', $id)->get();
                // ->orWhere('user_id', $id)
            }else{
                $data = Ticketit::with(['priority','agent','status_name','category_name'])->whereNotIn('status_id',[2])->Where('agent_id', $id)->get();

                //->orWhere('user_id', $id)
            }

        }elseif(admin_role_id() ==1 ){

             if($status){
                $data = Ticketit::with(['priority','agent','status_name','category_name'])->where('status_id',2 )->get();
            }else{
                $data = Ticketit::with(['priority','agent','status_name','category_name'])->whereNotIn('status_id',[2])->get();
            }
        }

        // dd($data);
        return view('admin::ticket.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        $category = Category::pluck('name','id')->toArray();
        $agent = AdminUser::whereNotIn('role_id',[1])->pluck('username','id')->toArray();
        $priority = TicketitPriority::pluck('name','id')->toArray();

        return view('admin::ticket.create', compact('priority', 'agent' , 'category'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $users = Category_user::where('category_id',$request['category_id'])->get()->random(1);
        $agent_id = $users[0]->user_id;

        $ticket  = new Ticketit();
        $ticket->fill($request->all());
        $ticket->user_id = Auth::guard('admin')->id();
        $ticket->agent_id = $agent_id;
        $ticket->status_id = 7;
        $ticket->save();
        return redirect()->route('tickets.list');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function detail($id)
    {
        $data = Ticketit::with(['priority','status_name','category_name'])->where('id', $id)->first();
        $comment = Comment::with('comment_user')->where('ticket_id', $id)->get();
        return view('admin::ticket.detail', compact('data', 'comment'));
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function comment(Request $request)
    {
        $comment = new Comment();
        $comment->fill($request->all() );
        $comment->save();
        return back();
    }

/**
     * Show the specified resource.
     * @return Response
     */
    public function complete($id)
    {
        Ticketit::where('id', $id)->update(['status_id'=> 2 ]);
        return redirect()->route('tickets.list');
    } 

    /**
    * Show the specified resource.
     * @return Response
     */
    public function reopen($id)
    {
        Ticketit::where('id', $id)->update(['status_id'=> 3]);
        return redirect()->route('tickets.list');
    }
    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::pluck('name','id');
        $priority = TicketitPriority::pluck('name','id');
        $ticket = Ticketit::where('id', $id)->first()->toArray();
        $agent = AdminUser::whereIn('id',$this->get_category_user($ticket['category_id']))->where('role_id',2)->pluck('username','id');
        $status_list = status_list::status_list();
        return view('admin::ticket.edit', compact( 'ticket' , 'priority', 'agent' , 'category', 'status_list'));
    }

    public function get_category_user($cat_id ){

        return $user_ids = Category_user::where('category_id',$cat_id )->pluck('user_id')->toArray();
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        Ticketit::where('id',$request->tid)->update($request->except('_token','tid','id'));
        return redirect()->route('tickets.list');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         Ticketit::where('id', $id)->delete();
        return redirect()->route('tickets.list');
    }
}
