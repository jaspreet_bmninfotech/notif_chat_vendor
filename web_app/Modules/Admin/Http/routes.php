<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/', 'AdminController@index');
    

Route::group(['middleware'=>'guest:admin'] , function(){

  	Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login.form');
	Route::post('admin-logined', 'Auth\LoginController@login')->name('admin.logined');

});

	Route::group(['middleware'=>'admin'] , function(){

    Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::get('/admin-users', 'Auth\RegisterController@admin_users')->name('admin.admin.users');
    Route::post('/register', 'Auth\RegisterController@register')->name('admin.register.store');

    Route::get('admin-home', 'AdminController@index')->name('admin.home.index');
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

/*DIspute */

Route::get('dispute', 'Dispute\AdminDisputeController@index')->name('admin.dispute.index');
Route::get('dispute-detail/{id}', 'Dispute\AdminDisputeController@detail')->name('admin.dispute.detail');
Route::get('dispute-status/{id}/{status}', 'Dispute\AdminDisputeController@status_change')->name('admin.dispute.status');
Route::post('dispute-verdict', 'Dispute\AdminDisputeController@dispute_verdict')->name('admin.dispute.verdict');

/*DIspute  */

/*Tickets */
Route::get('ticket/ajax', ['as'=>'tickets.ajax', 'uses'=>'Ticket\TicketController@ticket_data']);
Route::get('ticket/create', 'Ticket\TicketController@create')->name('ticket.create');
Route::post('ticket/store', ['as'=>'tickets.store' , 'uses'=>'Ticket\TicketController@store']);
Route::post('ticket-comment', ['as'=>'tickets.comment' , 'uses'=>'Ticket\TicketController@comment']);
Route::get('ticket/detail/{id}', 'Ticket\TicketController@detail')->name('ticket.detail');
Route::get('ticket/complete/{id}', 'Ticket\TicketController@complete')->name('ticket.complete');
Route::get('ticket/reopen/{id}', 'Ticket\TicketController@reopen')->name('ticket.reopen');
Route::get('ticket/delete/{id}', 'Ticket\TicketController@destroy')->name('ticket.delete');
Route::get('ticket/edit/{id}', 'Ticket\TicketController@edit')->name('ticket.edit');
Route::post('ticket/update', 'Ticket\TicketController@update')->name('tickets.update');
Route::get('ticket/{status?}', ['as'=>'tickets.list', 'uses'=>'Ticket\TicketController@index']);
/* Priority*/

  Route::get('ticket-priorities', 'Ticket\PriorityController@index')->name('ticket.priority.index');
  Route::get('ticket-priority/create', 'Ticket\PriorityController@create')->name('ticket.priority.create');
  Route::post('ticket-priority/store', 'Ticket\PriorityController@store')->name('ticket.priority.store');

/* Priority*/
/* Status*/

  Route::get('ticket-status', 'Ticket\StatusController@index')->name('ticket.status.index');
  Route::get('ticket-status/create', 'Ticket\StatusController@create')->name('ticket.status.create');
  Route::post('ticket-status/store', 'Ticket\StatusController@store')->name('ticket.status.store');


/* Status*/

/* Category*/

  Route::get('ticket-category', 'Ticket\CategoryController@index')->name('ticket.category.index');
  Route::get('ticket-category/create', 'Ticket\CategoryController@create')->name('ticket.category.create');
  Route::post('ticket-category/store', 'Ticket\CategoryController@store')->name('ticket.category.store');


/* Category*/

/* Category*/

  Route::get('ticket-agent', 'Ticket\AgentController@index')->name('ticket.agent.index');
  Route::get('ticket-agent/create', 'Ticket\AgentController@create')->name('ticket.agent.create');
  Route::post('ticket-agent/store', 'Ticket\AgentController@store')->name('ticket.agent.store');
  Route::post('ticket-agent-category/store', 'Ticket\AgentController@agent_category')->name('ticket.agentcat.store');
  Route::get('ticket-agent-remove/{id}', 'Ticket\AgentController@agent_remove')->name('ticket.agentcat.remove');
  Route::match(['get', 'post'],'ticket-agent-create', 'Ticket\AgentController@agent_create')->name('ticket.agent.create');




/* Category*/


/*Tickets */


  // Route::group(['middleware' => 'admin'], function(){

  // });
/*MODULE ROUTE*/

    Route::get('module', ['as'=>'module.list' , 'uses'=>'ModuleController@index']);
    Route::post('module/store', ['as'=>'module.store' , 'uses'=>'ModuleController@store']);

/*END MODULE ROUTE*/


/*ROLE ROUTE*/

    Route::get('roles', ['as'=>'role.list' , 'uses'=>'role\RoleController@index']);
    Route::post('role/store', ['as'=>'role.store' , 'uses'=>'role\RoleController@store']);

/*END ROLE ROUTE*/
  Route::post('update/{id}',['as' => 'role.update', 'uses'=>'role\RoleController@updaterole']);
  Route::get('delete/{id}',['as' => 'role.delete', 'uses'=>'role\RoleController@delete']);

/*ROLE Permisson*/

    Route::get('role-permisson/{role_id?}', ['as'=>'role.permisson' , 'uses'=>'role\RolePermissonController@index'] );
    Route::post('role-permisson/store', ['as'=>'role-permisson.store' , 'uses'=>'role\RolePermissonController@store']);

/*END ROLE Permisson*/

  Route::get('user/delete/{id}',['as' => 'user.delete', 'uses'=>'UsersController@userdelete']);
  Route::post('adminuser/edit/{id}',['as' => 'adminuser.edit', 'uses'=>'UsersController@useredit']);
    Route::get('/profile', 'ProfileController@index')->name('admin.profile');
    Route::post('/profile', 'ProfileController@create')->name('profile.create');
    Route::post('change/password', 'ProfileController@changePassword')->name('change.password');

Route::get('/getcategory', 'SettingController@getcategory');
Route::get('/getstate/{id}', 'user\UserController@state');
  Route::get('/getcity/{id}', 'user\UserController@city');
  Route::get('/adduser',['as'=>'admin.adduser','uses'=>'user\UserController@index']);
  Route::post('/adduser',['as'=>'admin.adduser.add','uses'=>'user\UserController@create']);
  Route::get('/users',['as'=>'admin.users','uses'=>'user\UserController@list']);
  Route::get('/useredit/{id}',['as'=>'admin.user.edit','uses'=>'user\UserController@edit']);
  Route::post('/userupdate/{id}',['as'=>'admin.user.update','uses'=>'user\UserController@update']);
  Route::delete('/delete/{id}',[
           'as' => 'admin.user.delete', 'uses'=>'user\UserController@delete'
        ]);
Route::group(['prefix' => 'category'], function () {
        // show list categoriess
        Route::get('/', ['as' => 'category.list', 'uses' => 'CategoryController@index']);
        // add category
        Route::get('/add',['as' => 'category.add', 'uses'=>'CategoryController@insert']);
        Route::post('/add',['as' => 'category.add', 'uses'=>'CategoryController@addinsert']);
        Route::POST('/icon',['as' => 'category.icon', 'uses'=>'CategoryController@addicon']);
        // edit category
        Route::get('/edit/{id}',['as' => 'category.edit', 'uses'=>'CategoryController@edit']);
        //update category
        Route::post('/edit',['as' => 'category.edit', 'uses'=>'CategoryController@update']);
        // delete
        Route::delete('/delete/{id}',['as' => 'category.delete', 'uses'=>'CategoryController@delete']);
        // view subcategory
        Route::get('/subcategory/{id}',['as' => 'category.subcategory', 'uses'=>'CategoryController@getsubcategory']);
        // add subcategory
        Route::post('/addsubcat',['as' => 'category.subcategory.add', 'uses'=>'CategoryController@addsubcategory']);
         //update category
        Route::post('/subcat/edit',['as' => 'category.subcategory.update', 'uses'=>'CategoryController@updatesubcategory']);
        // show nextsubcategory
        Route::get('/nextsubcat/{id}',['as' => 'category.nextsubcat', 'uses'=>'CategoryController@getnextsubcategory']);
        Route::post('/sub',['as' => 'category.nextsubcategory.add', 'uses'=>'CategoryController@addnextsubcategory']);
         //update category
        Route::post('/sub/edit',['as' => 'category.nextsubcategory.update', 'uses'=>'CategoryController@updatenextsubcategory']);
    });

 Route::group(['prefix' => 'user', 'middleware' => []], function () {
        //<!--VENDOR LIST-->
        Route::get('vendor/', [
           'as' => 'user.vendor.list', 'uses' => 'UsersController@list'
        ]);
        //<!--VENDOR EDIT-->
        Route::get('/vendor/{id}/edit',[
          'as' => 'user.vendor.edit', 'uses'=>'UsersController@edit'
        ]);
        Route::post('/vendor/profileuploads', [
           'as' => 'user.vendor.profileuploads', 'uses' => 'UsersController@uploadprofiles'
        ]);
        
        //<!--VENDOR UPDATE-->
        Route::post('/vendor/{id}/edit',[
          'as' => 'user.vendor.update', 'uses'=>'UsersController@update'
        ]);
        //<!--VENDOR DELETE-->
         Route::delete('/deletevendor/{id}',[
           'as' => 'user.vendor.delete', 'uses'=>'UsersController@vendordelete'
        ]);
    //<!--VENDOR END LIST,EDIT AND DELETE-->

      //<!--CLIENT LIST-->
        Route::get('client/', [
           'as' => 'user.client.list', 'uses' => 'UsersController@clientlist'
        ]);
         //<!--CLIENT EDIT-->
        Route::get('/client/{id}/edit',[
          'as' => 'user.client.edit', 'uses'=>'UsersController@clientedit'
        ]);
         //<!--CLIENT UPDATE-->
        Route::post('/client/{id}/edit',[
          'as' => 'user.client.update', 'uses'=>'UsersController@clientupdate'
        ]);
         //<!--CLIENT DELETE-->
         Route::delete('/delete/{id}',[
           'as' => 'user.client.delete', 'uses'=>'UsersController@delete'
        ]);
  
    //<!--CLIENT END LIST,EDIT AND DELETE-->
    });


Route::group(['prefix' => 'setting', 'middleware' => []], function () {
         Route::get('/home', [
           'as' => 'admin.setting.home', 'uses' => 'SettingController@home'
        ]);
       Route::get('/perjob', [
           'as' => 'admin.setting.perjob', 'uses' => 'SettingController@index'
        ]);
       Route::match(['get','post'],'/category-home',['as'=>'admin.setting.category','uses' => 'SettingController@managecategory']);

       Route::get('/updatejob', [
           'as' => 'admin.setting.updateperjob', 'uses' => 'SettingController@update'
        ]);
        Route::get('/show', [
           'as' => 'admin.setting.show', 'uses' => 'SettingController@show'
        ]);
        Route::post('/updatecategory', [
           'as' => 'admin.setting.updatecategory', 'uses' => 'SettingController@updatecategory'
        ]);
        Route::get('/removecategory', [
           'as' => 'admin.setting.deletecat', 'uses' => 'SettingController@removecategory'
        ]);
       // Route::match(['get', 'post'],  '/insert',[
       //     'as' => 'admin.setting.insert', 'uses' => 'SettingController@create'
       //  ]);
     });



Route::group(['prefix'=>'address'],function (){
      Route::get('/',['as'=>'admin.adddress','uses'=> 'AddressController@index']);
      Route::get('/countrytable',['as'=>'admin.address.countrytable','uses'=>'AddressController@countrytable']);
      Route::get('/statetable/{id}',['as'=>'admin.address.statetable','uses'=>'AddressController@statetable']);
      Route::get('/citytable/{id}',['as'=>'admin.address.citytable','uses'=>'AddressController@citytable']);
      Route::POST('/add',['as'=>'admin.adddress.add','uses'=> 'AddressController@store']);
      Route::post('/addstate',['as'=>'admin.address.addstate','uses'=>'AddressController@addstate']);
      Route::post('/addcity',['as'=>'admin.address.addcity','uses'=>'AddressController@addcity']);
      Route::post('/updatestate',['as'=>'admin.address.updatestate','uses'=>'AddressController@updatestate']);
      Route::post('/updatecity',['as'=>'admin.address.updatecity','uses'=>'AddressController@updatecity']);
      Route::post('/updatecountry',['as'=>'admin.address.updatecountry','uses'=>'AddressController@updatecountry']);
      Route::get('/deletecountry',[
           'as' => 'admin.address.delete', 'uses'=>'AddressController@delete'
        ]);
      Route::get('/deletestate',[
           'as' => 'admin.address.deletestate', 'uses'=>'AddressController@deletestate'
        ]);
      Route::get('/deletecity',[
           'as' => 'admin.address.deletecity', 'uses'=>'AddressController@deletecity'
        ]);
   

     });



Route::group(['prefix'=>'tourguide'],function (){
       Route::get('/',['as'=>'admin.tourguide','uses'=> 'tourguide\TourguideController@index']);
       Route::post('/',['as'=>'admin.tourguide.add','uses'=> 'tourguide\TourguideController@createtourguide']);
        Route::get('/list',['as'=>'admin.tourguide.all','uses'=>'tourguide\TourguideController@show']);
       Route::get('/edit/{id}',['as'=>'admin.tourguide.edit','uses'=>'tourguide\TourguideController@edit']);
       Route::post('/update/{id}',['as'=>'admin.tourguide.update','uses'=>'tourguide\TourguideController@update']);
       Route::delete('/delete/{id}',[
            'as' => 'admin.tourguide.delete', 'uses'=>'tourguide\TourguideController@delete'
        ]);
        Route::get('/removetourguideimg', [
           'as' => 'admin.tourguide.removetourguideimg', 'uses' => 'tourguide\TourguideController@removetourguideimage'
        ]);
     });

     Route::group(['prefix' => 'pages', 'middleware' => []], function () {
       Route::get('/', [
           'as' => 'admin.pages', 'uses' => 'pagesController@list'
        ]);
       
    
       Route::get('/add', [
           'as' => 'admin.pages.add', 'uses' => 'pagesController@index'
        ]);
       Route::post('/add', [
           'as' => 'admin.pages.add', 'uses' => 'pagesController@create'
        ]);
       Route::get('/edit/{id}',[
            'as' => 'pages.edit', 'uses'=>'pagesController@edit'
        ]);
       Route::get('/removePagesimg', [
           'as' => 'admin.pages.removePagesimg', 'uses' => 'pagesController@removePagesimage'
        ]);
       Route::get('/checkSlug', [
           'as' => 'admin.pages.checkSlug', 'uses' => 'pagesController@checkSlug'
        ]);
        //update pages
        Route::post('/edit/{id}',[
            'as' => 'pages.edit', 'uses'=>'pagesController@update'
        ]);
        // delete
        Route::delete('/delete/{id}',[
            'as' => 'pages.delete', 'uses'=>'pagesController@delete'
        ]);
       
       // Route::match(['get', 'post'],  '/insert',[
       //     'as' => 'admin.setting.insert', 'uses' => 'SettingController@create'
       //  ]);
    });
	});

});
