<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Route;
class Module extends Model
{
    protected $fillable = [ 'name', 'route', 'parent', 'status'];

     public static function getRouteListArray($from = null)
    {
        $routes = Route::getRoutes();
        foreach($routes as $route)
        {
            $routeList[NULL]= "Select Route";
            if($from == null){
                if(substr($route->uri ,0,1)=='_'){

                }else{
                    $rout =  str_replace('/{id}','',$route->uri);
                    $newRoute = str_replace('/{id?}','',$rout);
                    if(starts_with($newRoute, 'admin')){
                   	 $routeList[$newRoute] = $newRoute;
                    }
                }
            }elseif(substr($route->uri ,0,1) != '_'){
                $routeList[$route->uri] = $route->uri;
            }
        }
        return $routeList;
    }

    public function sub_module(){

    	return $this->hasMany('Modules\Admin\Entities\Module', 'parent');
    }
    
}
