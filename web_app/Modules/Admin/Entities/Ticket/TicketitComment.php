<?php

namespace Modules\Admin\Entities\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketitComment extends Model
{
    protected $fillable = ['to', 'to_agent',  'content', 'user_id', 'ticket_id', 'type'];

    public function comment_user(){

    	return $this->belongsTo('App\AdminUser', 'user_id');
    }
}
