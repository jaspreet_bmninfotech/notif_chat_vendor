<?php

namespace Modules\Admin\Entities\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketitStatus extends Model
{
    protected $fillable = ['name', 'color', 'status' ,'updated_at', 'created_at'];
    public $timestamps = true;


    public static function status_list(){
    	return self::where('status',1)->pluck('name','id');
    }
}
