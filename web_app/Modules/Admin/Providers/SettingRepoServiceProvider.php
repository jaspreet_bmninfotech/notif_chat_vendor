<?php

namespace Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;

class SettingRepoServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Modules\Admin\SettingRepositories\Home\HomeInterface', 'Modules\Admin\SettingRepositories\Home\HomeRepository');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
