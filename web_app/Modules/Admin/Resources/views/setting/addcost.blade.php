@extends ('admin::layouts/base') 
  @section('content')    

<div class="page-content-wrap">
   <div class="col-md-6 col-lg-6">
       @if(session('success'))
       <div class="alert alert-success">
          {{session('success')}}
       </div>
       @endif
   </div>

   <div class="row">
        <div class="col-md-12">
            
            <form class="form-horizontal" action="{{route('admin.setting.insert')}}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>ADD Cost</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
               
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon">$</span>
                                <input type="text" name="costvalue"  required="" class="form-control"/>
                                
                            </div>                                            
                            <span class="help-block">This is sample of name field</span>
                        </div>
                    </div>
                 
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <textarea name="description" id="description" required="" class="form-control" rows="5"></textarea> 
                            <span class="help-block">This is sample of textarea field</span>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" value="">
                <div class="panel-footer">
                                                        
                    <input type="submit" class="btn btn-primary pull-right">Submit</button>

                    <a href="" class="btn btn-primary">Back</a>

                </div>
            </div>
            </form>
        </div>
    </div>                    
</div>
@endsection
