@extends ('admin::layouts/base') 
    @section('content')
<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span>Per Job Cost</h2> 
    <div class="row">
        <div class="col-md-6 col-lg-6">
            @if(session('success'))
            <div class="alert alert-success">
            {{session('success')}}
            </div>
            @endif
        </div>
    </div>
</div>    

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
    			@foreach($setting as $k=>$v)
                    <div class="panel-body">

	                        <div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-2">
		                        		<label>{{ preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', ucwords(str_replace("_", " ", $v->key))) }}</label>
		                        	</div>

			                        <div class="col-md-4">                          
			                            <div class="input-group">
                                			<span class="input-group-addon">$</span>
                                			<input type="text"  name="jobcost{{$v->id}}" class="form-control" value="{{$v->value}}">
                            			</div>
			                        </div>
			                        <div class="col-md-3 pull-right">  
			                        	<a href="javascript:void(0)" onclick="Updatejobcost( {{$v->id}} )" class="btn btn-raised btn-primary btn-md">Update</a>
			                        </div>                                    
		                        </div> 
		                        <div class="clearfix"></div>  <br>
		                      {{--   <div class="row">
		                        	<div class="col-md-2">
		                        	<label>Description</label>
		                        	</div>
		                        	<div class="col-md-4">
		                        		<textarea rows="5" max-length="5000" name="description" class="form-control" ></textarea>
		                        	</div>
		                        </div> --}} 
		                        <br>
		                        <div class="border_bottom"></div>
	                   		</div>
                    </div>
    			@endforeach

                </div>
            </div>
        </div>
    </div>
</div>      
    @endsection
     @section('script')
     <script src="{{asset('assets/admin/js/perjobcost.js')}}"></script>
    @endsection
