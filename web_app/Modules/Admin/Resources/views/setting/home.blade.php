
@extends ('admin::layouts/base') 
    @section('content')
 @section('addstyle')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 450px; }
  #sortable li { margin: 3px 3px 3px 0; padding: 4px; float: left; width: 100px; height: 90px; font-size: 15px; text-align: center; }
  </style>
 @show
<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span>Category Setting</h2> 
    <div class="row">
        <div class="col-md-6 col-lg-6">
            @if(session('success'))
            <div class="alert alert-success">
            {{session('success')}}
            </div>
            @endif
        </div>
    </div>
</div>    

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="">
              <form action="{{ route('admin.setting.category')}}" name="postForm" method="POST" enctype="multipart/form-data">
                      {{ csrf_field() }}
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-2">
                              <label>Select Category</label>
                            </div>
                            <div class="col-md-4">                          
                                <select name="choosecat" class="form-control" id="cat-select" >
                                        <option value="">Select Category</option>         
                                        @foreach($category as $k=>$v)
                                        <option value="{{$v->id}}">{{$v->name}}</option>         
                                          @endforeach
                                    </select>
                            </div>
                                                              
                          </div> 
                          <div class="clearfix"></div> <br>
                          <br> 
                          <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label></label>
                                </div>
                                <div class="col-md-4">
                                  @if($getcat == "NULL")
                                  
                                   <ul id="sortable" class="homePageCategory">
                                      
                                    </ul>
                                    
                                    @else
                                     <ul id="sortable" class="homePageCategory">
                                      @foreach($getcat as $k=> $v)
                                      <li class="ui-state-default" value="{{ $v['id'] }}">
                                        <i data-id="{{$v['id']}}" class="fa fa-times remove_icon" onclick="removecategory({{ $v['id'] }},this)"></i>{{ $v['name'] }}
                                      </li>
                                      @endforeach
                                    </ul>
                                    @endif
                                    <input type="hidden" name="homecategory" class="homecategory" value="{{@$strcat}}">  
                                    <input type="hidden" name="setting" value="home">  
                              
                                </div>                             
                            </div> 
                            <div class="clearfix"></div>  <br> 
                        </div>
                          <div class="col-md-6 offset-2 text-center"> 
                          @if($getcat== "NULL") 
                                  <button type="submit" class="btn btn-raised btn-primary btn-md" onclick="save(this)"> Save </button>
                                  @else
                                  <button type="submit" class="btn btn-raised btn-primary btn-md" onclick="update(this)"> update </button>
                          @endif
                          </div> 
                      </div>
                    </div>
                </div>
                <br>  
                </br>     
              </form>
            </div>
        </div>
    </div>
</div>      
    @endsection
     @section('script')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/admin-setting.js')}}"></script>

    @endsection
