@extends ('admin::layouts/base') 
  @section('content')
 <div class="well bs-component">

 	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        

        <div class="container">
              <div class="row">
        <div class="col-md-12">    
           {!! Form::open([
                        'route'=>'admin.register.store',
                        'method' => 'POST',
                        'class' => 'form-horizontal'
                        ]) !!}
              {!! csrf_field() !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>ADD ADMIN USER</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('name',null, ['class'=>"form-control"])!!}
                                {{-- <input type="name" name="name" id="name" required="" class="form-control"/> --}}
                            </div>                                               
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">USER NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('username',null, ['class'=>"form-control"])!!}
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">EMAIL</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                               {!! Form::text('email',null, ['class'=>"form-control"])!!}
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">PASSWORD</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-key"></i></span>
                               {!! Form::text('password',null, ['class'=>"form-control"])!!}
                            </div>                                               
                        </div>
                    </div>

                       <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">ROLE</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-message"></i></span>
                               {!! Form::select('role_id', $roles,null, ['class'=>"form-control"])!!}
                            </div>                                               
                        </div>
                    </div>
                </div>
                
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="{{route('admin.admin.users')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div> 
            </div>
            
           
        {!! Form::close() !!}
    </div>


@endsection