<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title> VendorForest</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="_token" content="{{ csrf_token() }}">
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        {{--<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">--}}
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> 
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('assets/admin/css/theme-default.css')}}"/>
        <!-- <link rel="stylesheet" type="text/css" id="theme" href="{{asset('assets/admin/css/index.css')}}"/> -->
         @section('addstyle')
         @show
        <!-- EOF CSS INCLUDE -->                                       
    </head>
  <!-- START PRELOADS -->
    <body>
        <div class="page-container">
            <input type="hidden" value="{{ csrf_token() }}" name="_token">
            @include ('admin::layouts/admin-sidebar')
            <!-- PAGE CONTENT -->
            <div class="page-content">
                @section ('header')
                    @include ('admin::layouts/admin-header')
                @show 
                    <?php $brdcrmb = count(explode('/',$_SERVER['REQUEST_URI']));
                    $countbre =  $brdcrmb;  ?>
                <ul class="breadcrumb">
                    @for ($i = 1; $i < $countbre; $i++)
                    @if($brdcrmb-1 == 1)
                    <li><a href="/{{Request::segment($i) }}">{{Request::segment($i) }}</a></li>     
                    @else
                        @if($i == 1)
                        <li><a href="/{{Request::segment($i) }}">{{Request::segment($i) }}</a></li>     
                        @else
                        <li><a href="/{{Request::segment($i-1) }}/{{Request::segment($i) }}">{{Request::segment($i) }}</a></li>     
                    @endif
                    @endif
                    @endfor
                </ul>
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="{{route('admin.logout')}}" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX--> 
        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <!-- <script type="text/javascript" src="{{asset('assets/admin/js/plugins/jquery/jquery.min.js')}}"></script> -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/jquery/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/bootstrap/bootstrap.min.js')}}"></script>        
        <!-- END PLUGINS -->
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/icheck/icheck.min.js')}}"></script>        
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/morris/raphael-min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/morris/morris.min.js')}}"></script>       
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/rickshaw/d3.v3.js')}}"></script>
        <!-- <script type="text/javascript" src="{{asset('assets/admin/js/plugins/rickshaw/rickshaw.min.js')}}"></script> -->
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>                
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>                
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/owl/owl.carousel.min.js')}}"></script>                 
        
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script> 
        <script type="text/javascript" src="{{asset('assets/admin/js/settings.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins.js')}}"></script>        
        <script type="text/javascript" src="{{asset('assets/admin/js/actions.js')}}"></script>  
        <!-- <script type="text/javascript" src="{{asset('assets/admin/js/demo_dashboard.js')}}"></script>  -->
        @section('script')
        @show

    </body>
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->     
</html>

    