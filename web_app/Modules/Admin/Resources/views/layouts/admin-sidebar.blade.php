<?php
$admin_Side_bar = admin_Side_bar();
$admin_role_id = admin_role_id();
$permisson = role_permisson();
// dump($admin_role_id , );
?>
                    
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="#">Vendor Forest</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="/assets/admin/images/users/avatar.jpg" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    @if(getAdminData()->attachment['path'] != "")
                        <img src="/{{getAdminData()->attachment['path']}}" alt="{{getAdminData()['username']}}"/>
                    @else
                        <img src="/images/no-user.png" alt="{{getAdminData()['username']}}"/>
                    @endif
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">{{getAdminData()['username']}}</div>
                    {{-- <div class="profile-data-title">Web Developer/Designer</div> --}}
                </div>
                <div class="profile-controls">
                    <a href="{{route('admin.profile')}}" class="profile-control-left"><span class="fa fa-cog"></span></a>
                    {{-- <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a> --}}
                </div>
            </div>                                                                        
        </li>
        {{-- <li class="xn-title">Navigation</li> --}}
        <li class="active">
            <a href=""><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>
       {{--  <li class="">
            
            <a href="{{route('category.list')}}"><span class="fa fa-columns"></span> <span class="xn-text">Category</span></a>                        
        </li>
        <li class="active">
            <a href=""><span class="fa fa-file"></span> <span class="xn-text">Pages</span></a>                        
        </li>
        <li class="">
            
            <a href="{{route('admin.pages')}}"><span class="fa fa-columns"></span> <span class="xn-text">All Pages</span></a>                        
        </li> --}}

       {{--  <li class="xn-title">Components</li>                    
            <li class="xn-openable active">
                <a href="{{route('admin.users')}}"><span class="fa fa-cogs"></span> <span class="xn-text">User</span></a>
                <ul>
                    <li><a href="{{route('user.vendor.list')}}"><span class="fa fa-heart"></span> Vendor</a></li>                            
                    <li><a href="{{route('user.client.list')}}"><span class="fa fa-cogs"></span> Client</a></li> 
                    <li><a href="{{route('admin.tourguide.all')}}"><span class="fa fa-cogs"></span> Tourguide</a></li> 
                </ul>
            </li>  --}}
              
              @if($admin_role_id==1)

                 @foreach($admin_Side_bar as $key => $val)
                    <li class="xn-openable ">
                        <a href=""><span class="fa fa-cogs"></span> <span class="xn-text">{{$val['name']}}</span></a>
                         @if(!empty($val['sub_module']))

                            @foreach($val['sub_module'] as $sub_key => $sub_val)
                              
                                    <ul>
                                        <li>
                                            <a href="{{url($sub_val['route'])}}"><span class=""></span>{{$sub_val['name']}}
                                            </a>
                                        </li>                            
                                    </ul>
                               
                            @endforeach
                        @endif
                    </li>
                @endforeach


              @else
                @foreach($admin_Side_bar as $key => $val)
                 @if(in_array($val['id'], $permisson))
                    <li class="xn-openable ">
                        <a href=""><span class="fa fa-cogs"></span> <span class="xn-text">{{$val['name']}}</span></a>
                         @if(!empty($val['sub_module']))
                            @foreach($val['sub_module'] as $sub_key => $sub_val)
                                @if(in_array($sub_val['id'], $permisson))
                                    <ul>
                                        <li>
                                            <a href="{{url($sub_val['route'])}}"><span class=""></span>{{$sub_val['name']}}
                                            </a>
                                        </li>                            
                                    </ul>
                                @endif
                            @endforeach
                        @endif
                    </li>
                @endif
                @endforeach
            @endif
            {{--<li class="xn-openable active">
                <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Setting</span></a>
                <ul>     
             <li><a href="{{route('admin.setting.perjob')}}"><span class="fa fa-cogs"></span>costperjob</a></li>       
                </ul>
            </li>--}}     
    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR