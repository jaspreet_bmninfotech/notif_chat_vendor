@extends ('admin::layouts/base') 
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:white;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
        @if (Session::has('errors'))
          <div class="alert alert-danger">
            <p style="color:white;">{{Session::get('errors')}}</p>
            @php Session::forget('errors'); @endphp
          </div>
        @endif
<div class="page-title">                    
    <h2><span class="fa fa-cogs"></span> Edit Profile</h2>
</div>
<!-- END PAGE TITLE -->                     

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">    
    <div class="row">                        
        <div class="col-md-4 col-sm-4 col-xs-5">
            
            <form action="{{route('profile.create')}}" class="form-horizontal" method="post">
                {!! csrf_field() !!}
            <div class="panel panel-default">                                
                <div class="panel-body">
                    <h3><span class="fa fa-user"></span> {{getAdminData()['username']}}</h3>
                    {{-- <p>Web Developer / Administrator</p> --}}
                    <div class="text-center" id="user_image">
                        @if(getAdminData()->attachment['path'] != "")
                        <img src="/{{getAdminData()->attachment['path']}}" class="img-thumbnail"/>
                    @else
                        <img src="/images/no-user.png" class="img-thumbnail"/>
                    @endif
                    </div>                                    
                </div>
                <div class="panel-body form-group-separated">
                    
                    <div class="form-group">                                        
                        <div class="col-md-12 col-xs-12">
                            @if(getAdminData()->attachment['path'] == "")
                            <a href="#" class="btn btn-primary btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_photo">Upload Photo</a>
                        @else
                            <a href="#" class="btn btn-primary btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_photo">Change Photo</a>
                        @endif
                        </div>
                    </div>
                    
                    {{-- <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">#ID</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" value="{{$admin['id']}}" class="form-control" disabled/>
                        </div>
                    </div> --}}
                    
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Login</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="username" value="{{$admin['username']}}" class="form-control" required/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">E-mail</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="email" name="email" value="{{$admin['email']}}" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">                                        
                        <div class="col-md-12 col-xs-12">
                            <a href="#" class="btn btn-danger btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_password">Change password</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-5">
                            <button class="btn btn-primary btn-rounded pull-right">Save</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            </form>
            
        </div>
        <div class="col-md-8 col-sm-8 col-xs-7">
            
            <form action="{{route('profile.create')}}" class="form-horizontal" method="post">
                {!! csrf_field() !!}
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-pencil"></span> Profile</h3>
                </div>
                <div class="panel-body form-group-separated">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">First Name</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" value="{{$admin['firstName']}}" name="firstName" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Last Name</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" value="{{$admin['lastName']}}" name="lastName" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Address</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" value="{{$admin['address']}}" name="address" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-5">
                            <button class="btn btn-primary btn-rounded pull-right">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- MODALS -->
        <div class="modal animated fadeIn" id="modal_change_photo" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Upload photo</h4>
                    </div>                    
                    <form id="cp_crop" method="post" action="http://aqvatarius.com/themes/atlant_v1_5/html/assets/crop_image.php">
                    {{-- <div class="modal-body">
                        <div class="text-center" id="cp_target">Use form below to upload file. Only .jpg files.</div>
                        <input type="hidden" name="cp_img_path" id="cp_img_path"/>
                        <input type="hidden" name="ic_x" id="ic_x"/>
                        <input type="hidden" name="ic_y" id="ic_y"/>
                        <input type="hidden" name="ic_w" id="ic_w"/>
                        <input type="hidden" name="ic_h" id="ic_h"/>                        
                    </div>  --}}                   
                    </form>
                    <form id="cp_upload" method="post" enctype="multipart/form-data" action="{{route('profile.create')}}">
                        {!! csrf_field() !!}
                    <div class="modal-body form-horizontal form-group-separated">
                        <div class="form-group">
                            <label class="col-md-4 control-label">New Photo</label>
                            <div class="col-md-4">
                                <input type="file" class="fileinput btn-info" name="file" id="cp_photo" data-filename-placement="inside" title="Select file"/>
                            </div>                            
                        </div>                        
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success">Accept</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Change password</h4>
                    </div>
                    <form id="cp_upload" method="post" action="{{route('change.password')}}">
                        {!! csrf_field() !!}
                    <div class="modal-body form-horizontal form-group-separated">                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Old Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="oldpassword"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">New Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="newpassword"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Repeat New</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="repassword"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" >Proccess</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>        
        <!-- EOF MODALS -->
@endsection
@section('script') 
 <script type="text/javascript" src="{{asset('assets/admin/js/demo_edit_profile.js')}}"></script>
@endsection