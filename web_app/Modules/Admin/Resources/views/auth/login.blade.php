<!DOCTYPE html>
<html lang="en" class="body-full-height">   
<head>        
        <!-- META SECTION -->
        <title>Atlant - Responsive Bootstrap Admin Template</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script> 
        <!-- END META SECTION -->
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('assets/admin/css/theme-default.css')}}"/>
        <!-- EOF CSS INCLUDE -->      
</head>
    <body>
        <div class="login-container">
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>, Please login</div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.logined') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                                {{-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> --}}

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="admin1@vendorforest.com" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            {{-- <label for="password" class="col-md-4 control-label">Password</label> --}}

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" placeholder="Password" name="password" required value="123456"> 
                            </div>
                        </div>

                       {{--  <div class="form-group">
                            <div class="col-md-6 ">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <div class="col-md-6">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-info btn-block">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                       
                    </div>
                    <div class="pull-right">
                        
                    </div>
                </div>
            </div>
        </div>
    </body>