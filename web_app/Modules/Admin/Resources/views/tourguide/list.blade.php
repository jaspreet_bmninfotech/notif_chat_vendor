@extends ('admin::layouts/base') 
@section('addstyle')
 <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
	@endsection
  @section('content')
  <div class="page-content-wrap">
    <div class="col-md-12">
      @if(Session::has('success'))
            <div class="alert alert-success">
              <p style="color:green;">{{Session::get('success')}}</p>
              @php Session::forget('success'); @endphp
            </div>
        @endif
    </div>
      <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                  <div class="panel-heading">
                      <h1 class="panel-title"><strong>List</strong> </h1>
                          <a href="{{route('admin.tourguide')}}" class="btn btn-info btn-md pull-right">Add Tourguide</a>              
                  </div>
                    <div class="panel-body">
                        @if($tourguide)
                        <table class="table datatable" id="tourexample">
                        <thead>
                            <tr id="removesort">
                                <th>ID</th>
                                <th>USERNAME</th>
                                <th>Email</th>
                                <th>Phone</th>
        
                               
                                <th id="rmsort" class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                          <?php $i = 1; ?>
                            @foreach($tourguide as $tguide)
                            <tr>
                                <td>{{$i++}}</td>  
                                <td>{{$tguide->username}}</td>
                                <td>{{$tguide->email}}</td> 
                                <td>{{$tguide->phone}}</td> 
                                <td class="text-center">
                                  <a class="btn btn-raised btn-primary btn-sm" href="{{route('admin.tourguide.edit',$tguide->id)}}" id="edittour" data-id="{{$tguide->id}}">
                                       <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> || <form method="post" id= "delete-form-{{$tguide->id}}" action="{{route('admin.tourguide.delete',$tguide->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                                <button onclick='swal({
                                             title: "Are you sure?",
                                              text: "You will not be able to recover this imaginary file!",
                                              type: "warning",
                                              showCancelButton: true,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Yes, delete it!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            }).then(function(isConfirm) {
                                              if (Object.keys(isConfirm)[0] == "dismiss") {
                                                swal("Cancelled", "Your imaginary file is safe :)", "error");
                                              } else {
                                                document.getElementById("delete-form-{{ $tguide->id}}").submit();
                                              }
                                            });' 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                            @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  @endsection  
  @section('script')
  <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/admin/js/tourguide.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	// $(document).ready(function() {
 //    $('#example').DataTable( {
        
 //    } );
	// } );
	</script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#removesort').find('#rmsort').removeAttr('class','sorting');
    });
    $(document).on('click','body',function(){
        $('#removesort').find('#rmsort').removeAttr('class','sorting');
    });
</script>
  @endsection