@extends ('admin::layouts/base') 
@section('addstyle')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
 @endsection
 @section('content')
<div class="page-content-wrap">
    @if(Session::has('success'))
    <div class="alert alert-success">
        <p style="color:green;">{{Session::get('success')}}</p>
        @php Session::forget('success'); @endphp
    </div>
    @endif
    <div class="form-fields col-md-12">
        <form role="form" method="POST" enctype="multipart/form-data" action="{{route('admin.tourguide.add')}}">
            {!! csrf_field() !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Add Tourguide</strong> </h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class=" form-group row">
                                <div class="col-md-6">
                                    <label>First Name <em>*</em></label>
                                    <input type="text" name="firstName" value="" class="form-control">

                                </div>
                                <div class="col-md-6 last">
                                    <label>Last Name <em>*</em></label>
                                    <input type="text" name="lastName" value="" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Username <em>*</em></label>
                                    <input type="text" name="username" class="form-control" value="" />
                                </div>
                            
                                <div class="col-md-6">
                                    <label>Email <em>*</em></label>
                                    <input type="text" name="email" class="form-control" value=""/>
                                </div>
                            </div>
                             <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Password <em>*</em></label>
                                    <input type="password" name="password" class="form-control" value="" />
                                </div>
                            
                                <div class="col-md-6">
                                    <label>Confirm Password <em>*</em></label>
                                    <input type="password" name="confpassword" class="form-control" value=""/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>&nbsp;</label>
                                    <a href="" class="button default">Change password</a>
                                </div>
                            </div>
                           
                            <div class="form-group row">
                                <div class="col-md-6">
                                  <label>Ph no. <em>*</em></label>
                                    <div class="form-group">
                                      <span class="col-md-3">
                                          <input type="number" class="form-control" name="" maxlength="3"/>
                                      </span>   
                                        <span class="col-md-9">
                                          <input type="text" class="form-control" name="phone" value="" min="10" maxlength ="12">
                                        </span>
                                    </div>
                                      <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Date of Birth</label>
                                    <div class="input-group date" id="datePicker1">
                                        <input type="text" name="dob" value="" class="form-control">
                                        <span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                          <div class="row ">
                            <div class="col-md-8">
                              <div  id="tourguideprofilefiles" class="user-image">
                                <img src="/assets/admin/images/users/no-image.jpg" >
                              </div>
                            </div>
                            <div class="col-md-12">
                              <span class="button solid btnupload fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Choose Profiles...</span>
                                    <input  type="file" id="pfiles" name="pfile" >
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="col-md-8">
                          <div class="form-group row">
                            <div class="col-md-5">
                              <label>Gender</label>
                              <div class="form-check row "> 
                                  <input class="form-check-input" type="radio" name="gender" value="M">
                                  <label class="form-check-label" for="">Male</label>
                              </div>
                              <div class="form-check row ">   
                                  <input class="form-check-input" type="radio" name="gender" value="F">
                                  <label class="form-check-label" for="">Female</label>
                              </div>      
                            </div>
                            <div class="col-md-7">
                              <label>Race</label>
                                <div class="form-check form-check-inline"> 
                              @foreach($race as $key=> $racevalue)
                                  <input class="form-check-input" type="radio" name="race" value="{{$key}}">
                                  <label class="form-check-label" for=""> {{$racevalue}}</label>
                              @endforeach
                                </div>
                            </div>  
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Profile end-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Location And Nationality</strong> </h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <div class="col-md-6 ">
                                    <label>Birth Country <em>*</em></label>
                                    <select name="birth_country" class="form-control">
                                        <option value="">Country</option>
                                        @foreach($country as $key=>$val)
                                        <option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label>Country Of Residence <em>*</em></label>
                                    <select name="country" id="country" onchange="getstate(this)" class="form-control">
                                        <option value="">Country</option>
                                        @foreach($country as $key=>$val)
                                        <option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 last">
                                    <label>State</label>
                                    <select name="state" id="state" onchange="getcity(this)" class="form-control">
                                        <option value="">State</option>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4">
                                    <label>City <em>*</em></label>
                                    <select name="city" id="city" class="form-control">
                                        <option>City</option>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Postal Code</label>
                                    <input type="text" name="postalCode" class="form-control">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-6">
                                    <label>Street Address</label>
                                    <input type="text" name="address" class="form-control">
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <div class="form-group row">
                                    <div class="col-md-3">  
                                      <label>Status</label>
                                      @foreach($nationality_status as $key=>$val) 
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" name="nationality_status" value="{{$key}}">
                                        <label class="form-check-label" for=""> {{$val}}</label>
                                      </div>
                                        @endforeach
                                    </div>  
                                  <div class="col-md-9">
                                      <label class="margin_bottom1">Upload a document supporting your claim</label>
                                      <div class="form-group">
                                        <div class="col-md-5">
                                            <div class="col-md-8">
                                              <span class="button solid btnupload fileinput-button">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                    <span>Choose Profiles...</span>
                                                    <input  type="file" id="claimfiles" name="claimfile" >
                                            <p>File not exceed 2mb</p>
                                              </span>
                                            </div>
                                            <div class="col-md-4 ">
                                              <div  id="tourguideclaimfiles" class="">
                                                <img src="/assets/admin/images/users/no-image.jpg" >
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div id="docfiles" class="viewimg">
                                            </div>
                                        </div>
                                      </div>
                                  </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Location and Nationality End -->
            <!-- profile photos start -->
             <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Profile Photos</strong></h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                          <div class="form-group">
                            <div class="col-md-12">
                             <p>Add 5 photos that show others how amazing you are. This is your chance to make an awesome first impression! Remember: A 
                              friendly smile makes friends :slightly_smiling_face:
                              Important: We reserve our right to change your main photo or reject photos you upload that does not comply with our profile photo guidelines. Learn more.</p>
                                • Your face should be clearly visible in your main photo.<br>
                                • Photos should be natural and bright without effects. Blurry, low-resolution photos may be removed.<br>
                                • Photos should not have text on them.<br>
                                • If other people are in your photos, it should be obvious who you are.<br>
                                • Explicit, sexually suggestive, or aggressive photos are not welcome.<br>
                                • Avoid children and pets in your photos.<br>
                                <p>   Use your five photos to leave a good first impression on local hosts and other travelers! Since photos are critical in building trust among our users, we reserve the right to reject photos or profiles that do meet the above-listed guidelines.</p>
                                <p>   Important: We love all languages, but we kindly ask to fill out this section in English. Please avoid writing brands, company names and links to other sites in your description.</p>
                              
                              
                                  <div class="col-md-5">
                                     <div class="col-md-8">
                                        <span class="button solid btnupload fileinput-button">
                                              <i class="glyphicon glyphicon-plus"></i>
                                              <span>Choose Profiles...</span>
                                              <input  type="file" id="profilefiles" name="profilefiles" >
                                      <p>File not exceed 2mb</p>
                                        </span>
                                      </div>
                                      <div class="col-md-4 ">
                                        <div  id="tourguidenprofilefiles" class="">
                                          <img src="/assets/admin/images/users/no-image.jpg" >
                                        </div>
                                      </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <!-- profile photos end -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Language & Culture</strong></h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Fluent Language</label>
                                    <div class="input-group" id="extend">
                                        
                                        <div class="input-group text-field1">
                                            <input type="text" name="guidelanguage[]" value="" class="form-control">
                                            <span class="input-group-addon addtext"><a href="#" class="add_field_button"><i class="fa fa-plus"></i>Add</a></span>
                                        </div>
                                         
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>For a scale of 1 to 10 being very well.How well do you know the country</label>
                                    <div class="radiobut">
                                        <input class="one form-check" type="radio" name="know_country" value="1" >
                                        <span class="onelb">1</span>
                                        <input class="two form-check" type="radio" name="know_country" value="2">
                                        <span class="onelb">2</span>
                                        <input class="two form-check" type="radio" name="know_country" value="3">
                                        <span class="onelb">3</span>
                                        <input class="two form-check" type="radio" name="know_country" value="4">
                                        <span class="onelb">4</span>
                                        <input class="two form-check" type="radio" name="know_country" value="5">
                                        <span class="onelb">5</span>
                                        <input class="two form-check" type="radio" name="know_country" value="6">
                                        <span class="onelb">6</span>
                                        <input class="two form-check" type="radio" name="know_country" value="7">
                                        <span class="onelb">7</span>
                                        <input class="two form-check" type="radio" name="know_country" value="8">
                                        <span class="onelb">8</span>
                                        <input class="two form-check" type="radio" name="know_country" value="9">
                                        <span class="onelb form-check">9</span>
                                        <input class="two form-check" type="radio" name="know_country" value="10">
                                        <span class="onelb">10</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Language and Culture end -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Activies</strong></h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                          <div class="form-group row">
                            <div class="col-md-12">
                              <label>What Activies do you do with your visitors? </label>
                              <br>
                              <div class="touractivities">
                                  <ul>
                                    @foreach($activies as $key=>$val)
                                      <li>
                                          <input type="checkbox" value="{{$key}}" name="tourguideactivities[]" class="pull-left form-check">
                                          <label for="">{{$val}}</label>
                                      </li>
                                    @endforeach
                                  </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Activities tourguide end -->
            <!-- Show you tourguide start -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>I Will Show You</strong></h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <div class="col-md-8"> 
                            <label>How would you describe the ideal tour around your city ? Be specific</label>
                            <br>
                            <textarea name="showyou" placeholder="Where would you take your visitors for a tour" id="showyou" class="form-control"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <!-- Show you tourguide end -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Transportation</strong></h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label>Do you have a car ?</label>
                                </div>
                                <div class="col-md-6 radiobut radbutn">
                                    <input class="one form-check-input" type="radio" name="isCars" value="yes">
                                    <span class="onelb">Yes</span>
                                    <input class="two form-check-input" type="radio" name="isCars" value="no">
                                    <span class="onelb">No</span>
                                </div>
                            </div>
                            <div class="form-group row" id="carforTour">
                                <div class="col-md-6">
                                    <label>Will you be using your car for the tour ?</label>
                                </div>
                                <div class="col-md-6 radiobut cartour">
                                    <input class="one form-check-input" type="radio" name="carforTour" value="yes">
                                    <span class="onelb form-check-input">Yes</span>
                                    <input class="two" type="radio" name="carforTour" value="no">
                                    <span class="onelb">No</span>
                                </div>
                            </div>
                            <div id="cardocuments">
                                <div class="form-group row" id="cardocs">
                                    <div class="col-md-4">
                                        <label>Make</label>
                                        <select name="make" id="make" class="form-control">
                                            <option value="">-Select-</option>
                                            <option value="10oct">10 OCT</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Model</label>
                                        <select name="model" id="model" class="form-control">
                                            <option value="">-Select-</option>
                                            <option value="2012">2012</option>
                                            <option value="2015">2015</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Year</label>
                                        <select name="year" id="year" class="form-control" >
                                            <option value="">-Select-</option>
                                            <option value="2012">2012</option>
                                            <option value="1999">1999</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" id="plate_licence_no">
                                    <div class="col-md-6">
                                        <label>Plate No.</label>
                                        <input type="text" name="plateNo" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Driving Liscence No.</label>
                                        <input type="text" name="licenceNo" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row" id="licencecopy">
                                    <div class="col-md-12">
                                        <label class="margin_bottom1">Upload a copy of your licence</label>
                                        <div class="row">
                                            <div class="col-md-5">
                                               <div class="col-md-8">
                                                <span class="button solid btnupload fileinput-button">
                                                      <i class="glyphicon glyphicon-plus"></i>
                                                      <span>Choose Profiles...</span>
                                                      <input  type="file" id="licencefiles" name="licencefiles" >
                                                  <p>File not exceed 2mb</p>
                                                </span>
                                               </div>
                                                <div class="col-md-4 ">
                                                  <div  id="tourguidelicencefiles" class="">
                                                    <img src="/assets/admin/images/users/no-image.jpg" >
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="isCarinsurance">
                                    <div class="col-md-4">
                                        <label>Do you have a car insurance ?</label>
                                    </div>
                                    <div class="col-md-8 radiobut insurance">
                                        <input class="one" type="radio" name="carinsurance" value="yes">
                                        <span class="onelb">Yes</span>
                                        <input class="two" type="radio" name="carinsurance" value="no">
                                        <span class="onelb">No</span>
                                    </div>
                                </div>
                                <div class="form-group row" id="insurance_reason">
                                    <div class="col-md-6">
                                        <textarea name="insurancereason" cols="10" rows="5" placeholder="reason" id="reason" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row" id="insurance_docs">
                                    <div class="col-md-12">
                                        <label class="margin_bottom1">Upload a copy of your insurance Document</label>
                                        <div class="row">
                                            <div class="col-md-8 loaddoc">
                                                <div class="col-md-5">
                                                      <div class="col-md-8">
                                                        <span class="button solid btnupload fileinput-button">
                                                            <i class="glyphicon glyphicon-plus"></i>
                                                            <span>Choose Profiles...</span>
                                                            <input  type="file" id="insurancefiles" name="insurancefiles" >
                                                        <p>File not exceed 2mb</p>
                                                        </span>
                                                      </div>
                                                  <div class="col-md-4 ">
                                                    <div  id="tourguideinsurancefiles" class="">
                                                      <img src="/assets/admin/images/users/no-image.jpg" >
                                                    </div>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit"  class="btn btn-primary">Create Profile</button>
                </div>
            
            <br>
            <br>

        </form>
    </div>
</div>
@endsection 
@section('script')
 <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/tourguide.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

@endsection
