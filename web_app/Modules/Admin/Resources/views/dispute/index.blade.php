@extends('admin::layouts.base')

@section('content')

<div class="panel panel-default">

   @include('admin::dispute.dispute_header')

    

    <div class="panel-body">
        <div id="message"></div>

       {{--  <a href="{{route('ticket.create')}}"> Create Ticket</a> --}}

        <table class="table table-condensed table-stripe ddt-responsive" class="ticketit-table" id="listdispute">
    <thead>
        <tr>
            <td>id</td>
            <td>Subject</td>
            <td>Status</td>
            <td>Agent</td>
            <td>Job </td>
            <td>Client</td>
            <td>Vendor</td>
            <td>Tour Guide</td>
            <td>Category</td>
            <td>Apply by </td>
            <td>Apply at </td>
          

            
        </tr>
    </thead>
    <tbody>
    	@foreach($data as $key =>  $val)
    	<tr>
    		<td> {{$key+1}}</td>
    		<td> <a href="{{route('admin.dispute.detail',['id'=>$val->id])}}">{{$val->subject}} </a></td>
    		<td>{{$val['status']['name']}}</td>
            <td>{{agent_user_name($val->agent_id) }}</td>
            <td> {{$val['job']['name']}}</td>
            <td> cl </td>
            <td> v </td>
            <td> t </td>
            <td> {{@$val['category']['name']}} </td>
            <td> {{$val->dispute_apply_by}} </td>
            <td> {{date('m/d/Y H:i:s', strtotime($val->created_at))}} </td>
    		
    	</tr>
    	@endforeach
    </tbody>
</table>

        {{-- @include('ticketit::tickets.partials.datatable') --}}
    </div>

</div>
@endsection
@section('script')
<script type="text/javascript">
    $('#listdispute').DataTable( {     
    });
  </script>
@endsection