@extends('admin::layouts.base')

@section('content')

  @include('admin::dispute.dispute_header')

@php 
$list_status = $status_list->toArray();
$data = $data->toArray();

$comments = collect($data['comment']);

$except = ['dispute_verdicts', 'comment' ,'id' , 'status_id', 'category_id','job_id', 'client_id', 'vendor_id', 'vendor_id', 'tour_guide_id', 'agent_id', 'updated_at'];
@endphp 
<div class="panel-body">
    <div class="form-group">
    	<h2> Current Status: {{$list_status[$data['status_id']] }}</h2> 
		@php 
		  unset($list_status[$data['status_id']]);
		@endphp
			<span> Status Change to </span>
    	@foreach($list_status as $skey => $sval)
        	<a href="{{route('admin.dispute.status',['id'=>$data['id'], 'status_id'=> $skey])}}" class="btn btn-success"> {{$sval}} </a>
    	@endforeach
    </div>
		<div class="col-md-12">
            <div class="panel panel-default form-horizontal">
                <div class="panel-body">
                    <h3><span class="fa fa-info-circle"></span> Dispute Info</h3>
                </div>
                <div class="panel-body form-group-separated">
					@foreach(array_except($data, $except) as $key => $val)
                    @if(in_array($key , ['category','job']))
						<div class="form-group tleft">
 							<label class="col-md-4 col-xs-5 control-label">{{ucfirst($key)}}</label>
                            <div class="col-md-8 col-xs-7 line-height-30">{{$val['name']}}</div>
                        </div>

                    @else
                        
                            <div class="form-group tleft">
                                <label class="col-md-4 col-xs-5 control-label">{{ucfirst(str_replace("_", " ", $key)) }}</label>
                                <div class="col-md-8 col-xs-7 line-height-30">{{ ($key == 'created_at') ? date("m/d/Y H:i:s", strtotime($val)) : $val}}</div>
                            </div>
                    @endif
					@endforeach 
				 </div>
			</div>
		</div>

<!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">

 <!-- START CONTENT FRAME BODY -->
 	<div class="col-md-12">
        <h3> Comments </h3>
                   <div class="panel panel-default tabs">
                                <ul class="nav nav-tabs nav-justified disputetab">
                                    <li class="active"><a href="#tab1" data-toggle="tab">{{username($data['vendor_id'])}}</a></li>
                                    <li><a href="#tab2" data-toggle="tab">{{username($data['client_id'])}}</a></li>
                                   
                                </ul>
                                <div class="panel-body tab-content" >

                                    <!-- <div class="content-frame-body content-frame-body-left">
                        
                                    </div > -->
                                  <div class="tab-pane active" style="margin-right: 800px;" id="tab1">
                                    	{{-- <h2> Message to {{username($data['client_id'])}} </h2> --}}
                                        <div style="overflow-y: scroll; height: auto; max-height:200px;">
                                            
                                        @foreach($comments->where('to', $data['vendor_id']) as $ckey => $cVal)
                                            @if(!empty($cVal['to_agent']))
                                            <div style="margin:5px 0px 5px 40%;background:#fcf8e3;padding: 5px; border-radius: 5px; border:1px solid gray">
                                                <p class="warning">{{username($cVal['to']) }}<span class="fa fa-user"></span></p><h4 style="" > {{$cVal['content']}}</h4>
                                            </div>
                                            @else
                                            <div style="margin:5px 40% 10px 0px;background:#f2dede;padding: 5px; border-radius: 5px; border:1px solid gray">
                                                <span>{{agent_user_name($cVal['user_id']) }}</span><h4 class="" >&nbsp;&nbsp;{{$cVal['content']}}</h4>
                                            </div>
                                            @endif
                                        @endforeach
                                        </div>
                                       {!! Form::open(['route'=>'tickets.comment'    ]) !!}
                                                <input type="hidden"  name="type"  value="dispute">
                                                <input type="hidden" name="ticket_id" value="{{$data['id']}}">
                                                <input type="hidden" name="user_id" value="1">
                                                <input type="hidden" name="to" value="{{$data['vendor_id']}}">
                                                <br>
                                                <textarea name="content" id="" cols="30" rows="5" class="form-control" placeholder="text here"></textarea><br>
                                            {!! Form::submit('send message',['class'=>'btn btn-info']) !!}

                                            {!! Form::close() !!}
                                    </div>
                                    <div class="tab-pane" style="margin-left: 800px;" id="tab2">
                                         <div style="overflow-y: scroll; height: auto; max-height:200px;">
                                         @foreach($comments->where('to', $data['client_id']) as $ckey => $cVal)

                                          @if(!empty($cVal['to_agent']))
                                            <div style="margin:5px 0px 5px 40%;background:#fcf8e3;padding: 5px; border-radius: 5px; border:1px solid gray">
                                                <p>{{username($cVal['to']) }}</p><h2 style="margin-left:50px;" > {{$cVal['content']}}</h2>
                                            </div>
                                            @else
                                            <div style="margin:5px 40% 10px 0px;background:#f2dede;padding: 5px; border-radius: 5px; border:1px solid gray">
                                                <span>{{agent_user_name($cVal['user_id']) }}</span><h4 class="warning" >{{$cVal['content']}}</h4>
                                            </div>
                                            @endif
                                        @endforeach
                                         </div>
                                        {!! Form::open(['route'=>'tickets.comment'    ]) !!}
												<input type="hidden"  name="type"  value="dispute">
												<input type="hidden" name="ticket_id" value="{{$data['id']}}">
												<input type="hidden" name="user_id" value="1">
												<input type="hidden" name="to" value="{{$data['client_id']}}">
												<textarea name="content" id="" cols="30" rows="5" class="form-control" placeholder="text here"></textarea><br>
											{!! Form::submit('send message client',['class'=>'btn btn-info']) !!}
										{!! Form::close() !!}
                                    </div>
                                                       
                                </div>
                            </div>                        
                            <!-- END TABS WIDGET -->
                        </div>
                    </div>
  <div class="col-md-12">
      
     <h3> DISPUTE FINAL VERDICT OF </h3>
      @foreach($data['dispute_verdicts'] as $verdic_key => $verdict_val)
        <h5>{{$loop->iteration  }} {{ $verdict_val['content']}}   </h5>
      @endforeach

{!! Form::open(['route'=>'admin.dispute.verdict'    ]) !!}
<input type="hidden" name="dispute_id" value="{{$data['id']}}">
<input type="hidden" name="agent_id" value="8">
<textarea name="content" id="" cols="30" rows="10" class="form-control" placeholder="text here"></textarea><br>
{!! Form::submit('Submit',['class'=>'btn btn-info']) !!}

{!! Form::close() !!}
  </div>                  
</div> 
@endsection