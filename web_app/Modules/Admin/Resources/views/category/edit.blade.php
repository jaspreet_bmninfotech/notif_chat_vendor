@extends ('admin::layouts/base') 
    @section('content')


<div class="page-content-wrap">
   <div class="row">

        <div class="col-md-12">
            
            <form class="form-horizontal"  method="post">
            {{ csrf_field() }}
            
                 <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Edit Category</strong> </h1>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
            
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><span class="fa fa-pencil"></span></span>

                            <input type="name" name="name" id="name" required="" value="{{ $category->name}}" class="form-control"/>
                                
                            </div>                                            
                        </div>
                    </div>
                 
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <textarea name="description"  id="description" required="" class="form-control" rows="5">{{ $category->description}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Update</button>
                    <a href="{{route('category.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div>                 
</div>
@endsection
