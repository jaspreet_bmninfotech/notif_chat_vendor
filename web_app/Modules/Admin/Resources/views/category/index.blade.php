@extends ('admin::layouts/base') 
@section('addstyle')
 <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
    @endsection
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
<div class="page-title">                    
    <h2> Categories List <span class="fa fa-arrow-circle-o-down"></span></h2>
    <a href="" id="addcategory" data-toggle="modal" data-target="#myModal" class="btn btn-info pull-right" role="button">ADD CATEGORY</a>
</div> 
        <div id="tabcategory">
            <div class="container">
              <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add Category</h4>
                    </div>
                <form method="post" action="{{ route('category.add') }}" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="modal-body ">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Category Name<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="categoryName" id="categoryName" class="form-control"/>
                               <span class="text-danger">
                                <strong id="categoryName-error"></strong>
                              </span>   
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Description<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                               <textarea name="description" id="description" required="" class="form-control" rows="5"></textarea>
                                <span class="text-danger">
                                <strong id="description-error"></strong>
                              </span> 
                  
                            </div>
                            <span class="help-block">for ex. india as IN</span>
                        </div>
                        <div class="form-group row">
                                
                                <label class="col-md-3 col-xs-12 control-label">Upload icon<em>*</em></label>
                                <div class="col-md-6 col-xs-12">                                            
                                   <span class="button solid btnupload fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Choose Profiles...</span>
                                        <input  type="file" id="catfiles" name="catfile" >
                                        <p>File not exceed 2mb</p>
                                  </span> 
                                </div>
                                <div class="col-md-3">
                                    <div class="catfilesap">
                                        
                                    </div>
                                    
                                </div>
                                <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary ">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>

                    
                  </div>
                </div>
              </div>
            </div>
        </div> 
        <div class="container">
              <!-- Modal for edit-->
              <div class="modal fade" id="editModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Category</h4>
                    </div>
                    <div class="modal-body ">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">NAME</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="getCatName" id="getCatName" required="" value="" class="form-control"/>
                                </div>                                            
                                    <span class="text-danger">
                                        <strong id="getCatName-error"></strong>
                                    </span> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <textarea name="getCatDesc"  id="getCatDesc" required="" class="form-control" rows="5"></textarea>
                                <span class="text-danger">
                                        <strong id="getCatDesc-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  editcategory">Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>                                       
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($category)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>DESCRIPTION</th>
                                <th>CREATED DATE</th>
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                        @foreach($category as $category)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$category->name}}</td>
                                <td>{{$category->description}}</td>
                                <td>{{$category->created_at ? $category->created_at->diffForHumans() : 'no date'}}</td>
                                <td class="text-center"><a href="{{route('category.subcategory',$category->id)}}" id="viewsubcategory" class="btn btn-raised btn-info btn-md">View Subcategory</a> || <a class="edit-modal btn btn-raised btn-primary btn-sm" href="#" data-id="{{$category->id}}" data-name="{{$category->name}}" data-desc="{{$category->description}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> || 
                                <form method="post" id= "delete-form-{{$category->id}}" action="{{route('category.delete',$category->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                               <button onclick='swal({
                                             title: "Are you sure?",
                                              text: "You will not be able to recover this imaginary file!",
                                              type: "warning",
                                              showCancelButton: true,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Yes, delete it!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            }).then(function(isConfirm) {
                                              if (Object.keys(isConfirm)[0] == "dismiss") {
                                                swal("Cancelled", "Your imaginary file is safe :)", "error");
                                              } else {
                                                document.getElementById("delete-form-{{ $category->id}}").submit();
                                              }
                                            });' 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
        <script type="text/javascript" src="{{asset('assets/admin/js/category.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
@endsection