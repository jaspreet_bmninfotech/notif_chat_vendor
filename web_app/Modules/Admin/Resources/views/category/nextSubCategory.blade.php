@extends ('admin::layouts/base') 
	@section('addstyle')
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
	@endsection
	@section('content')
<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span> SubCategories List</h2>
    <a href="" id="addSubCategories" data-toggle="modal" data-target="#mysubModal" class="btn btn-info pull-right" role="button">Add Subcategory</a>
</div> 
        <div id="tabnextsubcategory">
            <div class="container">
              <!-- Modal -->
              <div class="modal fade" id="mysubModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add SubCategory</h4>
                    </div>
                    <div class="modal-body ">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Category Name<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="catName" id="catName" value="{{$cat->name}}" class="form-control" readonly="">   
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">SUBCATEGORY NAME</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="sub_subname" id="sub_subname" class="form-control"/>
                                <span class="text-danger">
                                    <strong id="sub_subname_error"></strong>
                                </span>  
                                 <input type="hidden" name="sub_subId" value="{{$cat->id}}">                                         
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">                                            
                               <textarea name="sub_subdesc" id="sub_subdesc" required="" class="form-control" rows="5"></textarea>
                                <span class="text-danger">
                                <strong id="sub_subdesc_error"></strong>
                              </span> 
                            </div>
                            <span class="help-block">for ex. india as IN</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  saveSubSubCategory">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>  
        <div class="container">
              <!-- Modal for edit-->
              <div class="modal fade" id="editnextsubModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit SubCategory</h4>
                    </div>
                    <div class="modal-body ">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">NAME</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="getnextCatName" id="getnextCatName" required="" value="" class="form-control"/>
                                </div>                                            
                                    <span class="text-danger">
                                        <strong id="getnextCatName-error"></strong>
                                    </span> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <textarea name="getnextCatDesc"  id="getnextCatDesc" required="" class="form-control" rows="5"></textarea>
                                <span class="text-danger">
                                        <strong id="getnextCatDesc-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  editnextsubcategory">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>     
<div class="page-content-wrap">
	@if(Session::has('success'))
		    <div class="alert alert-success">
		      <p style="color:green;">{{Session::get('success')}}</p>
		      @php Session::forget('success'); @endphp
		    </div>
		@endif

    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($subcategory)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>DESCRIPTION</th>
                                <th>CREATED DATE</th>
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                           @foreach($subcategory as $category)
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td>{{$category->description}}</td>
                                <td>{{$category->created_at ? $category->created_at->diffForHumans() : 'no date'}}</td>
                                <td class="text-center"> <a class=" edit-nextsubModal btn btn-raised btn-primary btn-sm" href="#" data-id="{{$category->id}}" data-name="{{$category->name}}" data-desc="{{$category->description}}" data-toggle="modal" data-target="#editnextsubModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> || 
                                <form method="post" id= "delete-form-{{$category->id}}" action="{{route('category.delete',$category->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                               <button onclick='swal({
                                             title: "Are you sure?",
                                              text: "You will not be able to recover this imaginary file!",
                                              type: "warning",
                                              showCancelButton: true,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Yes, delete it!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            }).then(function(isConfirm) {
                                              if (Object.keys(isConfirm)[0] == "dismiss") {
                                                swal("Cancelled", "Your imaginary file is safe :)", "error");
                                              } else {
                                                document.getElementById("delete-form-{{ $category->id}}").submit();
                                              }
                                            });' 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
        <script type="text/javascript" src="{{asset('assets/admin/js/category.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
@endsection