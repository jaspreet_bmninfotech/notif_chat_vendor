@extends ('admin::layouts/base') 
  @section('content')    

<div class="page-content-wrap">
   <div class="col-md-6 col-lg-6">
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
   </div>

   <div class="row">
        <div class="col-md-12">    
            <form class="form-horizontal" action="{{route('category.subcategory.add',$cat->id)}}" method="POST">
              {!! csrf_field() !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>{{$cat->name}}</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">SUBCATEGORY NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="name" name="name" id="name" required="" class="form-control"/>
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <textarea name="description" id="description" required="" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" value="">
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="{{route('category.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div>                    
</div>
@endsection
