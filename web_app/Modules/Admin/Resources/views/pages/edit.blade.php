@extends ('admin::layouts/base') 
    @section('content')


<div class="page-content-wrap">
   <div class="row">

        <div class="col-md-12">
            
            <form class="form-horizontal"  method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            
                 <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Edit Page</strong> </h1>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
            
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">TITLE</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><span class="fa fa-pencil"></span></span>

                        <input type="name" name="title" id="title" required="" value="{{ $pages->title}}" class="form-control"/>
                                
                            </div>                                            
                        </div>
                    </div>
                 
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <textarea name="description"  class="form-control">{{ strip_tags($pages->description) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Slug</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" name="slug" id="slug" value="{{ $pages->slug}}" required="" class="form-control" readonly="" />
                            </div>                                                
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">upload Image</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="btn btn-primary">
                               <span class="input-group-addon"></span>
                                <input type="file" name="imageFile" id="file" class="form-control"/>
                                <input type="hidden" name="attachid" id="attachid" value="{{$pages->image_id}}" />
                            </div>
                            <div id="adminviewimg">
                                <div class="profileimage-inline">
                                            <?php if ($pages->image_id == NULL) {
                                    ?>
                                    <p></p>
                                    <?php
                                }else {
                                    ?>
                                    <div class="profileimagesf">
                                    <i class="fa fa-times" onclick="removePagesimg({{$pages->image_id}},{{$pages->id}},'{{$pages->path}}','{{$pages->name}}')"></i><img src="/{{$pages->path}}" width="100px">
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                               
                            </div>                                              
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Date</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                                <div class="input-group date" id="admineditdatePicker">
                                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                        <input type="text" name="publishDate" required="" class="form-control" value="{{$pages->publishdate}}" />
                                        <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>                                               
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Update</button>
                    <a href="{{route('admin.pages')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div>                 
</div>
@endsection
@section('script') 
<script>
    
document.getElementById('file').onchange = function(e){
    console.log(e);
    if (e.target.files.length < 6) {
         for (var i = 0; i < e.target.files.length;i++) {
            if (e.target.files[i].size <= 2000000) {
          // $.each(e.target.files, function (index, file) {
          var image = e.target.files[i];
    console.log(image);
          loadImage(image, function(img){
           document.querySelector("#adminviewimg").appendChild(img);
           },{
            maxWidth:100,
            maxHeight:100
           }); 
           }else{
              alert('File size should be 2mb');
            } 
         }
     }else{
      alert('Maximum five files are allowed');
    }
    
 };
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
 <script src="/js/pages.js"></script> 
 <script src="/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
@endsection
