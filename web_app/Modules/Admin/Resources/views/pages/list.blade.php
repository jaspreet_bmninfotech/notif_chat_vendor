@extends ('admin::layouts/base') 
 @section('addstyle')
  <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
         @show
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
<div class="page-title">                    
    <h2> Pages List <span class="fa fa-arrow-circle-o-down"></span></h2>
    <a href="{{route('admin.pages.add')}}" class="btn btn-info pull-right" role="button">ADD PAGE</a>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($pages)
                        <table class="table datatable">
                        <thead>
                            <tr id="removesort">
                                <th>ID</th>
                                <th width="200px">TITLE</th>
                                <th width="200px">DESCRIPTION</th>
                                <th width="200px">SLUG</th>
                                <!-- <th>IMAGES</th> -->
                                <th>CREATED DATE</th>
                                <th class="text-center" id="rmsort">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$page->title}}</td>
                                <td>{{ strip_tags($page->description) }}</td>
                                <td>{{$page->slug}}</td>
                               <!--  <?php if ($page->path == NULL) {
                                    ?>
                                <td>Image not Uploaded</td>
                                <?php
                                } else {
                                    ?>
                                <td>{{$page->name}}</td>
                                    <?php
                                } ?> -->
                                <td>{{$page->created_at}}</td>
                                <td class="text-center"><a class="btn btn-raised btn-primary btn-sm" href="{{route('pages.edit',$page->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> ||
                                <form method="post" id= "delete-form-{{$page->id}}" action="{{route('pages.delete',$page->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                                <button onclick='swal({
                                             title: "Are you sure?",
                                              text: "You will not be able to recover this imaginary file!",
                                              type: "warning",
                                              showCancelButton: true,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Yes, delete it!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            }).then(function(isConfirm) {
                                              if (Object.keys(isConfirm)[0] == "dismiss") {
                                                swal("Cancelled", "Your imaginary file is safe :)", "error");
                                              } else {
                                                document.getElementById("delete-form-{{ $page->id}}").submit();
                                              }
                                            });' 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
       $('#removesort').find('#rmsort').removeAttr('class','sorting');
   });
   $(document).on('click','body',function(){
       $('#removesort').find('#rmsort').removeAttr('class','sorting');
   });
</script>
@endsection
