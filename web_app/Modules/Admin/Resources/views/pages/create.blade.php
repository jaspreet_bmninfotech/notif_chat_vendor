@extends ('admin::layouts/base')
@section('addstyle')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css"/>
         @endsection
  @section('content')    
<div class="page-content-wrap">
   <div class="col-md-6 col-lg-6">
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
   </div>

   <div class="row">
        <div class="col-md-12">    
            <form class="form-horizontal" method="POST" enctype="multipart/form-data">
              {!! csrf_field() !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add Pages</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">TITLE</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" name="title" id="title" required="" class="form-control"/>
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <textarea name="description" class="form-control" ></textarea>
                        </div>
                    </div>
                <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">SLUG</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" name="slug" id="slug" required="" class="form-control" readonly="" />
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">UPLOAD IMAGE</label>
                        <div class="col-md-6 col-xs-12">                                            
                            
                                <input type="file" name="imageFile" id="file" class="form-control"/>
                            
                            <div id="adviewimg">
                                
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DATE</label>
                        <div class="col-md-6 col-xs-12"> 
                        <div class="input-group date" id="admindatePicker">
                          <span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
                          <input type="text" name="publishDate" value="" class="form-control">
                        </div>                                                                                     
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right" id="btn">Save</button>
                    <a href="{{route('admin.pages')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div>                    
</div>
@endsection
@section('script')
<script>
    
document.getElementById('file').onchange = function(e){
    console.log(e);
    if (e.target.files.length < 6) {
         for (var i = 0; i < e.target.files.length;i++) {
            if (e.target.files[i].size <= 2000000) {
          // $.each(e.target.files, function (index, file) {
          var image = e.target.files[i];
    console.log(image);
          loadImage(image, function(img){
           document.querySelector("#adviewimg").appendChild(img);
           },{
            maxWidth:100,
            maxHeight:100
           }); 
           }else{
              alert('File size should be 2mb');
            } 
         }
     }else{
      alert('Maximum five files are allowed');
    }
    
 };
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
 <script src="/js/pages.js"></script>
 <script src="/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
@endsection
