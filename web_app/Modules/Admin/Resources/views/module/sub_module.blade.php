
@foreach($sub_module as $sub_key => $sub_val)
<ul>
	<li>
		<h3 class="left-padd"> {{$sub_val->name}} </h3>
		 @include('admin::module.sub_module_form',['module_id'=>$sub_val['id']] )
		@if(!empty($sub_val['sub_module']))
		     @include('admin::module.sub_module',['sub_module'=>$sub_val['sub_module']] )
		@endif
		
	</li>

</ul>
@endforeach
