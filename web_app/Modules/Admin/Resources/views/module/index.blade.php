<?php
use Modules\Admin\Entities\Module;
$module_list = Module::getRouteListArray();
// dump($module);
?>
@extends ('admin::layouts/base') 
  @section('content')    
<style type="text/css">
  .page-content {
    height: 100% ! important;
  }
</style>
<div class="page-content-wrap">
   <div class="col-md-6 col-lg-6">
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
   </div>

   <div class="row">
        <div class="col-md-12">
           
            {!! Form::open(['route'=>'module.store' , 'method'=>'post'])!!}
                    {!! Form::token();!!}

         {{-- {{csrf_field()}} --}}
           
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add Module</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-1 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('name',null,['class'=>"form-control"]) !!}
                               
                            </div>                                               
                        </div><br><br>  
                    </div>
                    <div class="form-group">
                        <label class="col-md-1 col-xs-12 control-label">ROUTE</label>
                        <div class="col-md-6 col-xs-12">                                            
                            {{-- <textarea name="description" id="description" required="" class="form-control" rows="5"></textarea> --}}
                            {!! Form::select('route',$module_list, null, ['class'=>"form-control"]) !!}

                        </div>
                    </div>
                </div>
               
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="{{route('module.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
           {!! Form::close() !!}
        </div>
    </div>  

    <div class="row">
      @foreach($module as $key => $val)
      <div class="col-md-12 col-xs-12">
            <button data-toggle="collapse" class="btn btn-default" style="width:100%;padding:15px 30px 15px 30px;margin:0px 0px 0px 0px;" data-target="#demo{{$val->id}}"><div class="faq-item"><div class="faq-title"><span class="fa fa-angle-down"></span>{{ $val->name }}</div></div></button>
      </div>
        <br clear="both">
          <div id="demo{{$val->id}}" class="collapse sub_mod ">
            <ul>
              <li><h3 class="left-padd"> {{ $val->name }} </h3> 
                 <div  class="{{$val->id}}" >
                    @include('admin::module.sub_module_form',['module_id'=>$val['id']] )
                </div>
                  @if(!empty($val['sub_module']))
                    @include('admin::module.sub_module',['sub_module'=>$val['sub_module']] )
                  @endif
              </li>
            </ul>
          </div>

      @endforeach




     {{--  <ul>
        <li><h3 class="left-padd"> {{ $val->name }} </h3> 
          

           <div  class="{{$val->id}}" >

              @include('admin::module.sub_module_form',['module_id'=>$val['id']] )

          </div>

            @if(!empty($val['sub_module']))
              @include('admin::module.sub_module',['sub_module'=>$val['sub_module']] )
            @endif
        </li>

        
      </ul>
 --}}
  

    </div>                  
</div>
@endsection

