<div class="col-md-12">
           
            {!! Form::open(['route'=>'module.store' , 'method'=>'post'])!!}
                    
                    {!! Form::hidden('parent', $module_id) !!}

         {{-- {{csrf_field()}} --}}
           
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add Sub Module</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-1 col-xs-12 control-label">Name</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('name',null,['class'=>"form-control", 'required'=>'required']) !!} 
                            </div>                                               
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-1 col-xs-12 control-label">Route</label>
                        <div class="col-md-6 col-xs-12">                                            
                            {{-- <textarea name="description" id="description" required="" class="form-control" rows="5"></textarea> --}}
                            {!! Form::select('route',$module_list, null, ['class'=>"form-control" , 'required'=>'required']) !!}

                        </div>
                         <div class="clearfix"></div>
                    </div>
                </div>
               
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="{{route('module.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
           {!! Form::close() !!}
</div>