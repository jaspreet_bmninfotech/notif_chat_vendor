@extends ('admin::layouts/base') 
@section('addstyle')
<link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
	@endsection
  @section('content') 
  	<div class="pull-right">
  		<a href="#" id="addcity" data-toggle="modal" data-target="#cityModal" class="btn btn-info margin_bottom">Add City</a>
	</div>
        <div id="tabcity">
            <div class="container">
              <!-- Modal -->
              <div class="modal fade" id="cityModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add city</h4>
                    </div>
                    <div class="modal-body ">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Add City<em>*</em></label>
                                <div class="col-md-6 col-xs-12">                                            
                                   <input type="text" name="newcity" id="newcity" class="form-control">
                                    <span class="text-danger">
                                        <strong id="newcity-error"></strong>
                                    </span>
                                   <input type="hidden" name="cityid" value="{{$id}}">
                                </div>
                            </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  savecity">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
       </div>
       <div class="container">
              <!-- Modal for edit-->
              <div class="modal fade" id="editcityModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit City</h4>
                    </div>
                     <div class="modal-body ">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Edit city<em>*</em></label>
                                <div class="col-md-6 col-xs-12">                                            
                                   <input type="text" name="editcity" id="editcity" class="form-control">
                                    <span class="text-danger">
                                        <strong id="editcity-error"></strong>
                                    </span>
                                   <input type="hidden" name="cid" value="{{$id}}">
                                </div>
                            </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  editcity">Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
             
 	<table id="example" class="display city" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>cityID</th>
                <th>cityName</th>   
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>cityID</th>
                <th>cityName</th>   
                <th>Action</th>
            </tr>
        </tfoot>
			<tbody>
               	@foreach($data as $val)
                <tr id="city{{$val->id}}">
                    <td>{{$val->id}}</td>
                    <td>{{$val->name}}</td>
                   
                    <td class="text-center">
                    	<a class="city-editmodal btn btn-raised btn-primary btn-sm" href="#" data-id="{{$val->id}}" data-toggle="modal" data-target="#editcityModal" data-name="{{$val->name}}">
                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a> || <a class="deletecity btn btn-danger" data-id="{{$val->id}}"  
                    data-name="{{$val->name}}"  href="javascript:void(0);">
                   <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
               	@endforeach
            </tbody>
    </table>
  @endsection  
  @section('script')
  <script type="text/javascript">
    $('#example').DataTable( {   
    } );
  </script>
	 <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
  @endsection