@extends ('admin::layouts/base') 
@section('addstyle')
<link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
	@endsection
  @section('content') 
  	<div class="pull-right">
  		<a href="#" id="addstate" data-toggle="modal" data-target="#stateModal" class="btn btn-info margin_bottom">Add State</a>
	</div>
        <div id="tabstate">
            <div class="container">
              <!-- Modal -->
              <div class="modal fade" id="stateModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add State</h4>
                    </div>
                    <div class="modal-body ">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Add State<em>*</em></label>
                                <div class="col-md-6 col-xs-12">                                            
                                   <input type="text" name="newstate" id="newstate" class="form-control">
                                    <span class="text-danger">
                                      <strong id="newstate-error"></strong>
                                    </span>
                                   <input type="hidden" name="cid" value="{{$id}}">

                                </div>
                            </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  savestate">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
       </div>
       <div class="container">
              <!-- Modal for edit-->
              <div class="modal fade" id="editstateModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit State</h4>
                    </div>
                     <div class="modal-body ">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Edit State<em>*</em></label>
                                <div class="col-md-6 col-xs-12">                                            
                                   <input type="text" name="editstate" id="editstate" class="form-control">
                                    <span class="text-danger">
                                      <strong id="editstate-error"></strong>
                                    </span>
                                   <input type="hidden" name="cid" value="{{$id}}">
                                </div>
                            </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  editstate">Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
 	<table id="example" class="display state" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>stateID</th>
                <th>stateName</th>   
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>stateID</th>
                <th>stateName</th>   
                <th>Action</th>
            </tr>
        </tfoot>
			<tbody>
               	@foreach($data as $val)
                <tr id="state{{$val->id}}">
                    <td>{{$val->id}}</td>
                    <td>{{$val->name}}</td>
                   
                    <td class="text-center">
                    	<a href="{{route('admin.address.citytable',$val->id)}}" id="poststate" class="btn btn-raised btn-info btn-md" >view city</a>
                    	|| <a class="state-editmodal btn btn-raised btn-primary btn-sm" href="#" data-id="{{$val->id}}" data-toggle="modal" data-target="#editstateModal" data-name="{{$val->name}}">
                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a> || <a class="deletestate btn btn-danger" data-id="{{$val->id}}"
                    data-name="{{$val->name}}"  href="javascript:void(0);">
                   <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
               	@endforeach
            </tbody>
    </table>

  @endsection  
  @section('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	
    $('#example').DataTable( {
        
    } );
	
	</script>
	 <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
  @endsection