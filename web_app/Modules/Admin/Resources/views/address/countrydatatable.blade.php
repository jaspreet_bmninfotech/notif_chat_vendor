@extends ('admin::layouts/base') 
@section('addstyle')
<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
  @endsection
  @section('content') 
    <div class="pull-right">
      <a href="#" id="addcountry" data-toggle="modal" data-target="#myModal" class="btn btn-info margin_bottom">Add Country</a>
  </div>
        <div id="tabcountry">
            <div class="container">
              <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add Country</h4>
                    </div>
                    <div class="modal-body addaddress">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add Country<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="newcountry" id="newcountry" class="form-control"/>
                               <span class="text-danger">
                                <strong id="newcountry-error"></strong>
                              </span>   
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add sortName<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="newsortName" id="newsortName" class="form-control"/>
                                <span class="text-danger">
                                <strong id="newsortName-error"></strong>
                              </span> 
                  
                            </div>
                            <span class="help-block">for ex. india as IN</span>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add phoneCode<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="newphoneCode" id="newphoneCode" class="form-control"/>
                                <span class="text-danger">
                                <strong id="newphoneCode-error"></strong>
                              </span>
                  
                            </div>
                            <span class="help-block">for ex. Australia as 61</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  saveaddress">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="container">
              <!-- Modal for edit-->
              <div class="modal fade" id="editModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Country</h4>
                    </div>
                    <div class="modal-body ">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add Country<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="getcountry" id="getcountry" class="form-control"/>
                                 <span class="text-danger">
                                <strong id="getcountry-error"></strong>
                              </span> 
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add sortName<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="getsortName" id="getsortName" class="form-control"/>
                                 <span class="text-danger">
                                <strong id="getsortName-error"></strong>
                              </span> 
                            </div>
                            <span class="help-block">for ex. india as IN</span>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add phoneCode<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="getphoneCode" id="getphoneCode" class="form-control"/>
                                 <span class="text-danger">
                                  <strong id="getphoneCode-error"></strong>
                              </span> 
                            </div>
                            <span class="help-block">for ex. Australia as 61</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  editcountry">Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
  <table id="example" class="display countrylist" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>countryID</th>
                <th>countryName</th>
                <th>sortName</th>  
                <th>phoneCode</th>  
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>countryID</th>
                <th>countryName</th>
                <th>sortName</th>    
                <th>phoneCode</th>    
                <th>Action</th>
            </tr>
        </tfoot>
      <tbody>
                @foreach($data as $val)
                <tr id="country{{$val->id}}">
                    <td>{{$val->id}}</td>
                    <td>{{$val->name}}</td>
                    <td>{{$val->sortName}}</td>
                    <td>{{$val->phoneCode}}</td>
                    <td class="text-center">
                      <a href="{{route('admin.address.statetable',$val->id)}}" id="viewstate" class="btn btn-raised btn-info btn-md" data-id="{{$val->id}}">view State</a>
                      || <a class="edit-modal btn btn-raised btn-primary btn-sm" href="#" data-id="{{$val->id}}" data-name="{{$val->name}}" data-stname="{{$val->sortName}}" data-code="{{$val->phoneCode}}" data-toggle="modal" data-target="#editModal">
                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>|| <a class="deletecountry btn btn-danger" data-id="{{$val->id}}"
                    data-name="{{$val->name}}"  href="javascript:void(0);">
                   <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
    </table>

  @endsection  
  @section('script')
  <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
  <script type="text/javascript">
 
    $('#example').DataTable( {
        
    } );
  
  </script>
  @endsection