@extends ('admin::layouts/base') 
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success');
              
             @endphp
          </div>
        @endif
<div class="page-title">                    
   @include('admin::role.create')
    <h2> Role List <span class="fa fa-arrow-circle-o-down"></span></h2>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($data)
                        <table class="table datatable">
                        <thead>
                            <tr id="removesort">
                                <th>ID</th>
                                <th>TITLE</th>
                                <th id="rmsorts">Assign Permisson</th>
                                <th id="rmsort">Action</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $key => $val)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$val->name}}</td>
                                <td><a href="{{route('role.permisson',['role_id'=>$val->id ])}}"> Assign Permisson</a></td>
                                <td><a class="edit-modal btn btn-raised btn-primary btn-sm" href="#" data-id="{{$val->id}}" data-name="{{$val->name}}" data-toggle="modal" data-target="#editModal{{$val->id}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a><div class="modal fade" id="editModal{{$val->id}}" role="dialog">
                                    <form method="post" action="{{route('role.update',$val->id)}}">
                                       {{ csrf_field() }}
                                        <div class="modal-dialog">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Edit Role</h4>
                                            </div>
                                            <div class="modal-body ">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-xs-12 control-label">NAME</label>
                                                    <div class="col-md-6 col-xs-12">                                            
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                            <input type="text" name="rolename" id="getCatName" required="" value="{{$val->name}}" class="form-control"/>
                                                        </div>                                            
                                                            <span class="text-danger">
                                                                <strong id="getCatName-error"></strong>
                                                            </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary  editcategory">Update</button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                          </div>
                                        </div>
                                    </form>
                                      </div> || 
                                <a href="{{route('role.delete',$val->id)}}" class="btn btn-raised btn-danger btn-sm" onclick="if(confirm('Are You Sure, You Went To Delete This?')"><i class="fa fa-trash-o" aria-hidden="true"></i>
                               </td>
                            </tr>


                            @endforeach 
                       
                        </tbody>
                        </table>
                       @endif
              <!-- Modal for edit-->
              <div class="modal fade" id="editModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Role</h4>
                    </div>
                    <div class="modal-body ">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">NAME</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="name" id="getCatName" required="" value="" class="form-control"/>
                                </div>                                            
                                    <span class="text-danger">
                                        <strong id="getCatName-error"></strong>
                                    </span> 
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  editcategory">Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('#removesort').find('#rmsort').removeAttr('class','sorting');
        $('#removesort').find('#rmsorts').removeAttr('class','sorting');
    });
    $(document).on('click','body',function(){
        $('#removesort').find('#rmsort').removeAttr('class','sorting');
        $('#removesort').find('#rmsorts').removeAttr('class','sorting');
    });
</script>
@endsection
