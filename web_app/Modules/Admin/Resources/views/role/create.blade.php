<div class="col-md-12">
           
            {!! Form::open(['route'=>'role.store' , 'method'=>'post'])!!}
                    {!! Form::token();!!}
         {{-- {{csrf_field()}} --}}
           
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add Role</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Role Name</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('name',null,['class'=>"form-control", 'required'=>'required']) !!}
                               
                            </div>                                               
                        </div>
                    </div>
                </div>
               
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="{{route('role.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
           {!! Form::close() !!}
</div>