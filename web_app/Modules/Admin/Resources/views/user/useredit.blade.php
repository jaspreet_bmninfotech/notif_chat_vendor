@extends ('admin::layouts/base') 
@section('addstyle')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css"/>
<link rel="stylesheet" href="{{asset('plugin/intl-tel-input-master/build/css/intlTelInput.css')}}">
 @endsection
    @section('content')
    <div class="page-content-wrap">
    	@if(Session::has('success'))
		    <div class="alert alert-success">
		      <p style="color:green;">{{Session::get('success')}}</p>
		      @php Session::forget('success'); @endphp
		    </div>
		@endif
		
	    <div class="row">
	        <div class="form-fields col-md-12">
		<form role="form" method="POST" enctype="multipart/form-data" action="{{route('admin.user.update',['id' => $user->userid])}}">
			{!! csrf_field() !!}
			<div class="panel panel-default">
				<div class="panel-heading">
                    <h1 class="panel-title"><strong>Edit User</strong> </h1>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<div class="col-md-6">
				                        <label>First Name<em>*</em></label>
			                            <input type="text" name="firstName" id="firstName" value="{{$user->firstName}}" class="form-control"/>   
								</div>
								<div class="col-md-6 last">
									 <label>Last Name <em>*</em></label>
									<input type="text" name="lastName" value="{{$user->lastName}}" class="form-control">
									
								</div>
							</div>
							<div class="clearfix"></div>
							<br>
							<div class="form-group">
								<div class="col-md-6">
									<label>Username <em>*</em></label>
									<input type="text" name="username" class="form-control" value="{{$user->username}}" />
								</div>
							
								<div class="col-md-6">
									<label>Email <em>*</em></label>
									<input type="text" name="email" class="form-control"  value="{{$user->email}}" />
								</div>
							</div>
							<div class="clearfix"></div><br>
							@if($user->password=NULL)
							<div class="form-group row">
								<div class="col-md-6">
									<label>Password <em>*</em></label>
									<input type="password" name="password" class="form-control" value="" />
								</div>
							
								<div class="col-md-6">
									<label>Confirm Password <em>*</em></label>
									<input type="password" name="cpassword" class="form-control"  value="" />
								</div>
							</div>
							@endif
							<div class="form-group row">
								<div class="col-md-6">
									<label>Type <em>*</em></label>
										<select name="accounttype" class="form-control">
											<option value="" >-Select-</option>
											<option value="vn" {{ ($user->type=='vn')?'selected':''}}>vendor</option>
											<option value="cn" {{ ($user->type=='cn')?'selected':''}}>client</option>
											<option value="tg" {{ ($user->type=='tg')?'selected':''}}>tourguide</option>
										</select>
								</div>
								<div class="col-md-6">
									<label>Ph no. <em>*</em></label>
										<div class="form-group">
											<div class="col-md-3">
												<input type="tel" id="demo" class="form-control" name="dialCode" value="{{ (empty($user->countryDialcode) ? null :$user->countryDialcode)}}" />
											</div>
											<div class="col-md-9 ">
												<input type="text" class="form-control" name="phone" value="{{$user->phone}}" min="10" maxlength ="12">
											</div>
										</div>
									
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6">
									<label>Date of Birth</label>
									<div class="input-group date" id="datePicker">
										<input type="text" name="dob" value="{{date('Y-m-d', strtotime($user->dob))}}" class="form-control">
										<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
										
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row ">
								<div class="col-md-8">
									<div  id="userprofilefiles" class="user-image">
										<?php
										if ($attachment != NULL) {
											?>
										<img src="/{{$attachment->path}}">
										<?php
										}else{
											?>
										<img src="/assets/admin/images/users/no-image.jpg" >
										<?php

										}
										?>
									</div>
								</div>
								<div class="col-md-12">
									<span class="button solid btnupload fileinput-button">
								        <i class="glyphicon glyphicon-plus"></i>
								        <span>Choose Profiles...</span>
								        <input  type="file" id="file" name="file"  multiple>
									</span>
								</div>
								
							</div>
						</div>
						<div class="clearfix"></div><br>
					<div class="col-md-8">
						<div class="form-group row">
							<div class="col-md-6">
								<div class="form-check row ">	
										<input class="form-check-input" type="radio" name="gender" value="M" {{ ($user->gender=='M') ? 'checked':''}}>
										<label class="form-check-label" for="">Male</label>
								</div>
								<div class="form-check row ">		
										<input class="form-check-input" type="radio" name="gender" value="F" {{ ($user->gender=='F') ? 'checked':''}}>
										<label class="form-check-label" for="">Female</label>
								</div>			
							</div>	
						</div>

					</div>
					</div>
				</div>
			</div>
			<br>
			<br><br>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-8">
							<div class="form-row row">
								<div class="col-md-6">
				                        <label>Fill Address<em>*</em></label>
			                           <input type="text" name="address" class="form-control" value="{{$user->address}}"> 
								</div>
							</div>
							<br>
							<div class="form-group row">
								<div class="col-md-6">
				                        <label>Select Country<em>*</em></label>
			                           <select class=" form-control" name="country" id="country" onchange="getstate(this)">
										  <option value="">-select-</option>
			                           	@foreach($country as $key=>$val)
			                           		<option value="{{$val->id}}" data-id="{{$val->id}}" {{ ($user->country_id==$val->id)?'selected':''}}>{{$val->name}}</option>
										  @endforeach
										</select>  
								</div>
								<div class="col-md-6 last">
									 <label>Select State <em>*</em></label>
										<select class="form-control" name="state" id="state" onchange="getcity(this)">
										  <option value="">-select-</option>
										</select>
										<input type="hidden" name="stateedit" id ="stateedit" value="{{$user->state_id}}">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6">
				                        <label>Select City<em>*</em></label>
			                           <select class=" form-control" name="city" id="city">
										  <option value="">-select-</option>
										</select>  
										<input type="hidden" name="cityedit" id ="cityedit" value="{{ $user->city_id }}">
								</div>
								<div class="col-md-6 last">
									 <label>Postal Code<em>*</em></label>
									<input type="text" name="postalCode" class="form-control" value="{{ $user->postalCode }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="align-center" style="text-align: center">
				<button type="submit" class="btn btn-primary">Update</button>
			</div>
			<br>
			<br>

	<!-- Profile end-->
		</form>
	    </div> 

	</div>
</div>
    @endsection

    @section('script')
    <script type="text/javascript" src="{{asset('assets/admin/js/user.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
     <script src="{{ asset('plugin/intl-tel-input-master/build/js/intlTelInput.js') }}"></script>
	<script type="text/javascript">
	
		$("#demo").intlTelInput();
		var code=$('.country-list  .country').find('.dial-code').html();
		$('input[type=tel]').val(code);
		$("#demo").intlTelInput("loadUtils", "lib/libphonenumber/build/utils.js");
		$("#mobile-number").intlTelInput();
		// console.log($("li[data-dial-code = $('.selected-flag').attr('title').split(':')[1] ]").html());
				$('input[name = dialCode]').val($('.selected-flag').attr('title').split(':')[1]);
				$('.country-list li').click(function(){
					$('input[name = dialCode]').val($(this).find('.dial-code').html());
				});
</script>
    @endsection