@extends ('admin::layouts/base') 
@section('addstyle')
 <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
@endsection
  @section('content')

<div class="page-title">                    
    <h2>Admin User List <span class="fa fa-arrow-circle-o-down"></span></h2> 
    <div class="row">
        <div class="col-md-6 col-lg-6">
            @if(session('success'))
            <div class="alert alert-success">
            {{session('success')}}
            </div>
            @endif
        </div>
    </div>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-heading">
                        <h1 class="panel-title"><strong>User List</strong> </h1>
                    <a href="{{route('admin.register')}}" class="btn btn-info btn-md pull-right">Add Admin User</a>
                    </div>
                    <div class="panel-body">
                        @if($user)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>Role</th>
                                <th>JOIN</th>
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($user as $user)
                            <tr>
                                <td>{{$i++}}</td>  
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                @if ($user['role']['name'] == null)
                                <td>No roll</td>
                                @else
                                <td>{{$user['role']['name']}}</td>
                                @endif
                                <td>{{$user->created_at ? $user->created_at->diffForHumans() : 'no date'}}</td>
                                <td class="text-center"><a class="edit-modal btn btn-raised btn-primary btn-sm" href="#" data-id="{{$user->id}}" data-name="{{$user->name}}" data-toggle="modal" data-target="#editModal{{$user->id}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a><div class="modal fade" id="editModal{{$user->id}}" role="dialog">
                                    <form method="post" action="{{route('adminuser.edit',$user->id)}}">
                                    {{ csrf_field() }}
                                        <div class="modal-dialog">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Edit Admin User</h4>
                                            </div>
                                            <div class="modal-body ">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-xs-12 control-label">NAME</label>
                                                    <div class="col-md-6 col-xs-12">                                            
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                            <input type="text" name="rolename" id="getCatName" required="" value="{{$user->username}}" class="form-control"/>
                                                        </div>                                            
                                                            <span class="text-danger">
                                                                <strong id="getCatName-error"></strong>
                                                            </span> 
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-xs-12 control-label">EMAIL</label>
                                                    <div class="col-md-6 col-xs-12">                                            
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                            <input type="emailt" name="roleemail" id="getCatName" required="" value="{{$user->email}}" class="form-control"/>
                                                        </div>                                            
                                                            <span class="text-danger">
                                                                <strong id="getCatName-error"></strong>
                                                            </span> 
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary editcategory">Update</button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                          </div>
                                        </div>
                                </form>
                                <button onclick='swal({
                                             title: "Are you sure?",
                                              text: "You will not be able to recover this imaginary file!",
                                              type: "warning",
                                              showCancelButton: true,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Yes, delete it!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            }).then(function(isConfirm) {
                                              if (Object.keys(isConfirm)[0] == "dismiss") {
                                                swal("Cancelled", "Your imaginary file is safe :)", "error");
                                              } else {
                                                document.getElementById("delete-form-{{ $user->id}}").submit();
                                              }
                                            });' 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                               
                            </tr>
                            @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
  @endsection