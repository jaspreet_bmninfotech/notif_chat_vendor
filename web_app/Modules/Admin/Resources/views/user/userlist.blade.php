@extends ('admin::layouts/base') 
	@section('addstyle')
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
	@endsection
	@section('content')
<div class="page-content-wrap">
    <div class="col-md-12">
    	@if(Session::has('success'))
    		    <div class="alert alert-success">
    		      <p style="color:green;">{{Session::get('success')}}</p>
    		      @php Session::forget('success'); @endphp
    		    </div>
    	@endif
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                	<div class="panel-heading">
	                    <h1 class="panel-title"><strong>Users List</strong> </h1>
	                        <a href="{{route('admin.adduser')}}" class="btn btn-info btn-md pull-right">Add User</a>              
                	</div>
                    <div class="panel-body">
                        @if($user)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>USERNAME</th>
                                <th>EMAIL</th>
                                <th>TYPE</th>
                                <th>SUCCESS RATE</th>
                                <th>JOIN</th>
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($user as $user)
                            <tr>
                                <td>{{$i++}}</td>  
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->type}}</td>
                                <td>{{$user->success_rate}}</td>
                                <td>{{$user->created_at ? $user->created_at->diffForHumans() : 'no date'}}</td>
                                <td class="text-center">
                                	<a class="btn btn-raised btn-primary btn-sm" href="{{route('admin.user.edit',$user->id)}}" data-id="{{$user->id}}">
                                       <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> || <form method="post" id= "delete-form-{{$user->id}}" action="{{route('admin.user.delete',$user->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                               <button onclick='swal({
                                             title: "Are you sure?",
                                              text: "You will not be able to recover this imaginary file!",
                                              type: "warning",
                                              showCancelButton: true,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Yes, delete it!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            }).then(function(isConfirm) {
                                              if (Object.keys(isConfirm)[0] == "dismiss") {
                                                swal("Cancelled", "Your imaginary file is safe :)", "error");
                                              } else {
                                                document.getElementById("delete-form-{{ $user->id}}").submit();
                                              }
                                            });' 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                            @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
	@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
@endsection