@extends ('admin::layouts/base') 
    @section('content')


<div class="page-content-wrap">
   <div class="row">

        <div class="col-md-12">
            
            <form class="form-horizontal"  method="post">
            {{ csrf_field() }}
            
                 <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Edit Vendor</strong> </h1>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
            
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                          
                            <div class="input-group">
                               <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                               <input type="username" name="username" id="username" required="" value="{{ $user->username}}" class="form-control"/>
                               @if ($errors->has('username'))
                                <span class="help-block">
                                   <strong>{{ $errors->first('username') }}</strong>
                                </span>
                               @endif
                            </div>                                            
                            {{-- <span class="help-block">This is sample of username field</span> --}}
                        </div>
                    </div>
                 
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">EMAIL</label>
                        <div class="col-md-6 col-xs-12">                          
                            <div class="input-group">
                               <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                               <input type="email" name="email" id="email" required="" value="{{ $user->email}}" class="form-control"/>
                               @if ($errors->has('email'))
                                <span class="help-block">
                                   <strong>{{ $errors->first('email') }}</strong>
                                </span>
                               @endif
                            </div>                                            
                             {{-- <span class="help-block">This is sample of email field</span> --}}
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Update</button>

                    <a href="{{route('user.vendor.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div>                 
</div>
@endsection
