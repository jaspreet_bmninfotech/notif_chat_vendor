@extends ('admin::layouts/base') 
    @section('content')
<div class="page-content-wrap">
   <div class="row">
        <div class="col-md-12"> 
            <form class="form-horizontal"  method="post">
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title"><strong>Edit Client</strong> </h1>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body"> 
                   <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">First Name</label>
                        <div class="col-md-6 col-xs-12">                          
                            <input type="firstName" name="firstName" id="firstName" required="" value="{{ $user-> firstName }}" class="form-control"/>
                           @if ($errors->has('firstName'))
                            <span class="help-block">
                               <strong>{{ $errors->first('firstName') }}</strong>
                            </span>
                           @endif                                           
                        </div>    
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Name</label>
                        <div class="col-md-6 col-xs-12">                          
                           <input type="lastName" name="lastName" id="lastName" required="" value="{{ $user->lastName}}" class="form-control"/>
                           @if ($errors->has('lastName'))
                            <span class="help-block">
                               <strong>{{ $errors->first('lastName') }}</strong>
                            </span>
                           @endif                                           
                        </div>
                    </div>                                  
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">User Name</label>
                        <div class="col-md-6 col-xs-12">                          
                           <input type="username" name="username" id="username" value="{{ $user->username}}"  required="" class="form-control"/>
                           @if ($errors->has('username'))
                            <span class="help-block">
                               <strong>{{ $errors->first('username') }}</strong>
                            </span>
                           @endif                                          
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Email</label>
                        <div class="col-md-6 col-xs-12">                          
                           <input type="email" name="email" id="email" required="" value="{{ $user->email}}" class="form-control"/>
                           @if ($errors->has('email'))
                            <span class="help-block">
                               <strong>{{ $errors->first('email') }}</strong>
                            </span>
                           @endif                                           
                        </div>
                    </div>
                   {{--  <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Password</label>
                        <div class="col-md-6 col-xs-12">                          
                           <input type="password" name="password" id="password" required="" value="{{ $user->password }}" class="form-control"/>
                           @if ($errors->has('password'))
                            <span class="help-block">
                               <strong>{{ $errors->first('password') }}</strong>
                            </span>
                           @endif                                          
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
                        <div class="col-md-6 col-xs-12">                          
                           <input type="password" name="password" id=" confirmpassword" required="" value="{{ $user->confirmpassword}}" class="form-control"/>
                           @if ($errors->has('confirmpassword'))
                            <span class="help-block">
                               <strong>{{ $errors->first('confirmpassword')}}</strong>
                            </span>
                           @endif                                            
                            
                        </div>
                   </div> --}}
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Update</button>

                    <a href="{{route('user.client.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div>                 
</div>
@endsection
