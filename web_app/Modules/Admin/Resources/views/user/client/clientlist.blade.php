@extends ('admin::layouts/base') 
@section('addstyle')
  <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.css" type="text/css" />
@show
  @section('content')

<div class="page-title">                    
    <h2>Client List <span class="fa fa-arrow-circle-o-down"></span></h2> 
    <div class="row">
        <div class="col-md-6 col-lg-6">
            @if(session('success'))
            <div class="alert alert-success">
            {{session('success')}}
            </div>
            @endif
        </div>
    <h2><span class="fa fa-arrow-circle-o-left"></span>Client List</h2> 
    <div class="row">
    </div>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($user)
                        <table class="table datatable">
                        <thead>
                            <tr id="removesort">
                                <th>ID</th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>JOIN</th>
                                <th id="rmsort" class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($user as $user)
                            <tr>
                                <td>{{$i++}}</td>  
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->created_at ? $user->created_at->diffForHumans() : 'no date'}}</td>
                                <td class="text-center"><a class="btn btn-raised btn-primary btn-sm" href="{{route('user.client.edit',$user->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> || <form method="post" id= "delete-form-{{$user->id}}" action="{{route('user.client.delete',$user->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                                <button onclick='swal({
                                             title: "Are you sure?",
                                              text: "You will not be able to recover this imaginary file!",
                                              type: "warning",
                                              showCancelButton: true,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Yes, delete it!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            }).then(function(isConfirm) {
                                              if (Object.keys(isConfirm)[0] == "dismiss") {
                                                swal("Cancelled", "Your imaginary file is safe :)", "error");
                                              } else {
                                                document.getElementById("delete-form-{{ $user->id}}").submit();
                                              }
                                            });' 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                            @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('#removesort').find('#rmsort').removeAttr('class','sorting');
    });
    $(document).on('click','body',function(){
        $('#removesort').find('#rmsort').removeAttr('class','sorting');
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.1/sweetalert2.min.js"></script>
@endsection