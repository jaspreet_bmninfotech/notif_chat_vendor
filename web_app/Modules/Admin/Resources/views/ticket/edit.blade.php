@extends('admin::layouts.base')


@section('content')


    <div class="well bs-component">
        @include('admin::ticket.admin_ticket_header') <div class="container">


    <div class="row">
        {{-- {{dd($ticket)}} --}}
    <form action="{{route('tickets.update')}}" method="post">
              {!! csrf_field() !!}
           
        <div class="col-md-12">
              {!! Form::hidden('tid',$ticket['id']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Edit Ticket</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('subject',$ticket['subject'], ['class'=>"form-control"])!!}
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::textarea('content',$ticket['content'], ['class'=>"form-control"])!!}
                       </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Priority</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::select('priority_id', $priority, $ticket['priority_id']  , ['class'=>"form-control"]) !!}
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Agents</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::select('agent_id', $agent , $ticket['agent_id']  , ['class'=>"form-control"]) !!}
                      </div>
                    </div>   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Categories</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::select('category_id', $category , $ticket['category_id']  , ['id'=>'category', 'class'=>"form-control"]) !!}
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::select('status_id',$status_list , $ticket['status_id']  , ['class'=>"form-control"]) !!}
                      </div>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" value="">
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="" class="btn btn-primary">Back</a>
                </div>
            </div>
            
        </div>
    </form>
    </div> 
</div>
<script>
    $(document).on('change','#category', function(){
        alert('12');
    });

</script>
@endsection

