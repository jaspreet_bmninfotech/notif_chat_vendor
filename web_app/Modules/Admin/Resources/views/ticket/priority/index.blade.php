@extends('admin::layouts.base')

@section('content')

<div class="panel panel-default">
   @include('admin::ticket.admin_ticket_header')

    <div class="panel-heading">
        <h2>Priority List
        </h2>

        <h2> <a href="{{route('ticket.priority.create')}}">Create New Priority </a></h2>
    </div>

    <div class="panel-body">
        <div id="message"></div>

        <table class="table table-condensed table-stripe ddt-responsive" class="table datatabl">
    <thead>
        <tr>
            <td>id</td>
            <td>Name</td>
            <td>status</td>
            
        </tr>
    </thead>
    <tbody>
    	{{-- {{dd($data)}} --}}

    	@foreach($data as $key =>  $val)
    	<tr>
    		<td> {{$val->id}}</td>
    		<td> {{$val->name}} </td>
    		<td> - </td>
    		<td> </td>
    	</tr>
    	@endforeach
    </tbody>
</table>

        {{-- @include('ticketit::tickets.partials.datatable') --}}
    </div>

</div>
@endsection
