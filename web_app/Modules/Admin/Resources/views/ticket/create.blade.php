@extends('admin::layouts.base')


@section('content')

    <div class="well bs-component">
           {{-- @include('admin::ticket.admin_ticket_header') --}}
        <div class="container">
              <div class="row">
        <div class="col-md-12">    
           {!! Form::open([
                        'route'=>'tickets.store',
                        'method' => 'POST',
                        'class' => 'form-horizontal'
                        ]) !!}
              {!! csrf_field() !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add Ticket</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('subject',null, ['class'=>"form-control"])!!}
                                {{-- <input type="name" name="name" id="name" required="" class="form-control"/> --}}
                            </div>                                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">DESCRIPTION</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::textarea('content',null, ['class'=>"form-control"])!!}
                       </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">PRIORITY</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::select('priority_id',[null=>'Please Select'] + $priority, null  , ['class'=>"form-control"]) !!}
                      </div>
                    </div>
                    {{-- <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">AGENTS</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::select('agent_id',[null=>'Please Select'] + $agent , null  , ['class'=>"form-control"]) !!}
                      </div>
                    </div>    --}}
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">CATEGORIES</label>
                        <div class="col-md-6 col-xs-12">  
                            {!! Form::select('category_id',[null=>'Please Select'] + $category , null  , ['class'=>"form-control"]) !!}
                      </div>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" value="">
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="{{url('admin/ticket')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div> 
            </div>
            
           
        {!! Form::close() !!}
    </div>
@endsection

