@extends('admin::layouts.base')

@section('content')

  @include('admin::ticket.admin_ticket_header')

<div class="panel-body">
                                   
    <div class="form-group">
       @if($data->status_id ==2)
        <a href="{{route('ticket.reopen',['id'=>$data->id])}}" class="btn btn-success">Reopen-Ticket</a>
		@else
        <a href="{{route('ticket.complete',['id'=>$data->id])}}" class="btn btn-success">Mark as Complete</a>
       @endif
        <a href="{{route('ticket.edit',['id'=>$data->id])}}" class="btn btn-info">Edit</a>
        <a href="{{route('ticket.delete',['id'=>$data->id])}}" class="btn btn-danger">Delete</a>
                                                                       
    </div>

 

              <div class="col-md-12">
					{{--  <div class="panel panel-colorful">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{$data->subject}}</h3><br>
                                    <div class="form-group">
									       @if($data->status_id ==2)
									        <a href="{{route('ticket.reopen',['id'=>$data->id])}}" class="btn btn-success">Reopen-Ticket</a>
											@else
									        <a href="{{route('ticket.complete',['id'=>$data->id])}}" class="btn btn-success">Mark as Complete</a>
									       @endif
									        <a href="{{route('ticket.edit',['id'=>$data->id])}}" class="btn btn-info">Edit</a>
									        <a href="{{route('ticket.delete',['id'=>$data->id])}}" class="btn btn-danger">Delete</a>
									                                                                       
									    </div>
                                </div>
                                <div class="panel-body">
                                    <p>Fusce imperdiet neque at lectus faucibus, eu dapibus nibh imperdiet. Cras porttitor magna ut justo iaculis feugiat. Aliquam semper in dolor nec mattis.</p>
                                    <p>Duis varius arcu in quam laoreet scelerisque. Aenean porta mi et massa congue, sit amet eleifend dui dignissim. Ut venenatis, tortor ac egestas fringilla, nisi mauris rhoncus nibh, facilisis lacinia mi ligula a est.</p>
                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-default">Back</button>
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>                            
                            </div> --}}




                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        <h3><span class="fa fa-info-circle"></span> Ticket Info</h3>
                    </div>
                    <div class="panel-body form-group-separated">
							<div class="form-group">
	                            <label class="col-md-4 col-xs-5 control-label">Subject</label>
	                            <div class="col-md-8 col-xs-7 line-height-30">{{$data->subject}}</div>
	                        </div>
	                        	<div class="form-group">
	                            <label class="col-md-4 col-xs-5 control-label">Content</label>
	                            <div class="col-md-8 col-xs-7 line-height-30">{{$data->content}}</div>
	                        </div>
	                        	<div class="form-group">
	                            <label class="col-md-4 col-xs-5 control-label">Preority Name</label>
	                            <div class="col-md-8 col-xs-7 line-height-30">{{$data->priority->name}}</div>
	                        </div>
	                        	<div class="form-group">
	                            <label class="col-md-4 col-xs-5 control-label">Status Name</label>
	                            <div class="col-md-8 col-xs-7 line-height-30">{{$data->status_name->name}}</div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 col-xs-5 control-label">Category Name</label>
	                            <div class="col-md-8 col-xs-7 line-height-30">{{$data->category_name->name}}</div>
	                        </div>
	                       
					 </div>
				</div>
				</div>
				<h3> Comments </h3>
<!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">
                @foreach($comment as $commentKey => $commentVal )
			 <div class="row">
				<div class="col-md-12">
				    <div class="panel panel-default ">
				            <h6 style="padding:5px;">{{ $commentVal['comment_user']['username'] }} </h6>
				        <div class="panel-body">
				            <h5 style="background:#fcf8e3;padding:10px;border-radius: 5px;	">{{ $commentVal->content }}</h5>
				        </div>
				    </div>
				</div>
			   
			</div>
			@endforeach
        </div>

{!! Form::open(['route'=>'tickets.comment'    ]) !!}

<input type="hidden" name="ticket_id" value="{{$data->id}}">
<input type="hidden" name="user_id" value="{{$data->user_id}}">
<textarea name="content" id="" cols="30" rows="10" class="form-control" placeholder="text here"></textarea><br>
{!! Form::submit('SUBMIT',['class'=>'btn btn-info']) !!}<br>

{!! Form::close() !!}
@endsection