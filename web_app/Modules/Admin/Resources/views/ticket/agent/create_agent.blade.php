@extends('admin::layouts.base')

@section('content')
<div class="panel panel-default">
   @include('admin::ticket.admin_ticket_header')

    <div class="panel-heading">
        <h2>Add Agent 
        </h2>

      
    </div>

    <div class="panel-body">
        <div id="message"></div>

        <table class="table table-condensed table-stripe ddt-responsive" class="table datatabl">
    
    <tbody>

{!! Form::open(['route'=>'ticket.agent.create' ])  !!}
    	@foreach($users as $key => $val)
    	<tr> <td>
	    		@if($val['role_id'] ==2)
	    			<input type="Checkbox" checked="checked" name="user_id[]" value="{{$val['id']}} " >
	    		@else
	    			<input type="Checkbox" name="user_id[]" value="{{$val['id']}} " >
	    		@endif
    		</td>
    		 <td>{{$val['username']}}</td>
    	</tr>
    	@endforeach
    	{!! Form::submit('save') !!} 
{!! Form::close() !!}
    </tbody>

</table>
</div>
</div>

@endsection
