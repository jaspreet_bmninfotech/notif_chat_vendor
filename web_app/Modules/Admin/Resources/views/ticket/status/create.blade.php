@extends('admin::layouts.base')


@section('content')


   @include('admin::ticket.admin_ticket_header')

    <div class="well bs-component">
        




        <div class="container">
              <div class="row">
        <div class="col-md-12">    
           {!! Form::open([
                        'route'=>'ticket.status.store',
                        'method' => 'POST',
                        'class' => 'form-horizontal'
                        ]) !!}
              {!! csrf_field() !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>ADD  Status</strong></h3>
                    <ul class="panel-controls"> 
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                   
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">NAME</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('name',null, ['class'=>"form-control"])!!}
                                {{-- <input type="name" name="name" id="name" required="" class="form-control"/> --}}
                            </div>                                               
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Color</label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                               {!! Form::text('color',null, ['class'=>"form-control"])!!}
                            </div>                                               
                        </div>
                    </div>
                </div>
                
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Submit</button>
                    <a href="{{route('ticket.status.index')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            </form>
        </div>
    </div> 
            </div>
            
           
        {!! Form::close() !!}
    </div>
@endsection

