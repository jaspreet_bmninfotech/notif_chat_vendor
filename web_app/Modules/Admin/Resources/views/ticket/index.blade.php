@extends('admin::layouts.base')

@section('content')

<div class="panel panel-default">

   @include('admin::ticket.admin_ticket_header')
    <div class="page-title">
        <h2>My ticket </h2>                    
          <a href="{{route('ticket.create')}}" class="btn btn-info pull-right" role="button"> Create Ticket</a>
    </div>
    <div class="panel-body">
        <div id="message"></div>

        <table class="table table-condensed table-stripe ddt-responsive" class="ticketit-table" id="ticket">
    <thead>
        <tr>
            <td>Id</td>
            <td>Subject</td>
            <td>Status</td>
            <td>Agent</td>
            <td>Priority</td>
            <td>Owner</td>
            <td>Category</td>
            <td>Action</td>
            
        </tr>
    </thead>
    <tbody>
    	

    	@foreach($data as $key =>  $val)
    	<tr>
    		<td> {{$key +1}}</td>
    		<td> <a href="{{route('ticket.detail',['id'=>$val->id])}}">{{$val->subject}} </a></td>
    		<td>{{$val['status_name']['name']}}</td>
            <td>{{agent_user_name($val->agent_id) }}</td>

             @if(!empty($val['priority']))
            <td>{{$val['priority']['name']}}</td>
              @else
              <td> </td>
            @endif

            <td> {{agent_user_name($val->user_id) }} </td>
            <td> {{@$val['category_name']['name']}} </td>
            <td><a class="edit-modal btn btn-raised btn-primary btn-sm" href="{{route('ticket.edit',['id'=>$val->id])}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a> || 
            <a href="{{route('ticket.delete',['id'=>$val->id])}}" class="btn btn-raised btn-danger btn-sm" onclick="if(confirm('Are You Sure, You Went To Delete This?')"><i class="fa fa-trash-o" aria-hidden="true"></i>
           </td>
    		
    	</tr>
    	@endforeach
    </tbody>
</table>

        {{-- @include('ticketit::tickets.partials.datatable') --}}
    </div>

</div>
@endsection
@section('script')
<script type="text/javascript">
    $('#ticket').DataTable( {     
    });
  </script>
@endsection