<?php 
namespace Modules\Admin\SettingRepositories\Home;
use Modules\Admin\SettingRepositories\Home\HomeInterface as HomeInterface;
use Illuminate\Database\Eloquent\Model;
use App\Setting;
use App\Category;

class HomeRepository implements HomeInterface
{
    // model property on class instances
    public $setting;
    public $category;

    // Constructor to bind model to repo
    public function __construct(Setting $setting , Category $category)
    {
        $this->setting = $setting;
        $this->category = $category;
    }

    // Get all instances of model
    public function all()
    {
    	$cat= $this->category::where('parent_id',NULL)->get();
        return $cat;
    }

    // create a new record in the database
    public function create(array $data)
    {

    	$haskey= $data['setting'];

    	$categoryArray = explode('-',$data['homecategory']);
        $arraycategory = array_filter($categoryArray);
       
    	$serializrArray = serialize([$this->setting::home_category => $arraycategory] );
        // dd($serializrArray);
        $hasexist = $this->setting::where('key',$haskey)->first();
        // dd($haskey);
    	if($hasexist != NULL)
    	{
    	 $this->setting->where('key',$haskey)->update(['key' => $haskey , 'value' => $serializrArray]);
    	return true;
    	}
    	else{
    		$this->setting->create(['key' => $haskey , 'value' => $serializrArray]);
    		return true;
		}
        // return $this->setting->create($data);
    }
    // update record in the database
    public function update(array $data)
    {
    	
        $record = $this->find($id);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->setting->destroy($id);
    }
    public function deletecategory($id)
    {
    	$catget= $this->setting::where('key','home')->first();
        if($catget)
         {
	    	$unserial = unserialize($catget->value);
	    	if(in_array($id, $unserial['home_category'])){
                $keyToRemove = array_search($id, $unserial['home_category']);

                unset($unserial['home_category'][$keyToRemove]);
            }
            // dd($unserial);
            $newval = $unserial['home_category'];
    		$serializrArray = serialize([$this->setting::home_category => $newval] );
    		$result= $this->setting->where('key','home')->update([ 'value' => $serializrArray]);
			return $result;
            

            // $rcat=array();
            // $newval=array();
            
            // foreach($unserial['home_category'] as $k=>$v)
            // {    
            //  if( $id != $v){
            //      $newval[] = $v;
            //  }
        }
           
    }
    // show the record with the given id
    public function show()
    {
    	 $catget= $this->setting::where('key','home')->first();

         if($catget)
         {
            $unserial = unserialize($catget->value);
           
	    	$result = array();
           
	    	foreach($unserial['home_category'] as $k=>$v)
	    	{
	    	 	$result[] = Category::where('id',$v)->select('id','name')->first();
	    	} 
    	 	return $result;
    	 }
    }

    // Get the associated model
    public function getModel()
    {
        return $this->setting;
    }

    // Set the associated model
    public function setModel($setting)
    {
        $this->setting = $setting;
        return $this;
    }
    // Eager load database relationships
    public function with($relations)
    {
        return $this->setting->with($relations);
    }
}