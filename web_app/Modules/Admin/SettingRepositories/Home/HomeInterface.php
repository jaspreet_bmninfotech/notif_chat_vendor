<?php 
namespace Modules\Admin\SettingRepositories\Home;

interface HomeInterface
{
    public function all();

    public function create(array $data);

    public function update(array $data);

    public function delete($id);

    public function deletecategory($id);

    public function show();
}