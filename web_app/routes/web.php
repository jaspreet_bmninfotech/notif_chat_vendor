<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([ 'middleware' => ['auth'] ], function () {


Route::get('dispute/{type?}', 'Dispute\DisputeController@index')->name('dispute.index');
Route::get('dispute-apply', 'Dispute\DisputeController@create')->name('dispute.create');
Route::post('dispute-store', 'Dispute\DisputeController@store')->name('dispute.store');
Route::get('dispute-detail/{id}', 'Dispute\DisputeController@detail')->name('dispute.detail');

});

    Route::get('searchtourguides/', [
    'as' => 'searchtourguide', 'uses' => 'searchController@searchtourguides'
        ]);
    Route::post('searchtourguides/', [
    'as' => 'searchtourguide', 'uses' => 'searchController@searchtourguidesfilter'
        ]);
    Route::get('getvendors/{page}', [
    'as' => 'getvendors', 'uses' => 'searchController@getvendors'
        ]);
    Route::get('/tourguidefilter/{name}', [
    'as' => 'tourguidefilter', 'uses' => 'searchController@tourguidefilter'
        ]);
Route::get('/', 'HomeController@index');
Route::get('/category/{id}',['as'=>'home.category','uses'=>'HomeController@homesubcat']);
Route::get('/freelancer-categories','HomeController@freelancercategories');
Route::get('index/category', 'HomeController@category');

Auth::routes();

Route::post('/login_check',['as'=>'login','uses'=>'UserController@login']);
Route::get('/logout',function(){
    Auth::logout();
    return Redirect::to('/');
})->middleware("auth");

Route::get('job-post-form', 'JobpostingformController@create');
Route::post('job-post-form', ['as'=>'job-post', 'uses'=>'JobpostingformController@submit']);

Route::match(['get', 'post'], 'search-job', ['as' => 'search-job', 'uses' => 'JobController@search']);

Route::match(['get', 'post'], 'switch-accounttype', ['as' => 'switch-accounttype', 'uses' => 'UserController@swtich']);

Route::get('/jobvendors', 'Dispute\DisputeController@jobvendors');
Route::get('/getsubcategories/{id}', 'CategoryController@create');
Route::get('/getsubSubcategories/{id}', 'CategoryController@createsubcat');
Route::get('/getstate/{id}', 'AddressController@state');
Route::get('/getcity/{id}', 'AddressController@city');
Route::get('/getbussinessdata', [
           'as' => 'vendor.businessdata', 'uses' => 'vendor\BusinessInfoController@getbussiness'
        ]);
Route::get('/getprofileaddress','vendor\ProfileController@getprofileaddress');
Route::get('/getclientprofile','client\ProfileController@getprofileaddress');
Route::get('/gettourguideprofile','tourguide\ProfileController@gettourguideprofile');


 // Route::get('/stripe/webhook',['as'=>'stripe.webhook','uses'=>'WebhookController@webhook']);
Route::post(
    'stripe/webhook',
    '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook'
);

        Route::post('client/cprofileuploads', [
               'as' => 'client.cprofileuploads', 'uses' => 'client\ProfileController@uploadcprofiles'
            ]);
Route::group(['prefix' => 'client', 'middleware' => ['auth', 'client','subscription']], function () { 
    Route::get('/notifications' , ['as' => 'client.all.notification' , 'uses' => 'NotificationController@getAllNotification']);

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [
           'as' => 'client.profile', 'uses' => 'client\ProfileController@select'
        ]); 
        Route::post('/update', [
           'as' => 'client.profile.update', 'uses' => 'client\ProfileController@update'
        ]);

        Route::match(['get', 'post'],'/mailingaddress', [
            'as' => 'client.profile.mailing', 'uses' => 'client\ProfileController@mailing'
        ]);
        Route::get('/changePassword', [
           'as' => 'client.profile.changePassword', 'uses' => 'changePasswordController@selectchangePassword'
        ]);
        Route::post('/changePassword', [
           'as' => 'client.profile.changePassword', 'uses' => 'changePasswordController@createchangePassword'
        ]);
         
    });
      
    Route::group(['prefix' => 'contract'], function () {
        Route::get('/', [
           'as' => 'client.contract', 'uses' => 'client\ContractController@list'
        ]);
        Route::get('/add', [
           'as' => 'client.contract.add', 'uses' => 'client\ContractController@add'
        ]);
        Route::get('/{id}/detail', [
           'as' => 'client.contract.detail', 'uses' => 'client\ContractController@detail'
        ]);
        Route::get('/{id}/{team_id}/detail', [
           'as' => 'client.contract.teamdetail', 'uses' => 'client\ContractController@detail'
        ]);
        Route::get('/{id}/closed', [
           'as' => 'client.contract.closed', 'uses' => 'client\ContractController@endContract'
        ]);
        Route::post('/{id}/closed', [
           'as' => 'client.contract.closed', 'uses' => 'client\ContractController@contractUpdate'
        ]);
        Route::get('/edit', [
            'as' => 'client.contract.edit', 'uses' => 'client\ContractController@edit'
         ]);
        Route::delete('/delete', [
           'as' => 'client.contract.delete', 'uses' => 'client\ContractController@delete'
        ]);
        Route::post('/fund/funded', [
          'as' => 'fund.funded', 'uses' => 'client\ContractController@fundFunded'
       ]);
        Route::get('/{id}/contractlist',['as'=>'client.contract.contractlist','uses'=>'client\ContractController@contractlist']);
    });
    Route::group(['prefix' => 'freelancer'], function () {
        Route::get('/', [
           'as' => 'client.freelancer', 'uses' => 'client\FreelancerController@list'
        ]);
        Route::get('/{client_id}', [
            'as' => 'client.freelancer.my', 'uses' => 'client\FreelancerController@myfreelancers'
        ]);
        Route::post('/invite-to-job', [
            'as' => 'client.freelancer.invite', 'uses' => 'client\FreelancerController@invite_to_job'
        ]);
    });
        Route::post('/postuploads', [
           'as' => 'client.postuploads', 'uses' => 'client\JobController@jobupload'
        ]);
    Route::group(['prefix' => 'job'], function () {
        Route::get('/autocomplete', [
            'as' => 'client.job.autocomplete', 'uses' => 'client\JobController@autocomplete'
        ]);
        Route::post('/fileupload', [
            'as' => 'client.job.fileupload', 'uses' => 'client\JobController@uploadfiles'
        ]);
        Route::get('/', [
            'as' => 'client.job', 'uses' => 'client\JobController@list'
        ]);
        Route::match(['get', 'post'], '/add', [
            'as' => 'client.job.add', 'uses' => 'client\JobController@add'
        ]);
        Route::get('/all', [
            'as' => 'client.job.all', 'uses' => 'client\JobController@all'
        ]);
        Route::get('/removejobimg', [
            'as' => 'client.job.removejobimg', 'uses' => 'client\JobController@removejobimages'
        ]);
        Route::match(['get', 'post'], 'edit/{id}', [
            'as' => 'client.job.edit', 'uses' => 'client\JobController@edit'
        ]);
        Route::match(['get', 'post'], 'delete/{id}', [
            'as' => 'client.job.delete', 'uses' => 'client\JobController@delete'
        ]);
        Route::get('/close', [
            'as' => 'client.job.close', 'uses' => 'client\JobController@close'
        ]);
        Route::get('/reopen/{id}', [
            'as' => 'client.job.reopen', 'uses' => 'client\JobController@reopen'
        ]);
        Route::get('/details/{id}', [
            'as' => 'client.job.details', 'uses' => 'client\JobController@details'
        ]);
        
        
        //  Route::post('/setting',['as'=>'client.job.setting','uses'=>'client\JobController@setting']);
        // });
    });
    Route::group(['prefix' => 'team'], function () {
        Route::get('/', [
            'as' => 'client.team.search', 'uses' => 'client\FreelancerController@team'
        ]);
        Route::post('/searchteam', [
            'as' => 'client.team.search', 'uses' => 'client\FreelancerController@searchteam'
        ]);
        Route::match(['get', 'post'],'/hire/{id}',['as'=>'client.team.hire','uses'=>'client\FreelancerController@teamhire']);
        Route::get('/details', 'client\FreelancerController@team_details');
        Route::post('/invite-to-team', [
            'as' => 'client.team.invite', 'uses' => 'client\FreelancerController@invite_to_team'
        ]);
        Route::match(['get','post'],'/hire/{job_id}/{team_id}/{bid_id}', 'client\FreelancerController@hire');
        Route::post('/hire-confirm/{job_id}/{team_id}', 'client\FreelancerController@hire_confirm');

    }); 
     Route::get('/tourguide/jobs', [
            'as' => 'client.tourguide.jobs', 'uses' => 'searchController@list'
        ]);
        Route::get('/tourguide/{id}/detail', [
            'as' => 'client.tourguide.detail', 'uses' => 'searchController@detail'
        ]);
         Route::get('/tourguide/edit/{id}', [
            'as' => 'client.tourguide.edit', 'uses' => 'searchController@edit'
        ]);
    Route::get('/search/tourguide/', [
    'as' => 'client.search.tourguide', 'uses' => 'searchController@tourguide'
        ]);
    Route::post('/search/tourguide/', [
    'as' => 'client.search.tourguide', 'uses' => 'searchController@createtourguide'
        ]);
    // Route::get('/{client_id}', [
    //         'as' => 'client.freelancer.my', 'uses' => 'searchController@myfreelancers'
    //     ]);
     Route::post('/tourguide/edit/{id}', [
            'as' => 'client.tourguide.edit', 'uses' => 'searchController@update'
        ]);
       Route::get('/tourguide/close', [
            'as' => 'client.tourguidejob.close', 'uses' => 'searchController@removepost'
        ]);
});

Route::group(['prefix' => 'vendor', 'middleware' => ['auth', 'vendor','subscription']], function () {
//hire proposal
    

    Route::get('/hires', [
        'as' => 'vendor.hire.list', 'uses' => 'vendor\HireController@index']);
    Route::get('/hires/{id}', [
        'as' => 'vendor.hire.show', 'uses' => 'vendor\HireController@show']);
    Route::post('/hires/{id}/accept', [
        'as' => 'vendor.hire.accept', 'uses' => 'vendor\HireController@accept']);
    Route::post('/hires/{id}/decline', [
        'as' => 'vendor.hire.decline', 'uses' => 'vendor\HireController@decline']);
//---  end hire
    Route::get('/jobs', [
       'as' => 'vendor.dashboard', 'uses' => 'vendor\DashboardController@index'
    ]);
    Route::get('/jobs/{page}', [
       'as' => 'vendor.getjobs', 'uses' => 'vendor\DashboardController@getjobs'
    ]);
    Route::get('/jobs/quick-view-jobs/{jobId}', [
       'as' => 'vendor.quickdetails', 'uses' => 'vendor\DashboardController@quickdetails'
    ]);
    Route::get('/bids', [
       'as' => 'vendor.bids', 'uses' => 'vendor\BidController@index'
    ]);
    Route::get('/progress', [
       'as' => 'vendor.progress', 'uses' => 'vendor\DashboardController@progressinfo'
    ]);
    Route::get('/notifications' , ['as' => 'vendor.all.notification' , 'uses' => 'NotificationController@getAllNotification']);
    Route::group(['prefix' => 'job'], function () {
        Route::get('/{id}/details', [
            'as' => 'vendor.job-detail', 'uses' => 'vendor\DashboardController@detail'
        ]);
        Route::get('/{job_id}/{team_id}/details', [
            'as' => 'vendor.job-detail.team', 'uses' => 'vendor\DashboardController@detailJob'
        ]);
        Route::get('/{id}/proposal', [
           'as' => 'vendor.proposal', 'uses' => 'vendor\ProposalController@index'
        ]);
         Route::get('/{id}/{team_id}/proposal', [
           'as' => 'vendor.proposal.team', 'uses' => 'vendor\ProposalController@index'
        ]);
        Route::post('/proposals', [
           'as' => 'vendor.proposal.add', 'uses' => 'vendor\ProposalController@create'
        ]);
    });
        Route::post('/coverfileupload', [
           'as' => 'vendor.coverfileupload', 'uses' => 'vendor\ProposalController@coverfileuploads'
        ]);
        Route::get('/proposal/{id}/details', [
           'as' => 'vendor.proposal.{id}.detail', 'uses' => 'vendor\ProposalController@detail'
        ]);
         Route::get('/proposal/{id}/{team_id}/details', [
           'as' => 'vendor.proposal.{id}.{team_id}.detail', 'uses' => 'vendor\ProposalController@detail'
        ]);
    Route::group(['prefix' => 'business'], function () {
        Route::get('/', [
           'as' => 'vendor.business', 'uses' => 'vendor\BusinessInfoController@index'
        ]);
        Route::post('/add', [
           'as' => 'vendor.business.add', 'uses' => 'vendor\BusinessInfoController@insert'
        ]);
        Route::post('/file', [
            'as' => 'vendor.business.fileupload', 'uses' => 'vendor\BusinessInfoController@uploadfiles'
        ]);
        // Route::get('/getbussinessdata', [
        //    'as' => 'vendor.businessdata', 'uses' => 'vendor\BusinessInfoController@getbussiness'
        // ]);
    });
    Route::group(['prefix' => 'payment'], function () {
        Route::get('/', [
           'as' => 'vendor.payment', 'uses' => 'vendor\PaymentController@index'
        ]);
        Route::post('/add', [
           'as' => 'vendor.payment.add', 'uses' => 'vendor\PaymentController@addpayment'
        ]);
    });
    
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [
           'as' => 'vendor.profile', 'uses' => 'vendor\ProfileController@index'
        ]);
        Route::match(['get', 'post'],'/mailingaddress', [
            'as' => 'vendor.profile.mailing', 'uses' => 'vendor\ProfileController@mailing'
        ]);
        Route::get('/changePassword', [
           'as' => 'vendor.profile.changePassword', 'uses' => 'changePasswordController@selectchangePassword'
        ]);
        Route::post('/changePassword', [
           'as' => 'vendor.profile.changePassword', 'uses' => 'changePasswordController@createchangePassword'
        ]);
         
    });
    Route::get('/{id}/info', [
           'as' => 'vendor.information', 'uses' => 'vendor\ProfileController@info'
        ]);
    Route::get('/info/deletePost', [
           'as' => 'vendor.info.deletePost', 'uses' => 'vendor\ProfileController@deletepostinfo'
        ]);
    Route::get('/info/removebusinessimg', [
           'as' => 'vendor.info.removebusinessimg', 'uses' => 'vendor\BusinessinfoController@removebusinessimages'
        ]);
    Route::get('/info/removePostimg', [
           'as' => 'vendor.info.removePostimg', 'uses' => 'vendor\ProfileController@removePostimages'
        ]);
    Route::get('/info/removePostvideo', [
           'as' => 'vendor.info.removePostvideo', 'uses' => 'vendor\ProfileController@removePostvideo'
        ]);
    Route::get('/getjobs/{page}', [
       'as' => 'vendor.getjobs', 'uses' => 'vendor\ProfileController@getjobs'
    ]);
    Route::post('/{id}/info', [
           'as' => 'vendor.info', 'uses' => 'vendor\ProfileController@create'
        ]);
    Route::get('/postinfo', [
           'as' => 'vendor.postinfo', 'uses' => 'vendor\ProfileController@postinfo'
        ]);
    Route::post('/uploadsImageText', [
           'as' => 'vendor.imageTextuploads', 'uses' => 'vendor\ProfileController@uploadimageText'
        ]);
    Route::post('/uploadsImagefiles', [
           'as' => 'vendor.uploadsImagefiles', 'uses' => 'vendor\ProfileController@imageUploadfile'
        ]);
    Route::post('/fileuploads', [
           'as' => 'vendor.fileuploads', 'uses' => 'vendor\ProfileController@uploadfiles'
        ]);
    Route::post('/uploadsvideoText', [
           'as' => 'vendor.videoTextuploads', 'uses' => 'vendor\ProfileController@uploadvideoText'
        ]);
    Route::post('/uploadsvideofiles', [
           'as' => 'vendor.uploadsvideofiles', 'uses' => 'vendor\ProfileController@videoUploadfile'
        ]);
    Route::post('/fileuploadv', [
           'as' => 'vendor.fileuploadv', 'uses' => 'vendor\ProfileController@uploadfilesv'
        ]);
    Route::group(['prefix' => 'contract'], function () {
        Route::get('/', [
           'as' => 'vendor.contract', 'uses' => 'vendor\ContractController@list'
        ]);
        Route::get('/{id}/detail', [
           'as' => 'vendor.contract.detail', 'uses' => 'vendor\ContractController@detail'
        ]);
         Route::get('/{id}/{team_id}/detail', [
           'as' => 'vendor.contract.teamdetail', 'uses' => 'vendor\ContractController@detail'
        ]);
        Route::get('/{id}/closed', [
           'as' => 'vendor.contract.closed', 'uses' => 'vendor\ContractController@endContract'
        ]);
        Route::post('/{id}/closed', [
           'as' => 'vendor.contract.closed', 'uses' => 'vendor\ContractController@contractUpdate'
        ]);
        Route::post('/fund/request', [
           'as' => 'fund.request', 'uses' => 'vendor\ContractController@fundRequest'
       ]);
       Route::get('/{id}/list',['as'=>'vendor.contract.contractlist','uses'=>'vendor\ContractController@contractlist']);
    });
    Route::group(['prefix' => 'job'], function () {
        Route::get('/', [
           'as' => 'vendor.job', 'uses' => 'vendor\JobController@list'
        ]);
         
    });
    Route::group(['prefix' => 'invites'], function () {
        Route::get('/', [
           'as' => 'vendor.invites.list', 'uses' => 'vendor\InvitesController@list'
        ]);
       
        Route::get('/{id}/detail', [
            'as' => 'vendor.invites.detail', 'uses' => 'vendor\InvitesController@detail'
        ]);
        Route::get('/declined', [
            'as' => 'vendor.invites.declined', 'uses' => 'vendor\InvitesController@declined'
        ]);
    });
 //        Route::get('/edit', [
 //            'as' => 'vendor.profile.edit', 'uses' => 'vendor\ProfileController@edit'
 //         ]);
 //    });
    Route::group(['prefix' => 'proposal'], function () {
        Route::get('/', [
           'as' => 'vendor.proposal', 'uses' => 'vendor\ProposalController@index'
        ]);
        Route::get('/add', [
            'as' => 'vendor.proposal.add', 'uses' => 'vendor\ProposalController@add'
         ]);
        Route::get('/{id}', [
            'as' => 'vendor.proposal.edit', 'uses' => 'vendor\ProposalController@edit'
         ]);
        Route::delete('/{id}', [
            'as' => 'vendor.proposal.delete', 'uses' => 'vendor\ProposalController@delete'
        ]);
    });
    Route::get('/search/tourguide/', [
    'as' => 'vendor.search.tourguide', 'uses' => 'searchController@tourguide'
        ]);
    Route::post('/search/tourguide/', [
    'as' => 'vendor.search.tourguide', 'uses' => 'searchController@createtourguide'
        ]);
    // Route::get('/{client_id}', [
    //         'as' => 'vendor.my', 'uses' => 'searchController@myfreelancers'
    //     ]);
    Route::get('searchtourguide-details/{id}', 'searchController@searchtourguide_details');
        Route::post('/invite-to-job', [
            'as' => 'vendor.invite', 'uses' => 'searchController@invite_to_job'
        ]);
        Route::get('/tourguide/jobs', [
            'as' => 'vendor.tourguide.jobs', 'uses' => 'searchController@list'
        ]);
        Route::get('/tourguide/{id}/detail', [
            'as' => 'vendor.tourguide.detail', 'uses' => 'searchController@detail'
        ]);
                 Route::get('/tourguide/edit/{id}', [
            'as' => 'vendor.tourguide.edit', 'uses' => 'searchController@edit'
        ]);
          Route::post('/tourguide/edit/{id}', [
            'as' => 'vendor.tourguide.edit', 'uses' => 'searchController@update'
        ]);
        Route::get('/tourguide/close', [
            'as' => 'vendor.tourguidejob.close', 'uses' => 'searchController@removepost'
        ]);
    Route::group(['prefix' => 'team'], function () {
         Route::match(['get', 'post'],'/', [
            'as' => 'vendor.team.add', 'uses' => 'vendor\TeamController@addnewteam'
        ]);
        Route::get('/autoshowvendor', [
            'as' => 'vendor.team.autoshowvendor', 'uses' => 'vendor\TeamController@autoshowvendor'
        ]);
        Route::get('/list', [
            'as' => 'vendor.team.teaminvite', 'uses' => 'vendor\TeamController@teaminvite'
        ]);
        Route::get('/view/team/{id}', [
            'as' => 'vendor.view.team', 'uses' => 'vendor\TeamController@viewTeam'
        ]);
        Route::post('/edit/{id}', [
            'as' => 'vendor.team.edit', 'uses' => 'vendor\TeamController@editteam'
        ]);
        Route::get('/delete', [
            'as' => 'vendor.team.delete', 'uses' => 'vendor\TeamController@delete'
        ]);
        Route::get('/invite', [
            'as' => 'vendor.team.memberinvite', 'uses' => 'vendor\TeamController@memberinvite'
        ]);
         Route::get('/declined', [
            'as' => 'vendor.team.declined', 'uses' => 'vendor\TeamController@declined'
        ]); 
        Route::get('/accept', [
            'as' => 'vendor.team.accept', 'uses' => 'vendor\TeamController@accept'
        ]);
    });
});
        Route::post('vendor/profile/update', [    
           'as' => 'vendor.profile.update', 'uses' => 'vendor\ProfileController@update'
        ]);
    Route::post('vendor/profileuploads', [
           'as' => 'vendor.profileuploads', 'uses' => 'vendor\ProfileController@uploadprofiles'
        ]);


Route::group(['prefix' => 'auth'], function () {
    Route::match(['get', 'post'], '/login', [
        'as' => 'login', 'uses' => 'auth\LoginController@login'
    ]);
    Route::match(['get', 'post'], '/register', [
        'as' => 'register', 'uses' => 'auth\RegisterController@register'
        ]);
    Route::get('/logout', [
        'as' => 'logout', 'uses' => 'auth\LoginController@logout'
        ]);
    Route::get('/forgot', [
        'as' => 'forgotpass', 'uses' => 'auth\ForgotPasswordController@index'
    ]);
    Route::post('/forgot', [
        'as' => 'forgotpass', 'uses' => 'auth\ForgotPasswordController@sendResetLinkEmail'
    ]);
    Route::get('password/reset/{token?}', [
        'as' => 'forgotpass.reset', 'uses' => 'auth\ResetPasswordController@showResetForm'
    ]);
    Route::post('password/reset/{token?}', [
        'as' => 'forgotpass.resets', 'uses' => 'auth\ResetPasswordController@updateResetForm'
    ]);
     Route::post('password/email', [
        'as' => 'forgotpass.email', 'uses' => 'auth\ForgotPasswordController@sendResetLinkEmail'
    ]);
    Route::get('reset/{token}', [
        'as' => 'reset', 'uses' => 'auth\ResetPassswordController@index'
    ]);
    Route::get('/checkusername', [
        'as' => 'auth.checkusername', 'uses' => 'auth\RegisterController@checkusername'
    ]);
    Route::get('/email/emailcheck', [
        'as' => 'auth.emailcheck', 'uses' => 'auth\RegisterController@checkemail'
    ]);
    Route::get('verify/{id}/{token}', ['as' => 'authenticated.activate', 'uses'=>'auth\RegisterController@confirmUser']);
});
Route::group(['middleware' => ['auth']], function () {
    Route::get('tourguide/{id}/info', [
           'as' => 'tourguide.information', 'uses' => 'tourguide\ProfileController@info'
        ]);
});
Route::group(['prefix' => 'tourguide', 'middleware' => ['auth', 'tour_guide']], function () {
    Route::get('/progress', [
       'as' => 'tourguide.progress', 'uses' => 'tourguide\ProfileController@progressinfo'
    ]);
    Route::get('/', [
        'as' => 'tourguide', 'uses' => 'tourguide\TourGuideController@index'
    ]);
    Route::get('/info/removePostimg', [
           'as' => 'tourguide.info.removePostimg', 'uses' => 'tourguide\ProfileController@removePostimages'
        ]);
    Route::post('/{id}/info', [
           'as' => 'tourguide.info', 'uses' => 'tourguide\ProfileController@create'
        ]);
    Route::get('/getjobs/{page}', [
       'as' => 'tourguide.getjobs', 'uses' => 'tourguide\ProfileController@getjobs'
    ]);
    Route::get('/postinfo', [
           'as' => 'tourguide.postinfo', 'uses' => 'tourguide\ProfileController@postinfo'
        ]);
    Route::post('/uploadsImageText', [
           'as' => 'tourguide.imageTextuploads', 'uses' => 'tourguide\ProfileController@uploadimageText'
        ]);
    Route::post('/uploadsImagefiles', [
           'as' => 'tourguide.uploadsImagefiles', 'uses' => 'tourguide\ProfileController@imageUploadfile'
        ]);
     Route::post('/multiprofileuploads', [
           'as' => 'tourguide.multiprofileuploads', 'uses' => 'tourguide\ProfileController@multiuploadfiles'
        ]);
     Route::post('/fileuploads', [
           'as' => 'tourguide.fileuploads', 'uses' => 'tourguide\ProfileController@uploadfilestourguide'
        ]);
    Route::post('/uploadsvideoText', [
           'as' => 'tourguide.videoTextuploads', 'uses' => 'tourguide\ProfileController@uploadvideoText'
        ]);
    Route::post('/uploadsvideofiles', [
           'as' => 'tourguide.uploadsvideofiles', 'uses' => 'tourguide\ProfileController@videoUploadfile'
        ]);
    Route::post('/fileuploadv', [
           'as' => 'tourguide.fileuploadv', 'uses' => 'tourguide\ProfileController@uploadfilesv'
        ]);
    Route::get('/notifications' , ['as' => 'tourguide.all.notification' , 'uses' => 'NotificationController@getAllNotification']);
     //----Hire Tourguide---//
    Route::get('/hires', [
        'as' => 'tourguide.hire.list', 'uses' => 'tourguide\TourGuideController@list']);
    Route::get('/hires/{id}', [
        'as' => 'tourguide.hire.show', 'uses' => 'tourguide\TourGuideController@show']);
    Route::post('/hires/{id}/accept', [
        'as' => 'tourguide.hire.accept', 'uses' => 'tourguide\TourGuideController@accept']);
    Route::post('/hires/{id}/decline', [
        'as' => 'tourguide.hire.decline', 'uses' => 'tourguide\TourGuideController@decline']);
    //----END Hire---//
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [
           'as' => 'tourguide.profile', 'uses' => 'tourguide\ProfileController@select'
        ]); 
        Route::post('/update', [
           'as' => 'tourguide.profile.update', 'uses' => 'tourguide\ProfileController@update'
        ]);
        Route::get('/changePassword', [
           'as' => 'tourguide.profiles.changePassword', 'uses' => 'changePasswordController@selectchangePassword'
        ]);
        Route::post('/changePassword', [
           'as' => 'tourguide.profile.changePassword', 'uses' => 'changePasswordController@createchangePassword'
        ]);
        // Route::match(['get', 'post'],'/mailingaddress', [
        //     'as' => 'tourGuide.profile.mailing', 'uses' => 'tourGuide\ProfileController@mailing'
        // ]);
         Route::get('/deletePost', [
           'as' => 'tourguide.profile.deletePost', 'uses' => 'tourguide\ProfileController@deletepostinfo'
        ]);
         Route::get('/updatePost', [
           'as' => 'tourguide.profile.updatePost', 'uses' => 'tourguide\ProfileController@updatepostinfo'
        ]);
    });
    Route::group(['prefix' => 'job'], function () {
        Route::get('/removeProfileimg', [
            'as' => 'tourguide.job.removeProfileimg', 'uses' => 'tourguide\ProfileController@removeprofileimages'
        ]);
        Route::get('/removedocimg', [
            'as' => 'tourguide.job.removedocimg', 'uses' => 'tourguide\ProfileController@removedocimages'
        ]);
        Route::get('/removelicimg', [
            'as' => 'tourguide.job.removelicimg', 'uses' => 'tourguide\ProfileController@removelicimages'
        ]);
        Route::get('/', [
            'as' => 'tourguide.job', 'uses' => 'tourguide\JobsController@list'
                    ]);
        Route::get('/accept', [
            'as' => 'tourguide.job.accept', 'uses' => 'tourguide\JobsController@accept'
        ]);

        Route::get('/declined', [
            'as' => 'tourguide.job.declined', 'uses' => 'tourguide\JobsController@declined'
        ]);

        Route::get('/{id}/detail', [
            'as' => 'tourguide.job.detail', 'uses' => 'tourguide\JobsController@detail'
        ]);
   }); 
    Route::group(['prefix' => 'business'], function () {
        Route::get('/', [
           'as' => 'tourguide.business', 'uses' => 'tourguide\BussinessTourController@index'
        ]);
        Route::post('/add', [
           'as' => 'tourguide.business.add', 'uses' => 'tourguide\BussinessTourController@insert'
        ]);
    });
    Route::group(['prefix' => 'payment'], function () {
        Route::get('/', [
           'as' => 'tourguide.payment', 'uses' => 'tourguide\PaymentController@index'
        ]);
        Route::post('/save', [
           'as' => 'tourguide.payment.save', 'uses' => 'tourguide\PaymentController@savepayment'
        ]);
    });
});
       Route::post('tourguide/tfileuploads', [
           'as' => 'tourguide.tfileuploads', 'uses' => 'tourguide\ProfileController@uploadfiles'
        ]);
        Route::get('tourguide/updateImageText', [
           'as' => 'tourguide.updateTextupdate', 'uses' => 'tourguide\ProfileController@updateimageText'
        ]);
       Route::post('tourguide/updatefileuploads/{id}', [
           'as' => 'tourguide.updatefileuploads', 'uses' => 'tourguide\ProfileController@updateuploadfiles'
        ]);
        Route::post('tourguide/updatevfileuploads', [
           'as' => 'tourguide.vfileuploads', 'uses' => 'tourguide\ProfileController@updateuploadvfiles'
        ]);
    Route::post('tourguide/docuploads', [
           'as' => 'tourguide.docuploads', 'uses' => 'tourguide\ProfileController@uploaddoc'
        ]);
    Route::post('tourguide/licenceuploads', [
           'as' => 'tourguide.licenceuploads', 'uses' => 'tourguide\ProfileController@uploadlicence'
        ]);
    Route::post('tourguide/insuranceuploads', [
           'as' => 'tourguide.insuranceuploads', 'uses' => 'tourguide\ProfileController@uploadinsurance'
        ]);
    Route::get('searchtourguide-details/{id}', 'searchController@searchtourguide_details');
        Route::post('/invite-to-job', [
            'as' => 'client.invite', 'uses' => 'searchController@invite_to_job'
        ]);
Route::get('client/freelancer-details/{id}', 'client\FreelancerController@freelancer_details');
Route::any('client/hire/{job_id}/{user_id}/{bid_id}', 'client\JobController@hire');
Route::any('client/hire-confirm/{job_id}/{user_id}', 'client\JobController@hire_confirm');

Route::any('vendor/hire/{job_id}/{user_id}/{tgi_id}', 'searchController@hire');
Route::any('client/hires/{job_id}/{user_id}/{tgi_id}', 'searchController@hire');
Route::any('vendor/hire-confirm/{job_id}/{user_id}', 'searchController@hire_confirm');
Route::any('client/confirm-hire/{job_id}/{user_id}', 'searchController@hire_confirm');

Route::get('/messages', [
   'as' => 'messages', 'uses' => 'MessagesController@index'
]);
Route::get('/message/{parent_message_id}', [
   'as' => 'message.thread', 'uses' => 'MessagesController@get_message_thread'
]);
Route::post('/message/send', [
   'as' => 'message.send', 'uses' => 'MessagesController@send_message'
]);

Route::GET('/markread/{notif_id}', [ 'as' => 'markread', 'uses' => function($notif_id){
    if( Auth::user()->notifications->where('id',$notif_id)->first() ){
        Auth::user()->notifications->where('id',$notif_id)->markAsRead();
    }
    // Auth::user()->unreadNotifications->markAsRead();  
}] ); 
Route::get('/markall',['as'=>'markall','uses'=>function(){
    Auth::user()->unreadNotifications->markAsRead();
    return back();
}]);
Route::get('chat/{contractId}',[ 'as' => 'chat' , 'uses'=> 'chatController@chat']);
Route::get('chats/{hire_id}',[ 'as' => 'get.user.chat' , 'uses'=> 'chatController@chat']);
Route::post('send', 'chatController@send');
Route::post('saveChat', ['as' => 'chat.save', 'uses' => 'chatController@saveChat']);