<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $dates = ['deleted_at'];
    protected $table = 'post';
    protected $fillable=['title','message','type','user_id'];
    public $timestamps = false;
     public function user()
    {
        return $this->belongsTo('App\User');
    }
     public function jobs()
    {
        return $this->belongsTo('App\Job');
    }
    public function Attachment()
    {
        return $this->hasMany('postattachment', 'id', 'post_id');
    }

}
