<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispute extends Model
{
    protected $fillable = [ 'job_id', 'subject', 'content', 'status_id', 'category_id', 'client_id', 'vendor_id', 'tour_guide_id', 'agent_id', 'dispute_apply_by'];

    public function job(){
    	return $this->belongsTo('App\Job', 'job_id');
    }

    public function status(){
    	return $this->belongsTo('Modules\Admin\Entities\Ticket\TicketitStatus', 'status_id');
    }

    public function category(){
    	return $this->belongsTo('Modules\Admin\Entities\Ticket\TicketitCategory', 'category_id');
    }

    public function dispute_verdicts(){
        return  $this->hasMany('Modules\Admin\Entities\Dispute\DisputeVerdicts' , 'dispute_id');
    } 

  public function comment(){
        return  $this->hasMany('Modules\Admin\Entities\Ticket\TicketitComment' , 'ticket_id')->whereType('dispute');
    } 


        
}
