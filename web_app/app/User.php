<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
     protected $table ="user";
     protected $fillable=['firstName','lastName','username','type','email','password','confirmpassword','remember_token','isconfirmed','dob','gender','phone','profilePercent','countryDialcode','membership','profile_image','address_id','subscription_id','success_rate'];
     public $timestamps = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function bids()
    {
        return $this->hasMany('App\Bid');
    }
    public function hires()
    {
        return $this->hasMany('App\Hires');
    }
    public function jobs()
    {
        return $this->hasMany('App\Job');
    }
    public function openedJobs() {
        return $this->hasMany('App\Job')->where('status', '1');
    }
    public function closedJobs() {
        return $this->hasMany('App\Job')->where('status', '0');
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    public function profileimg(){
        return $this->hasMany('App\Attachment', 'id','profile_image');
    }
    public function address(){
        return $this->hasMany('App\Address', 'id','address_id');
    }
   //   public static function boot()
   // {   
   //      parent::boot();    
   //         // cause a delete of a product to cascade to children so they are also deleted
   //         static::deleted(function($teamdel)
   //         {
   //         $teamdel->teammember()->delete();
   //         });
   // }
}
