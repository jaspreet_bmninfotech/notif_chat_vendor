<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
    protected $table = 'feedback';
   protected $fillable=['hire_id','feedToId','feedFromId','review','rating','starRate'];
   public $timestamps = true;

    public function user()
  {
    return $this->belongsTo('App\User','feedToId','id');
  }

  public function hire()
  {
    return $this->belongsTo('App\Hires');
  }
  public static function getfeedback($user_id)
  { 
      $getfeedback= feedback::with('user')->where('feedToId',$user_id)->get()->toArray();
      if(sizeof($getfeedback) > 0)
      {
            $getdata=array_column($getfeedback,'starRate');
            // dd($getdata);
            $max = [];
            foreach ($getdata as $k => $v) {
                if(!array_key_exists((string)$v, $max))
                {
                    $max[(string)$v] = 1;
                }else{
                    $max[(string)$v] = (string)$max[$v]+1;
                }
            }
           
            $totalstar=0;
            $n=0;
           foreach ($max as $stars => $votes)
           {
            $totalstar += $stars * $votes;
            $n +=$votes;
            }
            if( $totalstar != 0 ){

              $rating= $totalstar/$n;
            }else{
              $rating = 0;
            }
               return $rating;
      }
      else{
        $rating=0.0;
        return $rating;
      }

  }

}
