<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SearchTourguide extends Model
{
	const status_open = 1;
    const status_close = 0;
    const status_expired = 2;
    
    public static function all_records()
	{
		return DB::table('user as u')
					->selectRaw('u.id, u.firstName, u.lastName, u.username, u.success_rate, u.profile_image,attachment.path, bz.description as business_desc, bz.ratePerHour, bz.info, country.name as address_country')
					->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
					->leftjoin('address as adr', 'adr.id', '=', 'u.address_id')
					->leftjoin('country', 'country.id', '=', 'adr.country_id')
					->leftjoin('attachment', 'attachment.id', '=', 'u.profile_image')
					->where(array('u.type' => 'tg'))
					->get();
	}

	public static function freelancer_details($user_id)
	{
		$freelancer_details = DB::table('user as u')->leftjoin('attachment', 'attachment.id', '=', 'u.profile_image')->select('u.id','u.username','u.firstName','u.lastName','u.profile_image','attachment.path')->where(array('u.id' => $user_id))
								->first();

		return sizeof($freelancer_details) > 0 ? (array) $freelancer_details : array();
	}

	public static function search_tourguides($search_params)
	{
		$freelancers = DB::table('user as u')
						->where('u.id' , '!=' , null)
					->selectRaw('u.id, u.firstName, u.lastName, u.username, u.success_rate, u.profile_image,attachment.path, bz.description as business_desc, bz.ratePerHour,bz.job_success_rate, bz.info, country.name as address_country,tour_guide_info.*')
					->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
					->leftjoin('tour_guide_info', 'tour_guide_info.user_id', '=', 'u.id')
					->leftjoin('attachment', 'attachment.id', '=', 'u.profile_image');
		$is_search = false;
		$where_str = "u.type = 'tg' ";
		
		if (isset($search_params['filter_name']) && $search_params['filter_name'] != '')
		{
			$is_search = true;
			$where_str .= " AND (";

			$exp_name = explode(" ", $search_params['filter_name']);
			for ($i=0; $i < sizeof($exp_name); $i++)
			{
				if ($i > 0)
				{
					$where_str .= " OR ";
				}

				$where_str .= "u.firstName LIKE '%" . $exp_name[$i] . "%' OR u.lastName LIKE '%" . $exp_name[$i] . "%'";
			}

			$where_str .= ")";
		}

		// if (isset($search_params['filter_language']) && $search_params['filter_language'] != '')
		// {
		// 	$is_search = true;
		// 	$where_str .= " AND language.name LIKE '%" . $search_params['filter_language'] . "%'";
		// }

		if (isset($search_params['filter_country']) && $search_params['filter_country'] != '')
		{
			$is_search = true;
			$where_str .= " AND country.name LIKE '%" . $search_params['filter_country'] . "%'";
			
		}

		if (isset($search_params['filter_gender']) && $search_params['filter_gender'] != '')
		{
			$is_search = true;
			$where_str .= " AND user.gender LIKE '%" . $search_params['filter_gender'] . "%'";
			
		}

		if (isset($search_params['filter_status']) && $search_params['filter_status'] != '')
		{
		
			$is_search = true;
			$where_str .= " AND tour_guide_info.nationalityStatus LIKE '%" . $search_params['filter_status'] . "%'";
			
		}
		if (isset($search_params['filter_activities']) && $search_params['filter_activities'] != '')
		{
        
			$is_search = true;

			$where_str .= " AND tour_guide_info.tourguideActivities LIKE '%" . $search_params['filter_activities'] . "%'";
			
		}
		


		if (isset($search_params['filter_job_success']) && $search_params['filter_job_success'] != '')
		{
		
			$is_search = true;
			if ($search_params['filter_job_success'] == '80')
			{
				$freelancers = $freelancers->whereBetween('job_success_rate', array(80, 100));
			}
			elseif ($search_params['filter_job_success'] == '90')
			{
				$freelancers = $freelancers->whereBetween('job_success_rate', array(90, 100));
			}
		}

		if ($is_search)
		{

			$freelancers = $freelancers->join('address as adr', 'adr.id', '=', 'u.address_id');
		}
		else
		{
			$freelancers = $freelancers->leftjoin('address as adr', 'adr.id', '=', 'u.address_id');
		}
		return $freelancers->leftjoin('country', 'country.id', '=', 'adr.country_id')
							->whereRaw($where_str)->get();
	}
}
