<?php

use Modules\Admin\Entities\Module;
use App\AdminUser;
use App\User;
// use Auth;


use Modules\Admin\Entities\RolePermisson;

	 function admin_Side_bar() {

		return Module::where('status', 1)->whereNull('parent')->get();
	}



	function agent_user_name($id){

		$name = AdminUser::where('id', $id);
		if($name->exists()){
			return AdminUser::where('id', $id)->first()->username;
		}
		return;
	}

	function username($id){
		
	 $user = user_detail($id);

	 return $user->username;
	}

	function user_detail($id){

		return User::Find($id);
	}

function admin_user_id(){

	return Auth::guard('admin')->id();

}
function admin_role_id(){
	return  $role_id = Auth::guard('admin')->user()->role_id;
}
function role_permisson(){
	$role_id = admin_role_id();
	return  RolePermisson::where(['role_id'=> $role_id, 'status'=>1])->pluck('module_id')->toArray();
}


function getAdminData(){
	$user = AdminUser::where('id',Auth::guard('admin')->user()->id)->with('attachment')->first();
	return $user;
}
?>