<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReleaseFundNotify extends Notification
{
   use Queueable;

   /**
    * Create a new notification instance.
    *
    * @return void
    */
  public function __construct($releasefund,$notif,$senduser,$fromuser)
   {
       $this->releasefund = $releasefund;
       $this->notif = $notif;
       $this->senduser = $senduser;
       $this->fromuser = $fromuser;
   }

   /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
   public function via($notifiable)
   {
        return ['database'];
   }

   /**
    * Get the mail representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return \Illuminate\Notifications\Messages\MailMessage
    */
    public function toDatabase($notifiable)
   {
       return [
           'date'    => Carbon::now()->format('Y-m-d H:i:s'),
           'content' => $this->releasefund,
           'user'    => $notifiable,
           'notif'   => $this->notif,
           'fromuser'=> $this->fromuser
       ];
   }
   /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
   public function toArray($notifiable)
   {
       return [
           //
       ];
   }
}