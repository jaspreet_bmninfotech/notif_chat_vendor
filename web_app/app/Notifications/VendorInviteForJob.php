<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class VendorInviteForJob extends Notification
{
    use Queueable;

    public $invites;
    public $senduser;
    public $notif;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($invites,$senduser,$notif)
    {
        $this->invites = $invites;
        $this->senduser = $senduser;
        $this->notif = $notif;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {

        return [
            'date'    => Carbon::now()->format('Y-m-d H:i:s'),
            'content' => $this->invites,
            'user'    => $notifiable,
            'notif'   => $this->notif,
            'senduser'=> $this->senduser
        ];
    }
     public function toBroadcast($notifiable)
    {

        return new BroadcastMessage([
            
                    'date'    => Carbon::now()->format('Y-m-d H:i:s'),
                    'content' => $this->invites,
                    'user'    => $notifiable,
                    'notif'   => $this->notif,
                    'senduser'=> $this->senduser
            
        ]);
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
