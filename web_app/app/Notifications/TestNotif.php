<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TestNotif extends Notification
{
    use Queueable;

    protected $user;
    protected $hires;
    protected $notif;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($hires, $notif)
    {
        $this->hires = $hires;
        $this->notif = $notif;
       
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'date'    => Carbon::now()->format('Y-m-d H:i:s'),
            'content' => $this->hires,
            'user'    => $notifiable,
            'notif'   => $this->notif
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
