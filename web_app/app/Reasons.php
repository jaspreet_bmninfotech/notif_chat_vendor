<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reasons extends Model
{
    protected $table = 'reasons';
    protected $fillable =['type','title'];
    public $timestamps = true;

    public static function getReasons($type) {
        return DB::table('reasons')->where('type', $type)->get();
    }
}
