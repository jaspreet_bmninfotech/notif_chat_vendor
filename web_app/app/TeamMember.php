<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamMember extends Model 
{
	 use SoftDeletes;
	const status_declined = 0;
    const status_accepted = 1;
    const status_pending = 2;
    protected $dates = ['deleted_at'];
    protected $table = 'teammember';
    protected $fillable =['id','user_id','team_id','description','status'];
    public $timestamps = true;

    public function team()
    {
    	return $this->belongsTo('App\Team', 'team_id' , 'id');
    }
}