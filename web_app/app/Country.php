<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model 
{
  use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'country';
    protected $fillable=['sortName','name','phoneCode'];
    public $timestamps = false;

    public function States()
    {
        return $this->hasMany('State', 'country_id', 'id');
    }
}