<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAttachment extends Model
{
    protected $table = 'jobattachment';
   protected $fillable=['job_id','attachment_id'];
    public $timestamps = true;


    public function job()
    {
        return $this->belongsTo('job');
    }

    public function Attachment()
    {
        return $this->hasOne('Attachment', 'attachment_id', 'id');
    }
}
