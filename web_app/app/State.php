<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'state';
    protected $fillable=['name','country_id'];
    public $timestamps = false;

    public function hasCountry()
    {
        return $this->belongsTo('Country', 'id');
    }

    public function cities()
    {
        return $this->hasMany('City', 'state_id');
    }

}