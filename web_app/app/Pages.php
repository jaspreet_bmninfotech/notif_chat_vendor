<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'pages';
    protected $fillable=['title','description','slug','image_id','publishdate'];
    public $timestamps = true;
}
