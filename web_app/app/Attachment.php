<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Input;
use File;

class Attachment extends Model
{
	protected $table ="attachment";
    protected $fillable=['name','path','mimeType','job_id'];
    public $timestamps = true;
      public function Job()
    {
        return $this->belongsTo('Job');
    }

    public function BusinessAttachment()
    {
        return $this->belongsTo('Attachment', 'id', 'attachment_id');
    }

    public static function addAttachment($name, $path)
    {
        
        if ( !file_exists(public_path($path)) )
        {
            mkdir(public_path($path), 0777, true);
        }
        $destinationPath = public_path($path);
        
        $ext = Input::file($name)[0]->getClientOriginalExtension();
        
        
        $fileName = substr_replace(str_slug(Input::file($name)[0]->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
        
        $file = Input::file($name)[0]->move($destinationPath, $fileName);
        return array('path' => $destinationPath . DIRECTORY_SEPARATOR . $fileName,
         			'name' => $fileName,
         			'originalName' => Input::file($name)[0]->getClientOriginalName(),
         			'mimeType' => File::mimeType($destinationPath . DIRECTORY_SEPARATOR . $fileName));
    }
   
}
