<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'setting';
    protected $fillable=['key','value'];
    public $timestamps = true;
    
    const Per_Job_Cost="per_job_cost";
    const home_category="home_category";
    const home_tab_category="home_tab_category";
}
