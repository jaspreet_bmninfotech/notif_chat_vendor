<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobTransaction extends Model 
{

    protected $table = 'jobtransaction';
    public $timestamps = false;
    public $fillable = ['hire_id','amount','amt','from','to','request_fund','approve_fund','request_release','approve_release','status'];

 	// $transactionStatus = ['fund_requested','funded','release_request','released'];   
 	 public function hires()
    {
        return $this->belongsTo('App\Hires','hire_id','id');
    }
    public function getUser()
    {
    	return $this->belongsTo('App\User','from', 'id');
    }
}
