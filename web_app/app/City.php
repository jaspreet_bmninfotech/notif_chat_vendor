<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model 
{
	 use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'city';
    protected $fillable =['name','state_id'];
    public $timestamps = false;

    public function state()
    {
        return $this->belongsTo('State', 'id');
    }

}