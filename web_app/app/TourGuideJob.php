<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class TourGuideJob extends Model
{
	const status_open = 1;
    const status_close = 0;
    const status_expired = 2;

    protected $table = 'tourguidejob';
     protected $fillable=['id','title','user_id','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','fromdatetime','todatetime','totalpeople'];
    public $timestamps = true;

	public static function job_details($job_id)
        {
            return DB::table('tourguidejob as j')
                        ->select('j.*')
                        ->where(array('id' => $job_id))
                        ->first();
        }
        public static function job_hires($job_id)
    {
        return DB::table('tourguidehire as h')
                    ->selectRaw('h.*, u.id, u.firstName,u.profile_image, u.lastName, u.userName,atch.path, bz.description as business_desc, bz.ratePerHour, bz.info, country.name as address_country')
                    ->leftjoin('user as u', 'u.id', '=', 'h.user_id')
                    ->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
                    ->leftjoin('address as adr', 'adr.id', '=', 'u.address_id')
                    ->leftjoin('attachment as atch', 'atch.id', '=', 'u.profile_image')
                    ->leftjoin('country', 'country.id', '=', 'adr.country_id')
                    ->where(array('h.tourguide_job_id' => $job_id))
                    ->get();
    }
    public static function job_proposals($job_id)
    {
        return DB::table('tourguideinvites as tgi')
                    ->selectRaw('tgi.*, tgi.id as tgi_id, u.id, u.firstName, u.lastName, u.userName, bz.description as business_desc, bz.ratePerHour, country.name as address_country')
                    ->leftjoin('user as u', 'u.id', '=', 'tgi.user_id')
                    ->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
                    ->leftjoin('address as adr', 'adr.id', '=', 'u.address_id')
                    ->leftjoin('country', 'country.id', '=', 'adr.country_id')
                    ->where(array('tgi.tourguide_job_id' => $job_id, 'tgi.status' => 1))
                    ->get();
    }
}
