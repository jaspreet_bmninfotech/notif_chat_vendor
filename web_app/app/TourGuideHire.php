<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class TourGuideHire extends Model
{
   	const status_close = 0;
	const status_opened = 1;
	const status_pending = 2;

    protected $table = 'tourguidehire';
     protected $fillable=['id','user_id','tourguide_job_id','status'];
    public $timestamps = true;

    public static function save_hire($insert_arr, $user_id, $tid)
	{
		$hire = new TourGuideHire();
		$hire->user_id = $user_id;
		$hire->tourguide_job_id = $insert_arr->id;
		$hire->created_at = date('Y-m-d H:i:s');
		$hire->status = TourGuideHire::status_pending;
        $hire->hasExist = '1';
		$hire->save();
		return $hire->id;
	}
	 public function tourguidejob()
    {
        return $this->belongsTo('App\TourGuideJob','tourguide_job_id','id');
    }
    public static function updateStatus($hire_id, $status, $message, $reason = 0)
    {
        DB::table('tourguidehire')->where('id', $hire_id)
            ->update([
                'status' => $status,
                'message' => $message,
                'decline_reason' => $reason
            ]);

    }
}
