<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model 
{

    protected $table = 'bankdetails';
    public $timestamps = false;
    public $fillable = ['user_id','account_id','status'];

 	// $transactionStatus = ['fund_requested','funded','release_request','released'];   

}
