<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\job;
use App\user;
use App\Bid;
use Auth;
use Validator;
use Session;
use Response;
class BidController extends Controller
{
   public function index(){
   	 $user_id =Auth::id();
   	$result =\DB::table('bid')->join('job','job.id','=','bid.job_id')
        
    	->select('job.id','job.name','job.created_at')->where('bid.user_id','=',$user_id)->orderBy('bid.id','desc')->get();
 	$data = json_decode( json_encode($result), true);
    	
   	return view('vendor/bid/bids',['data'=>$data]);
   }
}
