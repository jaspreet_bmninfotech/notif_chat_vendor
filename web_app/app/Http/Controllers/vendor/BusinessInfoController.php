<?php
namespace App\Http\Controllers\vendor;

use App\Bussiness;
use App\BusinessAttachment;
use App\Address;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Input;
use Response;
use Session;
use App\Category;
use App\Attachment;
use App\User;
use App\Languages;

use Auth;
use Validator;

class BusinessInfoController extends Controller
{
    public function index()
    {
        $id =Auth::id();
        $category   = Category::select('id','name','shortCode','parent_id')
                        ->where('parent_id','=',null)->get();
        $country    = Country::all();
        $language   = Languages::all();
        $businessattach = \DB::table('bussiness as bz')->leftjoin('businessattachment as bza','bza.business_id','=','bz.id')->leftjoin('attachment','attachment.id','bza.attachment_id')->select('attachment.path','attachment.name','bza.attachment_id','bza.business_id')->where('bz.user_id','=',$id)->get();
        return view('vendor.info.business',['category'   => $category,
                                            'businessattach'    => $businessattach,
                                            'country'    => $country,
                                            'language' =>$language
                                            ]);
    }
    public function getbussiness(Request $request)
    {
        $id =Auth::id();
        if($request->isMethod('get'))
        {
            $getbussiness= \DB::table('bussiness')
                            ->leftjoin('address','bussiness.address_id','=','address.id')
                            ->leftjoin('category','bussiness.category_id','=','category.id')
                            ->select('bussiness.*','address.*','category.id as categoryId','category.shortCode')
                            ->where('user_id',$id)->get()->toArray();

                            // dd($getbussiness);
                            // die();
            $response   =   array("code" => "","success" => "","data"=>""); 
            if(!$getbussiness) 
            {
                $response['code']   = 0;
                $response['success']= false;
                $response['data']   = '';
            } else 
            {   
                $getbussiness[0]->info=  unserialize($getbussiness[0]->info);
                $response['code']   = 1;
                $response['success']= true;
                $response['data']   = $getbussiness[0];
            }
            return response()->json($response);
        }
    }
   public function removebusinessimages(Request $request){
        $id = Auth::user()->id;
        $remove_id = $request->remove_id;
        $busiremove_id = $request->busiremove_id;
        $paths = $request->paths;
        $filename = $request->filename;
        $data = \DB::table('businessattachment')->where('business_id','=',$busiremove_id)->where('attachment_id','=',$remove_id)->delete();
        unlink(public_path('/uploads/vendor/Business/'.$id.'/'.$filename));
        return response('success');
    }

      
    public function insert(Request $request)
    {
        
        
        if($request->isMethod('post'))
        {
            $data=Input::except(array('_token'));
                $rule=array(
                            'businesscategory'   =>  'Required',
                            'cantravel'         =>  'Required',
                            'businessname'      =>  'Required',  
                           );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('vendor.business')->withErrors($validator)->with('bussinessRedirected','Please fill bussiness');
            }
      
            $category          =$request->Input('businesscategory');
            $category          = explode("_",$category);
            $categoryId        = $category[0] ? $category[0] : NULL;
            $categoryShortCord =  isset($category[1]) ? $category[1] : null;
            $subcategoryId     = $request->Input('subcategory');
            try{
                    $id = Auth::id();
                    $hasBussiness = Bussiness::where('user_id', $id)->first();
                    $address=Null;
                    $data = array(
                        'category_id'   => $categoryId,
                        'canTravel'     => $request->cantravel === "yes",
                        'name'          => $request->businessname,
                        'user_id'       => $request->user()->id
                    );

                    $info = array();
                    switch( $categoryShortCord)
                    {
                        case 'model' :
                            
                            $info = array(
                                            'unitMeasurement' =>$request->unitMesurement=="imperial" ? 'imperial':'metric',
                                            'height'        => $request->height,
                                            'hairColor'     => $request->haircolor,
                                            'eyeColor'      => $request->eyecolor,
                                            'weight'        => $request->weight,
                                            'bust'          => $request->bust,
                                            'hips'          => $request->hips,
                                            'shoeSize'      => $request->shoesize,
                                            'inSchool'      => $request->get('inschool') == 'yes' ? 1: 0,
                                            'grade'         => $request->grade,
                                            'superPower'    => $request->Input('superpower'),
                                            'famousPerson'  => $request->Input('famousperson'),
                                            'talent'        => $request->Input('talent'),
                                            // 'typeOfModel'    => $modelingtypeArr,
                                        );
                            $modelingtype =$request->Input('modelingtype');
                            
                            if($modelingtype){
                                $modelingTypeArr = array();
                                    foreach ($modelingtype as $key => $value) {
                                    $modelingTypeArr[] = $value;
                                }  
                            $info['typeOfModel']    = $modelingTypeArr;
                            }

                        break;
                        case 'venues' :                  
                            $address = [
                                            'country_id'    => $request->get('country'),
                                            'state_id'      => $request->get('state'),
                                            'city_id'       => $request->get('city'),
                                            'postalCode'    => $request->get('postalCode'),
                                            'address'       => $request->get('streetaddress')
                                            ];
                            $venueType           = $request->Input('venueType');
                            if($venueType){
                                $venueTypeArr = array();
                                foreach ($venueType as $key => $value) {
                                    $venueTypeArr[] = $value;
                                }
                                $info['venueType']       = $venueTypeArr;
                            }
                             
                            $venueSetting       = $request->Input('venueSetting');
                            if($venueSetting){
                                $venueSettingArr =array();
                                foreach ($venueSetting as $key => $value) 
                                {
                                    $venueSettingArr[] = $value;
                                }
                                 
                                $info['venueSetting']   = $venueSettingArr;
                            }
                             
                            $venueActivities       = $request->Input('venueActivities');
                            if($venueActivities){
                                $venueActivitiesArr =array();
                                 foreach ($venueActivities as $key => $value) 
                                {
                                    $venueActivitiesArr[] = $value;
                                }
                                 // $venueActivies           = implode(',',$venueActivies);
                                 $info['venueActivities']   = $venueActivitiesArr;
                            }
                            $info['maximumCapacity']      = $request->Input('maximumCapacity');                   
                        break;
                        case 'guide' :
                            $country                    = $request->get('countryOfResidence');
                            $lawfull                    = $request->get('lawfull');
                            $language                   = $request->get('language');
                            $guidepeople                = $request->Input('guidepeople');
                            if($guidepeople){
                              $guidepeopleArr         =array(); 
                               foreach ($guidepeople as $key => $value) 
                                {
                                    $guidepeopleArr[] = $value;
                                } 
                            // $guidepeople                = implode(',', $guidepeople);
                            $info['guidepeople']        = $guidepeopleArr;                      
                            }
                            $scale                      =$request->scale;
                            $info['countryofresidence'] =$country;
                            $info['lawfull']            = $lawfull;
                            $info['language']           =$language;                       
                            $info['scale']              = $scale;
                        break;
                        default:
                            $data['description']        =$request->description;
                            $data['sub_category_id']    = $subcategoryId;
                            $hearabout                  =$request->get('hearaboutus');
                            $info['hearaboutus']        = $hearabout;  
                        break;
                   }
                  
                    $data['info']           = serialize($info);
                     $address_id = NULL;
                    if($address!= NULL){
                        if($hasBussiness   != NULL)
                        {
                            $address_id     = $hasBussiness->address_id;
                        }
                        if($address_id     != NULL)
                        {
                            $address        =   Address::where('id',$address_id)
                                                ->update($address);   
                        }else{
                            
                            $address        =   Address::create($address);
                            $address_id     =   $address->id;      
                        }
                        $data['address_id'] =   $address_id;
                    }
                      
                    if($hasBussiness == NULL){
                        $business = Bussiness::create($data);
                        $business->save();
                        $business_id = $business->id;
                        $profilePrecentage = Auth::user()->profilePercent;
                        if($profilePrecentage == 40){
                            $user      = User::where('id',$id)->update(['profilePercent'=>50]);
                        }
                        Session::flash('success','Successful Bussiness Created ');
                    }
                    else{
                        $business= Bussiness::where('user_id',$id)->update($data);
                        $business_id = Bussiness::select('id')->where('user_id',$id)->first();
                        Session::flash('success','Successful bussiness Updated ');
                    }
                          $files = Input::file('files');
                            if ($files) {
                                $fileid = count($files);
                                if ($fileid < 5) {
                                 $id = Auth::id();
                                $destinationPath = "uploads/vendor/Business/".$id."/";
                                foreach ($files as $key => $file) {
                                   $fileSize = $file->getClientSize();
                                    if ($fileSize < 2000000) {
                                    $ext = $file->getClientOriginalExtension();
                                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                                    $file = $file->move($destinationPath, $fileName);
                                    $data = Attachment::create(
                                                     ['name' => $fileName,
                                                      'path' => $file,
                                                      'mimeType' => $ext
                                                    ]
                                                );
                                    $data->save();
                                    $attachment_id = $data->id;
                                    $data = BusinessAttachment::create(
                                                     ['business_id' => $business_id['id'],
                                                      'attachment_id' => $attachment_id]
                                                );
                                    $data->save();

                                 }
                                    }
                                        }else{
                                            return redirect()->route('vendor.business')->with('status', 'Max 5 files are allowed.');
                                        }
                            }
                    return redirect()->route('vendor.business');
            }
                catch (Exception $e)
                {
                return response()->json([
                'error' => $e->getMessage()
                ]);
                }
        }     // return view('vendor.info.business');
    }
   
}
