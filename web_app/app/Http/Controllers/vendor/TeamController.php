<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\AcceptInviteTeam;
use App\Notifications\DeclineInviteTeam;
use App\Notifications\InviteTeamNotify;
use App\User;
use App\Team;
use App\TeamMember;
use Response;
use Validator;
use Auth;
use Input;
use Session;
use DB;

class TeamController extends Controller
{
	 public function autoshowvendor(Request $request)
    {
        $data       =   User::select("username","id")->where("username","LIKE","%{$request->Input('typeahead')}%")
                        ->where("type",'=',"vn")->get();
        $response   =   array("code" => "","success" => "","data"=>""); 
        if(!$data) 
        {
            $response['code']   = 0;
            $response['success']= false;
            $response['html']   = 'no data';
        } else 
        {   
            $response['code']   = 1;
            $response['success']= true;
            $response['data']   = $data;
        }
        return response()->json($response);
    }
    public function addnewteam(Request $request)
    {
    	 if($request->isMethod('post'))
        {
            $data=Input::except(array('_token'));
                $rule=array(
                    'name'=>'Required|Alpha',
                    'description' =>'Required|max:1000',
                    'member'   =>'Required',
                );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
            }
            $data=[
            	'name'=>$request->name,
            	'description'=>$request->description,
            	'user_id'	=>Auth::id()
            ];
            $team_id =Team::new_team($data);   
           	$member =$request->member;
           	foreach ($member as $key => $value) {
	           	$model = User::where('username', $value )->first();
	          	if($model){
	           		$data = [
	           					'team_id' => $team_id,
	           					'status' => TeamMember::status_pending,
	           					'user_id' => $model->id
           					];
           		$TeamMember = TeamMember::create($data);
           		$user= DB::table('teammember')->leftjoin('user','user.id','=','teammember.user_id')->where('teammember.id',$TeamMember->id)->first();
           		$tid=$user->team_id;
           		$tname=Team::where('id',$tid)->select('name')->first();
	            $teamname=$tname->name;
	            $uname=$user->username;
	            $operation='You Have Been Invited to join the team '.$teamname. ' by';
	            $senduser=User::where('id',$user->id)->first();
	            $senduser->notify(new InviteTeamNotify($user, $uname , $operation));
	          	}
           	}
			Session::flash('success','Successful Team Created');
			return redirect()->route('vendor.team.teaminvite')->with('success','Successful Team Created');
        }
        return view('vendor/team/addnewteam');
    }
     public function teaminvite()
    {
    	$id = Auth::user()->id;
                
        $data= Team::with('teammember')->where(['user_id'=>$id,'status'=>'1'])->get();
        // dd($data);

        // $team = [];
     //        foreach ($data as $key => $value) {
     //            $value['countmember'] = $value['teammember']->count();
     //            $team[] = $value;
     //        }
        return view('vendor/team/teaminvitelist',['team'=>$data]);
    }
    public function editteam(Request $request,$id)
    {
        $member =$request->member;

            foreach ($member as $key => $value) {
                $model = User::where('username', $value )->first();
                if($model){
                    $data = [
                                'team_id' => $id,
                                'status' => TeamMember::status_pending,
                                'user_id' => $model->id
                                ];
                    $TeamMember = TeamMember::create($data);
            $user= DB::table('teammember')->leftjoin('user','user.id','=','teammember.user_id')->where('teammember.id',$TeamMember->id)->first();
                $tid=$user->team_id;
                $tname=Team::where('id',$tid)->select('name')->first();
                $teamname=$tname->name;
                $uname=$user->username;
                $operation='You Have Been Invited to join the team '.$teamname.'  by';
                $senduser=User::where('id',$user->id)->first();
                $senduser->notify(new InviteTeamNotify($user,$uname ,$operation));
                }
            }
            return redirect()->back();
    }
    public function delete(Request $request)
    {
        $id=$request->member_id;
        $teamdel = TeamMember::where('id',$id)->delete();


        return response()->json();
    }
    public function viewTeam($id)
    {
        $data = Team::with('teammember')->where(['id'=>$id,'status'=>'1'])->first();

        $tdata=DB::table('teammember')->leftjoin('user','user.id','=','teammember.user_id')->select('teammember.*','user.username')
        ->where('team_id',$id)->where('teammember.deleted_at',NULL)->get();

        return view('vendor/team/teamdetail',['team'=>$data,'member'=>$tdata]);
    }
    public function memberinvite()
    {
        $id = Auth::user()->id;
        $data = TeamMember::with(['team'])->where(['user_id' => Auth::user()->id])->get();


        $invites = [];

        foreach($data as $k => $v){
            $teammember_id=$v['id'];
            $memberstatus=$v['status'];
            $user_id = $v['team']['user_id'];
            $adminName = User::where('id' , $user_id)->first()->username;
            $userprofile = User::select('profile_image')->with('profileimg')->where('id',$user_id)->first();
            $team_id = $v['team']['id'];
            $countMember = TeamMember::where(['team_id' => $team_id , 'deleted_at' => null])->get()->count();
            $invites[] = [
                            'member_id'=>$teammember_id,
                            'name' => $v['team']['name'],
                            'created_at' => $v['team']['created_at'],
                            'adminName' => $adminName,
                            'profile_image' => $userprofile->profileimg[0]['path'],
                            'members' => $countMember,
                            'memberstatus'=>$memberstatus
                        ];
        }
        return view('vendor/team/teaminvite' , ["data" => $invites] );
	}
     public function declined(Request $request){      
        
        $declined_id = $request->declined_id;
       
        $data = TeamMember::where('id','=',$declined_id)->update(['status'=> TeamMember::status_declined]);
         $update =  TeamMember::select('id')->where(['id'=>$declined_id,'status'=>'0'])->first();
        if($declined_id!=NULL)
        {    
            if($update)
            {
                $operation = 'declined the invite team';
                $team_decline= TeamMember::with('team')->find($update->id);
                $userId=$team_decline->team->user_id;
               	$senduser= User::where('id',$userId)->first();
               	$uname=  User::where('id',$userId)->first()->username;
              	$senduser->notify(new DeclineInviteTeam($team_decline,$uname, $operation));     
            }
        }
         return response(['success'=>true]);
    }
    public function accept(Request $request)
    {       
        $accepted_id = $request->accepted_id;
       
        $data = TeamMember::where('id','=',$accepted_id)->update(['status'=> TeamMember::status_accepted]);
         $update =  TeamMember::select('id')->where(['id'=>$accepted_id,'status'=>'1'])->first();
        if($accepted_id != NULL)
        {    
            if($update)
            {
                $operation = 'Accept the invite team';
                $team_accept= TeamMember::with('team')->find($update->id); 
               	$uname = Auth::user()->username;
               	$userId = $team_accept->team->user_id;
               	$senduser= User::where('id',$userId)->first();
               	$uname=  User::where('id',$userId)->first()->username;
              	$senduser->notify(new AcceptInviteTeam($team_accept,$uname,$operation));    
            }
        }
         return response(['success'=>true]);
    }
}
