<?php

namespace App\Http\Controllers\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use App\Country;
use App\Address;
use App\Post;
use App\Job;
use App\State;
use App\City;
use App\Bussiness;
use App\Category;
use App\Hires;
use Response;
use Input;
use Validator;
use App\Attachment;
use App\PostAttachments;
use App\BusinessAttachment;
use Auth;
use DB;

class ProfileController extends Controller
{
    protected $x='';
    public function index(Request $request)
    {
          
        $id     =Auth::id();
        $data   = User::select('firstName','lastName','username','email','countryDialcode','phone','dob','profile_image','gender')->where('id', $id)->first();
        
        $attachment =\DB::table('attachment')->select('path')->where('id','=',$data['profile_image'])->first();
               
        $country= Country::all();
                                            
        return view('vendor.info.profile',['data'   => $data,
                                           'country'  => $country,'attachment'=>$attachment
                                           
                                          ]);
    }
     public function getprofileaddress(Request $request)
    {
        $id =Auth::id();
        if($request->isMethod('get'))
        {
            $getaddress= \DB::table('user')
                            ->leftjoin('address','user.address_id','=','address.id')
                            ->select('user.id as userid','address.*')
                            ->where('user.id',$id)->get()->toArray();
                            // dd($getaddress);
            $response   =   array("code" => "","success" => "","data"=>""); 
            if(!$getaddress) 
            {
                $response['code']   = 0;
                $response['success']= false;
                $response['data']   = '';
            } else 
            {   
                
                $response['code']   = 1;
                $response['success']= true;
                $response['data']   = $getaddress[0];
            }
            return response()->json($response);
            
        }
    }
    public function update(Request $request)
    {
        $id =Auth::id();
        if($request->isMethod('post'))
        {

            $data=Input::except(array('_token'));
                $rule=array(
                    'firstName'=>'Required_with:lastname|Alpha',
                    'lastName' =>'Required_with:firstName|Alpha',
                    'phone'     => 'required|min:10|max:12',
                    'dialCode'=>'required'
                );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('vendor.profile')->withErrors($validator)->with('profileRedirected','Please fill profile');
            }
           
            $id         =Auth::id();
            $data = ['firstName' => $request->firstName,
                    'lastName' => $request->lastName,
                    'gender' => $request->gender,
                    'countryDialcode'  =>  $request->dialCode,
                    'phone' => $request->phone,
                    'dob' => $request->dob,

                    ];

            $profilePrecentage = Auth::user()->profilePercent;
            if($profilePrecentage == 20||$profilePrecentage==0){
                $data['profilePercent'] = 30;
            }
            $user       = User::where('id',$id)->update($data);
            Session::flash('success','Successful Profile Updated ');
            if ($data) {
                    return response(['success'=>true]);   
                }else{
                    return redirect()->route('vendor.profile'); 
                }
        } 

    }
    public function mailing(Request $request)
    {  
        if($request->isMethod('post'))
        {
            $data=Input::except(array('_token'));
                $rule=array(
                            'country'   =>  'Required_with:state|Required_with:city',
                            'state'     =>  'Required_with:city|Required_with:country',
                            'city'      =>  'Required_with:country|Required_with:state',
                            'postalCode'=>  'Required|min:6|max:10',
                            'address'   =>  'Required',   
                           );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('vendor.profile')->withErrors($validator)->with('addressRedirected','Please fill address');
            }
            else
            {
                $id     =   Auth::id();
                $data   =   User::select('address_id')->where('id', $id)->first();
                $address=  [
                                            'country_id'    => $request->get('country'),
                                            'state_id'      => $request->get('state'),
                                            'city_id'       => $request->get('city'),
                                            'postalCode'    => $request->get('postalCode'),
                                            'address'       => $request->get('address')
                                            ];
                $profilePrecentage = Auth::user()->profilePercent;
                if($profilePrecentage == 30){
                    $data['profilePercent'] = 40;
                }
                if($data['address_id']==null)
                {
                    $address    =   Address::create($address);
                    $address->save();
                    $addressId  =   $address->id;
                    $address    =   User::select('address_id')->where('id',$id)
                                        ->update(['address_id'=>$addressId,
                                                 'profilePercent' => $data['profilePercent'],
                                                ]);
                }
            else{
                    $address    =   Address::where('id',$data['address_id'])
                                        ->update($address);
                }
                Session::flash('success','Mailing address has Saved');
                return redirect()->route('vendor.profile'); 
            }
        }   
    }
    public function info($id){
        $user_id =Auth::id();
        $data = User::select('id','firstName','lastName','username','email','phone','dob','address_id','profile_image')->where('id',$id)->first();
        $job = Job::select('id')->where('id', $id)->first();
        $jobs = Job::select('id','name','description','created_at','jobSuccess')->where('user_id', $user_id)->get();
        $hire = Hires::select('job_id')->where('user_id', $user_id)->get();
        $hires = count($hire);
        $business = Bussiness::select('id','name','description','category_id','address_id','ratePerHour','ratePerProject')->where('user_id', $id)->first();
        $category = Category::select('name')->where('id', $business['category_id'])->first();
        $address = Address::select('country_id','state_id','city_id')->where('id','=',$data['address_id'])->first();
        $country = Country::select('id','name')->where('id','=',$address['country_id'])->first();
        $state = State::select('name')->where('id','=',$address['state_id'])->first();
        $city = City::select('name')->where('id','=',$address['city_id'])->first();
        $attachment = Attachment::select('path')->where('id',$data['profile_image'])->first();
        $businessAttachments = \DB::table('BusinessAttachment')->leftjoin('attachment','attachment.id','=','businessAttachment.attachment_id')->select('attachment.id','attachment.path')->where('business_id','=',$business['id'])->get();

        $getfeedback= DB::table('feedback')->leftjoin('user','user.id','=','feedback.feedToId')->where('feedToId' , Auth::user()->id)->get();
        $getfeedback=json_decode($getfeedback,true);
        $processArray = [];
        $feedback_id = [];
        $countId = [];

        foreach ($getfeedback as $key => $value) {
            if(!in_array($value['id'], $feedback_id)){
                $countId[$value['id']] = 1; 
                $feedback_id[] = $value['id'];
                $processArray[$value['id']] = ['star_rate' => $value['starRate'] , 'count' => $countId[$value['id']]]; 
            }else{
                $lastStarRate = $processArray[$value['id']]['star_rate'];
                $lastcount = $countId[$value['id']];
                $countId[$value['id']] = $lastcount + 1; 
                $processArray[$value['id']] = ['star_rate' => ($lastStarRate + $value['starRate']) , 'count' => $countId[$value['id']]]; 

            }
        }
        $totalstars = array();
        foreach ($processArray as $key => $value) {
            $totalstar = 5*$value['count'];
            $totalstars[$key] = ['totalstars' => $totalstar, 'success_percent' => round(($value['star_rate']/$totalstar) * 100)];
        
        }
        return view('vendor/profile/index',['totalstars'=> $totalstars,'data'=>$data,'user_id'=>$user_id,'job'=>$job,'jobs'=>$jobs,'business'=>$business,'country'=>$country,'state'=>$state,'city'=>$city,'hires'=>$hires,'category'=>$category,'attachment'=>$attachment,'businessAttachments'=>$businessAttachments
        ]);
    }
    public function getjobs(Request $request,int $page){ 
        $user_id =Auth::id();

        if($request->limit == 0){
            $limit = 2;
        }else{
            $limit = $request->limit+2;
        }

        $paginate = Job::leftjoin('hires','hires.job_id','=','job.id')->select('job.id','job.name','job.maxRate','job.description','job.isOngoing','job.isHourly','job.user_id','job.budget','job.created_at','hires.status')->where('hires.user_id','=',$user_id);
        if ($request->filter == 'old') {    
            $paginate = $paginate->orderBy('hires.job_id', 'ASC')->limit($limit)->get();
            // $paginate = $paginate->orderBy('hires.job_id', 'ASC')->paginate(2,['*'], 'page',$page);
        }else{
            $paginate = $paginate->orderBy('hires.created_at', 'desc')->limit($limit)->get();
            // $paginate = $paginate->orderBy('hires.created_at', 'desc')->paginate(2,['*'], 'page',$page);
        }
            // $paginate->withPath(route('vendor.getjobs', ['page'=> intval($page + 1)]));

        echo json_encode($paginate, true);
    }

    public function postinfo() {
        $id =Auth::id();
        $post =\DB::table('post')
                            ->leftjoin('postattachments','post.id','=','postattachments.post_id')->leftjoin('attachment','attachment.id','=','postattachments.attachment_id')->select('post.id','post.title','post.message','post.type','post.user_id','attachment.path','attachment.name','postattachments.post_id','postattachments.attachment_id')->where('post.user_id','=',$id)->orderBy('post.id','DESC')->get();
                          
        echo json_encode($post, true);
    }

    public function deletepostinfo(Request $request) {

        $id =Auth::id();
        $postId = $request->postId;
        $attachmentId = $request->attachmentId;
        $filename = $request->filename;
        $post =\DB::table('post')->where('id','=',$postId)->delete();
        if ($filename != "") {
            $posta =\DB::table('postattachments')->where('attachment_id','=',$attachmentId)->delete();
            $attach =\DB::table('attachment')->where('id','=',$attachmentId)->delete();
            @unlink(public_path('/uploads/vendor/'.$id.'/'.$filename));
        }
        return response('success');
    }
    public function removePostimages(Request $request){
      
        $id = Auth::user()->id;
        $post_id = $request->remove_id;
        $attachment_id = $request->attachment_id;
        $paths = $request->paths;
        $filename = $request->filename;
        $data = \DB::table('postattachments')->where('attachment_id','=',$attachment_id)->where('post_id','=',$post_id)->delete();
        unlink(public_path('/uploads/vendor/'.$id.'/'.$filename));
        return response('success');
    }
    public function removePostvideo(Request $request){
      
        $id = Auth::user()->id;
        $post_id = $request->remove_id;
        $attachment_id = $request->attachment_id;
        $paths = $request->paths;
        $filename = $request->filename;
        $data = \DB::table('postattachments')->where('attachment_id','=',$attachment_id)->where('post_id','=',$post_id)->delete();
        unlink(public_path('/uploads/video/vendor/'.$id.'/'.$filename));
        return response('success');
    }
    public function uploadprofiles(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/vendor/profile/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $file = Input::file('file');
        $ext = $file->getClientOriginalExtension();
        $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
        $file = $file->move($destinationPath, $fileName);

        $hasattachment_id = User::select('profile_image')->where('id','=',$id)->first();
        if ($hasattachment_id['profile_image'] == "") {
        
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $attachment_id = $data->id;
            $datas = user::where('id',$id)->update(
                         ['profile_image' => $attachment_id
                        ]);
            
        }else{
             
        $data = Attachment::where('id',$hasattachment_id['profile_image'])->update(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
           
        }
        return response(['success'=>true]);
        
    }


    public function uploadimageText(Request $request){
        $user_id =Auth::id();
        $type = '2';
                $data = Post::create(
                         ['title' => $request->imagetitle,
                          'message' => $request->imagemessage,
                          'type' => $type,
                          'user_id' => $user_id
                        ]);
                $data->save();
                $request->session()->put('PostId', $data->id);

                    Session::flash('success');
                if ($data) {
                    return response(['success'=>true]);   
                }else{
                    return response(['success'=>false]);
                }

    }
    
    public function uploadfiles(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/vendor/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $post_id = $request->session()->get('PostId');
            $attachment_id = $data->id;
            $datas = PostAttachments::create(
                         ['post_id' => $post_id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save();
            
        }
       
        return response(['success'=>true]);
        
    }

    public function imageUploadfile(Request $request){
        $id = Auth::user()->id;
        $files  = json_decode($request->Input('hid'));
            
                foreach ($files as $key => $file) {
                    $attach =new Attachment();
                    $name   = $file->originalName;
                    $attach->name   =$name;
                    $attach->path   = $file->path;
                    $attach->mimeType= $file->mimeType;
                    $attach->job_id = $id;
                    $attach->save();  
                };
    }

    public function uploadvideoText(Request $request){
        $user_id =Auth::id();
        $type = '3';
                $data = Post::create(
                         ['title' => $request->videotitle,
                          'message' => $request->videomessage,
                          'type' => $type,
                          'user_id' => $user_id
                        ]);
                $data->save();
                $request->session()->put('PostId', $data->id);

                    Session::flash('success');
                if ($data) {
                    return response(['success'=>true]);   
                }else{
                    return response(['success'=>false]);
                }

    }
    
    public function uploadfilesv(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/video/vendor/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            if ($ext == 'mp4') {
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $post_id = $request->session()->get('PostId');
            $attachment_id = $data->id;
            $datas = PostAttachments::create(
                         ['post_id' => $post_id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save();
            }else {
                return response(['error'=>false]);
            }
            
        }
       
        return response(['success'=>true]);
        
    }
    public function videoUploadfile(Request $request){
        $id = Auth::user()->id;
        $files  = json_decode($request->Input('hid'));
            
                foreach ($files as $key => $file) {
                    $attach =new Attachment();
                    $name   = $file->originalName;
                    $attach->name   =$name;
                    $attach->path   = $file->path;
                    $attach->mimeType= $file->mimeType;
                    $attach->job_id = $id;
                    $attach->save();  
                };
    }    

    public function create(Request $request){
        $user_id =Auth::id();
       
        $title=$request->input('title');
        
        if($title)
        {
        $request->validate([
                            'title' => 'required',
                            'message' => 'required'
                            ]);
        $message=$request->input('message');
        $data = Post::create(
                 ['title' => $title,
                  'message' => $message,
                  'type' => 1,
                  'user_id' => $user_id,
                ]);
        $data->save();
            Session::flash('success');
            
        }

        $link=$request->input('link');
         if($link)
        {
             $request->validate([
                            'link' => 'required'
                            ]);
        $data = Post::create(
                 ['title' => $link,
                  'message' => "",
                  'type' => 4,
                  'user_id' => $user_id,
                ]);
        $data->save();
            Session::flash('success');
            return back();
        }
        return back();
    }
    
}
    