<?php

namespace App\Http\Controllers\vendor;

use App\Bid;
use App\Notifications\TestClientNotif;
use App\Notifications\TestNotif;
use App\Reasons;
use App\Statuses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hires;
use App\Category;
use App\User;
use App\Attachment;
use Auth;

class HireController extends Controller
{
    public function index() {
        $vendor_id = Auth::id();
        $hire_list = Hires::where([['user_id',$vendor_id], ['status', '=', '2'],['team_id', '=', NULL]])->with('job')->get();
        $teamhirelist =  Hires::where([['user_id',$vendor_id], ['team_id', '!=', NULL]])->orWhere('user_id',NULL)->where('status', '=', '2')->with('job')->get();
        return view('vendor.hire.list', [
            'hires'     => $hire_list,
            'teamhire'  =>  $teamhirelist
        ]);
    }
    public function show($id)
    {
        $hire_proposal = Hires::with('job')->find($id);
        $cat= Category::where('id',$hire_proposal->job->category_id)->select('id','name')->first();
        $reasons       = Reasons::getReasons('declined');
        $bid           = Bid::getJobBid($hire_proposal->user_id, $hire_proposal->job_id);
        $user=User::where(['id'=>$hire_proposal->job->user_id])->select('profile_image')->first();
        if($user!=NULL)
        {
          $attachment= Attachment::select('path')->where('id',$user->profile_image)->first();
        }else
        {
          $attachment = 'null';
        }
       
        if($hire_proposal == null) {
            abort(404);
        }
        return view('vendor.hire.detail', [
            'hire'    => $hire_proposal,
            'bid'     => $bid,
            'reasons' => $reasons,'category' =>$cat,'attach' =>$attachment
        ]);
    }
    public function accept(Request $request, $hire_id)
    {
       if($request->get('check') == 'on') {
           $message = $request->get('message');
           if($message != null) {
               //save message
           }
           // $status_name = 'Accept';
           $status_name =Hires::status_opened;
           $operation = 'Accepted hire offers';
           $hire_proposal = Hires::with('job')->find($hire_id);
           Hires::updateStatus($hire_id,$status_name,$message);
           // Hires::updateStatus($hire_id, Statuses::getStatusNumberByName('hires', $status_name), $message);
           auth()->user()->notify(new TestNotif($hire_proposal, $operation));
           $hire_proposal->job->user->notify(new TestClientNotif($hire_proposal, $operation));
           return redirect()->route('vendor.hire.list')
               ->with('hire_flash_accept', 'Offer is accepted');
       }
    }
    public function decline(Request $request, $hire_id)
    {
        if($request->get('reason') != null) {
            // $status_name = 'Declined';
            $status_name=Hires::status_close;
            $message = $request->get('message');
            $reason = $request->get('reason');
            Hires::updateStatus($hire_id,$status_name,$message, $reason);
            // Hires::updateStatus($hire_id, Statuses::getStatusNumberByName('hires', $status_name), $message, $reason);
            $hire_proposal = Hires::with('job')->find($hire_id);
            $operation = 'Declined hire offers';
            auth()->user()->notify(new TestNotif(Hires::find($hire_id), $operation));
            $hire_proposal->job->user->notify(new TestClientNotif($hire_proposal, $operation));
            return redirect()->route('vendor.hire.list')
                ->with('hire_flash_decline', 'Hire proposal declined');
        }
    }
}
