<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bussiness;
use App\User;
use Input;
use Response;
use Session;
use Validator;
use Auth;

class PaymentController extends Controller
{
   public function index(){
    $id     =Auth::id();
    $data= Bussiness::where('user_id', $id)->select('rateperHour','rateperProject')->first();
    	return view('vendor.info.payment',['data' =>$data]);
    }
    public function addpayment(Request $request)
    {
      if($request->isMethod('post'))
      {
        $data=Input::except(array('_token'));
            $rule=array(
                        'rateperHour'   =>  'Required',
                        'rateperProject'     =>  'Required',
                       );
        $validator=Validator::make($data, $rule);
        if($validator->fails()) {
            return redirect()->route('vendor.payment')->withErrors($validator)->with('paymentRedirected','Please fill payment');
        }
    	  $id = Auth::id();
        $hasBussiness = Bussiness::where('user_id', $id)->first();
        $rateperHour=$request->get('rateperHour');
        // $currencyPerhour 	= 	$request->get('currencyPerhour');
        // $rateperHour 	 	=	$currencyPerhour.'-'.$rateperHour;
        $rateperProject  	=	$request->get('rateperProject');
        // $currencyPerproject = 	$request->get('currencyPerproject');
        // $rateperProject 	=	$currencyPerproject.'-'.$rateperProject;
       	$data = array(
       					'ratePerHour' 		=>$rateperHour,
       					'ratePerProject' 	=>$rateperProject
    				);
       	$profilePrecentage = Auth::user()->profilePercent;
        if($profilePrecentage == 50)
        {
          $user = User::where('id',$id)->update(['profilePercent'=>60]);
        }  
        $business= Bussiness::where('user_id',$id)->update($data);
        Session::flash('success','Successful Payment Updated ');
        return redirect()->route('vendor.dashboard');
      }
    }
}
