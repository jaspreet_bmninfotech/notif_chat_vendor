<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\DeclineClientJob;
use Auth;
use DB;
use App\Invites;
use App\User;
use App\Job;
use App\Team;

class InvitesController extends Controller
{
    public function list(Request $request)
    {
        $id = Auth::user()->id; 
        $invite=Invites::leftjoin('job','job.id','=','invites.job_id')->select('invites.job_id','invites.invite_Message','job.status','invites.user_id','invites.created_at','job.name','invites.status as clientStatus')->where(['invites.user_id'=>$id,'invites.team_id' => NULL])->orderBy('job.id', 'desc')->paginate(5,['*'], 'invite');  
        $teaminvite=Invites::leftjoin('job','job.id','=','invites.job_id')->join('team','team.id','=','invites.team_id')
        ->select('invites.job_id','invites.invite_Message','job.status','invites.user_id','invites.created_at','job.name','invites.status as clientStatus','team.id as tid','team.name as tname')->where('invites.team_id', '!=', NULL)->orderBy('invites.id', 'desc')->paginate(5,['*'], 'teaminvite');
        $request->session()->put('teamId', $teaminvite);    
            return view('vendor.invites.list', ['invite' => $invite,'teaminvite' => $teaminvite]);
    }
     public function detail($id){
        $user_id = Auth::user()->id;
        $hasdata = Invites::select('job_id')->where('job_id','=',$id)->where('user_id','=',$user_id)->get();
        foreach ($hasdata as $key => $value) {
        if ($value['job_id'] == $id) {
        $data = DB::table('job')->leftjoin('invites as vi','vi.job_id','=','job.id')->select('job.name','job.created_at','job.status','vi.invite_message','job.id')->where('job.id','=',$id)->first();
        $user =User::select('firstName','lastName','username','email')->where('id',$user_id)->first();
        return view('vendor.invites.detail',['data' => $data]);
        }else{
        return redirect()->route('vendor.invites.list');
        }
        }
    }
    public function declined(Request $request){      
        $declined_id = $request->declined_id;
        $team_id = $request->teamId;
       
        $user_id = Auth::user()->id;    
        if($declined_id!=NULL)
        { 
        if(empty($team_id))   
        {
        $data = Invites::where('user_id','=',$user_id)->where('job_id','=',$declined_id)->update(['status'=> Invites::status_declined]);  
        }
        else
        {
             $data = Invites::where('team_id','=',$team_id)->where('job_id','=',$declined_id)->update(['status'=> Invites::status_declined]);
        }
         $update =  Invites::select('id')->where(['job_id'=>$declined_id,'user_id'=>$user_id,'status'=>'0'])->first(); 
            if($update)
            {
                $operation = 'declined the invite on job';
                $vendor_declinejob= Invites::with('job')->find($update->id); 
               $uname=Auth::user()->username;
              $vendor_declinejob->job->user->notify(new DeclineClientJob($vendor_declinejob,$uname, $operation));     
            }
        }
         return response(['success'=>true]);
    }

}
