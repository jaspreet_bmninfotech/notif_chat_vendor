<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\ChatEvent;
use App\Hires;
use App\Job;
use App\Attachment;
use App\JobTransaction;
use App\Chat;

class chatController extends Controller
{
 
    public function chat($id)
    {
        $profileId = Auth::user()->profile_image;
        $user = Attachment::where('id',$profileId)->first();
        if( $user != null){
            if( array_key_exists('path' , $user )){
                Auth::user()->profilePath = $user->path ;
            }else{
                Auth::user()->profilePath = '';
            }
        }
        
            $Job = JobTransaction::where(function( $query ){
                $query->where('from' ,'=', Auth::user()->id)->orWhere('to' ,'=', Auth::user()->id );
            })->with(['hires' => function($query){
                $query->with('job');
            }])->get();
            // dd(Attachment::select('path')->where('id', Auth::user()->profile_image)->first());
            $prevChat = Chat::where('transaction_id' , $id)->get();
    	return view('chat.chat',compact('id', 'Job','prevChat'));
    }

    public function send(Request $request)
    {	
    	return event( new ChatEvent( $request->message , Auth::user()->id, $request->transaction_id ));
    }
}
