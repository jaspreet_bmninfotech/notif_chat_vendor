<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\TourGuideInviteJob;
use App\Notifications\TourGuideHireNotify;
use App\User;
use App\Country;
use App\State;
use App\SearchTourguide;
use App\TourGuideJob;
use App\TourGuideHire;
use App\TourGuideInvites;
use App\Job;
use App\Bid;
use App\Hires;
use App\Category;
use App\Invites;
use Response;
use App\Attachment;
use App\Timezone;
use App\JobAttachment;
use App\Setting;
use Auth;
use Route;
use Session;
use Input;
use DB;

class searchController extends Controller
{
    public function tourguide(){
        // $Authdate=Auth::user()->date;
        // dd($Authdate);
        $type=Auth::user()->type;
        // $date= date("m-d-Y",strtotime($Authdate));
        $country= Country::all(); 
        return view('search/tourguide',['country'   => $country,
                                        // 'date'      => $date,
                                         'type' =>$type
                                         ]);
    }
    public function createtourguide(Request $request){
         $id=Auth::user()->id;
         $title = Input::get('title');
         $fromcountry   = Input::get('fromcountry');
         $fromstate     = Input::get('fromstate');
         $fromcity      = Input::get('fromcity');
         $tocountry     = Input::get('tocountry');
         $tostate       = Input::get('tostate');
         $tocity        = Input::get('tocity');
         $fromdate      = Input::get('fromdate');
         $todate        = Input::get('todate');
         $totalpeople   = Input::get('people');
         if ($request->isMethod('post')) {
            $request->validate([
                            'title'         => 'required',
                            'fromcountry'   => 'required',
                            'fromstate'     => 'required',
                            'fromcity'      => 'required',
                            'tocountry'     => 'required',
                            'tostate'       => 'required',
                            'tocity'        => 'required',
                            'fromdate'      => 'required',
                            'todate'        => 'required',
                            'people'        => 'required',
                            ]);
            $data = TourGuideJob::create(['user_id' => $id,
                             'title' => $title,
                             'from_country_id' => $fromcountry,
                             'from_state_id'   => $fromstate,
                             'from_city_id'    => $fromcity,
                             'to_country_id'   => $tocountry,
                             'to_state_id'     => $tostate,
                             'to_city_id'      => $tocity,
                             'fromdatetime'    => $fromdate,
                             'todatetime'      => $todate,
                             'totalpeople'     => $totalpeople,
                             'status'          =>  TourGuideJob::status_open 
                         ]);
            $data->save();
            //  $tourguidejobid = $data->id;
            //  $datas = TourGuideHire::create(
            //              ['user_id' => $id,
            //               'tourguide_job_id' => $tourguidejobid,
            //             ]);
            // $datas->save();
         }
         $type=Auth::user()->type;
         $country = Country::select('name')->where('id',$tocountry)->first();
            Session::flash('success','Your Job is successfully Added ');
            //return back();
              return redirect()->route('tourguidefilter',['country'=>$country['name']]);
    }
     public function searchtourguides()
    {
        $category   = Country::select('id','name','sortName','phoneCode')->get();
        if(Auth::User()){
            if(Auth::User()->type === 'vn'){
                return view('search/searchTourGuides',['category' => $category ]);
            }
            elseif(Auth::User()->type === 'cn'){
                return view('search/searchTourGuides',['category' => $category ]);
            }
            elseif(Auth::User()->type === 'tg'){
                return redirect()->route('tourguide');
            }
        }
        return view('search/searchTourGuides',['category' => $category ]);
    }
    public function searchtourguidesfilter(Request $request){
        $addcountry = Input::get('country');
        return redirect()->route('tourguidefilter',['addedcountry'=>$addcountry]);
    }
    public function tourguidefilter(Request $request,$addedcountry){
       $search_params = $request->all();
       if (sizeof($search_params) > 0)
       {
           $data['all_tourguide'] = SearchTourguide::search_tourguides($search_params);
        }
       else
       {
           $data['all_tourguide'] = SearchTourguide::all_records();
        }
       $all_countries = Country::get();
       $data['all_countries'] = $all_countries->toArray();
       $data['search_params'] = $search_params;
       // $data['vendor_all_jobs'] = \App\SearchTourguide::all_records(5);
       $data['vendor_all_jobs'] = TourGuideJob::where(['user_id'=> Auth::id()])->select('id','title')->get();
       $data['all_categories']  = Category::all_records(5);
        $country    = Country::all(); 
       $countryId  = Country::select('id')->where('name',$addedcountry)->first();
       $state      = State::select('id','name','country_id')->where('country_id',$countryId['id'])->get();
               $totalstars = array();
            $freelancers = DB::table('user as u')
                    ->selectRaw('u.id, u.firstName, u.lastName, u.username, u.success_rate, u.profile_image,attachment.path, bz.description as business_desc, bz.ratePerHour, bz.info,tour_guide_info.*,address.*')
                    ->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
                    ->leftjoin('tour_guide_info', 'tour_guide_info.user_id', '=', 'u.id')
                    ->leftjoin('address', 'address.id', '=', 'u.address_id')
                    ->leftjoin('attachment', 'attachment.id', '=', 'u.profile_image')
                    ->where('u.id' ,'!=', null )
                    ->get();
                    $countyTourguide = $freelancers->where('country_id' , $countryId );
     
           foreach($data['all_tourguide'] as $k => $v){
               $getfeedback= DB::table('feedback')->leftjoin('user','user.id','=','feedback.feedToId')->where('feedToId' , $v->id)->get();
               $getfeedback=json_decode($getfeedback,true);
               $processArray = [];
               $feedback_id = [];
               $countId = [];
               foreach ($getfeedback as $key => $value) {
                   if(!in_array($value['id'], $feedback_id)){
                       $countId[$value['id']] = 1;
                       $feedback_id[] = $value['id'];
                       $processArray[$value['id']] = ['star_rate' => $value['starRate'] , 'count' => $countId[$value['id']]];
                   }else{
                       $lastStarRate = $processArray[$value['id']]['star_rate'];
                       $lastcount = $countId[$value['id']];
                       $countId[$value['id']] = $lastcount + 1;
                       $processArray[$value['id']] = ['star_rate' => ($lastStarRate + $value['starRate']) , 'count' => $countId[$value['id']]];

                    }
                }
               foreach ($processArray as $key => $value) {
                   $totalstar = 5*$value['count'];
                   $totalstars[$key] = ['totalstars' => $totalstar, 'success_percent' => round(($value['star_rate']/$totalstar) * 100)];
         
               
               }
           }
               return view('search/TourGuideFilter',['stars' => $totalstars,'addedcountry'=>$addedcountry,'country' => $country,'state'=>$state,'data' =>$data
               ]);
       
    }    
        // $search_params = $assinedParams;
        // if (sizeof($search_params) > 0)
        // {
        //     $data['all_tourguide'] = SearchTourguide::search_tourguides($search_params);
        // }
        // else
        // {
        //     $data['all_tourguide'] = SearchTourguide::all_records();
        // }
    //     $all_countries = Country::get();
    //     $data['all_countries'] = $all_countries->toArray();
    //     $data['search_params'] = $search_params;
    //     // $data['vendor_all_jobs'] = \App\SearchTourguide::all_records(5);
    //     $data['vendor_all_jobs'] = TourGuideJob::where(['user_id'=> Auth::id()])->select('id','title')->get();
    //     $data['all_categories']  = Category::all_records(5);
    //     $country    = Country::all(); 
    //     $countryId  = Country::select('id')->where('name',$addedcountry)->first();
    //     $state      = State::select('id','name','country_id')->where('country_id',$countryId['id'])->get();
    //     if (sizeof($search_params) > 0)
    //     {
    //         echo json_encode($data['all_tourguide']);
    //     }
    //     else
    //     {
    //         return view('search/TourGuideFilter', [ 'addedcountry'=>$addedcountry ,'country' => $country ,'state'=>$state ,'data' =>$data ]);
    //     }
  
    public function filterTourGuide($requestedData , $countryId)
    {
        $country    = Country::all(); 
        $state      = State::select('id','name','country_id')->where('country_id',$countryId)->get();
        $freelancers = DB::table('user as u')
                ->selectRaw('u.id, u.firstName, u.lastName, u.username, u.success_rate, u.profile_image,attachment.path, bz.description as business_desc, bz.ratePerHour, bz.info,tour_guide_info.*,address.*')
                ->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
                ->leftjoin('tour_guide_info', 'tour_guide_info.user_id', '=', 'u.id')
                ->leftjoin('address', 'address.id', '=', 'u.address_id')
                ->leftjoin('attachment', 'attachment.id', '=', 'u.profile_image')
                ->where('u.id' ,'!=', null )
                ->get();
                $countyTourguide = $freelancers->where('country_id' , $countryId );

                $freelancers = $freelancers->where('country_id', $countryId);

                if(array_key_exists('filter_state', $requestedData)){
                    $freelancers = $freelancers->where('state_id', $requestedData['filter_state']);
        }
                if(array_key_exists('filter_city', $requestedData)){
                    $freelancers = $freelancers->where('city_id', $requestedData['filter_city']);
    }
                if(array_key_exists('filter_name', $requestedData)){
                    $freelancers = $freelancers->where('username', $requestedData['filter_name']);
                }
                // $freelancers['all_tourguide'] = $countyTourguide;
                // $freelancers['vendor_all_jobs'] = TourGuideJob::where(['user_id'=> Auth::id()])->select('id','title')->get();
                return json_encode([ 'addedcountry'=>$countryId ,'country' => $country ,'state'=>$state ,'data' =>$freelancers ]);
    }
     public function searchtourguide_details(Request $request)
    {
        $params     = Route::current()->parameters();
        $details    = SearchTourguide::freelancer_details($params['id']);
        echo json_encode(array("status" => 1, "data" => $details));die;
    }
    public function invite_to_job(Request $request)
    {
        $params = array();
        parse_str($request->input('form_data'), $params);
        try
        {
            $params['user_id'] = $params['invitation_rcvr_id'];
            $params['job_id']  = $params['invitation_jobs'];
            $params['invite_message'] = $params['invitation_message'];
            $is_invite_exist = TourGuideInvites::is_invite_exist($params['job_id'], $params['user_id']);
            if ($is_invite_exist)
            {
                $ret_data = array("status" => 0, "message" => "Your have already sent an invitation for this job.");
            }
            else
            {
                $insert_id = TourGuideInvites::send_invite($params);
                $ret_data  = array("status" => 1, "message" => "Your invitation sent successfully");
                $operation = 'Invited For Job';
                $invite_tourguide= TourGuideInvites::with('tourguidejob')->find($insert_id); 
                $uname =Auth::user()->username;                 
                $invite_tourguide->user->notify(new TourGuideInviteJob($invite_tourguide,$uname, $operation));
            }
        }
        catch (Exception $e)
        {
            $ret_data = array("status" => 0, "message" => "Unable to send invitation. Please try again.");
        }
        return $ret_data;
    }

    public function list(Request $request)
    {
        $user_id =Auth::id();
        $result =\DB::table('user as u')->leftjoin('tourguidejob as tgj','tgj.user_id','=','u.id')->select('tgj.id','tgj.title','tgj.status','tgj.created_at')->where('tgj.user_id','=',$user_id)->paginate(5);
        // $data = json_decode( json_encode($result), true);
        $user_type =Auth::User()->type;
        if ($user_type == 'vn') {
        return view('vendor/tourguide/list',['data'=>$result]);
        }else {

        return view('client/tourguide/list',['data'=>$result]);
        }
    }
    public function detail(Request $request, $id){
        $data['job_details'] = TourGuideJob::job_details($id);

        if (!$data['job_details'])
        {
            return back()->withErrors(array('err_msg' => 'Job details not found.'));
        }
        $data['job_proposals']  = TourGuideJob::job_proposals($id);
       $tour=TourGuideInvites::with(['user','tourguidejob'])->where('tourguide_job_id',$data['job_details']->id)->first();
       if($tour->user->profile_image!=NULL)
       {
        $path= Attachment::where('id',$tour->user->profile_image)->select('path')->first();

        }
        else{
            $path= NULL;
        }
        $data['attach'] = $path;
        $data['all_hires']      = TourGuideJob::job_hires($id);


        $data['hire'] = TourGuideHire::where('tourguide_job_id',$id)->first();
        
        $data['total_bids']     = Bid::select('user_id')->where(array('job_id' => $id))->count();
        $user_type =Auth::User()->type;
        if ($user_type == 'vn') {
            return view('vendor/tourguide/detail')->with('data', $data);
        } else{
            return view('client/tourguide/detail')->with('data', $data);
        }

    }
    public function hire(Request $request, $job_id, $user_id, $tgi_id)
    {
        $data['tgi_id'] = $tgi_id;

        $data['user_details']   = User::find($user_id);
        if($data['user_details']->profile_image!=NULL)
        {
             $path= Attachment::where('id',$data['user_details']->profile_image)->select('path')->first();
        }
        else
        {
            $path= NULL;
        }
         $data['attach'] = $path;
        $data['job_details']    = TourGuideJob::job_details($job_id);
         $user_type =Auth::User()->type;
        if ($user_type == 'vn'){
        return view('vendor.tourguide.hire_process')->with('data', $data);
         } else{
             return view('client.tourguide.hire_process')->with('data', $data);
         }
    }
    public function hire_confirm(Request $request, $job_id, $user_id)
    {
        $request->validate([
            'hire_tnc' => 'required'
        ]);
        $data = TourGuideJob::job_details($job_id);
        $tgi_id = $request->Input('tgi_id');
        $tid = TourGuideInvites::find($tgi_id);
        $user_type =Auth::User()->type;
        if ($data && $data->user_id == Auth::user()->id)
        {  
            $insert_id = TourGuideHire::save_hire($data, $user_id, $tid);
            if ($insert_id)
            {
                $operation      = 'Accepted hire';
                $hire_guide     = TourGuideHire::with('tourguidejob')->find($insert_id);
                $hireuserId     = $hire_guide->user_id;
                $s_user         = User::where('id',$hireuserId)->first();
                $send_uid       = $hire_guide->tourguidejob->user_id;
                $senduser       = Auth::user()->username;
                $s_user->notify(new TourGuideHireNotify($hire_guide,$senduser,$operation));
                $request->session()->flash('hire_flash_success_msg', 'Person hired successfully.');
                 
                 if ($user_type == 'vn'){
                return redirect('vendor/tourguide/' . $job_id.'/detail');
                }else{
                     return redirect('client/tourguide/' . $job_id.'/detail');
                }
            }
            $request->session()->flash('hire_flash_err_msg', 'Error occured while hiring process. Please Try again.');
        }
        else
        {
            $request->session()->flash('hire_flash_err_msg', 'Unauthorized Request.');
        }
        if ($user_type == 'vn'){
        return redirect('vendor/hire/' . $job_id . '/' . $user_id);
        }else{
             return redirect('client/hires/' . $job_id . '/' . $user_id);
        }
    }
    public function edit($id){
        $data['job_details']    = TourGuideJob::job_details($id);
        $data['all_hires']      = TourGuideJob::job_hires($id);
        $data['job_proposals']  = TourGuideJob::job_proposals($id);
        $data['total_bids']     = Bid::select('user_id')->where(array('job_id' => $id))->count();
        $user_type = Auth::User()->type;
        if ($user_type == 'vn') {
            return view('vendor/tourguide/edit')->with('data', $data);
        } else{
            return view('client/tourguide/edit')->with('data', $data);
        }
    }
    public function update(Request $request,$id){
        $data = TourGuideJob::where('id',$id)->update(['title'=>$request->title,'totalpeople'=>$request->totalpeople,'fromdatetime'=>$request->fromdate,'todatetime'=>$request->todate]);
        return back();
    }
    public function removepost(Request $request){
       $id= $request->id;
        $response = array();
        if($id)
        {
           $data = TourGuideJob::where('id',$id)->update(['status'=>TourGuideJob::status_close]);
            $response['status']  = 'success';
            $response['message'] = 'Job Closed Successfully ...';
        }
         else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to Closed Job ...';
        }
        echo json_encode($response);
    }
    public function getvendors(int $page){
        $paginate = \DB::table('user')->leftjoin('job','job.user_id','=','user.id')->leftjoin('attachment','attachment.id','user.profile_image')->leftjoin('bussiness','bussiness.user_id','user.id')->select('job.id','job.name','job.maxRate','job.minRate','job.isOngoing','job.isHourly','job.user_id','job.budget','job.created_at','user.username','user.address_id','attachment.path','attachment.id','bussiness.description','bussiness.job_success_rate')->where('user.type','tg')->paginate(3,['*'], 'page',$page);
        $paginate->withPath(route('searchtourguide', ['page'=> intval($page + 1)]));
        echo json_encode($paginate, true);
    }

}
