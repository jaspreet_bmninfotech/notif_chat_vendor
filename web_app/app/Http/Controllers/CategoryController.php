<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Response;

class CategoryController extends Controller
{
    public function create(Request $request,$id)
    {
		$data =\DB::table('category')->select('name','id')->where('parent_id',$id)->get();
			
		return response()->json($data);	
    }
     public function createsubcat(Request $request,$id)
    {
		$data =\DB::table('category')->select('name','id')->where('parent_id',$id)->get();
			
		return response()->json($data);	
    }
}
