<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Session;
use Hash;
use App\User;
use App\Country;
use App\Address;
use App\TourGuide;
use App\TourGuideLanguage;
use Input;
use Validator;
use App\Attachment;
use Auth;
class changePasswordController extends Controller
{
    public function selectchangePassword(){
        return view('/changePassword/changePassword');
    }

    public function createchangePassword(Request $request){
        if($request->isMethod('post'))
        {
             $user_id = Auth::user()->id;
            $selectoldPassword = User::select('password')->where('id','=',$user_id)->first();
            $oldPassword          = Input::get('oldPassword');
            $newPassword          = Input::get('newPassword');
            $confirmNewPassword   = Input::get('confirmNewPassword');
            $request->validate([
                'oldPassword' => 'required',
                'newPassword' =>'required|string|min:6',
                'confirmNewPassword' =>'required|string|min:6',
            ]);            
            if (Hash::check($oldPassword, $selectoldPassword['password'])) {
                if ($newPassword == $confirmNewPassword) {
                        $password = bcrypt($newPassword);
                        $confirmpassword = bcrypt($confirmNewPassword);
                        $user = User::where('id',$user_id)->update(['password' => $password,
                                                                   'confirmpassword' => $confirmpassword]);
            
            Session::flash('success','Password change Successfully.');                 
            return view('/changePassword/changePassword');
            }else{
            Session::flash('passerror','your password does not match.');                 
            return view('/changePassword/changePassword');
        }
        }else{
            Session::flash('passerror','your password does not match.');                 
            return view('/changePassword/changePassword');
        }
        }
        return view('/changePassword/changePassword');
    }
}
