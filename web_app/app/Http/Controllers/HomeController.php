<?php

namespace App\Http\Controllers;

use Auth;
use App\Category;
use App\Setting;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class HomeController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo = '/';
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::User()){
            if(Auth::User()->type === 'vn'){
                return redirect()->route('vendor.dashboard');
            }
            elseif(Auth::User()->type === 'cn'){
                return redirect()->route('client.job');
            }
            elseif(Auth::User()->type === 'tg'){
                return redirect()->route('tourguide.profile');
            }
        }
        $category   = Category::select('id','name','shortCode','parent_id')
                        ->where('parent_id','=',null)->paginate(8);
        $catsetting = Setting::select('id','key','value')->where('key','home')->first();
        $catdecode = unserialize($catsetting['value']);
        $homecategory = [];
        if(!empty($catdecode)){
           foreach ($catdecode['home_category'] as $key => $value) {
               $homecategory[] = Category::select('id','name')->where('id',$value)->where('parent_id','=',NULL)->first();                
           }
       }
        return view('index',['category' => $category,'homecategory' => $homecategory]);
    }
    public function category(){
        $category   = Category::select('id','name','shortCode','parent_id')
                        ->where('parent_id','=',null)->get();
                        echo json_encode($category, true); 
    }
    // public function freelancercategories()
    // {

    //     $category = Category::select('id','name','shortCode','parent_id')
    //                     ->where('parent_id',null)->get()->toArray();
    //     $sub_category=[];
    //        foreach($category as $key=>$val)
    //        {
    //             $sub_category[$val['name']] = Category::select('id','name','shortCode','parent_id')->where('parent_id',$val['id'])->get()->toArray();
    //        }
    //         return view('browseAllCategories',['sub_category' => $sub_category]);
    // }
    public function freelancercategories($parent_id = null, $user_tree_array = '') 
    {
        $category = Category::select('id','name','shortCode','parent_id')->where('parent_id',null)->get()->toArray();
        $data = [];
            foreach($category as $k => $cat){
                $subCategory = Category::select('id','name','shortCode','parent_id')->where('parent_id',$cat['id'])->get()->toArray();
                foreach ($subCategory as $key => $sub_subcategory) {
                    $sub_sub_category[$sub_subcategory['name']] = Category::select('id','name','shortCode','parent_id')->where('parent_id',$sub_subcategory['id'])->get()->toArray();
                }
                $data[][$cat['name']]= [ 'sub_sub_category' => $sub_sub_category ];
            }
            // dd($data);
            return view('browseAllCategories',['data' => $data]);
    }

    public function homesubcat($id)
    {   

        $name = Category::select('id','name')->where('id',$id)->first();
        $data = Category::select('id','name')->where('parent_id',$id)->with('childcategory')->get()->toArray();
        return view('homeCategories',['data' => $data,'name' => $name]);
    }
    
}