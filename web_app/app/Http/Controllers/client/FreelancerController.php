<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;
use App\Invites;
use App\User;
use App\Job;
use App\Bid;
use App\Hires;
use App\Address;
use Auth;
use Session;
use App\Notifications\VendorInviteForJob;
use App\Notifications\VendorHireNotify;
use App\Notifications\TeamHireNotify;
use App\Notifications\VendorTeamInviteJobNotify;
use App\Country;
use App\Category;
use App\Team;
use App\TeamMember;
use App\PaymentUser;
use App\JobTransaction;
use Validator;
use DB;

class FreelancerController extends Controller
{
    public function __construct()
    {
        # code...
    }

    public function list(Request $request)
    {
        $image_path = [];
        // prifle cmpltn percentg 60
        $search_params = $request->input();

        if (sizeof($search_params) > 0)
        {
            $data['all_freelancers'] = \App\Freelancer::search_freelancers($search_params);
            $data['success_rate'] =\App\Freelancer::success_rate();
            
        }
        else
        {
            $data['success_rate'] =\App\Freelancer::success_rate();
            
            $data['all_freelancers'] = \App\Freelancer::all_records();

        }

        if(!$data['all_freelancers']->isEmpty())
        {  
            foreach ($data['all_freelancers'] as $k => $v) {   
          
            $attachment[] =\DB::table('attachment')->select('path')->where('id','=',$v->profile_image)->first();

            }   
            foreach ($attachment as $kf => $vf) {      
                if($vf != null){
                    $image_path[] = $vf->path;
                }else{
                    $image_path[] = null;
                }
            }
        }    
        $data['attach'] = $image_path;
      
        $all_countries = Country::get();
        $data['all_countries']      = $all_countries->toArray();
        $data['search_params']      = $search_params;
        $data['vendor_all_jobs']    = Job::where(['user_id'=> Auth::id()])->get();//  all_records(5);
        $data['all_categories']     = Category::all_records();
        
        return view('client.freelancers.list')->with('data', $data);
    }
    public function freelancer_details(Request $request)
    {
        $params = Route::current()->parameters();
        $details = \App\Freelancer::freelancer_details(base64_decode($params['id']));
        echo json_encode(array("status" => 1, "data" => $details));die;
    }
    public function invite_to_job(Request $request)
    {
        $params = array();
        parse_str($request->input('form_data'), $params);
        try
        {
            $params['user_id'] = $params['invitation_rcvr_id'];
            $params['job_id'] = $params['invitation_jobs'];
            $params['invite_message'] = $params['invitation_message'];
            $is_invite_exist = \App\Invites::is_invite_exist($params['job_id'], $params['user_id']);
            if ($is_invite_exist)
            {
                $ret_data = array("status" => 0, "message" => "Your have already sent an invitation for this job.");
            }
            else
            {
                $insert_id = \App\Invites::send_invite($params);
                $ret_data = array("status" => 1, "message" => "Your invitation sent successfully");
                $operation = 'Invited For Job';
                $invite_vendor= Invites::with('job')->find($insert_id);
                $uid= $invite_vendor->job->user_id;
                $s_user = User::where('id',$uid)->select('username')->first(); 
                $senduser=$s_user->username;
                $invite_vendor->user->notify(new VendorInviteForJob($invite_vendor,$senduser,$operation));
            }
        }
        catch (Exception $e)
        {
            $ret_data = array("status" => 0, "message" => "Unable to send invitation. Please try again.");
        }
        return $ret_data;
    }
    public function team()
    {
        $data['vendor_all_job'] = Job::where('user_id',Auth::user()->id)->get();
        $all_countries = Country::get();
        $data['all_countries'] = $all_countries;
        $allteam= Team::get();

        $user =[];
        $attachment =[];
        foreach ($allteam as $key => $value) {
        $user[]=User::where(['id'=>$value['user_id']])->select('profile_image')->first();
        }
        foreach ($user as $k => $v) {    
        $attachment[] =\DB::table('attachment')->select('path')->where('id','=',$v['profile_image'])->first();
        } 
        return view('client.team.add',['data'=>$data,'attachment'=>$attachment]);
    } 
    public function searchteam(Request $request)
    {  
        if ($request->isMethod('post'))
        {
            $name = $request->name;
            $countryId =$request->countryId;
            $stateId =$request->stateId;
            $cityId =$request->cityId;
            if($request->data != NULL){
                $team = Team::select('team.name as name','team.id','team.created_at','attachment.path')->leftjoin('user','user.id','=','team.user_id')->leftjoin('attachment' , 'attachment.id' , '=' , 'user.profile_image');
                if( isset($request->data['name']) && !isset($request->data['countryId'])){   
                    $team = $team->where('team.name', 'LIKE' ,'%'.$request->data['name'].'%');

                }elseif( isset($request->data['countryId']) && !isset($request->data['stateId']) && !isset($request->data['name']) ){
                     $team = $team->leftjoin('address' , 'address.id' , '=' , 'user.address_id')->where('address.country_id', (int)$request->data['countryId']);
                }
                elseif(isset($request->data['stateId']) &&  !isset($request->data['cityId']))
                {  
                    $team = $team->leftjoin('address' , 'address.id' , '=' , 'user.address_id')->where('address.state_id',(int)$request->data['stateId']);  
                }
                elseif( isset($request->data['name']) && isset($request->data['countryId']) && !isset($request->data['stateId']) &&  !isset($request->data['cityId']))
                {
                    $team = $team->where('team.name',$request->data['name'])->leftjoin('address' , 'address.id' , '=' , 'user.address_id')->where('address.country_id', (int)$request->data['countryId']);  
                }
                elseif( isset($request->data['cityId']) && !isset($request->data['name']))
                {
                     $team = $team->leftjoin('address' , 'address.id' , '=' , 'user.address_id')->where(['address.state_id'=>(int)$request->data['stateId'],'address.country_id'=> (int)$request->data['countryId'],'address.state_id'=>(int)$request->data['stateId']]);  
                }
                elseif(isset($request->data['name']) &&  isset($request->data['cityId']))
                {
                     $team = $team->leftjoin('address' , 'address.id' , '=' , 'user.address_id')->where(['address.state_id'=>(int)$request->data['stateId'],'address.country_id'=> (int)$request->data['countryId'],'address.state_id'=>(int)$request->data['stateId']])->where('team.name',$request->data['name']); 
                }
                $team = $team->get()->toArray();
                if(is_array($team))
                {
                    foreach ($team as $key =>$value) {
                        $team[$key]['member'] = TeamMember::where('team_id',$value['id'])->count();   
                    }
                }
                
            }else{
                return response()->json(['error' => 'No Record Found']);
                
            }
            if(!empty($team))
            {    
                return response()->json(['success'=>$team]);
            }
            else
            {
                return response()->json(['error' => 'No Record Found']);
            }  
        }
    }
     public function team_details(Request $request)
    {
        // $params = Route::current()->parameters();
        $details = Team::team_details($request->t_id);
        echo json_encode(array("status" => 1, "data" => $details));die;
    }
    public function invite_to_Team(Request $request)
    {
        $params = array();
        parse_str($request->input('form_data'), $params);
        try
        {
            // $params['team_id'] = $request->id;
            $params['team_id'] = $params['invitation_rcvr_id'];
            $params['job_id'] = $params['invitation_jobs'];
            $params['invite_message'] = $params['invitation_message'];
            $is_invite_exist = \App\Invites::where(array('job_id' => $params['job_id'] ,'team_id'=>$params['team_id']))->first();
            if ($is_invite_exist)
            {
                $ret_data = array("status" => 0, "message" => "Your have already sent an invitation for this job.");
            }
            else
            {
                $invite = new Invites();
                // $invite->user_id = $params['user_id'];
                $invite->job_id = $params['job_id'];
                $invite->invite_message = $params['invite_message'];
                $invite->status =Invites::status_pending;
                $invite->team_id =$params['team_id'];
                $invite->save();
                $insert_id = $invite->id;
                $ret_data = array("status" => 1, "message" => "Your invitation sent successfully");
                $operation = 'Invited your team for job';
                $teaminvite=DB::table('invites')->leftjoin('team','invites.team_id','=','team.id')->join('job','invites.job_id','=','job.id')
                    ->select('team.user_id as uid','team.name as tname','team.status as tstatus','invites.status as istatus','job.*')->where('invites.id',$insert_id)->first(); 
                // $invite_vendor= Invites::with('job')->find($insert_id);  
                $s_user = User::where('id',$teaminvite->uid)->first();
                $senduser =  $s_user->username;
                $s_user->notify(new VendorTeamInviteJobNotify($teaminvite,$senduser,$operation));
            }
        }
        catch (Exception $e)
        {
            $ret_data = array("status" => 0, "message" => "Unable to send invitation. Please try again.");
        }
        return $ret_data;
    }

     public function hire(Request $request, $job_id, $team_id, $bid_id)
    {
        $data['bid_id'] = $bid_id;
        $data['team_details'] = Team::find($team_id);

        $data['job_details'] = Job::job_details($job_id);
        $data['hire'] = Hires::where(['job_id' => $job_id, 'team_id' => $team_id])->first();

        if($data['job_details'] != "")
        {    
            $attachment = \DB::table('user')->leftjoin('attachment as at', 'at.id','=','user.profile_image')
                            ->select('at.path', 'user.id as uid')->where('user.id','=',$data['team_details']->user_id)->first();
        }
        else
        {
            $attachment = null;
        }
        $data['attach'] = $attachment;
        return view('client.jobs.hire_process')->with('data', $data);
    }
    public function hire_confirm(Request $request, $job_id, $team_id)
    {
        $request->validate([
            'hire_tnc' => 'required',
            'work_desc' => 'required'
        ]);
        $data = Job::job_details($job_id);
        $bid_id = $request->Input('bid_id');

        $team= Team::find($team_id);

        $team_user_id = $team->user_id;
        $bid = Bid::find($bid_id);
        if ($data && $data->user_id == Auth::user()->id)
        {
            if(!$request->has('hireId'))
            {
            $data->work_desc = $request->Input('work_desc');
            $hire = new Hires();
            $hire->team_id = $team_id;
            $hire->job_id = $job_id;
            $hire->rate = $bid->bidPrice;
            $hire->hire_description = $data->work_desc;
            $hire->created_at = date('Y-m-d H:i:s');
            $hire->status = Hires::status_pending;
            $hire->job_type = $data->isHourly;
            $hire->save();
            $insert_id=$hire->id;
            }
            if($insert_id)
            {
            $clientId = Auth::user()->id;
            // $insert_id = '';
            }
            \Stripe\Stripe::setApiKey("sk_test_WvXhIX4DW8oZLvHLsQM7DjIx");
            $customer = PaymentUser::where('user_id',$clientId)->first();
                if($customer)
                {
                    $stripeCustomer = \Stripe\Customer::retrieve($customer->customer_id);
                    $hasCard = $stripeCustomer->default_source;  
                    if($hasCard){
                        $stripeCard = $hasCard;
                    }
                }else{ 
                    $stripeCard = '';
                    Session::flash('noCustomer', 'no customer');
                    
                    Session::put('hireId', $insert_id);
                    return back();
                }
                 if($insert_id || $request->hireId )
                {

                    if($insert_id == ''){
                        $insert_id = $request->hireId;
                    }
                $hire = DB::table('hires')->join('team','hires.team_id','=','team.id')->where('hires.id',$insert_id)->select('team.user_id as tuser_id')->first();
                $hireUserId = $hire->tuser_id;

                $stripeCustomer = \Stripe\Customer::retrieve($customer->customer_id);
                $hasCard = $stripeCustomer->default_source;  
                $hires = Hires::find($insert_id);
                $totalRate = $hires->rate;
                $stripeFees=(($totalRate * 2.9)/100) + 0.30;
                $amount = $stripeFees+$totalRate;
                $allCustomers = [];
                $customers = \Stripe\Customer::all();
                    foreach($customers['data'] as $k => $v){
                        $allCustomers[$v->description] = $v->id;
                    }    
                    if(array_key_exists(Auth::user()->email, $allCustomers))
                    {
                        $customerId = $allCustomers[Auth::user()->email];
                        $customer = \Stripe\Customer::retrieve($customerId);
                    }else{
                        Session::put('noCustomer', 'no customer');
                        return back();
                    }
                $transaction = \Stripe\Charge::create(array(
                                    "amount" => (int)$amount*100,
                                    "currency" => "usd",
                                    "description" => "Charge for vendorName",
                                    'customer' => $customer->id
                                ));
                $charge_id = $transaction->id;
                $charge_status = $transaction->status;
                    if($charge_status == "succeeded" && $charge_id != null)
                    {
                        $donePayment = PaymentUser::insert(['customer_id' => $customer->id, 'transaction_id' => $charge_id , 'user_id' => Auth::user()->id , 'type' => 'toStripe']);
                        $donePayment = JobTransaction::insert([
                                                                    'hire_id' => $insert_id,
                                                                    'amt' => $totalRate,
                                                                    'amount' => 100,
                                                                    'from' => Auth::user()->id,
                                                                    'to' => $hireUserId,
                                                                    'request_fund' => now(),
                                                                    'approve_fund' => now(),
                                                                    'status' => 0
                                                            ]);
                    }else{
                        return "false";
                    }
                    if($donePayment)
                    {
                        Session::flash('successTransaction', 'Payment and team hire successfully');
                    $operation = 'hire team';
                    $teaminvite=DB::table('hires')->join('team','hires.team_id','=','team.id')->join('job','hires.job_id','=','job.id')
                    ->select('team.user_id as uid','team.name as tname','team.status as tstatus','hires.status as hstatus','job.*')->where('hires.id',$insert_id)->first(); 
                    $hire_vend = Hires::with('job')->find($insert_id);
                    $s_user = User::where('id',$teaminvite->uid)->first(); 
                    $senduser=$s_user->username;
                    $s_user->notify(new TeamHireNotify($hire_vend,$senduser,$operation));
                    $bid->status = Bid::status_accepted;
                    $bid->save();
                    $request->session()->flash('hire_flash_success_msg', 'Payment & Person hired successfully.');
                    return redirect('client/job/details/' . $job_id);
                    }else{
                        return "false";
                    } 
                }
                $request->session()->flash('hire_flash_err_msg', 'Error occured while hiring process. Please Try again.');
            
        }
        else
        {
        $request->session()->flash('hire_flash_err_msg', 'Unauthorized Request.');
        }
        return redirect('client/hire/' . $job_id . '/' . $team_id);
    }
           // $data = Job::job_details($job_id);
        // $bid_id = $request->Input('bid_id');
        // $team= Team::find($team_id);
        // $team_user_id = $team->user_id;
        // $bid = Bid::find($bid_id);
        // if ($data && $data->user_id == Auth::user()->id)
        // {
        //     $data->work_desc = $request->Input('work_desc');
        //     $hire = new Hires();
        //     $hire->team_id = $team_id;
        //     $hire->job_id = $job_id;
        //     $hire->rate = $bid->bidPrice;
        //     $hire->hire_description = $data->work_desc;
        //     $hire->created_at = date('Y-m-d H:i:s');
        //     $hire->status = Hires::status_pending;
        //     $hire->job_type = $data->isHourly;
        //     $hire->save();
        //     $insert_id= $hire->id;
        //     if ($insert_id)
        //     {
        //         $operation = 'hire team';
        //          $teaminvite=DB::table('hires')->join('team','hires.team_id','=','team.id')->join('job','hires.job_id','=','job.id')
        //             ->select('team.user_id as uid','team.name as tname','team.status as tstatus','hires.status as hstatus','job.*')->where('hires.id',$insert_id)->first(); 
        //         $hire_vend = Hires::with('job')->find($insert_id);
        //         $s_user = User::where('id',$teaminvite->uid)->first(); 
        //         $senduser=$s_user->username;
        //         $s_user->notify(new TeamHireNotify($hire_vend,$senduser,$operation));
        //         $bid->status = Bid::status_accepted;
        //         $bid->save();
        //         $request->session()->flash('hire_flash_success_msg', 'Person & Payment hired successfully.');
        //         return redirect('client/job/details/' . $job_id);
        //     }
        //     $request->session()->flash('hire_flash_err_msg', 'Error occured while hiring process. Please Try again.');
        // }
        // else
        // {
        //     $request->session()->flash('hire_flash_err_msg', 'Unauthorized Request.');
        // }
        // return redirect('client/hire/' . $job_id . '/' . $team_id);
        


    public function teamhire(Request $request,$id)
    {
       $data['team_details'] = DB::table('team as t')
                    ->select('t.*')
                    ->leftjoin('teammember as tm', 'tm.team_id', '=', 't.id')
                    ->where(array('t.id' => $id))
                    ->first();
                  
        if (!$data['team_details'])
        {
            return back()->withErrors(array('err_msg' => 'Team details not found.'));
        }
        $data['all_member'] = TeamMember::where('team_id',1)->get();
        // $data['job_proposals'] = Job::job_proposals($id);
        // $data['status_hires'] = Statuses::getStatuses('hires');
        // $data['total_bids'] = Bid::select('user_id')->where(array('job_id' => $id))->count();
        return view('client.team.teamhire')->with('data', $data);     
    }
}
