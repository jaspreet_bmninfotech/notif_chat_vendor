<?php

namespace App\Http\Controllers\client;

use App\Job;
use App\Bid;
use App\Hires;
use App\Invites;
use App\Notifications\VendorHireNotify;
use App\Notifications\VendorInviteForJob;

use App\Statuses;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Auth;
use Validator;
use App\Team;
use Input;
use App\Category;
use App\Languages;
use App\Country;
use App\Attachment;
use App\Timezone;
use App\JobAttachment;
use App\Setting;
use Session;
use DB;
use App\PaymentUser;
use App\JobTransaction;

class JobController extends Controller
{
    public function autocomplete(Request $request)
    {
        $data       =   User::select("username","id")->where("username","LIKE","%{$request->Input('typeahead')}%")
                        ->where("type",'=',"vn")->get();
        $response   =   array("code" => "","success" => "","data"=>""); 
        if(!$data) 
        {
            $response['code']   = 0;
            $response['success']= false;
            $response['html']   = 'no data';
        } else 
        {   
            $response['code']   = 1;
            $response['success']= true;
            $response['data']   = $data;
        }
        return response()->json($response);
    }
    public function uploadfiles()
    {
        $id = Auth::user()->id;
        $attachment = Attachment::addAttachment("files", "uploads/client/".$id);
        return response()->json($attachment);
    }   
    /**
     * Create a job.
     *
     * @param Request $request
     */
    public function add(Request $request)
    {
        if ($request->isMethod('get'))
        {

           $id=Auth::user()->id;
            $cat = Category::select('id', 'name', 'parent_id')->where('parent_id', null)->get();
            
            // $isFree =Job::where(['user_id'=>$id,'isFree'=>1])->select('isFree')->get()->count();  
            $country    = Country::all();
            $timezone   = Timezone::all();
            $language   = Languages::all();
            return view('client.jobs.add', ['cat' => $cat,
                                            'country'=>$country,
                                            'timezone'=>$timezone,
                                            'language' =>$language
                                            ]);

        }

        $data       = Input::except(array('_token'));
        // dd($data);
        if($request->contracttype  == 'hourly')
        {
            $minrate=$request->minrate;
            $maxrate=$request->maxrate;
            $messages = [ 'minrate.required' => 'Please select a minrate',
                        'maxrate.required' => 'Please select a maxrate'
                        ];
        }
        else{
            $budget =$request->budget;
             $messages = [ 'budget.required' => 'Please select a budget'];
        }
        $dd=$request->contracttype;

        $validator  = Validator::make($data,['name' => 'required',
                                            'description' => 'required|max:500',
                                            'category_id' => 'required',
                                            'startDateTime'=>'required',
                                            
                                            ],$messages);
       
        if ($validator->fails())
        {   
            return redirect()->route('client.job.add')->withErrors($validator);
        }
            $typeresult = $request->typeahead;

            // if($typeresult!=NULL)
            // {
                // $getuser =User::where('username',)
            // }
                $eventtype  = $request->get('eventtype','pt');
                if($eventtype == Job::event_fulltime){
                    $eventtype = Job::event_fulltime; 
                }
                $date       =$request->get('startDateTime');           
                $datetime   =implode(',',$date);
                if (strpos($datetime, '-') !== false)
                {
                    list($year, $month, $day) = explode('-', $datetime);
                    $datetime = $year . '/' . $month . '/' .$day ;
                }
                else if (strpos($datetime, '.') !== false)
                {
                    list($year, $month, $day) = explode('.', $datetime);
                    $datetime = $year . '/' . $month . '/' .$day;
                }
                $vendpreffer  =$request->Input('vendorPreffer');
               
                $isPrivate=0;
                $vendprefferArr = array();
                if($vendpreffer)
                {
                    foreach ($vendpreffer as $key => $value)
                    {
                     $vendprefferArr[] = $value;
                    }    
                }  
                if(!empty($vendpreffer)){
                    $isPrivate = 1;
                }
                $address        =$request->get('addressInfo');
                $addressInfo = array();
                if($address)
                {
                    foreach ($address as $key => $value)
                    {
                        $addressInfo[] = $value;
                    }     
                }
        try
        {
        if($request->contracttype  == "fixed")
        {
            if ($request->budget != '') {
            $validator  = Validator::make($data,['budget' =>'required|regex:/[1-9]/']);
                if ($validator->fails()) {
                return redirect()->route('client.job.add')->withErrors($validator);
                }
            }
            if ($request->budget < 0) {
                $validator  = Validator::make($data,['budget' =>'required|regex:/[-]/']);
                if ($validator->fails()) {
                    return redirect()->route('client.job.add')->withErrors($validator);
                }
            }
        }else {
            if ($request->minrate != '' || $request->maxrate != ''){
            $validator  = Validator::make($data,['minrate' =>'required|regex:/[1-9]/',
                                                'maxrate' =>'required|regex:/[1-9]/']);
                return redirect()->route('client.job.add')->withErrors($validator);
            }
            if ($request->minrate < 0 || $request->maxrate < 0) {
                $validator  = Validator::make($data,['minrate' =>'required|regex:/[-]/',
                                                    'maxrate' =>'required|regex:/[-]/']);
                    return redirect()->route('client.job.add')->withErrors($validator);
            }
        }
                if ($validator->fails())
                {
                    return redirect()->route('client.job.add')->withErrors($validator);
                }
            $new_job = Job::create([
                'eventtype'        => $eventtype,
                'category_id'      => $request->get('category_id'),
                'subcategory_id'   => $request->get('subcategory_id'),
                'subSubcategory_id'=> $request->get('sub_subcategoryId'),
                'peopleAttending'  => $request->peopleAttending,
                'name'             => $request->name,
                'description'      => $request->description,
                'user_id'          => $request->user()->id,
                'status'           => '1',
                'budget'           => $request->budget ? $request->budget : '0',
                'minRate'          => $request->minrate ? $request->minrate : '0',
                'maxRate'          => $request->maxrate ? $request->maxrate : '0',
                'isOngoing'        => $request->term == '1',
                'isOneTime'        => $request->term == '0',
                'isHourly'         => $request->contracttype  == 'hourly',
                'vendorPreffer'    => serialize($vendprefferArr),
                'jobSuccess'       => $request->jobSuccess,
                'timeZone'         => $request->timeZone,
                'startDateTime'    => $datetime,
                'addressInfo'      => serialize($addressInfo),
                'language'         => $request->language,
                'canTravel'        => $request->canTravel === "yes",
                'isPrivate'        => $isPrivate
            ]);
           
            $new_job->save();
            $files  = json_decode($request->Input('hid'));
            $job_id = $new_job->id;
            $jobId=base64_encode($job_id);

            if($typeresult!=NULL)
            {
               $getuser= User::where('username',$typeresult)->select('id','email')->first();
                $params = array();
                $params['job_id'] = $new_job->id;
                $params['user_id'] =$getuser->id;
                $params['invite_message'] ="";
                $is_invite_exist = \App\Invites::is_invite_exist($params['job_id'], $params['user_id']);
                if($is_invite_exist)
                {
                }
                else
                {
                $insert_id = \App\Invites::send_invite($params);
               
                $operation = 'Invited For Job';
                $invite_vendor= Invites::with('job')->find($insert_id);
                $uid= $invite_vendor->job->user_id;
                $s_user = User::where('id',$uid)->select('username')->first(); 
                $senduser=$s_user->username;
                $invite_vendor->user->notify(new VendorInviteForJob($invite_vendor,$senduser,$operation));
                }
            }
            $files = Input::file('files');
                if ($files) {
                    $fileid = count($files);
                    if ($fileid < 5) {
                     $id = Auth::id();
                    $destinationPath = "uploads/client/job/".$id;
                    foreach ($files as $key => $file) {
                        $ext = $file->getClientOriginalExtension();
                        $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                        $file = $file->move($destinationPath, $fileName);
                        $data = Attachment::create(
                                         ['name' => $fileName,
                                          'path' => $file,
                                          'mimeType' => $ext
                                        ]
                                    );
                        $data->save();
                        $attachment_id = $data->id;
                        $data = JobAttachment::create(
                                         ['job_id' => $job_id,
                                          'attachment_id' => $attachment_id]
                                    );
                        $data->save();

                }
                }else{
                    return redirect()->route('client.job')->with('status', 'Max 5 files are allowed.');
            }
            }
            return redirect()->route('client.job');
        }
    
        catch (Exception $e)
        {
            return response()->json(['error' => $e->getMessage()]);
        }
    
    
    }
    /**
     * list all jobs
     *
     * @param Request $request
     */
    public function all(Request $request)
    {
        $user = $request->user();
        $openedJobs = $user->openedJobs()->paginate(5,['*'], 'openjob');
        
        $closedJobs = $user->closedJobs()->paginate(5,['*'], 'closedjob');
        return view('client.jobs.all', ['openedJobs' => $openedJobs, 'closedJobs' => $closedJobs]);
    }
    /**
     * Close the Job.
     *
     * @param Request $request
     */
    public function close(Request $request)
    {
        $id= $request->id;
        $response = array();
        if($id)
        {
            $job = Job::find($id);
            $job->status = '0';
            $job->save();   
            $response['status']  = 'success';
            $response['message'] = 'Job Closed Successfully ...';
        }
        
         else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to Closed Job ...';
        }
        echo json_encode($response);
    }
    /**
     * Reopen the Job.
     *
     * @param Request $request
     */
    public function reopen(Request $request, $id)
    {
        $job = Job::find($id);
        $job->status = '1';
        $job->save();
    
        return back();
    }
    /**
     * list jobs.
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $id = Auth::user()->id;
       
        if ($request->isMethod('get'))
        {
            $job = Job::with(['bids','hires','invites'])->where(['user_id'=>$id,'status'=>'1'])->orderBy('id', 'desc')->paginate(10);

            $jobs = [];
            foreach ($job as $key => $value) {
                $value['countBid'] = count($value['bids']);
                $value['countHires'] = count($value['hires']);
                $value['countInvites'] = count($value['invites']);
                $jobs[] = $value;
            }
            // $user = $request->user();
            // $jobs = $user->openedJobs()->get();
            $setting=Setting::select('id')->first();
            $settingId=$setting->id; 
            return view('client.jobs.list', ['jobs' => $jobs,
                                            'pjob' =>$job,
                                              // 'bids' => $bid,
                                              // 'offer' => $offer,
                                              // 'hires' => $hires,
                                              'setting'=> $settingId
                                          ]);
        }
    }
    /**
     * edit a job.
     *
     * @param Request $request
     */
    public function edit(Request $request, $id)
    {
        $job = Job::find($id);
       // $jobattach=JobAttachment::where('job_id',$id)->select('id','job_id','attachment_id')->get()->toArray();
       // $attach =Attachment::where('job_id',$id)->orderBy('job_id', 'desc')->get()->toArray();
        $attach = DB::table('jobattachment as jatch')->leftjoin('attachment as atch','atch.id','=','jatch.attachment_id')->where('jatch.job_id','=',$id)->get();
        $profileattach = \DB::table('attachment')->leftjoin('jobattachment','jobattachment.attachment_id','=','attachment.id')->where('jobattachment.job_id','=',$id)->select('attachment.id','attachment.path','attachment.name','jobattachment.job_id')->where('jobattachment.job_id','=',$id)->get();
       $attachinfoedit = array();
       if($attach)
        {
            foreach ($attach as $key => $value)
            {
                    $attachinfoedit[] = $value;
            }     
        }
        $cat = Category::select('id', 'name', 'parent_id')->where('parent_id', null)->get();
        $country    = Country::all();
        $language   = Languages::all();
        $timezone   = Timezone::all();
        $vendorPreffer= unserialize($job['vendorPreffer']);
        $addressedit    =unserialize($job['addressInfo']);
        
        if($addressedit)
            {
                foreach ($addressedit as $key => $value)
                {
                    $addressinfoedit[] = $value;
                }     
            }
        if ($request->isMethod('get'))
        {
            return view('client.jobs.edit', ['job' => $job,
                                            'cat' => $cat,
                                            'country'=>$country,
                                            'profileattach'    => $profileattach,
                                            'timezone'=>$timezone,
                                            'vendorPreffer'=>$vendorPreffer,
                                            'addressedit'   =>$addressinfoedit,
                                            'fileattach'    =>$attach,
                                            'language'    =>$language
                                            ]);
        }
        $data = Input::except(array('_token'));
        $validator = Validator::make($data, $this->rules());
        if ($validator->fails())
        {
            return redirect()->route('client.job.edit', ['id' => $id])->withErrors($validator);
        }  
         $eventtype  = $request->get('eventtype','pt');
                if($eventtype == Job::event_fulltime){
                    $eventtype = Job::event_fulltime; 
                }
                $date       =$request->get('startDateTime');           
                $datetime   =implode(',',$date);
                if (strpos($datetime, '-') !== false)
                {
                    list($year, $month, $day) = explode('-', $datetime);
                    $datetime = $year . '/' . $month . '/' .$day ;
                }
                else if (strpos($datetime, '.') !== false)
                {
                    list($year, $month, $day) = explode('.', $datetime);
                    $datetime = $year . '/' . $month . '/' .$day;
                }
                $vendpreffer  =$request->Input('vendorPreffer');

                $isPrivate=0;
                $vendprefferArr = array();
                if($vendpreffer)
                {
                        foreach ($vendpreffer as $key => $value)
                    {
                     $vendprefferArr[] = $value;

                    }    

                }  
                if(!empty($vendpreffer)){
                    $isPrivate = 1;
                }
                $address        =$request->get('addressInfo');
                $addressInfo = array();
                if($address)
                {
                    foreach ($address as $key => $value)
                    {
                        $addressInfo[] = $value;
                    }     
                }
        $editjob=['eventtype'        => $eventtype,
                'category_id'      => $request->get('category_id'),
                'subcategory_id'   => $request->get('subcategory_id'),
                'peopleAttending'  => $request->peopleAttending,
                'name'             => $request->name,
                'description'      => $request->description,
                'user_id'          => $request->user()->id,
                'status'           => '1',
                'budget'           => $request->budget ? $request->budget : '0',
                'minRate'          => $request->minrate ? $request->minrate : '0',
                'maxRate'          => $request->maxrate ? $request->maxrate : '0',
                'isOngoing'        => $request->term == '1',
                'isOneTime'        => $request->term == '0',
                'isHourly'         => $request->contracttype  == 'hourly',
                'vendorPreffer'    => serialize($vendprefferArr),
                'jobSuccess'       => $request->jobSuccess,
                'timeZone'         => $request->timeZone,
                'startDateTime'    => $datetime,
                'addressInfo'      => serialize($addressInfo),
                'language'         => $request->language,
                'canTravel'        => $request->canTravel === "yes",
                'isPrivate'        => $isPrivate
            ];
            Job::where('id',$id)->update($editjob);
            $files  = json_decode($request->Input('hid'));
             $uid=Auth::user()->id;
            $files = Input::file('files');
                if ($files) {
                    $fileid = count($files);
                    if ($fileid < 5) {
                     $id = Auth::id();
                    $destinationPath = "uploads/client/job/".$uid;
                    foreach ($files as $key => $file) {
                        $ext = $file->getClientOriginalExtension();
                        $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                        $file = $file->move($destinationPath, $fileName);
                        $data = Attachment::create(
                                         ['name' => $fileName,
                                          'path' => $file,
                                          'mimeType' => $ext
                                        ]
                                    );
                        $data->save();
                        $attachment_id = $data->id;
                        $data = ['job_id' => $job_id, //$id
                                'attachment_id' => $attachment_id];
                        JobAttachment::where('attachment_id',$attachment_id)->update($data);
                }
                }else{
                    return redirect()->route('client.job')->with('status', 'Max 5 files are allowed.');
                }
            }
            return redirect()->route('client.job');
    }
    /**
     * Job details
     *
     * @param Request $request
     */
    public function details(Request $request, $id)
    {
        $data['job_details'] = Job::job_details($id);
        $subcat= Category::select('name as subcat')->where('parent_id','=',$data['job_details']->category_id)->where('id','=',$data['job_details']->subcategory_id)->first();
        $data['subcat']=$subcat;
      
        if (!$data['job_details'])
        {
            return back()->withErrors(array('err_msg' => 'Job details not found.'));
        }
        $data['all_hires'] = Job::job_hires($id);
        $data['countvendors']=$data['all_hires']->where('user_id','!=', NULL)->count();
        $data['countteam'] = $data['all_hires']->where('team_id','!=',NULL)->count();
        $data['job_proposals'] = Job::job_proposals($id);

        if($data['job_details'] != "")
        {    
            $attachment = \DB::table('user')->leftjoin('attachment as at', 'at.id','=','user.profile_image')
                            ->select('at.path', 'user.id as uid')->where('user.id','=',$data['job_details']->user_id)->first();
        }
        else
        {
            $attachment = null;
        }
        $data['attach'] = $attachment;
        $data['status_hires'] = Statuses::getStatuses('hires');
        $data['total_bids'] = Bid::select('user_id')->where(array('job_id' => $id))->count();
        $totalstars = array();
        if(array_key_exists(0,$data['job_proposals']))
        {
            $getfeedback= DB::table('feedback')->leftjoin('user','user.id','=','feedback.feedToId')->where('feedToId' , $data['job_proposals'][0]->id)->get();
            $getfeedback=json_decode($getfeedback,true);
            $processArray = [];
            $feedback_id = [];
            $countId = [];
            foreach ($getfeedback as $key => $value) {
                if(!in_array($value['id'], $feedback_id)){
                    $countId[$value['id']] = 1; 
                    $feedback_id[] = $value['id'];
                    $processArray[$value['id']] = ['star_rate' => $value['starRate'] , 'count' => $countId[$value['id']]]; 
                }else{
                    $lastStarRate = $processArray[$value['id']]['star_rate'];
                    $lastcount = $countId[$value['id']];
                    $countId[$value['id']] = $lastcount + 1; 
                    $processArray[$value['id']] = ['star_rate' => ($lastStarRate + $value['starRate']) , 'count' => $countId[$value['id']]]; 

                }
            }
            
            foreach ($processArray as $key => $value) {
                $totalstar = 5*$value['count'];
                $totalstars[$key] = ['totalstars' => $totalstar, 'success_percent' => round(($value['star_rate']/$totalstar) * 100)];
            
            }
            return view('client.jobs.details')->with(['data' => $data , 'totalstars' => $totalstars]);
        }else
        {
            return view('client.jobs.details')->with(['data' => $data]);
        }   
    }
    public function hire(Request $request, $job_id, $user_id, $bid_id)
    {   

        $data['bid_id'] = $bid_id;
        $data['user_details'] = User::find($user_id);
        $data['job_details'] = Job::job_details($job_id);
        $data['hire'] = Hires::where(['job_id' => $job_id, 'user_id' => $user_id])->first();

        if($data['job_details'] != "")
        {    
            $attachment = \DB::table('user')->leftjoin('attachment as at', 'at.id','=','user.profile_image')
                            ->select('at.path', 'user.id as uid')->where('user.id','=',$user_id)->first();
        }
        else
        {
            $attachment = null;
        }
        $data['attach'] = $attachment;
      
        return view('client.jobs.hire_process')->with('data', $data);
    }
    public function hire_confirm(Request $request, $job_id, $user_id)
    {
        $request->validate([
            'hire_tnc' => 'required',
            'work_desc' => 'required'
        ]);
        $data = Job::job_details($job_id);
        
        $bid_id = $request->input('bid_id');
        $bid = Bid::find($bid_id);
        if ($data && $data->user_id == Auth::user()->id)
        {
            // $chekBidExist - hires::where('job_id' => $job_id , 'user_id' => $user_id);
            $data->work_desc = $request->input('work_desc');
            $insert_id = Hires::save_hire($data, $user_id, $bid);
            if($insert_id){

                $clientId = Auth::user()->id;
            $insert_id = '';
            if(!$request->has('hireId')){
                $insert_id = Hires::save_hire($data, $user_id, $bid);
            }
            
                \Stripe\Stripe::setApiKey("sk_test_WvXhIX4DW8oZLvHLsQM7DjIx");

                $customer = PaymentUser::where('user_id',$clientId)->first();
                if($customer)
                {
                    $stripeCustomer = \Stripe\Customer::retrieve($customer->customer_id);
                    $hasCard = $stripeCustomer->default_source;  
                    if($hasCard){
                        $stripeCard = $hasCard;
                    }
                }else{ 
                    $stripeCard = '';
                    Session::flash('noCustomer', 'no customer');
                    Session::put('hireId', $insert_id);
                    return back();
                }
            if ( $insert_id || $request->hireId )
            {
                if($insert_id == ''){
                    $insert_id = $request->hireId;
                }
                $hire = Hires::find($insert_id);
                $hireUserId = $hire->user_id;
                $stripeCustomer = \Stripe\Customer::retrieve($customer->customer_id);
                $hasCard = $stripeCustomer->default_source;  

                $hires = Hires::find($insert_id);
                $totalRate = $hires->rate;
                
                $stripeFees=(($totalRate * 2.9)/100) + 0.30;
                $amount = $stripeFees+$totalRate;

                $allCustomers = [];
                $customers = \Stripe\Customer::all();
                foreach($customers['data'] as $k => $v){
                    $allCustomers[$v->description] = $v->id;
                }    
                if( array_key_exists(Auth::user()->email, $allCustomers) ){
                    // user alerady as a customer
                    //check if customer has card
                    $customerId = $allCustomers[Auth::user()->email];
                    
                    $customer = \Stripe\Customer::retrieve($customerId);
                    
                }else{
                    Session::put('noCustomer', 'no customer');
                    return back();
                }

                $transaction = \Stripe\Charge::create(array(
                                    "amount" => (int)$amount*100,
                                    "currency" => "usd",
                                    "description" => "Charge for vendorName",
                                    'customer' => $customer->id
                                ));
                $charge_id = $transaction->id;
                $charge_status = $transaction->status;
                
                if($charge_status == "succeeded" && $charge_id != null){
                    $donePayment = PaymentUser::insert(['customer_id' => $customer->id, 'transaction_id' => $charge_id , 'user_id' => Auth::user()->id , 'type' => 'toStripe']);
                    $donePayment = JobTransaction::insert(
                                                            [
                                                                'hire_id' => $insert_id,
                                                                'amt' => $totalRate,
                                                                'amount' => 100,
                                                                'from' => Auth::user()->id,
                                                                'to' => $hireUserId,
                                                                'request_fund' => now(),
                                                                'approve_fund' => now(),
                                                                'status' => 0
                                                            ]
                                                        );
                }else{
                    return "false";
                }
                if($donePayment){
                    Session::flash('successTransaction', 'Payment and vendor hire successfully');
                    $operation = 'Accepted hire';
                    $hire_vend = Hires::with('job')->find($insert_id);
                    $send_uid= $hire_vend->job->user_id;
                    $s_user =User::where('id',$send_uid)->select('username')->first();
                    $senduser=$s_user->username;
                    $hire_vend->user->notify(new VendorHireNotify($hire_vend,$senduser,$operation));
                    $bid->status = Bid::status_accepted;
                    $bid->save();
                    $request->session()->flash('hire_flash_success_msg', 'Payment & Person hired successfully.');
                    return redirect('client/job/details/' . $job_id);
                }else{
                    return "false";
                } 
            }
            $request->session()->flash('hire_flash_err_msg', 'Error occured while hiring process. Please Try again.');
        }
    
        else
        {
            $request->session()->flash('hire_flash_err_msg', 'Unauthorized Request.');
        }
        return redirect('client/hire/' . $job_id . '/' . $user_id);
        }
    }
        public function jobupload(){
            $id = Auth::user()->id;
            $job_id = Input::get('jobId');
            $files = Input::file('files');
            if ($files) {
                $fileid = count($files);
                if ($fileid < 5) {
                 $id = Auth::id();
                $destinationPath = "uploads/client/job/".$id."/";
                foreach ($files as $key => $file) {
                   $fileSize = $file->getClientSize();
                    if ($fileSize < 2000000) {
                    $ext = $file->getClientOriginalExtension();
                    $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
                    $file = $file->move($destinationPath, $fileName);
                    $data = Attachment::create(
                                     ['name' => $fileName,
                                      'path' => $file,
                                      'mimeType' => $ext
                                    ]
                                );
                    $data->save();
                    $attachment_id = $data->id;
                    $data = JobAttachment::create(
                                     ['job_id' => $job_id,
                                      'attachment_id' => $attachment_id]
                                );
                    $data->save();
                    return response('success');
                    }
                    }
                }else{
                    return redirect()->route('client.job')->with('status', 'Max 5 files are allowed.');
                }
            }
        }
        public function removejobimages(Request $request){
            $id = Auth::user()->id;
            $remove_id = $request->remove_id;

            $jobId = $request->jobId;
            $paths = $request->paths;

            $filename = $request->filename;
            $data = \DB::table('jobattachment')->where('job_id','=',$jobId)->where('attachment_id','=',$remove_id)->delete();
            unlink(public_path('/uploads/client/job/'.$id.'/'.$filename));
            return response('success');
        }
    /**
     * validate params
     *
     * 
     */
        protected function rules()
        {
            $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
            return [
                'name' => 'required',
                'description' => 'required'
            ];
        }

}
