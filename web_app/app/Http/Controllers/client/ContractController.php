<?php

namespace App\Http\Controllers\client;
use App\Job;
use App\user;
use App\Bid;
use App\Bussiness;
use App\feedback;
use App\Hires;
use App\bankDetails;
use App\JobTransaction;
use App\TeamMember;
use App\Notifications\ReleaseFundNotify;
use App\Team;
use Auth;
use Carbon\Carbon;
use App\PaymentUser;
use App\Attachment;
use Validator;
use Session;
use Input;
use Response;
use Illuminate\Http\Request;
use App\Notifications\VendorEndContracts;
use App\Http\Controllers\Controller;


class ContractController extends Controller
{
    public function list(){
	    $user_id = Auth::id();
        $job_ids = Job::where('user_id', $user_id)->pluck('id'); 
        $res = hires::whereIn('job_id', $job_ids)->get();
        
        $hire_job = $res->pluck('rate', 'job_id');
        $jobs = $hire_job->keys()->toArray();
        $job_data = Job::whereIn('id', $jobs)->get()->keyBy('id'); 
       
        $result = \DB::table('job')->leftjoin('hires', 'hires.job_id', 'job.id')->select('hires.id as hire_id','job.id','job.name','job.created_at','hires.rate','job.isHourly','job.budget')->distinct('hires.job_id')->where([
                        ['job.user_id', $user_id],
                         ['hires.status', '=', 1]
                    ])->orderBy('hires.id', 'desc')->get();
      
        return view('client/contract/list',['data'=>$result]);
    }
    public function contractlist($id)
    {
        $user_id= Auth::user()->id;
        $Id= base64_decode($id);
        $result= \DB::table('job')->leftjoin('hires', 'hires.job_id', 'job.id')->select('hires.user_id','hires.id as hire_id','hires.job_id','hires.rate','hires.created_at','job.id','job.name','job.isHourly', 'job.budget','job.maxRate')->where([['job.user_id', $user_id],
            ['hires.job_id',$Id]])->where('hires.team_id','=',NULL)->get();

        $team= Job::leftjoin('hires', 'hires.job_id', 'job.id')->leftjoin('team','team.id','=','hires.team_id')->select('hires.user_id','hires.id as hire_id','hires.job_id','hires.rate','hires.created_at','job.id','job.name','job.isHourly', 'job.budget','job.maxRate','team.id as teamid','team.name as tname')->where([['job.user_id', $user_id],['hires.job_id',$Id]])->where('hires.team_id','!=',NULL)->get();
            $members=[];
        if($team!="")
        {
           foreach ($team as $k => $v) {
            $members= TeamMember::where('team_id',$v['teamid'])->get();
           }
        }
        return view('client/contract/teamplusvendors',['vendors'=>$result,'team'=>$team,'members'=>$members]);
    }
     public function detail($id,$tid=null){
        $id= base64_decode($id);
        $teamid= base64_decode($tid);

        \Stripe\Stripe::setApiKey("sk_test_WvXhIX4DW8oZLvHLsQM7DjIx");
        $client = Auth::user();
        $customer = PaymentUser::where('user_id',$client->id)->first();
        if($customer)
        {
            $stripeCustomer = \Stripe\Customer::retrieve($customer->customer_id);
            $hasCard = $stripeCustomer->default_source;  
            if($hasCard){
                $stripeCard = $hasCard;
            }
        }
        else{        
            $stripeCard = '';
        }
        $hire = Hires::select('created_at','updated_at','status', 'job_id', 'id', 'rate','user_id','team_id')->where('id' ,(int) $id)->first();
      
        $data = Job::select('id','name','description','maxRate','user_id','isHourly','status','budget','isOneTime')->where('id','=',$hire->job_id)->first();
        if($hire->user_id!=Null)
        {    
            $vendor = User::select('id','username','profile_image','created_at')->where('id','=',$hire->user_id)->first();
            $attachmentimg =  Attachment::select('id','path')->where('id','=',$vendor['profile_image'])->first();
            $bid = Bid::where( ['job_id' => $data->id , 'user_id' => $vendor->id] )->first();
            $serviceFee = $bid->serviceFee;
        }
        else
        {
           
            $vendorteam = Team::select('id','user_id')->where('id','=',$hire->team_id)->first();
            $vendor =  User::select('id','username','profile_image','created_at')->where('id','=',$vendorteam->user_id)->first();
            $attachmentimg =  Attachment::select('id','path')->where('id','=',$vendor['profile_image'])->first();
             $bid = Bid::where( ['job_id' => $data->id , 'team_id' => $hire->team_id] )->first();
            $serviceFee = $bid->serviceFee;
        } 
        $jobTransaction = JobTransaction::where(['from' => Auth::user()->id , 'hire_id' => $id])->first();

        $client_id = $client->id;
            $attachmentimg =  Attachment::select('id','path')->where('id','=',$vendor['profile_image'])->first();
        $bussiness = Bussiness::select('name')->where('user_id','=',$client_id)->first();
        $Paid = JobTransaction::select('amt')->where(['hire_id' => $id])->where('approve_release','!=',null)->get()->sum('amt');

        $pendingAmount = JobTransaction::where(['hire_id' => $id , 'approve_release' => null])->get()->sum('amt');
         $hasfeedback= feedback::where(['hire_id'=>$id,'feedFromId'=>$client_id])->first();
        $sendParams = [ 'data'    => $data,
                        'client'   => $client,
                        'vendor'   => $vendor,
                        'bussiness'=> $bussiness,
                        'hires'    => $hire,
                        'bid'    => $bid,
                        'jobTransaction'    => $jobTransaction,
                        'serviceFee' => $serviceFee,
                        'totalPaid' => $Paid,
                        'pendingAmount' => $pendingAmount,
                        'stripeCard' => $stripeCard,
                        'hasfeedback'=> $hasfeedback,
                        'profile_image'=> $attachmentimg['path']
                    ];
        return view('client/contract/detail',$sendParams);
    }
    // when client accept for funding
    public function fundFunded(Request $request)
    {
        \Stripe\Stripe::setApiKey("sk_test_WvXhIX4DW8oZLvHLsQM7DjIx");

        if($request->has('tranId')){

            $jobTransaction = JobTransaction::where('id',$request->tranId)->first();

            $amt = $jobTransaction->amt;

            //stripe fees
            // $FIXED_FEE = 0.30;
            // $PERCENTAGE_FEE = 0.029;
            // $amount = (($amt)+ $FIXED_FEE)/(1 - $PERCENTAGE_FEE);
            // $stripeFees = number_format((float)$amount - $amt , 2, '.', '');

            //vffees
            $hire_id = $jobTransaction->hire_id;
            $hire = Hires::where('id' , $hire_id)->first();
          
            $job_id = $hire->job_id;
            if($hire->user_id!=NULL)
            {
                $user_id = $hire->user_id;     
                $bid = Bid::where([ 'job_id' => $job_id , 'user_id' => $user_id ])->first();
            }
            else
            {
                $team = Team::select('id','user_id')->where('id','=',$hire->team_id)->first();
                $user_id = $team->user_id;
                $bid = Bid::where([ 'job_id' => $job_id , 'team_id' => $hire->team_id ])->first();
            }         
            $serviceFee = $bid->serviceFee;
            //total amount to be paid
            $vffees=(($amt * $serviceFee)/100);
            $deductvffees=$amt-$vffees;

            $stripeFees=(($deductvffees * 2.9)/100) + 0.30;
            $getstripeFees= number_format((float)$stripeFees  , 2, '.', '');
            $totalAmountToBePaid = $deductvffees-$getstripeFees ;

            
            // $bidAmount = $bid->bidPrice;
            // $paidAmount = jobTransaction::select('amount')->where([ 'hire_id' => $hire_id , 'from' => $user_id ])->where('approve_release' , '!=' , null)->get()->sum('amount');
            // $jobTransaction = jobTransaction::where('id',$request->tranId)->first();
            // $PendingAmount = ($bidAmount - ($bidAmount*$paidAmount)/100);
            // $percentageAmount = ($bidAmount * $jobTransaction->amount) /100;

            // $vfFees = ((($bidAmount * $jobTransaction->amount) /100)*$serviceFee) / 100;
            // $newAccount = (($bidAmount * $jobTransaction->amount) /100)-$vfFees;
            // dd($newAccount);
            // $FIXED_FEE = 0.30;
            // $PERCENTAGE_FEE = 0.029;


            // $amount = (($percentageAmount)+ $FIXED_FEE)/(1 - $PERCENTAGE_FEE);
            // $stripeFees = number_format((float)$amount - $percentageAmount , 2, '.', '');
            // dd($stripeFees);
            $stripeVendorId = bankDetails::where([ 'user_id' => $user_id , 'status' => 1 ])->first();
            $vendorStripeAccount = $stripeVendorId->account_id;
            $transferAmount =   \Stripe\Charge::create(array(
                                    "amount" => ($totalAmountToBePaid)*100,
                                    "currency" => "usd",
                                    "source" => "tok_visa",
                                    // "method" => "instant",
                                    "destination" => array(
                                    "account" => $vendorStripeAccount,
                                    ),
                                ));

            if( isset($transferAmount->status) ){
                if( $transferAmount->status == "succeeded"){
                    $donePayment = PaymentUser::insert([ 'transaction_id' => $transferAmount->id , 'user_id' => $user_id , 'type' => 'toBank']);

                    $donePayment = JobTransaction::where('id',$request->tranId)->update(['approve_release' => now()]);
                    return "true";
                }else{
                    return "false";
                }
            }

        }elseif($request->has('hasCustomer')){

            $transaction_id = $request->transactId;

            $model = JobTransaction::where('id',$transaction_id)->first();
            
            $amount = $model->amount;
            $hire_id = $model->hire_id;
            $hires = Hires::find($hire_id);
            $totalRate = $hires->rate;
            $allTransactions = JobTransaction::where('hire_id', $hire_id)->get()->sum('amt');
            // if( $allTransactions == (int)$totalRate ){
                $amt = $model->amt;
           
            $FIXED_FEE = 0.30;
            $PERCENTAGE_FEE = 0.029;
            $amount = (($amt)+ $FIXED_FEE)/(1 - $PERCENTAGE_FEE);
            $tranamount= number_format((float)$amount  , 2, '.', '');
            
            
            // }else{
            //     // $restAmount = ($totalRate - $allTransactions);
            //     // $amt = (($restAmount*$amount)/100);
            //     dd($amount);
            //     dd((int)$model->amt * $amount )/100;
            // }
            // dd($amt);
            $client = Auth::user();
            $customer_id = PaymentUser::where('user_id',$client->id)->first();

            if($customer_id == NULL){
                return "false";
            }

            $stripeCustomer = \Stripe\Customer::retrieve($customer_id->customer_id);
            $hasCard = $stripeCustomer->default_source;
            $stripeCard = '';
            if($hasCard){
                $stripeCard = $hasCard;
            }

            $transaction = \Stripe\Charge::create(array(
                                "amount" => ($tranamount)*100,
                                "currency" => "usd",
                                "description" => "Charge for vendorName",
                                'customer' => $customer_id->customer_id
                            ));
            $charge_id = $transaction->id;
            $charge_status = $transaction->status;
            if($charge_status == "succeeded" && $charge_id != null){
                $donePayment = PaymentUser::insert(['customer_id' => $customer_id->customer_id , 'token_id' => '' , 'transaction_id' => $charge_id , 'user_id' => Auth::user()->id , 'type' => 'toStripe']);
                $donePayment = JobTransaction::where('id',$transaction_id)->update(['approve_fund' => now()]);
            }else{
                return "false";
            }
            if($donePayment){
                return "true";
            }else{
                return "false";
            }

        }elseif($request->has('releaseFunds')){
            $jobTransaction = JobTransaction::where('id' , $request->transactId)->first();
            $user_id = $jobTransaction->to;
            $amt = $jobTransaction->amt;

            $stripeFees=(($amt * 2.9)/100) + 0.30;
            $tranamount= number_format((float)$stripeFees  , 2, '.', '');

           
            $hire_id = $jobTransaction->hire_id;

            $hire = Hires::where('id' , $hire_id)->first();
            $job_id = $hire->job_id;

            if($hire->user_id!=NULL)
            {
                $user_id = $hire->user_id;    
                $bid = Bid::where([ 'job_id' => $job_id , 'user_id' => $user_id ])->first();
            }
            else
            {
                $team = Team::select('id','user_id')->where('id','=',$hire->team_id)->first();
                $user_id = $team->user_id;
                $bid = Bid::where([ 'job_id' => $job_id , 'team_id' => $hire->team_id ])->first();
            }

            $serviceFee = $bid->serviceFee;
            // $vffees = (($amt*$serviceFee)/100);

            // $totalAmount = $amt-$vffees;

            // $stripeFees=(($totalAmount * 2.9)/100) + 0.30;

            // $amoutToBeSend = $totalAmount - $stripeFees;
            $stripeVendorId = bankDetails::where([ 'user_id' => $user_id , 'status' => 1 ])->first();
            $vendorStripeAccount = $stripeVendorId->account_id;

            $transferAmount =   \Stripe\Charge::create(array(
                                    "amount" => ($serviceFee)*100,
                                    "currency" => "usd",
                                    "source" => "tok_visa",
                                    // "method" => "instant",
                                    "destination" => array(
                                                        "account" => $vendorStripeAccount,
                                                    ),
                                ));
            if( isset($transferAmount->status) ){
                if( $transferAmount->status == "succeeded"){
                    $donePayment = PaymentUser::insert([ 'transaction_id' => $transferAmount->id , 'user_id' => $user_id , 'type' => 'toBank']);

                    $donePayment = JobTransaction::where('id',$request->transactId)->update(['approve_release' => now()]);
                    $releasefund = JobTransaction::find($request->transactId);
               $senduser = User::where('id',$releasefund->to)->first();
             
               $operation = 'Request for Release fund';
               $fromuser=Auth::user()->username;
               $senduser->notify(new ReleaseFundNotify($releasefund,$operation,$senduser,$fromuser));
                }else{
                    return "false";
                }
            }
        }else{
            $actualAmount = $request->hireRate;
           
            $amount = $request->hireRate;
            $hire_id = $request->hireId;
            $hires = Hires::find($hire_id);
            $totalRate = $hires->rate;
       
            
            $stripeFees=(($amount * 2.9)/100) + 0.30;
            $amount = $stripeFees+$amount;


            $allCustomers = [];
            $customers = \Stripe\Customer::all();
            foreach($customers['data'] as $k => $v){
                $allCustomers[$v->description] = $v->id;
            }    
            if( array_key_exists(Auth::user()->email, $allCustomers) ){
                // user alerady as a customer
                //check if customer has card
                $customerId = $allCustomers[Auth::user()->email];
                $token = \Stripe\Token::create(array(
                          "card" => array(
                            "number" => $request->card_number,
                            "exp_month" => $request->expireMonth,
                            "exp_year" => $request->expireYear,
                            "cvc" => $request->cvv
                          )
                        ));
                if($token->id){
                    $customer = \Stripe\Customer::retrieve($customerId);
                    $customer->sources->create(array("source" => $token));

                }else{
                    return "card Details are invalid";
                }
                
            }else{
                $customer = \Stripe\Customer::create(array(
                                "description" => Auth::user()->email
                            ));
                $token = \Stripe\Token::create(array(
                          "card" => array(
                            "number" => $request->card_number,
                            "exp_month" => $request->expireMonth,
                            "exp_year" => $request->expireYear,
                            "cvc" => $request->cvv
                          )
                        ));
                if($token->id){
                    $customer = \Stripe\Customer::retrieve($customer->id);
                    $customer->sources->create(array("source" => $token));

                }else{
                    return "card Details are invalid";
                }
            }

            $transaction = \Stripe\Charge::create(array(
                                "amount" => (int)$amount*100,
                                "currency" => "usd",
                                // "source" => $token->id, // obtained with Stripe.js
                                "description" => "Charge for vendorName",
                                'customer' => $customer->id
                            ));
            $charge_id = $transaction->id;
            $charge_status = $transaction->status;
            
            if($charge_status == "succeeded" && $charge_id != null){
                $donePayment = PaymentUser::insert(['customer_id' => $customer->id , 'token_id' => $token->id , 'transaction_id' => $charge_id , 'user_id' => Auth::user()->id , 'type' => 'toStripe']);
                $donePayment = JobTransaction::insert(
                                                        [
                                                            'hire_id' => $hire_id,
                                                            'amt' => $actualAmount,
                                                            'amount' => 100,
                                                            'from' => Auth::user()->id,
                                                            'to' => $hires->user_id,
                                                            'request_fund' => now(),
                                                            'approve_fund' => now(),
                                                            'status' => 0
                                                        ]
                                                    );

            }else{
                return "false";
            }
            if($donePayment){
                session()->forget('noCustomer');
                return "true";
            }else{
                return "false";
            }
        }
    }
    public function endContract($id){
    	$user_id =Auth::id();
        $contract = Hires::select('id',
                                'job_id',
                                'created_at',
                                'updated_at',
                                'status')
                    ->where('id',$id)
                    ->first();


    	$job = Job::select('id','name','description','maxRate','user_id','isHourly','status','budget','isOneTime')
            ->where('id','=',$contract->job_id)
            ->first();

    	return view('client/contract/endContract',['contract'=>$contract,
    											   'job'=>$job
    												]);
    }
    public function contractUpdate(Request $request,$id){
    	$user =Auth::user();
    	if($request->isMethod('post'))
        {
            $request->validate([
                'review' => 'required'
                
            ]);
            $hire_id = $id;
            $status =Hires::status_close; //status end contract (hires)
            $rating =['skill' =>Input::get('skill'),
            		'quality' =>Input::get('quality'),
            		'availability' =>Input::get('availability'),
            		'deadlines' =>Input::get('deadlines'),
            		'communication' =>Input::get('communication'),
            		'cooperation' =>Input::get('cooperation')];
                    $countrows= sizeof($rating);
                    $total = array_sum($rating);
                $totalstars = $countrows * 5;
                $initialstar= ($total * $countrows/ $totalstars);
                $avg=$initialstar/$countrows;
                $getstar=$initialstar - $avg;
                $starget= number_format((float)$getstar  , 2, '.', '');
            	$rating = serialize($rating);

            $review = Input::get('review');
            $hireuser= Hires::with('job')->find($hire_id);
            $user_id = $user->id;

            if ($user->type == 'cn') {
            	$client_id = $user_id;
            	$vendor_id = $hireuser->user_id;
            }else{
                $vendor_id = $user_id;
                $client_id = $hireuser->job->user_id;
            }
          //  $hire = Hires::select('id')->find($id);

            $hires = Hires::where('id','=',$hire_id)
                            ->update([
                                'status' => $status,
                                'updated_at' => Carbon::now()
                            ]);

    	        $feedback = feedback::create([
    	            'hire_id' => $hire_id,
                    'feedFromId' => $client_id,
                    'feedToId' => $vendor_id,
                    'review' => $review,
                    'rating' => $rating,
                    'starRate' =>$starget
                    ]);

            $client_endcontract = Hires::with('job')->find($hire_id);
            $senduser = Auth::user()->username;
            if($client_endcontract->status==0)
            {     
                $operation = 'end contracts';
                $client_endcontract->user->notify(new VendorEndContracts($client_endcontract,$senduser,$operation));
            }
	        Session::flash('success');
            return redirect()->route('client.contract.detail',['id'=>base64_encode($id)]); 
         }
    }
}
