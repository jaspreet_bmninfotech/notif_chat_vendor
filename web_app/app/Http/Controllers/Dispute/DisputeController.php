<?php

namespace App\Http\Controllers\Dispute;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Admin\Entities\Ticketit; 
use Modules\Admin\Entities\Ticket\TicketitCategory as Category;
use Auth;
use App\Job;
use App\User;
use App\Team;
use Validator;
use App\Dispute;
use App\Hires;
use Input;
use DB;


use Modules\Admin\Entities\Ticket\TicketitCategoryUser as Category_user;

class DisputeController extends Controller
{

	public function index($type=null){

        $user = Auth::user();
		if($user->type=='vn'){
            if($type){
                $dispute = Dispute::with([ 'job','status'])->WhereIn('dispute_apply_by', ['cn','tg'])->where(['vendor_id'=>$user->id ])->get();
            }else{
    			$dispute = Dispute::with([ 'job','status'])->where(['dispute_apply_by'=>'vn','vendor_id'=>$user->id ])->get();
            }
		}elseif($user->type=='cn'){
            if($type){
                $dispute = Dispute::with([ 'job','status'])->WhereIn('dispute_apply_by', ['vn','tg'])->where(['vendor_id'=>$user->id ])->get();
            }else{
			     $dispute = Dispute::where(['dispute_apply_by'=>'cn','client_id'=>$user->id ])->get();
                }
		}elseif($user->type=='tg'){
             if($type){
                $dispute = Dispute::with([ 'job','status'])->WhereIn('dispute_apply_by', ['vn','cn'])->where(['vendor_id'=>$user->id ])->get();
            }else{
			     $dispute = Dispute::where(['dispute_apply_by'=>'tg','tour_guide_id'=>$user->id ])->get();
             }
		}

		return view('dispute.index', compact('dispute') );
	}
	
    public function create(){

        if(Auth::user()->type=="vn") {
           $job_ids =  Hires::where('user_id', Auth::id())->pluck('job_id')->toArray();
           if(!empty($job_ids)){
    	       $job = Job::whereIn('id', [$job_ids])->pluck('name','id');
           }
        }elseif (Auth::user()->type=="cn") {
            // $job = Job::where('user_id', Auth::id() )->pluck('name','id');

            $job=DB::table('hires')->leftjoin('job','hires.job_id','=','job.id')->where('job.user_id',Auth::user()->id)->pluck('job.name','job.id');
           
        }
            $job = $job->toArray();
        if(empty($job)) {
            $job = ["dumy job 1" , "dumy job 2" ];
        }

        // Hires::
        $category = Category::pluck('name','id');
        $category=$category->toArray();
    	return view('dispute.create', compact('category', 'job'));
    }
     public function jobvendors(Request $request)
    { 
        $job= Job::with('user')->find($request->id);
        $user= Hires::with('user')->where('job_id',$request->id)->select('user_id','team_id')->get();
       
        $teamuser=array();$vend=array();
        foreach ($user as $key => $value) {
            if($value['user_id']==NULL && $value['team_id']!=NULL)
            {
                $team=Team::find($value['team_id']);
                $tuser= User::where('id',$team->user_id)->select('username')->first();
                $teamuser = ['user_id'=>$team->user_id,'uname'=>$tuser->username];
            }elseif($value['user_id']!=NULL)
            {

               $users[]= ['user_id'=>$value['user_id'],'uname'=>$value['user']->username];
            }

        }
        if(is_array($users))
        {
            foreach ($users as $key => $value) {
                $result[]=$value;
            }
        }
       return response()->json($result);
    }
    public function detail($id){
        $data = Dispute::with(['job', 'status' ,'category',  'dispute_verdicts', 'comment'])->whereId($id)->first();
        // dd($data, $id);
        return view('dispute.detail',compact('data'));
    }


    public function store(Request $request){

        $data       = Input::except(array('_token'));
        $validator  = Validator::make($data,['subject' => 'required',
                                            'content' => 'required|max:500',
                                            'category_id' => 'required',
                                            'job_id'=>'required',
                                        
                                            ]);
         if ($validator->fails())
        {
            return redirect()->route('dispute.create')->withErrors($validator);
        }
    	$users = Category_user::where('category_id',$request['category_id'])->get()->random(1);
        $request['agent_id'] = $users[0]->user_id;
    	$type ='vn';
    	$type = Auth::user()->type;
    	if($type =='vn'){
    		$request['vendor_id'] =  Auth::id();
    		$request['client_id'] = Job::select('user_id')->where('id', $request['job_id'])->first()->user_id;
    	}elseif($type=='cn'){
    		$request['client_id'] =  Auth::id();
             $request['vendor_id']=$request['vendors'];
    	}
    	$request['dispute_apply_by'] = $type;
    	$request['status_id'] = 1;
    	
    	$dispute = new Dispute();
    	$dispute->fill($request->all());
    	$dispute->save();

    	return redirect()->route('dispute.index');

    
    }
    
}
