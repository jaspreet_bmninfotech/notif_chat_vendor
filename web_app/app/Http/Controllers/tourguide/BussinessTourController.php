<?php
namespace App\Http\Controllers\tourguide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bussiness;
use App\Address;
use App\Country;
use Illuminate\Support\Facades\Storage;
use Input;
use Response;
use Session;
use App\Category;
use App\Attachment;
use App\User;
use Auth;
use Validator;

class BussinessTourController extends Controller
{
     public function index()
    {
        $category   = Category::select('id','name','shortCode','parent_id')
                        ->where('parent_id','=',null)->get();
        $country    = Country::all();
    	return view('tourguide/businessTour',['category'   => $category,
                                            'country'    => $country
                                            ]);
    }
   	public function insert(Request $request)
    {
        if($request->isMethod('post'))
        {
            $data=Input::except(array('_token'));
                $rule=array(
                            'businesscategory'   =>  'Required',
                            'cantravel'         =>  'Required',
                            'businessname'      =>  'Required',  
                           );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('tourguide.business')->withErrors($validator)->with('bussinessRedirected','Please fill bussiness');
            }
      
            $category          =$request->Input('businesscategory');
            $category          = explode("_",$category);
            $categoryId        = $category[0] ? $category[0] : NULL;
            $categoryShortCord =  isset($category[1]) ? $category[1] : null;
            $subcategoryId     = $request->Input('subcategory');
            try{
                    $id = Auth::id();
                    $hasBussiness = Bussiness::where('user_id', $id)->first();
        
                    $data = array(
                        'category_id'   => $categoryId,
                        'canTravel'     => $request->cantravel === "yes",
                        'name'          => $request->businessname,
                        'user_id'       => $request->user()->id
                    );  
                    $data['description']        =$request->description;
                    $data['sub_category_id']    = $subcategoryId;
                    $hearabout                  =$request->get('hearaboutus');
                    $info['hearaboutus']        = $hearabout;  
                    $data['info']           = serialize($info);
                    if($hasBussiness == NULL){
                        $business = Bussiness::create($data);
                        $business->save();
                        $profilePrecentage = Auth::user()->profilePercent;
                        if($profilePrecentage == 30){
                            $user      = User::where('id',$id)->update(['profilePercent'=>40]);
                        }
                        Session::flash('success','Successful Bussiness Created ');
                    }
                    else{
                        $business= Bussiness::where('user_id',$id)->update($data); 
                        Session::flash('success','Successful bussiness Updated ');
                    }
                    return redirect()->route('tourguide.business');
                }
                catch (Exception $e)
                {
                return response()->json([
                'error' => $e->getMessage()
                ]);
                }
        }
    }    
}
