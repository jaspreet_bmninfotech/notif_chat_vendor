<?php
namespace App\Http\Controllers\tourguide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Session;
use Hash;
use App\User;
use App\Country;
use App\Address;
use App\TourGuide;
use App\TourGuideLanguage;
use App\Job;
use App\Post;
use App\PostAttachments;
use App\TourGuideAttachment;
use App\TourGuideJob;
use App\Hires;
use App\Bussiness;
use App\Category;
use App\State;
use App\City;
use Input;
use Validator;
use App\Attachment;
use Auth;
use DB;

class ProfileController extends Controller
{
    public function select(){
        $id =Auth::id();

        $data = User::select('firstName','lastName','username','email','countryDialcode','phone','dob','profile_image')->where('id', $id)->first();

        $languages = \DB::table('user')
                                ->leftjoin('tour_guide_info','user.id','=','tour_guide_info.user_id')
                                ->leftjoin('tour_guide_language','tour_guide_info.id','=','tour_guide_language.tourguideInfo_Id')
                                ->select('tour_guide_language.language')
                                ->where('user.id',$id)->get()->toArray(); 
                     $getlanguage =array_column($languages ,'language');
                               
        // $date=Auth::user()->dob;
        
        // $dob= date("m-d-Y",strtotime($date));

        $attachment =\DB::table('attachment')->select('path')->where('id','=',$data['profile_image'])->first();
        
        $profileattach = \DB::table('attachment')->leftjoin('tourguideattachment','tourguideattachment.attachment_id','=','attachment.id')->where('tourguideattachment.tourguide_id','=',$id)->select('attachment.id','attachment.path','attachment.name')->where('tourguideattachment.tourguide_id','=',$id)->get();
        $docfile = \DB::table('tour_guide_info as tgi')->leftjoin('attachment','attachment.id','=','tgi.attachment_claim')->select('tgi.id','tgi.attachment_claim','attachment.path','attachment.name')->where('user_id','=',$id)->first();
        $licencefile = \DB::table('tour_guide_info as tgi')->leftjoin('attachment','attachment.id','=','tgi.attachment_licence')->select('tgi.id','tgi.attachment_licence','attachment.path','attachment.name')->where('user_id','=',$id)->first();
        $country= Country::all(); 

        return view('tourguide.profile',['data'      =>$data,
                                        'country'    => $country,
                                        'docfile'   => $docfile,
                                        'licencefile'   => $licencefile,
                                        'attachment' => $attachment,
                                        'profileattach' => $profileattach,
                                        'race'      => TourGuide::$race,
                                        'nationality_status'=>TourGuide::$nationality_status,
                                        'activies'  =>TourGuide::$activies,
                                        'getlanguage'=>$getlanguage
                                        ]);
    }
    public function gettourguideprofile(Request $request)
    {
        $id =Auth::id();
        if($request->isMethod('get'))
        {
            $getaddress= \DB::table('user')
                            ->leftjoin('address','user.address_id','=','address.id')
                            ->leftjoin('tour_guide_info','user.id','=','tour_guide_info.user_id')      
                            ->select('user.id as userid','user.gender','address.*','tour_guide_info.*')
                            ->where('user.id',$id)->first();

            $response   =   array("code" => "","success" => "","data"=>""); 
            if(!$getaddress) 
            {
                $response['code']   = 0;
                $response['success']= false;
                $response['data']   = '';
            } else 
            {    if($getaddress->tourguideActivities!='0')
                {

                $getaddress->tourguideActivities  = unserialize($getaddress->tourguideActivities);     
                }
                $response['code']   = 1;
                $response['success']= true;
                $response['data']   = $getaddress;
            }
            return response()->json($response);
        }
    }
    public function removeprofileimages(Request $request){
        $id = Auth::user()->id;
        $remove_id = $request->remove_id;
        $filename = $request->filename;
        $data = \DB::table('tourguideattachment')->where('tourguide_id','=',$id)->where('attachment_id','=',$remove_id)->delete();
        unlink(public_path('/uploads/tourguide/profiles/'.$id.'/'.$filename));
        return response('success');
    }
    public function removedocimages(Request $request){
        $id = Auth::user()->id;
        $remove_id = $request->remove_id;
        $filename = $request->filename;
        unlink(public_path('/uploads/tourguide/doc/'.$id.'/'.$filename));
        $attachment_id = Attachment::where('id','=',$remove_id)->delete();
        $data = \DB::table('tour_guide_info')->where('user_id','=',$id)->update(['attachment_claim' => NULL]);
        return response('success');
    }
    public function removelicimages(Request $request){
        $id = Auth::user()->id;
        $remove_id = $request->remove_id;
        $filename = $request->filename;
        $attachment_id = Attachment::where('id','=',$remove_id)->delete();
        unlink(public_path('/uploads/tourguide/licence/'.$id.'/'.$filename));
        $data = \DB::table('tour_guide_info')->where('user_id','=',$id)->update(['attachment_licence' => NULL]);
        return response('success');
    }
    public function removePostimages(Request $request){
       
        $id = Auth::user()->id;
        $post_id = $request->remove_id;
        $attachment_id = $request->attachment_id;
        $paths = $request->paths;
        $filename = $request->filename;
        $data = \DB::table('postattachments')->where('attachment_id','=',$attachment_id)->where('post_id','=',$post_id)->delete();
        unlink(public_path('/uploads/tourguide/info/'.$id.'/'.$filename));
        return response('success');
    }
    public function removePostvideo(Request $request){
      
        $id = Auth::user()->id;
        $post_id = $request->remove_id;
        $attachment_id = $request->attachment_id;
        $paths = $request->paths;
        $filename = $request->filename;
        $data = \DB::table('postattachments')->where('attachment_id','=',$attachment_id)->where('post_id','=',$post_id)->delete();
        unlink(public_path('/uploads/video/tourguide/info/'.$id.'/'.$filename));
        return response('success');
    }
    public function uploadfiles(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/profile/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $file = Input::file('file');

            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
        $hasattachment_id = User::select('profile_image')->where('id','=',$id)->first();
        if ($hasattachment_id['profile_image'] == "") {
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $attachment_id = $data->id;
            $datas = User::where('id',$id)->update(
                         ['profile_image' => $attachment_id
                        ]);
            }else{
             
        $data = Attachment::where('id',$hasattachment_id['profile_image'])->update(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            
       }
        return response(['success'=>true]);
        
    }

      public function updateimageText(Request $request){
        $postId = $request->postId;
        $imagetitle = $request->title;
        $imagemessage = $request->message;
        $user_id =Auth::id();
        
            $data = Post::where('id','=',$postId)->update(
                     ['title' => $imagetitle,
                      'message' => $imagemessage
                    ]);

            if ($data) {
                return response(['success'=>true]);   
            }else{
                return response(['success'=>false]);
            }
    
    }
      public function updateuploadfiles(Request $request)
   {
    

        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/info/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $post_id = $request->session()->get('PostId');
            $attachment_id = $data->id;
            $datas = PostAttachments::create(
                         ['post_id' => $post_id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save();
            
        }
       
        return response(['success'=>true]);
        
    }
     public function updateuploadvfiles(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/info/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $post_id = $request->session()->get('PostId');
            $attachment_id = $data->id;
            $datas = PostAttachments::create(
                         ['post_id' => $post_id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save();
            
        }
       
        return response(['success'=>true]);
        
    }
    /*tg document upload  */
     public function uploaddoc(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/doc/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $file = Input::file('file');
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
        $hasattachment_id = TourGuide::select('attachment_claim')->where('user_id','=',$id)->first();
        if ($hasattachment_id['attachment_claim'] == "") {
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $attachment_id = $data->id;
            $datas = TourGuide::where('user_id',$id)->update(
                         ['attachment_claim' => $attachment_id
                        ]);
            }else{
             
        $data = Attachment::where('id',$hasattachment_id['profile_image'])->update(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            
       }
        return response(['success'=>true]);
        
    }

    public function uploadlicence(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/licence/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $file = Input::file('file');
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
        $hasattachment_id = TourGuide::select('attachment_licence')->where('id','=',$id)->first();
        if ($hasattachment_id['attachment_licence'] == "") {
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $attachment_id = $data->id;
            $datas = TourGuide::where('user_id',$id)->update(
                         ['attachment_licence' => $attachment_id
                        ]);
            }else{
             
                 $data = Attachment::where('id',$hasattachment_id['attachment_licence'])->update(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            
       }
        return response(['success'=>true]);
        
    }

    /*  insurance doc*/

     public function uploadinsurance(Request $request)
   {
  
        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/insurance/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $file = Input::file('file');

            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
        $hasattachment_id = TourGuide::select('carinsurance_attachment_id')->where('id','=',$id)->first();
        if ($hasattachment_id['carinsurance_attachment_id'] == "") {
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $attachment_id = $data->id;
            $datas = TourGuide::where('user_id',$id)->update(
                         ['carinsurance_attachment_id' => $attachment_id
                        ]);
            }else{
             
                 $data = Attachment::where('id',$hasattachment_id['carinsurance_attachment_id'])->update(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            
       }
        return response(['success'=>true]);
        
    }
  public function info($id){
        $user_id =Auth::id();

        $data = User::select('id','firstName','lastName','username','email','phone','dob','address_id','profile_image')->where('id',$id)->first();
        $job = Job::select('id')->where('id', $id)->first();
        $jobs = Job::select('id','name','description','created_at','jobSuccess')->where('user_id', $user_id)->get();
        $hire = Hires::select('job_id')->where('user_id', $user_id)->get();
        $business = Bussiness::select('id','name','description','category_id','address_id','ratePerHour','ratePerProject')->where('user_id', $id)->first();
        $busiactivity = TourGuide::select('id','tourguideactivities','showyou')->where('user_id', $id)->first();
        
        $businessactiv = unserialize($busiactivity['tourguideactivities']);
        $language = TourGuideLanguage::select('id','language')->where('tourguideinfo_id', $business['id'])->get();
        $category = Category::select('name')->where('id', $business['category_id'])->first();
        $address = Address::select('country_id','state_id','city_id')->where('id','=',$data['address_id'])->first();
        $country = Country::select('id','name')->where('id','=',$address['country_id'])->first();
        $state = State::select('name')->where('id','=',$address['state_id'])->first();
        $city = City::select('name')->where('id','=',$address['city_id'])->first();
        $attachment = Attachment::select('path')->where('id',$data['profile_image'])->first();
        $businessAttachments = \DB::table('businessattachment')->leftjoin('attachment','attachment.id','=','businessattachment.attachment_id')->select('attachment.id','attachment.path')->where('business_id','=',$business['id'])->get();

        $getfeedback= DB::table('feedback')->leftjoin('user','user.id','=','feedback.feedToId')->where('feedToId' , Auth::user()->id)->get();
        $getfeedback=json_decode($getfeedback,true);
        $processArray = [];
        $feedback_id = [];
        $countId = [];

        foreach ($getfeedback as $key => $value) {
            if(!in_array($value['id'], $feedback_id)){
                $countId[$value['id']] = 1; 
                $feedback_id[] = $value['id'];
                $processArray[$value['id']] = ['star_rate' => $value['starRate'] , 'count' => $countId[$value['id']]]; 
            }else{
                $lastStarRate = $processArray[$value['id']]['star_rate'];
                $lastcount = $countId[$value['id']];
                $countId[$value['id']] = $lastcount + 1; 
                $processArray[$value['id']] = ['star_rate' => ($lastStarRate + $value['starRate']) , 'count' => $countId[$value['id']]]; 

            }
        }
        $totalstars = array();
        foreach ($processArray as $key => $value) {
            $totalstar = 5*$value['count'];
            $totalstars[$key] = ['totalstars' => $totalstar, 'success_percent' => round(($value['star_rate']/$totalstar) * 100)];
        
        }

        return view('tourguide/profile/index',['totalstars' => $totalstars, 'data'=>$data,'user_id'=>$user_id,'job'=>$job,'jobs'=>$jobs,'businessactiv'=>$businessactiv,'activites'=>TourGuide::$activies,'business'=>$business,'language'=>$language,'country'=>$country,'state'=>$state,'city'=>$city,'hire'=>$hire,'category'=>$category,'attachment'=>$attachment,'businessAttachments'=>$businessAttachments
                                            ]);
    }
    public function getjobs(Request $request,int $page){
      
        $user_id =Auth::id();
       $paginate = Job::leftjoin('hires','hires.job_id','=','job.id')->select('job.id','job.name','job.maxRate','job.description','job.isOngoing','job.isHourly','job.user_id','job.budget','job.created_at','hires.status')->where('hires.user_id','=',$user_id);
        if ($request->filter == 'old') {
            $paginate = $paginate->orderBy('hires.job_id', 'ASC')->paginate(2,['*'], 'page',$page);
            
        }else{
            $paginate = $paginate->orderBy('hires.created_at', 'desc')->paginate(2,['*'], 'page',$page);

        }
            $paginate->withPath(route('tourguide.getjobs', ['page'=> intval($page + 1)]));

        echo json_encode($paginate, true);

    }

    public function postinfo() {
        $id =Auth::id();
        $post =\DB::table('post')->leftjoin('postattachments','post.id','=','postattachments.post_id')->leftjoin('attachment','attachment.id','=','postattachments.attachment_id')->select('post.id','post.title','post.message','post.type','post.user_id','attachment.path','attachment.name','postattachments.post_id','postattachments.attachment_id')->where('post.user_id','=',$id)->orderBy('post.id','DESC')->get();
        echo json_encode($post, true);
    }

    public function deletepostinfo(Request $request) {
        $id =Auth::id();
        $postId = $request->postId;
        $attachmentId = $request->attachmentId;
        $filename = $request->filename;
        $post =\DB::table('post')->where('id','=',$postId)->delete();
        if ($filename != "") {
        $posta =\DB::table('postattachments')->where('attachment_id','=',$attachmentId)->delete();
        $attach =\DB::table('attachment')->where('id','=',$attachmentId)->delete();
        @unlink(public_path('/uploads/tourguide/info/'.$id.'/'.$filename));
        }
        return response('success');
    }

    public function uploadimageText(Request $request){
        $user_id =Auth::id();
        $type = '2';
        $files = Input::file('files');
        
                $data = Post::create(
                         ['title' => $request->imagetitle,
                          'message' => $request->imagemessage,
                          'type' => $type,
                          'user_id' => $user_id
                        ]);
                $data->save();
                $request->session()->put('PostId', $data->id);

                    Session::flash('success');
                if ($data) {
                    return response(['success'=>true]);   
                }else{
                    return response(['success'=>false]);
                }

    }
    
    
    public function uploadfilestourguide(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/info/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $post_id = $request->session()->get('PostId');
            $attachment_id = $data->id;
            $datas = PostAttachments::create(
                         ['post_id' => $post_id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save();
            
        }
       
        return response(['success'=>true]);
        
    }


     public function multiuploadfiles(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/tourguide/profiles/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $post_id = $request->session()->get('PostId');
            $attachment_id = $data->id;
            $datas = TourGuideAttachment::create(
                         ['tourguide_id' => $id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save();
            
        }
       
        return response(['success'=>true]);
        
    }

    public function imageUploadfile(Request $request){
        $id = Auth::user()->id;
        $files  = json_decode($request->Input('hid'));
            
                foreach ($files as $key => $file) {
                    $attach =new Attachment();
                    $name   = $file->originalName;
                    $attach->name   =$name;
                    $attach->path   = $file->path;
                    $attach->mimeType= $file->mimeType;
                    $attach->job_id = $id;
                    $attach->save();  
                };
    }

    public function uploadvideoText(Request $request){
        $user_id =Auth::id();
        $type = '3';
                $data = Post::create(
                         ['title' => $request->videotitle,
                          'message' => $request->videomessage,
                          'type' => $type,
                          'user_id' => $user_id
                        ]);
                $data->save();
                $request->session()->put('PostId', $data->id);

                    Session::flash('success');
                if ($data) {
                    return response(['success'=>true]);   
                }else{
                    return response(['success'=>false]);
                }

    }
    
    public function uploadfilesv(Request $request)
   {
    
        $id = Auth::user()->id;

        $destinationPath = "uploads/video/tourguide/info/".$id;
        
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
               
            
            if ($ext == 'mp4') {
                
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $post_id = $request->session()->get('PostId');
            $attachment_id = $data->id;
            $datas = PostAttachments::create(
                         ['post_id' => $post_id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save();
            }else {
                return response(['error'=>false]);
            }
            
        }
       
        return response(['success'=>true]);
        
    }
    public function videoUploadfile(Request $request){
        $id = Auth::user()->id;
        $files  = json_decode($request->Input('hid'));
            
                foreach ($files as $key => $file) {
                    $attach =new Attachment();
                    $name   = $file->originalName;
                    $attach->name   =$name;
                    $attach->path   = $file->path;
                    $attach->mimeType= $file->mimeType;
                    $attach->job_id = $id;
                    $attach->save();  
                };
    }    
    public function progressinfo(){
        $user_id = Auth::id();
      $data = Job::select('id','jobSuccess')->where('id',$user_id)->first();
        echo json_encode($data, true);   
    } 

    public function create(Request $request){
        $user_id =Auth::id();
       
        $title=$request->input('title');
        
        if($title)
        {
        $request->validate([
                            'title' => 'required',
                            'message' => 'required'
                            ]);
        $message=$request->input('message');
        $data = Post::create(
                 ['title' => $title,
                  'message' => $message,
                  'type' => 1,
                  'user_id' => $user_id,
                ]);
        $data->save();
            Session::flash('success');
            
        }

        $link=$request->input('link');
         if($link)
        { 
             $request->validate([
                            'link' => 'required'
                            ]);
        $data = Post::create(
                 ['title' => $link,
                  'message' => "",
                  'type' => 4,
                  'user_id' => $user_id,
                ]);
        $data->save();
            Session::flash('success');
            return back();
        }
        return back();
    }
    

    public function update(Request $request)
    {
        $id =Auth::id();
        if($request->isMethod('post'))
        {
        $address=Null;
        $data=Input::except(array('_token'));
        $rule=array(
                'firstName'=>'Required_with:lastname|Alpha',
                'lastName' =>'Required_with:firstName|Alpha',
                // 'birthCountry'  =>  'Required',
                'phone'     => 'Required|min:10|max:12',
                'country'   =>  'Required',
                'state'     =>  'Required',
                'city'      =>  'Required',
                'postalCode'=>  'Required|integer|min:6',
                'address'   =>  'Required',
        );
        $validator=Validator::make($data, $rule);
        if($validator->fails()) {
            return redirect()->route('tourguide.profile')->withErrors($validator)->with('profileRedirected','Please fill profile');
        }
        $id =Auth::id();

        $firstName=Input::get('firstName');
        $lastName=Input::get('lastName');
        $gender=$request->gender; 
        $dialCode=  $request->dialCode;
        $phone=Input::get('phone');
        $dob=Input::get('dob');
        $profile = ['firstName' => $firstName,
                    'lastName' => $lastName,
                    'gender' => $gender,
                    'countryDialcode'  => $dialCode,
                    'phone' => $phone,
                    'dob' => $dob,
                ];               
        $profilePrecentage = Auth::user()->profilePercent;
        if($profilePrecentage == 40 || $profilePrecentage==0){
            $profile['profilePercent'] = 50;
        }
        $user      = User::where('id',$id)->update($profile);
        $race=Input::get('race');
        $birth_country=Input::get('birth_country');
        $nationality_status =Input::get('nationality_status');
        $know_country =Input::get('know_country');
        $have_car =$request->get('isCars') == 'yes' ? 1: 0;
        $car_for_tour=$request->get('carforTour') == 'yes' ? 1: 0;
        $make =$request->get('make');
        $model =$request->get('model');
        $year =$request->get('year');
        $plateno=Input::get('plateNo');
        $car_licenceno=Input::get('licenceNo');
        $car_insurance=$request->get('carinsurance')==='yes';
        $insurancereason =Input::get('insurancereason');
        $touractivies=Input::get('tourguideactivities');
        $showyou =Input::get('showyou');
        $data = [
                'user_id'   =>Auth::user()->id,
                'race'      => $race,
                'birthCountry'     =>$birth_country,
                'nationalityStatus'=>$nationality_status,
                'knowCountry'  =>$know_country,
                'haveCar'      =>$have_car,
                'carForTour'  =>$car_for_tour,
                'carMake'      =>$make,
                'carModel'     =>$model,
                'carYear'      =>$year,
                'carPlate'     =>$plateno,
                'carLicenceno' =>$car_licenceno,
                'carInsurance' =>$car_insurance,
                'noInsuranceReason'=> $insurancereason,
                'showYou'=>$showyou
                ];
        $data['tourguideActivities']   =serialize($touractivies);     
        $address_id=Auth::user()->address_id;  
        $address = [
                    'country_id'    => $request->get('country'),
                    'state_id'      => $request->get('state'),
                    'city_id'       => $request->get('city'),
                    'postalCode'    => $request->get('postalCode'),
                    'address'       => $request->get('address')
                    ];      
        if($address!= NULL){
            if($address_id     != NULL)
            {
                $address        =   Address::where('id',$address_id)
                                    ->update($address);   
            }else{
                $address = Address::create($address);
                $address->save();
                $addressId      = $address->id;
                $address =User::select('address_id')->where('id',$id)
                                    ->update(['address_id'  =>$addressId
                                            ]);   
            }
        }
        // $hastourguide=TourGuide::where('user_id',$id)->first();
        // if($hastourguide == NULL){
        //     $tourguideinfo = TourGuide::create($data);
        //     $tourguideinfo->save();
        //     $tourguideinfo_id = $tourguideinfo->id;
        //     Session::flash('success','Successful Bussiness Created ');
        // }
        // else{

            $tourguideinfo= TourGuide::where('user_id',$id)->update($data);
            $tourguide = TourGuide::select('id')->where('user_id',$id)->first();
           
            $tourguideinfo_id=$tourguide->id;
             Session::flash('success','Successful Profile Updated');
        // }
            $guidelanguage=$request->get('guidelanguage');
            if($guidelanguage){
                $guidelanguageArr = array();
                TourGuideLanguage::where('tourguideInfo_Id',$tourguideinfo_id)->delete();
                    foreach ($guidelanguage as $key => $value) {
                        $guide=new TourGuideLanguage();
                        $guide->language=$value;
                        $guide->tourguideInfo_Id=$tourguideinfo_id;
                        $guide->save();
                    } 
            }  
         return redirect()->route('tourguide.profile');     
    }
    }


}
