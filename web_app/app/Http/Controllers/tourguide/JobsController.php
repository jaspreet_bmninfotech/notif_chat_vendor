<?php

namespace App\Http\Controllers\tourGuide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\TourGuideDeclineClientVendorJob;
use App\Notifications\TourGuideAcceptClientVendorJob;
use App\Job;
use App\Bid;
use App\Hires;
use App\TourGuideInvites;
use App\TourGuideJob;
use App\User;
use Response;
use Auth;
use Validator;
use App\feedback;
use Input;
use App\Category;
use App\Country;
use App\Attachment;
use App\Timezone;
use App\JobAttachment;
use App\Setting;
use Session;
use DB;

class JobsController extends Controller
{
 
    public function list(Request $request)
    {
        $id = Auth::user()->id;
        $tourguideinvite = DB::table('tourguideinvites')->leftjoin('tourguidejob','tourguidejob.id','=','tourguideinvites.tourguide_job_id')
            ->select('tourguideinvites.tourguide_job_id','tourguideinvites.inviteMessage','tourguidejob.status','tourguideinvites.user_id',
        'tourguideinvites.created_at','tourguidejob.title','tourguideinvites.status as clientStatus')->where('tourguideinvites.user_id','=',$id)->orderBy('tourguideinvites.id','desc')->paginate(10);
            
            return view('tourguide.jobs.list', ['tourguideinvite' => $tourguideinvite ]);
    }
    public function detail($id){
        $user_id = Auth::user()->id;
        $hasdata = TourGuideInvites::where('tourguide_job_id','=',$id)->where('user_id','=',$user_id)->get();
        $jobid = TourGuideJob::where('id','=',$id)->first();

        $clientuser =User::select('id','firstName','lastName','username','email','profile_image','address_id')->with('profileimg')->with('address')->where('id',$jobid['user_id'])->first();
        $country = Country::select('name')->where('id',$clientuser->address[0]['country_id'])->first();
        $totalJobs_id = Job::select('id')->where('user_id','=',$jobid['user_id'])->get();
            $totalJobs = count($totalJobs_id);
            $cJobs_id = Job::select('jobSuccess','maxRate','id')->where('user_id','=',$jobid['user_id'])->first();
            $getfeed= feedback::getfeedback($jobid['user_id']);
        foreach ($hasdata as $key => $value) {
            $value['tourguide_job_id'];
        if($value['tourguide_job_id'] == $id) {
        $data = DB::table('tourguidejob')->leftjoin('tourguideinvites as tgi','tgi.tourguide_job_id','=','tourguidejob.id')->select('tourguidejob.id','tourguidejob.title','tourguidejob.user_id','tourguidejob.created_at','tourguidejob.fromdatetime','tourguidejob.status','tgi.status as invitestatus','tourguidejob.todatetime','tourguidejob.totalpeople')->where('tourguidejob.id','=',$id)->first();

        $user =User::select('firstName','lastName','username','email')->where('id',$user_id)->first();
            $getfeedback= DB::table('feedback')->leftjoin('user','user.id','=','feedback.feedToId')->where('feedToId' , $data->user_id)->get();
            $getfeedback=json_decode($getfeedback,true);
            $processArray = [];
            $feedback_id = [];
            $countId = [];

            foreach ($getfeedback as $key => $value) {
                if(!in_array($value['id'], $feedback_id)){
                    $countId[$value['id']] = 1; 
                    $feedback_id[] = $value['id'];
                    $processArray[$value['id']] = ['star_rate' => $value['starRate'] , 'count' => $countId[$value['id']]]; 
                }else{
                    $lastStarRate = $processArray[$value['id']]['star_rate'];
                    $lastcount = $countId[$value['id']];
                    $countId[$value['id']] = $lastcount + 1; 
                    $processArray[$value['id']] = ['star_rate' => ($lastStarRate + $value['starRate']) , 'count' => $countId[$value['id']]]; 

                }
            }
            $totalstars = array();
            foreach ($processArray as $key => $value) {
                $totalstar = 5*$value['count'];
                $totalstars[] = ['totalstars' => $totalstar, 'success_percent' => round(($value['star_rate']/$totalstar) * 100)];
            
            }
        return view('tourguide.jobs.detail',['country'=>$country,'feedback' =>$getfeed,'cJobs_id'=>$cJobs_id,'totalJobs'=>$totalJobs,'data' => $data,'totalstars' => $totalstars,'clientuser'=>$clientuser]);
        }else{
        return redirect()->route('tourguide.job');
        }
        }
    }
    public function declined(Request $request){
        $declined_id = $request->declined_id;
        $user_id = Auth::user()->id;
        $data = TourGuideInvites::where('user_id','=',$user_id)->where('tourguide_job_id','=',$declined_id)->update(['status'=>TourGuideInvites::status_declined]);
        $update =  TourGuideInvites::select('id')->where(['tourguide_job_id'=>$declined_id,'user_id'=>$user_id,'status'=>'0'])->first();
        if($declined_id!=NULL)
        {    
            if($update)
            {
                $operation = 'declined job';
                $tourguide_declinejob= TourGuideInvites::with('tourguidejob')->find($update->id); 
                $user=$tourguide_declinejob->tourguidejob->user_id;
                $senduser =User::where('id',$user)->first();
                $uname= Auth::user()->username;
                $senduser->notify(new TourGuideDeclineClientVendorJob($tourguide_declinejob,$uname, $operation));     
            }
        }
         return response(['success'=>true]);
    }
    public function accept(Request $request){
        $accept_id = $request->accept_id;
        $user_id = Auth::user()->id;
        $data = TourGuideInvites::where('user_id','=',$user_id)->where('tourguide_job_id','=',$accept_id)->update(['status'=>TourGuideInvites::status_accepted]);
         $update =  TourGuideInvites::select('id')->where(['tourguide_job_id'=>$accept_id,'user_id'=>$user_id,'status'=>'1'])->first();
        if($accept_id!=NULL)
        {    
            if($update)
            {
                $operation = 'Accepted job';
                $tourguide_acceptjob= TourGuideInvites::with('tourguidejob')->find($update->id); 
                $user=$tourguide_acceptjob->tourguidejob->user_id;
                $senduser =User::where('id',$user)->first();
                $uname= Auth::user()->username;
                $senduser->notify(new TourGuideAcceptClientVendorJob($tourguide_acceptjob,$uname,$operation));     
            }
        }
         return response(['success'=>true]);
    }
}


