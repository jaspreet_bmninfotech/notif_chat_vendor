<?php

namespace App\Http\Controllers\tourguide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TourGuideHire;
use App\Reasons;
use App\User;
use App\Country;
use Auth;
use App\Notifications\VendorOfferAcceptTourguide;
use App\Notifications\VendorOfferDeclineTourguide;

class TourGuideController extends Controller
{
	public function index()
	{
		return view('tourguide/index');
	}
    public function list() {

        $id = Auth::id();

        $hire_list = TourGuideHire::where([['user_id',$id], ['status', '=', '2']])->with('tourguidejob')->get();
        return view('tourguide.hire.list', [
            'tourguidehire' => $hire_list
        ]);
    }
    public function show($id)
    {
        $hire_proposal = TourGuideHire::with('tourguidejob')->find($id);
        $hire_proposaljob = TourGuideHire::with('tourguidejob')->find($id);
        $from_country_id =Country::select('name')->where('id',$hire_proposaljob->tourguidejob->from_country_id)->first();
        $from_state_id =Country::select('name')->where('id',$hire_proposaljob->tourguidejob->from_state_id)->first();
        $from_city_id =Country::select('name')->where('id',$hire_proposaljob->tourguidejob->from_city_id)->first();
        $to_country_id =Country::select('name')->where('id',$hire_proposaljob->tourguidejob->to_country_id)->first();
        $to_state_id =Country::select('name')->where('id',$hire_proposaljob->tourguidejob->to_state_id)->first();
        $to_city_id =Country::select('name')->where('id',$hire_proposaljob->tourguidejob->to_city_id)->first();
        $user =User::select('username','profile_image')->with('profileimg')->where('id',$hire_proposaljob->tourguidejob->user_id)->first();
        $reasons       = Reasons::getReasons('declined');
        // $bid           = Bid::getJobBid($hire_proposal->user_id, $hire_proposal->job_id);
        if($hire_proposal == null) {
            abort(404);
        }
        return view('tourguide.hire.detail', [
            'tourguidehire'    => $hire_proposal,
            'user'    => $user,
             'hire_proposaljob'     => $hire_proposaljob,
             'from_country_id'     => $from_country_id,
             'from_state_id'     => $from_state_id,
             'from_city_id'     => $from_city_id,
             'to_country_id'     => $to_country_id,
             'to_state_id'     => $to_state_id,
             'to_city_id'     => $to_city_id,
            'reasons' => $reasons
        ]);
    }
    public function accept(Request $request, $hire_id)
    {
       if($request->get('check') == 'on') {
           $message = $request->get('message');
           if($message != null) {
               //save message
           }
           // $status_name = 'Accept';
           $status_name =TourGuideHire::status_opened;
           $operation = 'Accepted hire offers';
           $hire_proposal = TourGuideHire::with('tourguidejob')->find($hire_id);
           TourGuideHire::updateStatus($hire_id,$status_name,$message);
           $s_user = $hire_proposal->tourguidejob->user_id;
           $user = User::where('id',$s_user)->first();
           $senduser= Auth::user()->username;
           $user->notify(new VendorOfferAcceptTourguide($hire_proposal,$senduser,$operation));
           return redirect()->route('tourguide.hire.list')
               ->with('hire_flash_accept', 'Offer is accepted');
       }
    }
    public function decline(Request $request, $hire_id)
    {
        if($request->get('reason') != null) {
            // $status_name = 'Declined';
            $status_name=TourGuideHire::status_close;
            $message = $request->get('message');
            $reason = $request->get('reason');
            TourGuideHire::updateStatus($hire_id,$status_name,$message, $reason);
            $hire_proposal = TourGuideHire::with('tourguidejob')->find($hire_id);
            $operation = 'Declined hire offers';
           	$s_user = $hire_proposal->tourguidejob->user_id;
           	$user = User::where('id',$s_user)->first();
           	$senduser= Auth::user()->username;
            $user->notify(new VendorOfferDeclineTourguide($hire_proposal,$senduser, $operation));
            return back();
            return redirect()->route('tourguide.hire.list')
                ->with('hire_flash_decline', 'Hire offer declined');
        }
    }
}
