<?php

namespace App\Http\Controllers\tourguide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Bussiness;
use App\User;
use Input;
use Response;
use Session;
use Validator;

class PaymentController extends Controller
{
     public function index(){
	    $id     =Auth::id();
	    $data= Bussiness::where('user_id', $id)->select('rateperHour','rateperProject','minDuration')->first();
    	return view('tourguide/tourGuidePayment',['data' =>$data,
                                                'tourduration' =>Bussiness::$tourduration
                                                ]);
    }
    public function savepayment(Request $request)
    {
       if($request->isMethod('post'))
      {
        $data=Input::except(array('_token'));
            $rule=array(
                        'rateperHour'   =>  'Required',
                        'rateperProject'     =>  'Required',
                        'minduration'       =>'Required'
                       );
        $validator=Validator::make($data, $rule);
        if($validator->fails()) {
            return redirect()->route('tourguide.payment')->withErrors($validator)->with('paymentRedirected','Please fill payment');
        }
    	  $id = Auth::id();
        $hasBussiness = Bussiness::where('user_id', $id)->first();
        $rateperHour=$request->get('rateperHour');
        $rateperProject  	=	$request->get('rateperProject');
        $hasfree=$request->get('hasFree') ? 1:0;

        $minduration=$request->get('minduration');

       	$data = array(
       					'ratePerHour' 		=>$rateperHour,
       					'ratePerProject' 	=>$rateperProject,
                'user_id'         =>$id,
                'hasFree'         =>$hasfree,
                'minDuration'     =>$minduration
    				);

        if($hasBussiness==NULL)
        {
          $business= Bussiness::create($data);
          $business->save();
          $profilePrecentage = Auth::user()->profilePercent;
          if($profilePrecentage == 50)
          {
            $user = User::where('id',$id)->update(['profilePercent'=>60]);
          }
          Session::flash('success','Successful Project Charge Rate Saved ');
        }
        else{
          $business= Bussiness::where('user_id',$id)->update($data);  
          Session::flash('success','Successful Project Charge Rate updated ');
        }
        return back();
      }
    }
}