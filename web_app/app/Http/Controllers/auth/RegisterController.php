<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use Validator;
use Input;
use Auth;
use Response;
use App\TourGuide;
use App\Traits\ActivationTrait;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use ActivationTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    function register(Request $request)
    {
        if($request->isMethod('post'))
        {
            $request->validate([
                'username' => 'required',
                'email'=>'required',
                'password' =>'required|string|min:6',
                'confirmpassword' =>'required|string|min:6'
            ]);            
            $data =[
                'username'          => $request->username,
                'type'              => $request->accounttype,
                'email'             => $request->email,
                'password'          => bcrypt($request->password),
                'confirmpassword'   => bcrypt($request->confirmpassword),
                'remember_token'    => $request->_token,   
            ];
            if ($request->accounttype == 'vn') 
            {
                $data['profilePercent']    = 20;
            }
            else if($request->accounttype == 'cn')
            {
                $data['profilePercent'] = 40;
            }
            else if($request->accounttype == 'tg')
            {
                $data['profilePercent'] = 40;
            }
            $user = User::create($data);
            $user->save();
            $user_id=$user->id;
            if($request->accounttype == 'tg')
            {
                $tourguideinfo=new TourGuide();
                $tourguideinfo->user_id=$user_id;
                $tourguideinfo->save();
            }
              // $this->initiateEmailActivation($user);
            Session::flash('success','Successfully Registered.');                 
            return view('layouts.auth.login');
        }
        return view('layouts.auth.register');
    }

      public function checkusername()
    {
        $check = Input::get('username');
        $query = \DB::table('user')->select('username')
                 ->where('username','=',$check)
                 ->count();
        $response = array("code" => "","message" => "","success" => ""); 
        if( $query > 0)
        {
            $response['code'] = 0;
            $response['message'] = "Username not available";
            $response['success'] = false;
        } else 
        {  
            $response['code'] = 1;
            $response['message'] = "";
            $response['success'] = true;
        }
        echo json_encode($response);
    }
    public function checkemail()
    {    
        $mail=Input::get('email');
        $qre = \DB::table('user')->select('email')
                 ->where('email','=',$mail)
                 ->count();
        $response = array("code" => "","message" => "","success" => ""); 
        if( $qre > 0) 
        {
            $response['code'] = 0;
            $response['message'] = "email not available";
            $response['success'] = false;
        } 
        else {   
            $response['code'] = 1;
            $response['message'] = "";
            $response['success'] = true;
        }
        echo json_encode($response);
    }
    public function confirmUser($id, $token)
    { 
        $user = User::find($id);
        if (!isset($user)) {
            return view('layouts.auth.login')->withErrors('your token is not valid');
        }
        $userToken = $user->remember_token;
        if($userToken == $token)
        {
            $confirmed = User::find($id);
            $confirmed->isconfirmed = 1;
            $confirmed->save();
            return view('layouts.auth.login')->with('success','your confirmation has done,!please login');
        } else{
            return view('layouts.auth.login')->withErrors('your token is not valid');
        }       
    } 
}
