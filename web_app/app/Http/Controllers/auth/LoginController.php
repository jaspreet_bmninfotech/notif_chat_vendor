<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Routing\Redirector;
use Validator;
use Input;
use Auth;
use Response;
use URL;
use App\Traits\ActivationTrait;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    private $param;
    public function login(Request $request)
    {
        $param = $request->action;
        if($request->isMethod('post'))
        { 
            $data=Input::except(array('_token'));
            $rule=array(
                'email'=>'required|string|max:255',
                'password' =>'required|string|min:6',
            );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('login')->withErrors($validator);
            }
            else {  
                $userOrEmail =$request->input('email');
                  $field = filter_var($userOrEmail, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
                $request->merge([$field => $userOrEmail]);
                $userdata = array(
                                    // 'email'=>Input::get('email'),
                                    $field => $userOrEmail,
                                    'password'=>Input::get('password'),
                                    'isConfirmed'=> true,
                                );
                if($field=="username")
                {
                $user = User::where('username', $userdata[$field])->first();     
                }
                else
                {
                    $user = User::where('email', $userdata[$field])->first();     
                }
                if($user){
                        if($user->isconfirmed == 1){
                                    
                                if (Auth::attempt($userdata))
                                {
                                    if($param==Null)
                                    {
                                        if ($user->type == 'cn') {
                                            return redirect()->route('client.job');
                                        }
                                        else if ($user->type == 'vn') {
                                            return redirect()->route('vendor.dashboard');
                                        }
                                        else if($user->type == 'tg') {
                                            return redirect()->route('tourguide.profile');
                                        }
                                        
                                    }else
                                    {
                                        if ($user->type == 'cn') {
                                            if($param=='team')
                                            {
                                                return redirect('/client/team');
                                            }
                                            elseif($param=='trip')
                                            {
                                                return redirect('/client/search/tourguide');
                                            }
                                        }
                                        else if ($user->type == 'vn') {
                                            return redirect()->route('vendor.dashboard');
                                        }
                                        else if($user->type == 'tg') {
                                            return redirect()->route('tourguide.profile');
                                        }
                                    }

                                   
                                }else{
                                    return redirect()->route('login')->withErrors('Invalid email/password.');
                                }
                           
                         }else{
                              return redirect()->route('login')->withErrors('Please verify email address.');
                         }
                }else{
                    return redirect()->route('login')->withErrors('Invalid username/email.');
                }
               
            }
        }
        return view('layouts.auth.login');
    }
}
