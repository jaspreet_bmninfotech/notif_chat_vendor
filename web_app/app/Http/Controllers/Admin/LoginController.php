<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LoginController extends Controller
{
    

    public function showLoginForm(){

    	return view('admin.login');

    }

    public function login(Request $request){

    	if(Auth::guard('admin')->attempt(['email'=>$request['email'] , 'password'=> $request['password'] ] )){
    		return redirect()->route('admin.home.index');
    	}

    }
}
