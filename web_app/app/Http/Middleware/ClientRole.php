<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use App\User;

class ClientRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // only access client
        if ($request->user()->type != 'cn') {
            return redirect()->route('/');
        }
        else{
            $session = $request->session();
             $isProfileRedirected =  $session->exists('profileRedirected');
              $isAddressRedirected =  $session->exists('addressRedirected');
            $profilePercent = Auth::user()->profilePercent;
            if($profilePercent != 60 ||
                    $profilePercent < 60)
                {
                    if($profilePercent == 40 || $profilePercent == 0){
                        if(!$isProfileRedirected && 
                            $request->route()->getName() != "client.profile.update"){
                                return redirect()->route('client.profile')->with('profileRedirected','Please fill profile');
                        }
                    }
                    if($profilePercent == 50){
                        if(!$isAddressRedirected && 
                            $request->route()->getName() != "client.profile.mailing"){
                                return redirect()->route('client.profile')->with('addressRedirected','Please fill address');
                        }
                    }
                }
        }
        return $next($request);
    }
}
