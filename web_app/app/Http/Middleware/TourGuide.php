<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;

class TourGuide
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if ($request->user()->type != 'tg') {
            return redirect()->route('/');
        }
        else{
            $session = $request->session();
            $isProfileRedirected =  $session->exists('profileRedirected');
            // $isAddressRedirected =  $session->exists('addressRedirected');
            // $isBussinessRedirected =  $session->exists('bussinessRedirected');
            $isPaymentRedirected =  $session->exists('paymentRedirected');

            $profilePercent = Auth::user()->profilePercent;
            if($profilePercent != 60 ||
                    $profilePercent < 60)
                {
                    if($profilePercent == 40 || $profilePercent == 0){
                        if(!$isProfileRedirected && 
                            $request->route()->getName() != "tourguide.profile.update"){
                                return redirect()->route('tourguide.profile')->with('profileRedirected','Please fill profile');
                        }
                    }
                    
                    if($profilePercent == 50){
                        if(!$isPaymentRedirected && 
                            $request->route()->getName() != "tourguide.payment.save"){
                                return redirect()->route('tourguide.payment')->with('paymentRedirected','Please fill payment');
                        }
                    }

                }
        }
        return $next($request);
    }
}
