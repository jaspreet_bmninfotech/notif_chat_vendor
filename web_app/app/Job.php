<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Job extends Model 
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    const job_public = 0;
    const job_private = 1;
    const is_hourly = 0;
    const is_fixed = 1;
    const event_fulltime = 'ft';
    const event_parttime = 'pt';
   
    const status_open = 1;
    const status_close = 0;
    const status_expired = 2;

    protected $table = 'job';
    protected $fillable=['eventtype','category_id','subcategory_id','subSubcategory_id','name','description','budget','user_id','status','isHourly','vendorPreffer','peopleAttending','isOneTime','isOngoing','timeZone','startDateTime', 'minRate', 'maxRate','jobSuccess','addressInfo','language','canTravel','isPrivate'];
    public $timestamps = true;
    public function bids()
    {
        return $this->hasMany('App\Bid');
    }
    public function hires()
    {
        return $this->hasMany('App\Hires');
    }
     public function invites()
    {
        return $this->hasMany('App\Invites');
    }
    public function messages()
    {       
        return $this->hasMany('App\Messages');
    }
    public function user()
    {       
        return $this->belongsTo('App\User');
    }
    public function attachments()
    {
        return $this->hasMany('Attachment', 'job_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function reviews()
    {
        return $this->hasMany('Review');
    }
    public static function all_records()
    {
        $jobs = Job::get();
        return sizeof($jobs) > 0 ? $jobs->toArray() : array();
    }
    public static function all_paidjobs()
    {
        $jobs = Job::select('id','name')->where('isFree','=',1)->orWhere('hasPaid','=',1)->get();
        return sizeof($jobs) > 0 ? $jobs->toArray() : array();
    }
    public static function job_details($job_id)
    {
        return DB::table('job as j')
                    ->selectRaw('j.*, jc.name as category_name')
                    ->leftjoin('category as jc', 'jc.id', '=', 'j.category_id')
                    ->where(array('j.id' => $job_id))
                    ->first();
    }

    public static function delete_job($job_id)
    {
        return DB::table('job')->where(array('job_id' => $job_id))->update(array('deleted_at' => date("Y-m-d H:i:s")));
    }

    public static function job_proposals($job_id)
    {
        return DB::table('bid as b')
                    ->selectRaw('b.*, b.id as bid_id, u.id, u.firstName, u.lastName, u.userName,u.profile_image, bz.description as business_desc, bz.ratePerHour,bz.ratePerProject, bz.info, country.name as address_country,team.name')
                    ->leftjoin('user as u', 'u.id', '=', 'b.user_id')
                    ->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
                    ->leftjoin('address as adr', 'adr.id', '=', 'u.address_id')
                    ->leftjoin('country', 'country.id', '=', 'adr.country_id')
                    ->leftjoin('team', 'team.id', '=', 'b.team_id')
                    ->where(array('b.job_id' => $job_id, 'b.status' => 2))
                    ->get();
    }
    
    public static function job_hires($job_id)
    {
        return DB::table('hires as h')
                    ->selectRaw('h.*, u.id, u.firstName, u.lastName, u.userName, bz.description as business_desc, bz.ratePerHour, bz.info, country.name as address_country')
                    ->leftjoin('user as u', 'u.id', '=', 'h.user_id')
                    ->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
                    ->leftjoin('address as adr', 'adr.id', '=', 'u.address_id')
                    ->leftjoin('country', 'country.id', '=', 'adr.country_id')
                    ->where(array('h.job_id' => $job_id))
                    ->get();
    }
     
}