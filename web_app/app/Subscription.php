<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //
    protected $table = 'subscription';
    protected $fillable =['user_id','plan_id','price','valid_until','subscription_id','card_last_four','card_brand'];
    public $timestamps = true;
     
}
