<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\JobTransaction;
use App\chat;
use Auth;

class ChatEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $message;
    public $user;
    public $transaction_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message , $user ,$transaction_id)
    {
        $this->message = $message;
        $this->user = $user;
        $this->transaction_id = $transaction_id;

        $jobTraData = JobTransaction::find($transaction_id);
        $from = $jobTraData->from;
        $to = $jobTraData->to;

        if(Auth::user()->id == $from){
            $sender = $from;
            $reciever = $to;
        }else{
            $sender = $to;
            $reciever = $from;
        }
        $data = [
                    'sender' => $sender ,
                    'reciever' => $reciever,
                    'message' => $message,
                    'transaction_id' => $transaction_id
                ];
        $saveChat = Chat::insert($data);
        $this->dontBroadcastToCurrentUser();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat');
    }
}   