<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Bid extends Model 
{   
    const status_accepted = 0;
    const status_declined = 1;
    const status_pending = 2;

    protected $table = 'bid';
    protected $fillable=['user_id','job_id','cover','bidPrice','serviceFee','payment'];
    public $timestamps = true;
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function job()
    {
        return $this->belongsTo('App\Job','job_id','id');
    }

    public static function getJobBid($user_id, $job_id)
    {

        return DB::table('bid')->where([['user_id', $user_id], ['job_id', $job_id]])->first();
    }
}