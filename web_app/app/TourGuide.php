<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourGuide extends Model
{
    protected $table = 'tour_guide_info';
     protected $fillable=['user_id','race','birthCountry','nationalityStatus','attachment_claim','knowCountry','haveCar','carForTour','carMake','carModel','carYear','carPlate','carLicenceno','attachment_licence','carInsurance','noInsuranceReason','carinsurance_attachment_id'];
    public $timestamps = false;
    public static $race =array(
    							'0'=>'american indian or alaska native',
    							'1'=>'black or african american',
    							'2'=>'asian',
    							'3'=>'native hawaiian or other pacific islander',
    							'4'=>'white'
    						);
    public static $nationality_status =array(
											'0'=>'citizen',
											'1'=>'lawfulresident',
											'2'=>'workpermit'
    										);
    public  static $activies =array(
                                        '0'=>'history & culture' ,  
                                        '1'=>'pick up & driving tour',
                                        '2'=>'food & restaurants',
                                        '3'=>'art & museums',
                                        '4'=>'nightlife & bars',
                                        '5'=>'sports & creation',
                                        '6'=>'shopping',
                                        '7'=>'exploration & sightseeing',
                                        '8'=>'translation & interpretation'
                                    );
}


