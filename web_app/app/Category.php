<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table ="category";
    protected $fillable=['name','shortCode','description','parent_id','attachment_id'];
    public $timestamps = false;
     
    public function jobs()
    {
        return $this->hasMany('App\Job');
    }

    public static function all_records($limit=false)
    {
        if ($limit)
        {
            $categories = Category::limit($limit)->where('parent_id','=',NULL)->get();
        }
        else
        {
            $categories = Category::where('parent_id','=',NULL)->get();
        }
        
        return sizeof($categories) > 0 ? $categories->toArray() : array();
    }

    public function childcategory()
    {
        return $this->hasMany('App\Category','parent_id','id');
    }
    
}