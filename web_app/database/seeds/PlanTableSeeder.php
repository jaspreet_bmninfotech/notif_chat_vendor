<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Plan')->insert([
           'name'=>'Free Account',
           'description'=>'Low Visibility(Locally), Up to 3 contracts only, Accept Deposit up to $1000, Up to 5 photos, Up to 1 photo album(5 photos only), Up to 2 audio files, Add Youtube and Vimeo video, 5% Booking fees',
           'price'=>'0',
           'type'=>'vn'
        ]);
        DB::table('Plan')->insert([
            'name'=>'Pro Account',
            'description'=>'High Visibility(Locally, In the country), Up to 10 contracts only, Up to 5 teams only, Accept Deposit up to $5000, Up to 20 photos, Up to 5 photo album(5 photos only), Upload audio files, Up to 5 videos upload, Add Youtube and Vimeo video, 2.5% Booking fees',
            'price'=>'29.99',
            'type'=>'vn'
        ]);
        DB::table('Plan')->insert([
            'name'=>'Premium Account',
            'description'=>'Top priority in search result, High Visibility(Locally, In the country, Internationally), Unlimited contracts, Unlimited teams, Unlimited Deposit, Unlimited photos, Unlimited photo albums, Unlimited Upload audio files, Unlimited videos upload, Add Youtube and Vimeo video, Priority phone support, 2.5% Booking fees',
            'price'=>'39.99',
            'type'=>'vn'
        ]);
        DB::table('Plan')->insert([
            'name'=>'Free Account',
            'description'=>'1 Job Post, Up to 3 contracts only, Accept Deposit up to $1000, Phone support, Low visibility(Local vendors only), 5% Booking fees',
            'price'=>'0',
            'type'=>'cn'
        ]);
        DB::table('Plan')->insert([
            'name'=>'Pro Account',
            'description'=>'Up to 15 Job Post, Top priority in search result, Accept Deposit up to $5000, Up to 15 Contracts, Priority phone support, High visibility(Local vendors and International vendors), Up to 15 teams only',
            'price'=>'20.99',
            'type'=>'cn'
        ]);
        DB::table('Plan')->insert([
            'name'=>'Premium Account',
            'description'=>'Unlimited Job Post, Top priority in search result, Unlimited payments, Unlimited Job Contracts, Priority phone support, High visibility(Local vendors and International vendors), Unlimited teams only',
            'price'=>'29.99',
            'type'=>'cn'
        ]);
    }
}
