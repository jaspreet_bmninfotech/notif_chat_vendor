<?php

use Illuminate\Database\Seeder;


class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                $category= DB::table('category')->insertGetId(array('name'=>'Home','shortCode'=>'home','description'=>'','parent_id'=>null));
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Accessibility Construction and Remodels','shortCode'=>'accessibility construction and remodels','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Doorway Widening','shortCode'=>'doorway widening','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Modification for Accessibility','shortCode'=>'home modification for accessibility','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Roll-in Shower Installation','shortCode'=>'roll-in shower installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shower Grab Bar Installation','shortCode'=>'shower grab bar installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Toilet Grab Bar Installation','shortCode'=>'toilet grab bar installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wheelchair Ramp Installation','shortCode'=>'wheelchair ramp installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wheelchair Ramp Repair','shortCode'=>'wheelchair ramp repair','description'=>'','parent_id'=>$mainsubcategory),
                    
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Additions and Remodels','shortCode'=>'additions and remodels','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Attic Remodel','shortCode'=>'attic remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Balcony Addition','shortCode'=>'balcony addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Balcony Remodel','shortCode'=>'balcony remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Balcony Repair','shortCode'=>'balcony repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Barn Construction','shortCode'=>'barn construction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Basement Finishing or Remodeling','shortCode'=>'basement finishing or remodeling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Bathroom Remodel','shortCode'=>'bathroom remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Child Proofing','shortCode'=>'child proofing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Carport Addition','shortCode'=>'carport addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Closet Addition','shortCode'=>'closet addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Closet Remodel','shortCode'=>'closet remodel','description'=>'','parent_id'=>$mainsubcategory),
                    
                    array('name'=>'Construction Services','shortCode'=>'construction services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Deck Sealing','shortCode'=>'deck sealing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Deck Staining','shortCode'=>'deck staining','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Deck or Porch Remodel or Addition','shortCode'=>'deck or porch remodel or addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Deck or Porch Repair','shortCode'=>'deck or porch repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Deck or Porch Repair','shortCode'=>'deck or porch repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Demolition Services','shortCode'=>'doorway widening','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Energy Efficiency Remodel','shortCode'=>'Energy Efficiency Remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fire Escape Installation','shortCode'=>'home modification for accessibility','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fire Escape Maintenance and Repair','shortCode'=>'roll-in shower installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Foundation Installation','shortCode'=>'shower grab bar installation','description'=>'','parent_id'=>$mainsubcategory),
                     array('name'=>'Foundation Raising','shortCode'=>'foundation raising','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Foundation Repair','shortCode'=>'foundation repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garage Addition','shortCode'=>'garage addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garage Remodel','shortCode'=>'garage remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Greenhouse Addition','shortCode'=>'greenhouse addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Remodeling','shortCode'=>'home remodeling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Interior Wall Addition','shortCode'=>'interior wall addition','description'=>'','parent_id'=>$mainsubcategory),
                     array('name'=>'Interior Wall Removal','shortCode'=>'interior wall removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Kitchen Island Installation','shortCode'=>'kitchen island installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Kitchen Island Removal','shortCode'=>'kitchen island removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Kitchen Remodel','shortCode'=>'kitchen remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'New Home Construction','shortCode'=>'new home construction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Outdoor Kitchen Remodel or Addition','shortCode'=>'outdoor kitchen remodel or addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Patio Remodel or Addition','shortCode'=>'patio remodel or addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Patio Repair','shortCode'=>'patio repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pergola Addition','shortCode'=>'pergola addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Playhouse Construction','shortCode'=>'playhouse construction','description'=>'','parent_id'=>$mainsubcategory),
                     array('name'=>'Railing Installation or Remodel','shortCode'=>'railing installation or remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Room Extension','shortCode'=>'room extension','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Room Remodel','shortCode'=>'room remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Room Splitting','shortCode'=>'room splitting','description'=>'','parent_id'=>$mainsubcategory),
                    
                    array('name'=>'Sauna Installation','shortCode'=>'sauna installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sauna Repair or Maintenance','shortCode'=>'sauna repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Stair Installation or Remodel','shortCode'=>'stair installation or remodel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Stair and Staircase Repair','shortCode'=>'stair and staircase repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shed Construction','shortCode'=>'shed construction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sunroom Addition','shortCode'=>'sunroom addition','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Yurt Construction','shortCode'=>'yurt construction ','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Appliances','shortCode'=>'accessibility construction and remodels','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Appliance Installation','shortCode'=>'appliance installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Appliance Repair or Maintenance','shortCode'=>'appliance repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Dishwasher Installation','shortCode'=>'dishwasher installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garbage Disposal Installation','shortCode'=>'garbage disposal installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garbage Disposal Repair','shortCode'=>'garbage disposal repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Lawn Mower Repair','shortCode'=>'lawn mower repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Refrigerator Installation','shortCode'=>'refrigerator installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Satellite Dish Services','shortCode'=>'satellite dish services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Vacuum Cleaner Installation','shortCode'=>'vacuum cleaner installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Washing Machine Installation','shortCode'=>'washing machine installation','description'=>'','parent_id'=>$mainsubcategory),
                ));
 
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Architectural and Engineering Services','shortCode'=>'accessibility construction and remodels','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Structural Engineering Services','shortCode'=>'structural engineering services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Architectural Services','shortCode'=>'architectural services','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Carpentry and Woodworking','shortCode'=>'Carpentry and Woodworking','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                   array('name'=>'Cabinet Installation','shortCode'=>'cabinet installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Cabinet Refacing','shortCode'=>'cabinet refacing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Cabinet Refinishing','shortCode'=>'cabinet refinishing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Cabinet Repair','shortCode'=>'cabinet repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Cabinetry','shortCode'=>'cabinetry','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Custom Cabinet Building','shortCode'=>'custom cabinet building','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Custom Furniture Building','shortCode'=>'custom furniture building','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Framing Carpentry','shortCode'=>'framing carpentry','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'General Carpentry','shortCode'=>'general carpentry','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Trim or Molding Installation','shortCode'=>'trim or molding installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Finish Carpentry','shortCode'=>'finish carpentry','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Cleaning','shortCode'=>'Carpentry and Woodworking','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Asbestos Removal','shortCode'=>'asbestos removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Carpet Cleaning','shortCode'=>'carpet cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Commercial Carpet Cleaning','shortCode'=>'commercial carpet cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Commercial Cleaning','shortCode'=>'commercial cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Dumpster Rental','shortCode'=>'dumpster rental','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Floor Cleaning','shortCode'=>'floor cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Floor Polishing','shortCode'=>'floor polishing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garage, Basement or Attic Cleaning','shortCode'=>'garage, basement or attic cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Gutter Cleaning and Maintenance','shortCode'=>'gutter cleaning and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Organizing','shortCode'=>'home organizing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'House Cleaning','shortCode'=>'house cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Junk Removal','shortCode'=>'junk removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Odor Removal','shortCode'=>'odor removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pressure Washing','shortCode'=>'pressure washing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Roof Cleaning','shortCode'=>'roof cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Rug Cleaning','shortCode'=>'rug cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Solar Panel Cleaning','shortCode'=>'solar panel cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tile and Grout Cleaning','shortCode'=>'tile and grout cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Upholstery and Furniture Cleaning','shortCode'=>'upholstery and furniture cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Damage Cleanup and Restoration','shortCode'=>'water damage cleanup and restoration','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Window Cleaning','shortCode'=>'window cleaning','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Concrete/Cement/Asphalt','shortCode'=>'Carpentry and Woodworking','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Asphalt Installation','shortCode'=>'asphalt installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Asphalt Repair and Maintenance','shortCode'=>'asphalt repair and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Brick or Stone Repair','shortCode'=>'brick or stone repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Concrete Installation','shortCode'=>'concrete installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Concrete Removal','shortCode'=>'concrete removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Concrete Repair and Maintenance','shortCode'=>'concrete repair and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Concrete Sawing','shortCode'=>'concrete sawing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Masonry Construction Services','shortCode'=>'masonry construction services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Stucco Application','shortCode'=>'stucco application','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Stucco Repair','shortCode'=>'stucco repair','description'=>'','parent_id'=>$mainsubcategory),

                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Design & Decor','shortCode'=>'design & decor','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Home Staging','shortCode'=>'home staging','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Interior Design','shortCode'=>'interior design','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Muralist','shortCode'=>'muralist','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Picture Hanging and Art Installation','shortCode'=>'picture hanging and art installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wallpaper Installation or Repair','shortCode'=>'wallpaper installation or repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wallpaper Removal','shortCode'=>'wallpaper removal','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Doors','shortCode'=>'doors','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Door Installation','shortCode'=>'door installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Door Repair','shortCode'=>'door repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garage Door Installation or Replacement','shortCode'=>'garage door installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garage Door Repair','shortCode'=>'garage door repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Lock Installation and Repair','shortCode'=>'lock installation and repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pet Door Installation','shortCode'=>'pet door installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pet Door Removal','shortCode'=>'pet door removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shower Door Installation','shortCode'=>'shower door installation','description'=>'','parent_id'=>$mainsubcategory)
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Electrical','shortCode'=>'doors','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Circuit Breaker Panel or Fuse Box Installation','shortCode'=>'circuit breaker panel or fuse box installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Circuit Breaker Panel or Fuse Box Repair','shortCode'=>'circuit breaker panel or fuse box repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Electrical and Wiring Repair','shortCode'=>'electrical and wiring repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fan Installation','shortCode'=>'fan installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Generator Installation','shortCode'=>'generator installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Generator Repair','shortCode'=>'generator repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Automation','shortCode'=>'home automation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Solar Panel Installation','shortCode'=>'solar panel installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Solar Panel Repair','shortCode'=>'solar panel repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Switch and Outlet Installation','shortCode'=>'switch and outlet installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Switch and Outlet Repair','shortCode'=>'switch and outlet repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wiring Installation','shortCode'=>'wiring installation','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Flooring','shortCode'=>'flooring','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Carpet Installation','shortCode'=>'carpet installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Carpet Removal','shortCode'=>'carpet removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Carpet Repair or Partial Replacement','shortCode'=>'carpet repair or partial replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Epoxy Floor Coating','shortCode'=>'epoxy floor coating','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Floor Installation or Replacement','shortCode'=>'floor installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Floor Repair','shortCode'=>'floor repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Hardwood Floor Refinishing','shortCode'=>'hardwood floor refinishing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Heated Floor Installation','shortCode'=>'heated floor installation','description'=>'','parent_id'=>$mainsubcategory),

                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Furniture','shortCode'=>'furniture','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Baby Gate Assembly and Installation','shortCode'=>'baby gate assembly and installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Closet and Shelving System Installation','shortCode'=>'closet and shelving system installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Countertop Installation','shortCode'=>'countertop installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Countertop Repair or Maintenance','shortCode'=>'countertop repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fitness Equipment Assembly','shortCode'=>'fitness equipment assembly','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Furniture Assembly','shortCode'=>'furniture assembly','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Furniture Delivery','shortCode'=>'furniture delivery','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Furniture Refinishing','shortCode'=>'furniture refinishing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Furniture Repair','shortCode'=>'furniture repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Furniture Upholstery','shortCode'=>'furniture upholstery','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Gutters','shortCode'=>'gutters','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Gutter Installation or Replacement','shortCode'=>'gutter installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Gutter Repair','shortCode'=>'gutter repair','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                  
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Heating & Cooling','shortCode'=>'heating & cooling','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Air Quality Testing and Inspection','shortCode'=>'air quality testing and inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Central Air Conditioning Installation or Replacement','shortCode'=>'central air conditioning installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Central Air Conditioning Repair or Maintenance','shortCode'=>'central air conditioning repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Duct and Vent Cleaning','shortCode'=>'duct and vent cleaning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Duct and Vent Installation or Removal','shortCode'=>'duct and vent installation or removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Duct and Vent Repair','shortCode'=>'duct and vent repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fireplace and Chimney Cleaning or Repair','shortCode'=>'fireplace and chimney cleaning or repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fireplace and Chimney Installation','shortCode'=>'fireplace and chimney Installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Heating System Installation or Replacement','shortCode'=>'heating system installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Heating System Repair or Maintenance','shortCode'=>'heating system repair or 
                    maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Insulation Installation or Upgrade','shortCode'=>'insulation installation or upgrade','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Thermostat Installation or Repair','shortCode'=>'thermostat installation or repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Window, Wall, or Portable AC Repair or Maintenance','shortCode'=>'window, wall, or portable ac repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Window, Wall, or Portable AC Unit Installation','shortCode'=>'window, wall, or portable ac unit installation','description'=>'','parent_id'=>$mainsubcategory),

                 ));
               
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Home Damage Prevention/Repair','shortCode'=>'home damage prevention/repair','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Basement Drainage Installation','shortCode'=>'basement drainage installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Basement Drainage Repair','shortCode'=>'basement drainage repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Earthquake Damage Repair','shortCode'=>'earthquake damage repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Earthquake/Seismic Retrofit','shortCode'=>'earthquake/seismic retrofit','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fire or Smoke Damage Repair','shortCode'=>'fire or smoke damage repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Foundation or Basement Waterproofing','shortCode'=>'foundation or basement waterproofing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Roof or Gutter Winterization','shortCode'=>'roof or gutter winterization','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Storm or Wind Damage Recovery Service','shortCode'=>'storm or wind damage recovery service','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Home Services','shortCode'=>'home services','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'House Sitting','shortCode'=>'house sitting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Property Management','shortCode'=>'property management','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Home Theater','shortCode'=>'home theater','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Home Theater Construction','shortCode'=>'home theater construction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Theater Surround Sound System Installation','shortCode'=>'home theater surround sound system Installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Theater System Installation or Replacement','shortCode'=>'home theater system installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Theater System Repair or Service','shortCode'=>'home theater system repair or service','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Theater System Wiring','shortCode'=>'home theater system wiring','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'TV Mounting','shortCode'=>'tv mounting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'TV Repair Services','shortCode'=>'tv repair services','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Inspections','shortCode'=>'inspections','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Asbestos Inspection','shortCode'=>'asbestos inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Chimney Inspection','shortCode'=>'chimney inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Electrical Inspection','shortCode'=>'electrical inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fire Extinguisher Inspection','shortCode'=>'fire extinguisher inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Energy Auditing','shortCode'=>'home energy auditing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Inspection','shortCode'=>'home inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Land Surveying','shortCode'=>'land surveying','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Lead Testing','shortCode'=>'lead testing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Mold Inspection and Removal','shortCode'=>'mold inspection and removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pest Inspection','shortCode'=>'pest inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Plumbing Inspection','shortCode'=>'plumbing inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Radon Testing','shortCode'=>'radon testing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Real Estate Appraisal','shortCode'=>'real estate appraisal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Roof Inspection','shortCode'=>'roof inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Swimming Pool Inspection','shortCode'=>'swimming pool inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Termite Control Services','shortCode'=>'termite control services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Termite Inspection','shortCode'=>'termite inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Well Water Inspection','shortCode'=>'well water inspection','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Landscaping','shortCode'=>'landscaping','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Artificial Turf Installation','shortCode'=>'artificial turf installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Boulder Placement','shortCode'=>'boulder placement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Drip Irrigation System Maintenance','shortCode'=>'drip irrigation system maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Greenhouse Services','shortCode'=>'greenhouse services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Land Leveling and Grading','shortCode'=>'land leveling and grading','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Outdoor Landscaping and Design','shortCode'=>'outdoor landscaping and design','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sod Installation','shortCode'=>'sod installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sprinkler and Irrigation System Installation','shortCode'=>'sprinkler and irrigation system installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sprinkler and Irrigation System Repair and Maintenance','shortCode'=>'sprinkler and irrigation system repair and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Feature Installation','shortCode'=>'water feature installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Feature Repair and Maintenance','shortCode'=>'water feature repair and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Lawncare','shortCode'=>'lawncare','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Aeration','shortCode'=>'aeration','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fertilizing','shortCode'=>'fertilizing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Full Service Lawn Care','shortCode'=>'full service lawn care','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Gardening','shortCode'=>'gardening','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Lawn Mowing and Trimming','shortCode'=>'lawn mowing and trimming','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Leaf Clean Up','shortCode'=>'leaf clean up','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Mulching','shortCode'=>'mulching','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Outdoor Mosquito Control Services','shortCode'=>'outdoor mosquito control services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Outdoor Pesticide Application','shortCode'=>'outdoor pesticide application','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Plant Watering and Care','shortCode'=>'plant watering and care','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Seeding','shortCode'=>'seeding','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shrub Planting','shortCode'=>'shrub planting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shrub Trimming and Removal','shortCode'=>'shrub trimming and removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Snow Plowing','shortCode'=>'snow plowing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tree Planting','shortCode'=>'tree planting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tree Stump Grinding and Removal','shortCode'=>'tree stump grinding and removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tree Trimming and Removal','shortCode'=>'tree trimming and removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Weeding','shortCode'=>'weeding','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Lighting','shortCode'=>'lighting','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Lighting Installation','shortCode'=>'lighting installation','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Material Processing','shortCode'=>'material processing','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Engraving','shortCode'=>'engraving','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Metalwork','shortCode'=>'metalwork','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Powder Coating','shortCode'=>'powder coating','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Moving','shortCode'=>'moving','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Furniture Moving and Heavy Lifting','shortCode'=>'furniture moving and heavy lifting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Hot Tub Moving','shortCode'=>'hot tub moving','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Local Moving (under 50 miles)','shortCode'=>'local moving (under 50 miles)','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Long Distance Moving','shortCode'=>'long distance moving','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Office Moving','shortCode'=>'office moving','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Packing and Unpacking','shortCode'=>'packing and unpacking','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Piano Moving','shortCode'=>'piano moving','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pool Table Moving','shortCode'=>'pool table moving','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Outdoor Structures','shortCode'=>'outdoor structures','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Fence and Gate Installation','shortCode'=>'fence and gate installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fence and Gate Repairs','shortCode'=>'fence and gate repairs','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Gazebo Installation and Construction','shortCode'=>'gazebo installation and construction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Gazebo Repair and Maintenance','shortCode'=>'gazebo repair and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Mudjacking','shortCode'=>'mudjacking','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Outdoor Equipment or Furniture Assembly','shortCode'=>'outdoor equipment or furniture assembly','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Patio Cover and Awning Installation','shortCode'=>'patio cover and awning Installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Patio Cover and Awning Repair and Maintenance','shortCode'=>'patio cover and awning repair and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Play Equipment Construction','shortCode'=>'play equipment construction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Dock Services','shortCode'=>'water dock services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Well System Work','shortCode'=>'well system work','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Painting','shortCode'=>'painting','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Cabinet Painting','shortCode'=>'cabinet painting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Exterior Painting','shortCode'=>'exterior painting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Faux Finishing or Painting','shortCode'=>'faux finishing or painting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fence Painting','shortCode'=>'fence painting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Floor Painting or Coating','shortCode'=>'floor painting or coating','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Interior Painting','shortCode'=>'interior painting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Paint Removal','shortCode'=>'paint removal','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Plumbing','shortCode'=>'plumbing','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Emergency Plumbing','shortCode'=>'emergency plumbing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Gas Line Installation','shortCode'=>'gas line installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Plumbing Drain Repair','shortCode'=>'plumbing drain repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Plumbing Pipe Installation or Replacement','shortCode'=>'plumbing pipe installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Plumbing Pipe Repair','shortCode'=>'plumbing pipe repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Septic System Installation or Replacement','shortCode'=>'septic system installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Septic System Repair, Maintenance or Inspection','shortCode'=>'septic system repair, Maintenance or Inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shower and Bathtub Installation or Replacement','shortCode'=>'shower and bathtub installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shower and Bathtub Repair','shortCode'=>'shower and bathtub repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sink or Faucet Installation or Replacement','shortCode'=>'sink or faucet installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sink or Faucet Repair','shortCode'=>'sink or faucet repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sump Pump Installation or Replacement','shortCode'=>'sump pump installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sump Pump Repair or Maintenance','shortCode'=>'sump pump repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Toilet Installation or Replacement','shortCode'=>'toilet installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Toilet Repair','shortCode'=>'toilet repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Heater Installation or Replacement','shortCode'=>'water heater installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Heater Repair or Maintenance','shortCode'=>'water heater repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Treatment Repair or Maintenance','shortCode'=>'water treatment repair or maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Water Treatment System Installation or replacement','shortCode'=>'water treatment system installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Pools, Hot Tubs and Spas','shortCode'=>'pools, hot tubs and spas','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Above Ground Swimming Pool Installation','shortCode'=>'above ground swimming pool installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Hot Tub and Spa Cleaning and Maintenance','shortCode'=>'hot tub and spa cleaning and maintenance','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Hot Tub and Spa Installation','shortCode'=>'hot tub and spa installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Hot Tub and Spa Repair','shortCode'=>'hot tub and spa repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'In-Ground Swimming Pool Construction','shortCode'=>'in-ground swimming pool construction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Swimming Pool Cleaning, Maintenance, and Inspection','shortCode'=>'swimming pool cleaning, maintenance, and inspection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Swimming Pool Repair','shortCode'=>'swimming pool repair','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                  
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Roofing','shortCode'=>'Roofing','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                        array('name'=>'Roof Installation or Replacement','shortCode'=>'roof installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Roof Repair or Maintenance','shortCode'=>'roof repair or maintenance','description'=>'','parent_id'=>$mainsubcategory)
                 ));
                  
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Security and Damage Prevention/Repair','shortCode'=>'security and damage prevention/repair','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Bed Bug Extermination','shortCode'=>'bed bug extermination','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Security System Repair','shortCode'=>'home security system Repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Security and Alarms Install','shortCode'=>'home security and alarms Install','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Home Waterproofing','shortCode'=>'home waterproofing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pest Control Services','shortCode'=>'pest control services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Radon Mitigation','shortCode'=>'radon mitigation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Rodent and Animal Removal','shortCode'=>'rodent and animal Removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Weatherization','shortCode'=>'weatherization','description'=>'','parent_id'=>$mainsubcategory),

                 ));
                  
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Siding','shortCode'=>'siding','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Siding Installation','shortCode'=>'siding installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Siding Removal','shortCode'=>'siding removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Mobile Home Skirting Installation','shortCode'=>'mobile home skirting installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Siding Repair','shortCode'=>'siding repair','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Site Preparation','shortCode'=>'site preparation','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Excavation Services','shortCode'=>'excavation services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Land Clearing','shortCode'=>'land clearing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Swimming Pool Removal','shortCode'=>'swimming pool removal','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Tiling','shortCode'=>'tiling','description'=>'','parent_id'=>$category));
                 $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Tile Installation and Replacement','shortCode'=>'tile installation and Replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tile Repair','shortCode'=>'tile repair','description'=>'','parent_id'=>$mainsubcategory)
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Tiling','shortCode'=>'tiling','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Tile Installation and Replacement','shortCode'=>'tile installation and Replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tile Repair','shortCode'=>'tile repair','description'=>'','parent_id'=>$mainsubcategory)
                 ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Walls, Framing, and Stairs','shortCode'=>'walls, framing, and stairs','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Drywall Installation and Hanging','shortCode'=>'drywall installation and hanging','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Drywall Repair and Texturing','shortCode'=>'drywall repair and texturing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Plastering','shortCode'=>'plastering','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Popcorn Texture Removal','shortCode'=>'popcorn texture removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Railing Repair','shortCode'=>'railing repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sound Proofing','shortCode'=>'sound proofing','description'=>'','parent_id'=>$mainsubcategory),
                ));
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Windows','shortCode'=>'windows','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Commercial Window Tinting','shortCode'=>'commercial window tinting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Residential Window Tinting','shortCode'=>'residential window tinting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Screen Installation or Replacement','shortCode'=>'screen installation or replacement','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Shutter Removal','shortCode'=>'shutter removal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Skylight Installation or Repair','shortCode'=>'skylight installation or repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Window Installation','shortCode'=>'window installation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Window Repair','shortCode'=>'window repair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Window Treatment Installation or Repair','shortCode'=>'window treatment installation or repair','description'=>'','parent_id'=>$mainsubcategory),
                ));
                $category= DB::table('category')->insertGetId(array('name'=>'Wellness','shortCode'=>'wellness','description'=>'','parent_id'=>null));
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'beauty services','shortCode'=>'beauty services','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Hair Coloring and Highlights','shortCode'=>'hair coloring and highlights','description'=>'','parent_id'=>$mainsubcategory),
                ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Coaching','shortCode'=>'coaching','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Creativity Coaching','shortCode'=>'creativity coaching','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Health and Wellness Coaching','shortCode'=>'health and wellness coaching','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Life Coaching','shortCode'=>'life coaching','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Stress Management Coaching','shortCode'=>'stress management coaching','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Time and Organizational Management','shortCode'=>'time and organizational management','description'=>'','parent_id'=>$mainsubcategory)
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Counseling','shortCode'=>'counseling','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Family Counseling','shortCode'=>'family counseling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Grief Counseling','shortCode'=>'grief counseling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Hypnotherapy','shortCode'=>'hypnotherapy','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Marriage and Relationship Counseling','shortCode'=>'marriage and relationship counseling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Meditation Instruction','shortCode'=>'meditation instruction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Social Anxiety Counseling','shortCode'=>'social anxiety counseling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Therapy and Counseling','shortCode'=>'therapy and counseling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Relationship Counseling','shortCode'=>'relationship counseling','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Exercise','shortCode'=>'exercise','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Personal Training','shortCode'=>'personal training','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Private Pilates Instruction','shortCode'=>'private pilates instruction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Private Tai Chi Instruction','shortCode'=>'private tai chi instruction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Private Yoga Instruction','shortCode'=>'private yoga instruction','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Swimming Lessons','shortCode'=>'swimming lessons','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Healing','shortCode'=>'healing','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Alternative Healing','shortCode'=>'alternative healing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Physical Therapy','shortCode'=>'physical therapy','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Nutrition','shortCode'=>'nutrition','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Nutritionist','shortCode'=>'nutritionist','description'=>'','parent_id'=>$mainsubcategory),
                ));
               
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Spa Services','shortCode'=>'spa services','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Facial Treatments','shortCode'=>'facial treatments','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Massage Therapy','shortCode'=>'massage therapy','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Spirituality','shortCode'=>'spirituality','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Spiritual Counseling','shortCode'=>'spiritual counseling','description'=>'','parent_id'=>$mainsubcategory),
                ));
                $category= DB::table('category')->insertGetId(array('name'=>'Events / Wedding','shortCode'=>'events / wedding','description'=>'','parent_id'=>null));
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Dance Entertainment','shortCode'=>'dance entertainment','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Dance Entertainment','shortCode'=>'dance entertainment','description'=>'','parent_id'=>$mainsubcategory),
                ));
               
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Driver','shortCode'=>'driver','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Charter Bus Rental','shortCode'=>'charter bus rental','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Limousine and Chauffeur Services','shortCode'=>'limousine and chauffeur services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Party Bus Rental','shortCode'=>'party bus rental','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Entertainment Services','shortCode'=>'entertainment services','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Entertainment','shortCode'=>'entertainment','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tarot and Psychic Reader Entertainment','shortCode'=>'tarot and psychic reader entertainment','description'=>'','parent_id'=>$mainsubcategory),
                ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Event Services','shortCode'=>'event services','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Astrology Reading','shortCode'=>'astrology reading','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Balloon Twisting','shortCode'=>'balloon twisting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Body Painting','shortCode'=>'body painting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Caricaturing','shortCode'=>'caricaturing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Event Florist','shortCode'=>'event florist','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Event Security Services','shortCode'=>'event security services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Face Painting','shortCode'=>'face painting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Henna Tattooing','shortCode'=>'henna tattooing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Laser Show Entertainment','shortCode'=>'laser show entertainment','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Palm Reading','shortCode'=>'palm reading','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Portrait Artistry','shortCode'=>'portrait artistry','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Puppet Show Entertainment','shortCode'=>'puppet show entertainment','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Scrapbooking','shortCode'=>'scrapbooking','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tarot Card Reading','shortCode'=>'tarot card reading','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Temporary Tattoo Artistry','shortCode'=>'temporary tattoo artistry','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Valet Parking','shortCode'=>'valet parking','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Event Staff','shortCode'=>'event staff','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Bartending','shortCode'=>'bartending','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Event Bouncer Services','shortCode'=>'event bouncer services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Event Help and Wait Staff','shortCode'=>'event help and wait staff','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sommelier Services','shortCode'=>'sommelier services','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Events-other','shortCode'=>'events-other','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                     array('name'=>'Audio Recording','shortCode'=>'audio recording','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Food','shortCode'=>'food','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Barbecue and Grill Services','shortCode'=>'barbecue and grill services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Candy Buffet Services','shortCode'=>'candy buffet services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Chocolate Fountain Rental','shortCode'=>'chocolate fountain rental','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Food Truck or Cart Services','shortCode'=>'food truck or cart services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Ice Cream Cart Rental','shortCode'=>'ice cream cart rental','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Pastry Chef and Cake Making Services','shortCode'=>'pastry chef and cake making services','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Personal Chef','shortCode'=>'personal chef','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Popcorn Machine Rental','shortCode'=>'popcorn machine rental','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding and Event Catering','shortCode'=>'wedding and event catering','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wine Tastings and Tours','shortCode'=>'wine tastings and tours','description'=>'','parent_id'=>$mainsubcategory),
                ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Flowers','shortCode'=>'flowers','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Bouquets','shortCode'=>'bouquets','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Boutonnieres','shortCode'=>'boutonnieres','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Centerpieces','shortCode'=>'centerpieces','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Corsages','shortCode'=>'corsages','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Altar Decor','shortCode'=>'altar decor','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Aisle Decor','shortCode'=>'aisle decor','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Flowers Petal','shortCode'=>'flowers petal','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Flowers Basket','shortCode'=>'flowers basket','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Flowers Crown','shortCode'=>'flowers crown','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Chuppa Decor','shortCode'=>'chuppa decor','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Single Flowers','shortCode'=>'single flowers','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Artificial','shortCode'=>'artificial','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Non-Floral','shortCode'=>'non-floral','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Getting Ready','shortCode'=>'getting ready','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Alterations, Tailoring, and Clothing Design','shortCode'=>'alterations, Tailoring, and Clothing Design','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Crocheting','shortCode'=>'crocheting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Custom Airbrushing','shortCode'=>'custom airbrushing','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Embroidery','shortCode'=>'embroidery','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Quilting','shortCode'=>'quilting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Songwriting','shortCode'=>'songwriting','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wardrobe Consulting','shortCode'=>'wardrobe consulting','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Jewelry Services','shortCode'=>'jewelry services','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Antique Accessories','shortCode'=>'antique accessories','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Antique Engagement Rings','shortCode'=>'antique engagement rings','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Antique Wedding Bands','shortCode'=>'antique wedding bands','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Custom Accessories','shortCode'=>'custom accessories','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Custom Engagement Rings','shortCode'=>'custom engagement rings','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Custom Wedding Bands','shortCode'=>'custom wedding bands','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Designer Accessories','shortCode'=>'designer accessories','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Designer Engagement Rings','shortCode'=>'designer engagement rings','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Designer Wedding Rings','shortCode'=>'designer wedding rings','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Makeup','shortCode'=>'makeup','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Contour Makeup','shortCode'=>'contour makeup','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fake Lashes','shortCode'=>'fake lashes','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Makeup Trial','shortCode'=>'makeup trial','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Party Makeup','shortCode'=>'party makeup','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'On-Site Makeup','shortCode'=>'on-site makeup','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Eye Makeup','shortCode'=>'eye makeup','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Natural Makeup','shortCode'=>'natural makeup','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Airbrush Makeup','shortCode'=>'airbrush makeup','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Tattoo Coverage','shortCode'=>'tattoo coverage','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Brown / Lash Tinting','shortCode'=>'brown / lash tinting','description'=>'','parent_id'=>$mainsubcategory),
                    

                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Hair Style','shortCode'=>'hair style','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Hair Trial','shortCode'=>'hair trial','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'On-Site Hair','shortCode'=>'on-site hair','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Up dos/Styling','shortCode'=>'up dos/styling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Extensions','shortCode'=>'extensions','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Coloring','shortCode'=>'coloring','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Haircuts','shortCode'=>'haircuts','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Textured Hair Styling','shortCode'=>'textured hair styling','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Keratin','shortCode'=>'keratin','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Blowouts','shortCode'=>'blowouts','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Relaxer','shortCode'=>'relaxer','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Perm','shortCode'=>'perm','description'=>'','parent_id'=>$mainsubcategory),
                ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Venues','shortCode'=>'venues','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'House of worship','shortCode'=>'houseofworship','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Hotel','shortCode'=>'hotel','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Banquet Hall','shortCode'=>'banquethall','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Country Club','shortCode'=>'countryclub','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Farm or Barn','shortCode'=>'farmofbarn','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Historic Home ','shortCode'=>'historichome','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Museum','shortCode'=>'museum','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Theater','shortCode'=>'theter','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Restaurant','shortCode'=>'resturant','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Vineyard or Winery','shortCode'=>'vineyardorwinery','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Modern Space or Loft ','shortCode'=>'modernspace','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Stadium','shortCode'=>'stadium','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Beach','shortCode'=>'beach','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Boat','shortCode'=>'boat','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Government Building','shortCode'=>'governmentbuilding','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Garden','shortCode'=>'garden','description'=>'','parent_id'=>$mainsubcategory)
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'DJs','shortCode'=>'djs','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'MC','shortCode'=>'mc','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Bilingual MC','shortCode'=>'bilingualmc','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Consultation','shortCode'=>'consultation','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Do-Not-Play List','shortCode'=>'donotplaylist','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Video Projection','shortCode'=>'videoprojection','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Club','shortCode'=>'club','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Mobile','shortCode'=>'mobile','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Music producer','shortCode'=>'musicproducer','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Radio','shortCode'=>'radio','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Turntablist','shortCode'=>'turntablist','description'=>'','parent_id'=>$mainsubcategory)
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Musician','shortCode'=>'musician','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Acapella','shortCode'=>'acapella','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Brass - Quartet','shortCode'=>'brass-quartet','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Brass - Quintet','shortCode'=>'brass-quintet','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Brass - Trio','shortCode'=>'brass-trio','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Choir','shortCode'=>'choir','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Instrumental - Duo','shortCode'=>'instrumental-duo','description'=>'','parent_id'=>
                    $mainsubcategory),
                    array('name'=>'Instrumental - Solo','shortCode'=>'instrumental-solo','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Instrumental - Trio','shortCode'=>'instrumental-trio','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Orchestra','shortCode'=>'orchestra','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Soloist / Vocalist','shortCode'=>'soloist','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'String - Quartet','shortCode'=>'string-quartet','description'=>'','parent_id'=>
                    $mainsubcategory),
                    array('name'=>'String - Quintet','shortCode'=>'string-quintet','description'=>'','parent_id'=>
                    $mainsubcategory),
                    array('name'=>'String - Trio','shortCode'=>'string-trio','description'=>'','parent_id'=>$mainsubcategory),
                 ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Photography','shortCode'=>'photography','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Aerial photography','shortCode'=>'aerialphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Architectural photography','shortCode'=>'architecturalphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Candid photography','shortCode'=>'candidphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Documentary photography','shortCode'=>'documentaryphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Fashion photography','shortCode'=>'fashionphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Food photography','shortCode'=>'foodphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Landscape photography','shortCode'=>'landscapephotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Modeling photography','shortCode'=>'modelingphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Night-long exposure photography','shortCode'=>'nightlongexposurephotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Photojournalism','shortCode'=>'photojournalism','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Conceptual / Fine art photography','shortCode'=>'conceptual','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Portraiture','shortCode'=>'portraiture','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Sport photography','shortCode'=>'sportphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Street photography','shortCode'=>'streetphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'War photography','shortCode'=>'warphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wildlife photography','shortCode'=>'wildlife','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding photography','shortCode'=>'weddingphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Birthday photography','shortCode'=>'birthdayphotography','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Event photography','shortCode'=>'eventphotography','description'=>'','parent_id'=>$mainsubcategory),
                ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Planning','shortCode'=>'planning','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Balloon Decorations','shortCode'=>'balloon decorations','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Calligraphy','shortCode'=>'calligraphy','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Event Planning','shortCode'=>'event planning','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Fishing Trip Guide Services','shortCode'=>'fishing trip guide services','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Party Favors','shortCode'=>'party favors','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Sightseeing','shortCode'=>'sightseeing','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Tour Guiding','shortCode'=>'tour guiding','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Wedding and Event Decorating','shortCode'=>'wedding and event decorating','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Wedding and Event Invitations','shortCode'=>'wedding and event invitations','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Wedding and Event Venue Rental','shortCode'=>'wedding and event venue rental','description'=>'','parent_id'=>$mainsubcategory),
                ));
                 
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Specialty Act','shortCode'=>'specialty act','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Animal Show Entertainment','shortCode'=>'animal show entertainment','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Circus Act','shortCode'=>'circus act','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Clown Entertainment','shortCode'=>'clown entertainment','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Comedy Entertainment','shortCode'=>'comedy entertainment','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Contortionist','shortCode'=>'contortionist','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Costumed Character Entertainment','shortCode'=>'costumed character entertainment','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Impersonating','shortCode'=>'impersonating','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Impressionist Entertainment','shortCode'=>'impressionist entertainment','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Juggling','shortCode'=>'juggling','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'MC and Host Services','shortCode'=>'mc and host services','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Magician','shortCode'=>'magician','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Mobile Petting Zoo Entertainment','shortCode'=>'mobile petting zoo entertainment','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Motivational Speaking','shortCode'=>'motivational speaking','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Pony Riding','shortCode'=>'pony riding','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Singing Telegram','shortCode'=>'singing telegram','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Stilt Walker','shortCode'=>'stilt walker','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Storytelling','shortCode'=>'storytelling','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Ventriloquist','shortCode'=>'ventriloquist','description'=>'','parent_id'=>$mainsubcategory),
                ));
               
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Videography','shortCode'=>'videography','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Corporate Videography','shortCode'=>'corporate videography','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Video Editing','shortCode'=>'video editing','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Video Production','shortCode'=>'video production','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Video Streaming and Webcasting Services','shortCode'=>'video streaming and webcasting services','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Video Transfer Services','shortCode'=>'video transfer services','description'=>'','parent_id'=>$mainsubcategory),
                      array('name'=>'Wedding and Event Videography','shortCode'=>'wedding and event videography','description'=>'','parent_id'=>$mainsubcategory),
                ));
                
                $mainsubcategory = DB::table('category')->insertGetId(array('name'=>'Wedding','shortCode'=>'wedding','description'=>'','parent_id'=>$category));
                $subCategory = DB::table('category')->insert(array(
                    array('name'=>'Bridal Stylist','shortCode'=>'bridal stylist','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding Cakes','shortCode'=>'wedding cakes','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding Coordination','shortCode'=>'wedding coordination','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding Dance Lessons','shortCode'=>'wedding dance lessons','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding Florist','shortCode'=>'wedding florist','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding Officiant','shortCode'=>'wedding officiant','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding Planning','shortCode'=>'wedding planning','description'=>'','parent_id'=>$mainsubcategory),
                    array('name'=>'Wedding Ring Services','shortCode'=>'wedding ring services','description'=>'','parent_id'=>$mainsubcategory),
                ));
    }
}
