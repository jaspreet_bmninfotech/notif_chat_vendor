<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
	 protected $toTruncate = ['category'];
     
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // run composer dump-autoload first time

        //START TRUNCATE FOREIGN KEY AND NEXT NORMAL TRUNCATE
        Eloquent::unguard(); 
        DB::statement('SET FOREIGN_KEY_CHECKS=0;'); 
    	// Model::unguard();

    	// foreach ($this->toTruncate as $table) {
     //        DB::table($table)->truncate();
     //    }
        
        //START COUNTRY TABLE SEEDER 
        // $this->call(CountryTableSeeder::class);
        // $this->command->info('Country table seeded!');
        // $path ='app/database/dumps/country.sql';
        //END COUNTRY SEEDER
        
        //START STATE TABLE SEEDER
        // $this->call(StateTableSeeder::class);
        // $this->command->info('State table seeded!');
        // $path='app/database/dumps/state.sql';
        //END STATE SEEDER

        //START CITY TABLE SEEDER
        // $this->call(CityTableSeeder::class);
        // $this->command->info('City table seeded!');
        // $path='app/database/dumps/city.sql';
        //END CITY  TABLE SEEEDER
        

        //START LANGUAGE TABLE SEEDER
        // $this->call(LanguagesTableSeeder::class);
        // $this->command->info('Languages table seeded!');
        // $path='app/database/dumps/languages.sql';
        //END LANGUAGE  TABLE SEEEDER

        //START USERTABLE SEEDER,ADDRESSTABLE SEEDER,JOBTABLE SEEDER
        // $this->call(UserTableSeeder::class);
        // $this->call(AddressTableSeeder::class);    
        // $this->call(JobTableSeeder::class);

        $this->call(CategoryTableSeeder::class);
        $this->command->info("Category table seeded :)");
        // $this->call(PlanTableSeeder::class);
        // $this->command->info("Plan table seeded :)");

        //END THREE SEEDER
        //  $this->call(TimeZoneTableSeeder::class);
        // $this->command->info("Timezone table seeded :)");

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
       //END TRUNCATE FOREIGN KEY AND NEXT NORMAL TRUNCATE 
       Model::reguard();    
    }
}
