function vendor_remove_post(job_id)
{
    swal({
            title: 'Are you sure?',
            text: "It will be deleted permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            showLoaderOnConfirm: true,
              
            preConfirm: function() {
              return new Promise(function(resolve) {
                   
                 $.ajax({
                    url: '/vendor/tourguide/close',
                    type: 'GET',
                    data: 'id='+job_id,
                    dataType: 'json'
                 })
                 .done(function(response){
                    swal('Deleted!', response.message, response.status);
                     setTimeout(function() {
                      window.location.href = "/vendor/tourguide/jobs";
                    }, 1000);                
                 })
                 .fail(function(){
                    swal('Oops...', 'Something went wrong with ajax !', 'error');
                 });
              });
            },
            allowOutsideClick: false              
        });    
}
function client_remove_post(job_id)
{
    swal({
            title: 'Are you sure?',
            text: "It will be deleted permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#5fcf80',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            showLoaderOnConfirm: true,
              
            preConfirm: function() {
              return new Promise(function(resolve) {
                   
                 $.ajax({
                    url: '/client/tourguide/close',
                    type: 'GET',
                    data: 'id='+job_id,
                    dataType: 'json'
                 })
                 .done(function(response){
                    swal('Deleted!', response.message, response.status);
                     setTimeout(function() {
                      window.location.href = "/client/tourguide/jobs";
                    }, 1000);                
                 })
                 .fail(function(){
                    swal('Oops...', 'Something went wrong with ajax !', 'error');
                 });
              });
            },
            allowOutsideClick: false              
        });    
}