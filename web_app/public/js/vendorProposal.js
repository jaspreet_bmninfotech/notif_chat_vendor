var selectedFiles = [];
var jobId = '';
$(document).ready(function() {
		var bidPrice=$("#bidPrice");
    		bidPrice.keyup(function(){
    			
        var payment=isNaN(parseInt(bidPrice.val() * $("#serviceFee").val()/100)) ? 0 :(bidPrice.val()  * $("#serviceFee").val()/100)
        var totalpayment=isNaN(parseInt(bidPrice.val() - payment)) ? 0 :(bidPrice.val() - payment)
        $("#payment").val(totalpayment);
    });

	    $(".btn-submit").click(function(e){
	    	e.preventDefault();

	    	var bidPrice = $("input[name='bidPrice']").val();
	    	var serviceFee = $("input[name='serviceFee']").val();
	    	var payment = $("input[name='payment']").val();

	        $.ajax({
	            url: "/vendor/job/bid",
	            type:'POST',
	            data: {bidPrice:bidPrice, serviceFee:serviceFee, payment:payment},
	            success: function(data) {
	                if($.isEmptyObject(data.error)){
	                	alert(data.success);
	                }else{
	                	printErrorMsg(data.error);
	                }
	            }
	        });

	    }); 

	    function printErrorMsg (msg) {
			$(".print-error-msg").find("ul").html('');
			$(".print-error-msg").css('display','block');
			$.each( msg, function( key, value ) {
				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
			});
		}
		
	});
    $('#removecoverimg').click(function(){
        $(this).parents('.coverfile').find('.coverimagePreview').remove();
        $(this).parents('.vf-textarea-halfsize').find('.progress').remove();
        $('.coveri i').css('display','none');
        $(this).parents('.vf-textarea-halfsize').find('input').val('');
    });

 $(document).ready(function() {
    $('.profileimagesf').mouseenter(function(){
        $(this).find('i').show();
    });
    $('.profileimagesf').mouseleave(function(){
        $(this).find('i').hide();
    });


    $(document).on('click','.profileimagesf i',function(){
        var removeId= $(this).attr('data-id');
        console.log(removeId);
    });

});
	// image uploads
	$(function () {
         $('#submitProposal').click(function(e){
            e.preventDefault();
            $('.form-loader').fadeIn(200);
           var token = $('input[name=_token]').val();
           jobId = $('input[name=jobId]').val();
           var tid = $('input[name=tid]').val();
           var bidPrice = $('input[name=bidPrice]').val();
           var serviceFee = $('input[name=serviceFee]').val();
           var payment = $('input[name=payment]').val();
           var cover = $('textarea[name=cover]').val(); 
        $.ajax({
        url: "/vendor/job/proposals",
        type: 'POST',
        dataType: 'JSON',
        data:{bidPrice: bidPrice,
                serviceFee: serviceFee,
                payment: payment,
                cover: cover,
                _token:token,
                jId :jobId,
                tId :tid
            },
        success: function(data) {
         if (data.success===true) {
            if(data.success.selectedFiles==undefined)
            {   if(tid=="")
                {
                top.location.href ='/vendor/proposal/'+jobId+'/details';
                }
                else if(tid!=""){
                     top.location.href ='/vendor/proposal/'+jobId+'/'+tid+'/details'
                }
            }
            $('#coverfileupload').fileupload('send', {files : selectedFiles});
         }
         else if(data.message)
         {
            $('.message').html(data.message);
         }
            $('.form-loader').fadeOut(200);

        }
    });
    });
    'use strict';
    
  $('#coverfileupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });

    $('#coverfileupload').fileupload({
        url: '/vendor/coverfileupload',
        dataType: 'json',
        autoUpload: false,
        // singleFileUploads:false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        // previewCrop: true,
        success: function(e ,data) {
           console.log(tid);
                if (e.success) {
                    if(tid=="")
                    {
                     top.location.href ='/vendor/proposal/'+jobId+'/details';
                    }
                    else if(tid!=""){
                         top.location.href ='/vendor/proposal/'+jobId+'/'+tid+'/details'
                    }
                  
                }
            }

    }).on('fileuploadadd', function (e, data) {
                  
        data.context = $('<div class="coverimagePreview"/>').appendTo('#coverfiles');
        $('.coveri i').css('display','block');
        $.each(data.files, function (index, file) {
            var node = $('<p />')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
                $('#progress').css('display','block');
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
