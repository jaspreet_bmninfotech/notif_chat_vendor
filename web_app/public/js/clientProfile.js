var selectedFiles = [];
$(function () 
  {
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
      $.ajax({
      url:'/getclientprofile',
      type: "GET",
      dataType: "json",      
          success: function(response) 
          {

             if (response.code == 1) 
             {
            
            var data = response.data;
          
            $('input[name=address]').val(data.address);
            $('input[name=postalCode]').val(data.postalCode);
            $('#country').val(data.country_id);
            getstate($('#country')[0], function(){
              $('#state').val(data.state_id);
              getcity($('#state')[0], function(){
              $('#city').val(data.city_id);
            });
            });
          }
         }
      }); 

  });  
isValidphone = false;
isValidfirstName = false; 
isValidlastName =false;
$(function () {
         $('#createprofile').click(function(e){
            e.preventDefault();
            $('.firstName_error').hide();
            $(".lastName_error").hide();
            $(".countrycode_error").hide();
            $(".phone_error").hide();
      
            var token = $('input[name=_token]').val();
            var firstName = $('input[name=firstName]').val();
            if(firstName=="")
            {
                isValidfirstName = false;
                $('.firstName_error').html('<font color="red">' +
                    'Please enter firstName .</font>');
                  $('.firstName_error').show();
            }
            var lastName = $('input[name=lastName]').val();
            if(lastName=="")
            {
                isValidlastName = false;
                $('.lastName_error').html('<font color="red">' +
                    'Please enter lastName .</font>');
                  $('.lastName_error').show();
            }
            var gender = $("input[name='gender']:checked").val();
            var dialCode= $('input[name=dialCode]').val();

            if(dialCode=="")
            {
                $('.countrycode_error').html('<font color="red">' +
                    'Please choose valid Country dialCode .</font>');
                  $('.countrycode_error').show();
            }
            var phone = $('input[name=phone]').val();

            if(phone=="")
            {
                isValidphone = false;
                 $('.phone_error').html('<font color="red">' +
                    'Please enter valid phone number.</font>');
                  $('.phone_error').show();

            }else{
                var filter =/^(\d{10}|\d{12})$/;
                if (filter.test(phone)) {
                     isValidphone = true;
                    $('.phone_error').hide();

                }else
                {
                    if(phone.length <10){
                         isValidphone = false;
                      $('.phone_error').html('<font color="red">' +
                        'Please enter atleast 10 digit number .</font>');
                      $('.phone_error').show();
                    }else if(phone.length > 10 && phone.length<12)
                    {
                         isValidphone = false;
                        $('.phone_error').html('<font color="red">' +
                        'Phone number not exceed from 12 digit .</font>');
                      $('.phone_error').show();
                    }

                }  
            }
            var dob = $('input[name=dob]').val();          
        $.ajax({
        url: "/client/profile/update",
        type: 'POST',
        dataType: 'JSON',
        data:{firstName: firstName,
                lastName: lastName,
                gender: gender,
                phone: phone,
                dob: dob,
                dialCode:dialCode,
                _token:token
            },
        success: function(data) {
        
         var postId = data.postId;
         if (data.success===true) {
            if(data.success.selectedFiles==undefined)
            { 
                location.reload();
            }
            $('#cprofileupload').fileupload('send', {files : selectedFiles});
         }                   
        }
    });
    });
    function ValidationEvent() {
        if(!isValidphone || !isValidfirstName || !isValidlastName)
        {
            return false;
        } else {
          return true;
        }
        return true;
    }
  $('#cprofileupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });

    $('#cprofileupload').fileupload({
        url: '/client/cprofileuploads',
        dataType: 'json',
        autoUpload: false,
        // singleFileUploads:false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        
        // previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
                    location.reload();
         }
            }


    }).on('fileuploadadd', function (e, data) {
      $('#cprofilefiles').empty();
                   
        data.context = $('<div/>').appendTo('#cprofilefiles');
        $.each(data.files, function (index, file) {
          $('#cprofileimage').hide();

            var node = $('<p class=""/>')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});