$(document).ready(function(){
      $('.bussiness').hide();
      $('#other').show();
      $('#category').on('change', function() {
      var category =$(this).children(':selected').val();
            var catvalue =category.split('_');
            var targetBox = $("#" + catvalue[1]); 
            $('.bussiness').hide();
            // if(catvalue[1] == 'guide' 
            //   || catvalue[1] !== 'venues'
            //   & catvalue[1] !=='model')
            // {       
              $('#other').show();
            // }else{
              $(targetBox).show();
            // }
    });
            
});
 function removeBusinessimg(id,bzId,path,name){
     
        $.ajax({
        url: "/vendor/info/removebusinessimg",
        type:"get",
        data:{remove_id : id,
              busiremove_id : bzId,paths : path,filename : name},
        success: function(retJson){
             if (retJson) {
              location.reload();
         }
       
        }
    });
    }

 $(document).ready(function() {
    $('.profileimagesf').mouseenter(function(){
        $(this).find('i').show();
    });
    $('.profileimagesf').mouseleave(function(){
        $(this).find('i').hide();
    });


    $(document).on('click','.profileimagesf i',function(){
        var removeId= $(this).attr('data-id');
        console.log(removeId);
    });

});
   $(function () 
  {
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
      $.ajax({
      url:'/getbussinessdata',
      type: "GET",
      dataType: "json",      
          success: function(response) 
          {
             if (response.code == 1) 
             {
            console.log(response);
            var data = response.data;
            $("#name").val(data.name);
            $('#category').val(data.categoryId +"_"+ data.shortCode);
            if(data.shortCode=='venues')
            {
             $('#venues').show();
            
            $('#country').val(data.country_id);
             getstate($('#country')[0], function(){
            $('#state').val(data.state_id);
              getcity($('#state')[0], function(){
            $('#city').val(data.city_id);
            });
            });
            $('input[name="streetaddress"]').val(data.address);
            $('input[name="postalCode"]').val(data.postalCode);
            $('input[name="maximumCapacity"]').val(data.info.maximumCapacity);   
            $('input[name="venueType[]"]').val(data.info.venueType);   
            $('input[name="venueSetting[]"]').val(data.info.venueSetting);   
            $('input[name="venueActivities[]"]').val(data.info.venueActivities);
            }
            else if(data.shortCode=='model')
            {
               $('#model').show();
           
            var unitMesurement =data.info.unitMeasurement ? 'imperial':'metric';

            $('input[name="unitMesurement"]').val(unitMesurement).attr('checked',true);
            $('select[name="height"]').val(data.info.height);
            $('input[name="grade"]').val(data.info.grade);
            var inSchool = data.info.inSchool ? 'yes' :'no'; 
            $('input[name="inschool"]').val(inSchool).attr('checked',true);
            $('select[name="haircolor"]').val(data.info.hairColor);
            $('select[name="eyecolor"]').val(data.info.eyeColor);
            $('select[name="weight"]').val(data.info.weight);
            $('select[name="bust"]').val(data.info.bust);
            $('select[name="hips"]').val(data.info.hips);
            $('select[name="shoesize"]').val(data.info.shoeSize);
            $('input[name="modelingtype[]"]').val(data.info.typeOfModel); 
            $(".superpower").val(data.info.superPower);
            $(".famousperson").val(data.info.famousPerson);
            $(".talent").val(data.info.talent); 
            }
            else if(data.shortCode=='guide')
            {
             $('#guide').show();
            // $('.bussiness').hide();
            $('select[name="countryOfResidence"]').val(data.info.countryofresidence);
            var lawfull = data.info.lawfull ? 'yes':'no';
            $('input[name="lawfull"]').val(lawfull).attr('checked',true);
            $('input[name="guidepeople[]"]').val(data.info.guidepeople);
            $('input[name="scale"]').val(data.info.scale);
            $('input[name="language"]').val(data.info.language);
                
            }
            var canTravel = data.canTravel ? 'yes' : 'no';
            $('select[name="cantravel"]').val(canTravel);
            getSubcategories($('#category')[0], function(){
            $('#subcategory').val(data.sub_category_id);
            })
            $('select[name="hearaboutus"]').val(data.info.hearaboutus);
            $('#description').val(data.description); 
          }
          }
      }); 

  });   
  
function getSubcategories(ele, callback = false)
{
  var catId = $(ele).children(':selected').data('id');
  $.ajax({
    url: "/getsubcategories/"+catId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#subcategory').empty();
          
          $.each(data,function(index,subcatObj){
          $('#subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
        });
          if(typeof(callback) == 'function'){
            callback();
          }
        }
  });

}
function getstate(e ,callback=false)
{
  var countryId =$(e).children(':selected').data('id');
  $.ajax({
    url: "/getstate/"+countryId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#state').empty();
          $('#state').append('<option value="">select</option>');
          $.each(data,function(index,stateObj){
            $('#state').append('<option value="'+stateObj.id+'" data-id="'+stateObj.id+'">'+stateObj.name+'</option>');
          });
          if(typeof(callback) == 'function'){
            callback();
          } 
        }
  });

}
function getcity(es , callback =false)
{
  var stateId =$(es).children(':selected').data('id');
  $.ajax({
    url: "/getcity/"+stateId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#city').empty();
          $('#city').append('<option value="">select</option>');
          $.each(data,function(index,cityObj){
           $('#city').append('<option value="'+cityObj.id+'" data-id="'+cityObj.id+'" >'+cityObj.name+'</option>');
        });
            if(typeof(callback) == 'function'){
            callback();
          }   
        }
  });

}

document.getElementById('file').onchange = function(e){
    if (e.target.files.length < 6) {
         for (var i = 0; i < e.target.files.length;i++) {
            if (e.target.files[i].size <= 2000000) {
          // $.each(e.target.files, function (index, file) {
          var image = e.target.files[i];
          loadImage(image, function(img){
           document.querySelector("#viewimg").appendChild(img);
           },{
            maxWidth:100,
            maxHeight:100
           }); 
           }else{
              alert('File size should be 2mb');
            } 
         }
     }else{
      alert('Maximum five files are allowed');
    }
    
 };

  document.getElementById('guidefile').onchange = function(e){
    if (e.target.files.length < 6) {
       for (var i = 0; i < e.target.files.length;i++) {
          console.log(e.target.files[i].size);
          if (e.target.files[i].size <= 2000000) {

              var image = e.target.files[i];
              loadImage(image, function(img){
               document.querySelector("#viewguideimg").appendChild(img);
               },{
                maxWidth:100,
                maxHeight:100
               });  
          }else{
            alert('File size should be 2mb');
          }
        }
      
      // $.each(e.target.files, function (index, file) {
    }else{
    alert('Maximum five files are allowed');
    }
  };

    $(document).change(function(){
      var count = $("#viewguideimg > img").length
      if (count  == 5) {
      $('#viewguideimg').empty();
      }

      var counts = $("#viewimg > img").length
      if (counts  == 4) {
        $('#viewimg').empty();
        
      }
    });