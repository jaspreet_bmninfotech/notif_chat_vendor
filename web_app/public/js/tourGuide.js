 var jobListPage = 1;
    var loadedJobs = 0;
    $(document).ready(function(){
        $("#show-mores").click(function(){
            gettourguide();
        }).click();
    });
    

    function gettourguide(){
        $.ajax({
        url: "/getvendors/" + jobListPage,
        type: 'GET',
        dataType: 'JSON',
        success: function(data ) {
            loadedJobs += data.per_page;
            jobListPage = data.current_page + 1;
            var template = _.template($("#tourguideListTmpl").html());
            template = template({data:data.data});
            $("#vendorlist tbody").append(template);
            if(loadedJobs >= data.total){
                $("#show-mores").hide();
            }
        }
    });
    }
 var selectedFiles = [];
 $(document).ready(function() {
    $('.profileimagesf').mouseenter(function(){
        $(this).find('i').show();
    });
    $('.profileimagesf').mouseleave(function(){
        $(this).find('i').hide();
    });


    $(document).on('click','.profileimagesf i',function(){
        var removeId= $(this).attr('data-id');
        console.log(removeId);
    });

});
 $('#filterstate').change(function(){
        var stateId = $("#filterstate").val();
    $.ajax({
        url: "/getcity/"+stateId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#cityfilter').empty();
          $('#cityfilter').append('<option value="">select</option>');
            $.each(data,function(index,cityObj){
             $('#cityfilter').append('<option value="'+cityObj.id+'" data-id="'+cityObj.id+'" >'+cityObj.name+'</option>');
        });
              if(typeof(callback) == 'function'){
            callback();
          }     
        }
    });
    
});
    function removeProfileimg(id,name){
        $.ajax({
        url: "/tourguide/job/removeProfileimg",
        type:"get",
        data:{remove_id : id,filename : name},
        success: function(retJson){
             if (retJson) {
              location.reload();
         }
       
        }
    });
    }

    function removedocimg(id,name){
        $.ajax({
        url: "/tourguide/job/removedocimg",
        type:"get",
        data:{remove_id : id,filename : name},
        success: function(retJson){
             if (retJson) {
              location.reload();
         }
       
        }
    });
    }
    function removelicimg(id,name){
        $.ajax({
        url: "/tourguide/job/removelicimg",
        type:"get",
        data:{remove_id : id,filename : name},
        success: function(retJson){
             if (retJson) {
              location.reload();
         }
       
        }
    });
    }
$("#myForm").submit(function(){
    var filter_country = $('input[name="filter_country"]').val();
    var filter_state = $("#filterstate").val();
    var filter_city = $('#cityfilter').val();
    var filter_name = $('input[name="filter_name"]').val();
    var filter_gender = $('input[name="filter_gender"]:checked').val();
    var filter_status = $('input[name="filter_status"]:checked').val();
    var filter_language = $('input[name="filter_status"]').val();
    var filter_activities = $('#filtActivity option:selected').val();
    var filter_job_success = $('input[name="filter_job_success"]:checked').val();
    event.preventDefault();
    $.ajax({
        url: "/tourguidefilter/"+filter_country,
        type:"get",
        data:{method : 'filter' , filter_country : filter_country,filter_state : filter_state,filter_city : filter_city,filter_name:filter_name,filter_gender:filter_gender,filter_status:filter_status,filter_language:filter_language,filter_activities:filter_activities,filter_job_success:filter_job_success},
        success: function(datas){
            console.log(JSON.parse(datas).data);
            $("#tgtmplate").html('');
            var data = JSON.parse(datas);
            console.log('data.data');
            console.log(data);
            var template = _.template($("#tgListTmpl").html());
            template = template({data:data});
            $("#tgtmplate").append(template);
            
        }
    });
    });
$(document).ready(function(){
    $("#search_guide_btn").click(function(){
        $(".filters-div").toggleClass('hide');
    });

    $("#close_filter").click(function(){
        $(".filters-div").toggleClass('hide');
    });
});

function initiate_invite(user_id)
{
    $.ajax({
        url: "/searchtourguide-details/"+user_id,
        dataType: 'json',
        method: 'GET',
        success: function(retJson){
            console.log(retJson);
            $(".userimgappend").empty();
            if(retJson.data.firstName && retJson.data.lastName)
            {
            $(".invitation_rcvr_name").html(retJson.data.firstName + ' ' + retJson.data.lastName);
            $(".invitation_rcvr_id").val(retJson.data.id);
            $(".userimgappend").html('<img src="/'+retJson.data.path+'" class="img-responsive img-circle">');
            }
            else
            {
                $(".invitation_rcvr_name").html(retJson.data.username);
            $(".invitation_rcvr_id").val(retJson.data.id);
            $(".userimgappend").html('<img src="/'+retJson.data.path+'" class="img-responsive img-circle">');
            }
        }
    });

    $('#invitation_modal').modal('show');
}

function accept(user_id)
{
    console.log(user_id);
    $.ajax({
        url: "/tourguide/job/accept",
        type:"get",
        data:{accept_id : user_id},
        success: function(retJson){
            console.log(retJson);
             if (retJson.success) {
                 location.reload();   
         }

            
        }
    });

    
}
function declined(user_id)
{
    $.ajax({
        url: "/tourguide/job/declined",
        type:"get",
        data:{declined_id : user_id},
        success: function(retJson){
            console.log(retJson);
             if (retJson.success) {
                location.reload();
         }  
        }
    });
}

 function getSubcategories(ele)
{
    var catId = $(ele).children(':selected').data('id');
    $.ajax({
        url: "/getsubcategories/"+catId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
            $('#subcategory').empty();
            $('#subcategory').append('<option value="">Select</option>');
            $.each(data,function(index,subcatObj){
            $('#subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
           
        });
        }
    });

}

    
function getSubcategories(ele, callback = false)
{
    var catId = $(ele).children(':selected').data('id');
    $.ajax({
        url: "/getsubcategories/"+catId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
            $('#subcategory').empty();
          
            $.each(data,function(index,subcatObj){
            $('#subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
        });
            if(typeof(callback) == 'function'){
            callback();
          }
        }
    });

}
function getstate(e ,callback=false)
{
    var countryId =$(e).children(':selected').data('id');
    $.ajax({
        url: "/getstate/"+countryId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#state').empty();
          $('#state').append('<option value="">select</option>');
            $.each(data,function(index,stateObj){
                $('#state').append('<option value="'+stateObj.id+'" data-id="'+stateObj.id+'">'+stateObj.name+'</option>');
          });
            if(typeof(callback) == 'function'){
            callback();
          } 
        }
    });

}
function getcity(es , callback =false)
{
    var stateId =$(es).children(':selected').data('id');
    $.ajax({
        url: "/getcity/"+stateId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#city').empty();
          $('#city').append('<option value="">select</option>');
            $.each(data,function(index,cityObj){
             $('#city').append('<option value="'+cityObj.id+'" data-id="'+cityObj.id+'" >'+cityObj.name+'</option>');
        });
              if(typeof(callback) == 'function'){
            callback();
          }     
        }
    });

}

function fgetstate(e ,callback=false)
{
    var countryId =$(e).children(':selected').data('id');
    $.ajax({
        url: "/getstate/"+countryId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#fstate').empty();
          $('#fstate').append('<option value="">select</option>');
            $.each(data,function(index,stateObj){
                $('#fstate').append('<option value="'+stateObj.id+'" data-id="'+stateObj.id+'">'+stateObj.name+'</option>');
          });
            if(typeof(callback) == 'function'){
            callback();
          } 
        }
    });

}
function fgetcity(es , callback =false)
{
    var stateId =$(es).children(':selected').data('id');
    $.ajax({
        url: "/getcity/"+stateId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#city').empty();
          $('#city').append('<option value="">select</option>');
            $.each(data,function(index,cityObj){
             $('#fcity').append('<option value="'+cityObj.id+'" data-id="'+cityObj.id+'" >'+cityObj.name+'</option>');
        });
              if(typeof(callback) == 'function'){
            callback();
          }     
        }
    });

}

function tgetstates(e ,callback=false)
{
    var countryId =$(e).children(':selected').data('id');
    $.ajax({
        url: "/getstate/"+countryId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#states').empty();
          $('#states').append('<option value="">select</option>');
            $.each(data,function(index,stateObj){
                $('#states').append('<option value="'+stateObj.id+'" data-id="'+stateObj.id+'">'+stateObj.name+'</option>');
          });
            if(typeof(callback) == 'function'){
            callback();
          } 
        }
    });

}
function tgetcitys(es , callback =false)
{
    var stateId =$(es).children(':selected').data('id');
    $.ajax({
        url: "/getcity/"+stateId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#citys').empty();
          $('#citys').append('<option value="">select</option>');
            $.each(data,function(index,cityObj){
             $('#citys').append('<option value="'+cityObj.id+'" data-id="'+cityObj.id+'" >'+cityObj.name+'</option>');
        });
              if(typeof(callback) == 'function'){
            callback();
          }     
        }
    });

}

function send_invitation()
{
    if ($("textarea[name='invitation_message']").val() != '')
    {
        $.ajax({
            url: "/invite-to-job",
            headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
            data: {'form_data': $("#send_invitation_form").serialize()},
            dataType: 'json',
            method: 'POST',
            success: function(retJson){
                alert(retJson.message);
                if (retJson.status)
                {
                    $('#invitation_modal').modal('hide');
                }
            }
        });
    }
    else
    {
        alert('Please add your custom message to post a job.');
    }
}
 $(function () {
            


        $('#tprofileupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });
        var acceptFileTypesi = /^image\/(gif|jpe?g|png|pdf|doc|txt)$/i;
    $('#tprofileupload').fileupload({
        url: '/tourguide/tfileuploads',
        dataType: 'json',
        autoUpload: true,
        // singleFileUploads:false,
        acceptFileTypes: acceptFileTypesi,
        maxFileSize: 1990000,
        minFileSize: 1,
        maxNumberOfFiles:1,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
         }
            }


    }).on('fileuploadadd', function (e, data) {
      $('#tprofilefiles').empty();
                  console.log(data);  
        data.context = $('<div/>').appendTo('#tprofilefiles');
        $.each(data.files, function (index, file) {
          $('#tprofileimage').hide();

            var node = $('<p class=""/>')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

/*  Document Upload   */
$(function () {
            


        $('#docupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });
      var acceptFileTypesi = /^image\/(gif|jpe?g|png|pdf|doc|txt)$/i;  
    $('#docupload').fileupload({
        url: '/tourguide/docuploads',
        dataType: 'json',
        autoUpload: true,
        // singleFileUploads:false,
        acceptFileTypes: acceptFileTypesi,
        maxFileSize: 1990000,
        minFileSize: 1,
        maxNumberOfFiles:1,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
         }
            }


    }).on('fileuploadadd', function (e, data) {
      $('#docfiles').empty();
                  console.log(data);  
    $('#docupload').parents('.profprogdocs').find('.progress').css('display','block');
        data.context = $('<div/>').appendTo('#docfiles');
        $.each(data.files, function (index, file) {

            var node = $('<p class="imagePreview"/>')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

/* licence upload */

$(function () {
            


        $('#licenceupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });
        var acceptFileTypesi = /^image\/(gif|jpe?g|png|pdf|doc|txt)$/i;
    $('#licenceupload').fileupload({
        url: '/tourguide/licenceuploads',
        dataType: 'json',
        autoUpload: true,
        // singleFileUploads:false,
        acceptFileTypes: acceptFileTypesi,
        maxFileSize: 1990000,
        minFileSize: 1,
        maxNumberOfFiles:1,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
         }
            }


    }).on('fileuploadadd', function (e, data) {
      $('#licencefiles').empty();
                 
        data.context = $('<div/>').appendTo('#licencefiles');
        $('#licenceupload').parents('.profprogdocs').find('.progress').css('display','block');
        $.each(data.files, function (index, file) {

            var node = $('<p class=""/>')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progresslice .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

/* insurance upload*/
$(function () {
        $('#insuranceupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });
        var acceptFileTypesi = /^image\/(gif|jpe?g|png|pdf|doc|txt)$/i;
    $('#insuranceupload').fileupload({
        url: '/tourguide/insuranceuploads',
        dataType: 'json',
        autoUpload: true,
       //  singleFileUploads:true,
        acceptFileTypes: acceptFileTypesi,
        maxFileSize: 1990000,
        minFileSize: 1,
        maxNumberOfFiles:1,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
         }
            }


    }).on('fileuploadadd', function (e, data) {
      $('#insurancefiles').empty();
                  console.log(data);  
        data.context = $('<div/>').appendTo('#insurancefiles');
        $('#insuranceupload').parents('.profprogdocs').find('.progress').css('display','block');
        $.each(data.files, function (index, file) {

            var node = $('<p class=""/>')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
                
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
                
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

//tour guide multiple profile uploads

$('#profilesupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });

    $('#profilesupload').fileupload({
        url: '/tourguide/multiprofileuploads',
        dataType: 'json',
        autoUpload: true,
        // singleFileUploads:false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 2000000,
        
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
         }
            }


    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div class="imagePreview"/>').appendTo('#profilesuploadappend');
        $.each(data.files, function (index, file) {
            var node = $('<p />')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#mprofileprogress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

$(document).ready(function() {
  $('#enddatePicker')
    .datepicker({
        startDate: new Date(),
        autoclose: true,    // It is false, by default
        format: 'yyyy-mm-dd',
        todayHighlight: true,
    }); 
 $('#datePicker')
    .datepicker({
        startDate: new Date(),
        autoclose: true,    // It is false, by default
        format: 'yyyy-mm-dd',
        todayHighlight: true,
    }); 
 $('.timepicker-default').timepicker({
    format: 'hh:mm:ss',
    icons: {
    up: "fa fa-arrow-up",
    down: "fa fa-arrow-down"
    },
        maxHours: 24,
        showMeridian: false,
 });
     
});