var files = [];
var selectedFiles = [];
 $(document).ready(function() {
    $('.profileimagesf').mouseenter(function(){
        $(this).find('i').show();
    });
    $('.profileimagesf').mouseleave(function(){
        $(this).find('i').hide();
    });
    // $(document).on('click','.profileimagesf i',function(){
    //     var removeId= $(this).attr('data-id');
    //     console.log(removeId);
    // });
});

function removejobimg(id,jobId,name,e){
        $.ajax({
        url: "/client/job/removejobimg",
        type:"get",
        data:{remove_id : id,jobId : jobId,filename : name},
        success: function(retJson){
            if (retJson) {
              $(e).parents('.profileimagesf').remove();
            }
        }
    });
}
(function ($) {
    $('input[name="contracttype"]').click(function () {
        var selected = $('input[name="contracttype"]:checked').val();
        var unselected = $('input[name="contracttype"]:not(:checked)').val();
        $('.form-group.hidden.' + selected).removeClass('hidden');
        $('.form-group.' + unselected).not('.hidden').addClass('hidden');
    });
    $('input[name="create_type"]').click(function () {
        var selected = $('input[name="contracttype"]:checked').val();
        if (selected == 'old') {
            $('.old-jobs.hidden').removeClass('hidden')
        }
        else {
            $('.old-jobs').not('.hidden').addClass('hidden')
        }
    });

    $(function () {

        $('#postupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });
      var acceptFileTypesi = /^image\/(gif|jpe?g|png|pdf|doc|txt)$/i;  
    $('#postupload').fileupload({
        url: '/client/postuploads',
        dataType: 'json',
        autoUpload: true,
        // singleFileUploads:false,
        acceptFileTypes: acceptFileTypesi,
        maxFileSize: 1990000,
        minFileSize: 1,
        maxNumberOfFiles:5,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
         }
            }


    }).on('fileuploadadd', function (e, data) {
      $('#docfiles').empty();
                  console.log(data);  
        data.context = $('<div/>').appendTo('#viewimage');
        $.each(data.files, function (index, file) {

            var node = $('<p class=""/>')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#jobprogress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});


  $('#fileupload').fileupload({
         
            dataType: 'json',
            url: 'fileupload',
            maxNumberOfFiles: 5,
           
            done: function (e, data) {
                files.push(data.result);
                $("#fileattachment").val(JSON.stringify(files));      
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                console.log(progress);
                var progressTmpl = '<div class="progress">' +
                                    '<div class="progress-bar progress-bar-success"></div>'+
                                '</div>';
                $(progressTmpl).appendTo("#progress")
                $(progressTmpl).children(".progress-bar").css('width',progress + '%');
            },
       
    });

   $('button.cancel').click(function(){
    $('#fileupload').remove();
   });
})(jQuery);

$("input#typeahead").typeahead({
    onSelect: function(item) {

        console.log(item);
    },
    ajax: {
        url: "autocomplete",
        timeout: 500,
        displayField: "username",
        triggerLength: 1,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            // showLoadingMask(true);
            return {
                search: query
            }
        },
        preProcess: function (response) {
            // showLoadingMask(false);
            if (response.success === false) {
                // Hide the list, there was some error
                return false;
            }
            if (response.success === true) {
                // Hide the list, there was some error
                return response.data;
             
           }
        }
    }
});

 function getSubcategories(ele)
{
    var catId = $(ele).children(':selected').data('id');
    var sub_id= $('#sub_id').val();
    $.ajax({
        url: "/getsubcategories/"+catId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
            $('#subcategory').empty();
            $('#subcategory').append('<option value="">Select</option>');
            $.each(data,function(index,subcatObj){
            $('#subcategory').append('<option value="'+subcatObj.id+'" data-id="'+subcatObj.id+'">'+subcatObj.name+'</option>');
        });
            
            if(sub_id){
                $('#subcategory').val($('#sub_id').val());
            }
            $('#sub_id').val("");
        }
    });
}
 function getSubSubcategory(ele)
{
    var subcatId = $(ele).children(':selected').data('id');
    var sub_id= $('#sub_id').val();
    $.ajax({
        url: "/getsubSubcategories/"+subcatId,
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
            $('#sub_subcategory').empty();
            $('#sub_subcategory').append('<option value="">Select</option>');
            $.each(data,function(index,subcatObj){
            $('#sub_subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
            // if(sub_id){
            //     $('#subcategory').val($('#sub_id').val());
            // }
            // $('#sub_id').val("");
        }
    });
}
$(document).ready(function(){
    $('.typeahead li').click(function(){
        alert('getuser');
    });
});

$(document).ready(function() {
    // if edit mode
    if($('#sub_id').val() != ""){
        getSubcategories($("#category_id")[0]);
    }

 $('#datePicker').datepicker({
        autoclose: true,    // It is false, by default
        format: 'yyyy-mm-dd',
        todayHighlight: true,


    }); 
 $('.timepicker-default').timepicker({
    format: 'hh:mm:ss',
    icons: {
    up: "fa fa-arrow-up",
    down: "fa fa-arrow-down"
    },
        maxHours: 24,
        showMeridian: false,
        minuteStep : 1,
        defaultTime : false,
 });
            
});


        
    