(function ($) {
    var cur_menu = $('a[href="' + window.location.href + '"]');
    $(cur_menu).closest('li').addClass('active').closest('.has-submenu').addClass('active');
    $(cur_menu).attr('href', 'javascript::void(0);');
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip();   
	});
})(jQuery);