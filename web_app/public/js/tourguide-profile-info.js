var selectedFiles = [];
var postId = '';
 $(document).ready(function() {


});
$(document).ready(function(){
     getprofileinfo();

        
        $('#album-form').hide();
        $('#createImageInfo').hide();
        $('#textcolumn').hide();
        $('#createvideoInfo').hide();
        $('#imagecolumn').hide();
        $('#videocolumn').hide();
        $('#linkcolumn').hide();
        $('#texticon').click(function(){
            $('#textcolumn').show();
            $('#album-form').show();
            $('#post').show();
            $('#createImageInfo').hide();
            $('#createvideoInfo').hide();
            $('#imagecolumn').hide();
            $('#videocolumn').hide();
            $('#linkcolumn').hide();
            $('#youtubecolumn').hide();
        });
        $('#imageicon').click(function(){
            $('#imagecolumn').show();
            $('#album-form').show();
            $('#textcolumn').hide();
            $('#post').hide();
            $('#createvideoInfo').hide();
            $('#videocolumn').hide();
            $('#createImageInfo').show();
            $('#linkcolumn').hide();
            $('#youtubecolumn').hide();
        });
        $('#videoicon').click(function(){
            $('#videocolumn').show();
            $('#album-form').show();
            $('#textcolumn').hide();
            $('#post').hide();
            $('#createImageInfo').hide();
            $('#createvideoInfo').show();
            $('#imagecolumn').hide();
            $('#linkcolumn').hide();
            $('#youtubecolumn').hide();
        });
        $('#linkicon').click(function(){
            $('#linkcolumn').show();
            $('#createImageInfo').hide();
            $('#createvideoInfo').hide();
            $('#post').show();
            $('#album-form').show();
            $('#textcolumn').hide();
            $('#imagecolumn').hide();
            $('#videocolumn').hide();
            $('#youtubecolumn').hide();
        });
        
        
    });

    $(function () {
         $('#createImageInfo').click(function(e){
            e.preventDefault();
           var token = $('input[name=_token]').val();
            var imagetitle = $('#imagetitle').val();
            var message = $('#imagemessage').val();
            
        $.ajax({
        url: "/tourguide/uploadsImageText",
        type: 'POST',
        dataType: 'JSON',
        data:{imagetitle: imagetitle,
                imagemessage: message,
                _token:token
            },
        success: function(data) {

         var postId = data.postId;
         console.log('hello');
         console.log(data);
         if(data.success===true) {
            $('#fileupload').fileupload('send', {files : selectedFiles});
         }
         if(data.success===false) {
          var template = '<p class="alert alert-danger">select image first</p>';
            $('#imagecolumnerror').append(template);
         }
                          
        }
    });
    });
    'use strict';
    
  $('#fileupload').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });

    $('#fileupload').fileupload({
        url: '/tourguide/fileuploads',
        dataType: 'json',
        autoUpload: false,
        // singleFileUploads:false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 2000000,
        
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
                    location.reload();
         }
            }


    }).on('fileuploadadd', function (e, data) {
                  console.log(data);  
        data.context = $('<div class="imagePreview"/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p />')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        console.log(data);
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
// update post





      // video request

$(function () {
         $('#createvideoInfo').click(function(e){
            e.preventDefault();
           var token = $('input[name=_token]').val();
            var videotitle = $('#videotitle').val();
            var message = $('#videomessage').val();
            
        $.ajax({
        url: "/tourguide/uploadsvideoText",
        type: 'POST',
        dataType: 'JSON',
        data:{videotitle: videotitle,
                videomessage: message,
                _token:token
            },
        success: function(data) {

         var postId = data.postId;
         console.log(postId);
         if (data.success===true) {
            $('#fileuploads').fileupload('send', {files : selectedFiles});
         }
                          
        }
    });
    });
    'use strict';
    
  $('#fileuploads').bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });

    $('#fileuploads').fileupload({
        url: '/tourguide/fileuploadv',
        dataType: 'json',
        autoUpload: false,
        // singleFileUploads:false,
        acceptFileTypes: /^video\/.*$/,
        maxFileSize: 10000000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        // previewCrop: true,
        success: function(e ,data) {
            console.log(e);
             if (e.error == false) {
                alert('Check Your Video Type ');
            $('#errorvideo').append('<p class="btn btn-danger">its not valid video</p>')
        }
                if (e.success) {
                    location.reload();
         }
            }


    }).on('fileuploadadd', function (e, data) {
                  console.log(data);  
        data.context = $('<div/>').appendTo('#preview');
        $.each(data.files, function (index, file) {
            var node = $('<p class="imagePreview"/>')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        console.log(data);
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});


    

        


        function getprofileinfo(){
            $.ajax({
            url: "/tourguide/postinfo/",
            type: 'GET',
            dataType: 'JSON',
            success: function(data ) {
                $.each(data, function(index, el) {
        
                    if (el.type == 1) {
                    var template = _.template($('#textpost').html());
                    template = template({index,el});
                     $("#vendorPosts").append(template);
                    }
                    if (el.type == 2) {
                        console.log('true');
                        
                    var template = _.template($('#imagepost').html());
                    template = template({index,el});
                    $("#vendorPosts").append(template);
                        
                    }
                    if (el.type == 3) {
                    var template = _.template($('#videopost').html());
                    template = template({index,el});
                    $("#vendorPosts").append(template);
                    }
                    if (el.type == 4) {
                    var template = _.template($('#linkpost').html());
                    template = template({index,el});
                    $("#vendorPosts").append(template);
                    }
                 });
            }
        });
        };

    function deletePost(postid,attachmentid,filename){
        $.ajax({
        url: "/tourguide/profile/deletePost",
        type:"get",
        data:{postId : postid,attachmentId : attachmentid,filename : filename},
        success: function(retJson){
             if (retJson) {
              location.reload();
         }
       
        }
    });
    }

       function updatePost(postId){
            var title = $('input[name=imagetitles]').val();
           var message = $('textarea[name=imagemessages]').val();
         
        $.ajax({
        url: "/tourguide/updateImageText",
        type:"get",
        data:{title : title,message : message,postId :postId},
        success: function(retJson){
             if (retJson.success == true) {
                location.reload();
         }
       
        }
    });
    }
    function updatetextPost(postId){
         var title = $('input[name=texttitles]').val();
        var message = $('textarea[name=textmessages]').val();
        
           console.log(postId);
           console.log(title);
        $.ajax({
        url: "/tourguide/updateImageText",
        type:"get",
        data:{title : title,message : message,postId :postId},
        success: function(retJson){
             if (retJson.success == true) {
                location.reload();
         }
       
        }
    });
    }

    function updatevideoPost(postId){
         var title = $('input[name=videotitles]').val();
        var message = $('textarea[name=videomessages]').val();
        
           console.log(postId);
           console.log(title);
        $.ajax({
        url: "/tourguide/updateImageText",
        type:"get",
        data:{title : title,message : message,postId :postId},
        success: function(retJson){
             if (retJson.success == true) {
                location.reload();
         }
       
        }
    });
    }

    function editimagepost(id){

    var uploadid = '#updatefileupload'+id+""+id;
    console.log(uploadid);
    $('.profileimagesf'+id).mouseenter(function(){
        $(this).find('i').show();
    });
    $('.profileimagesf'+id).mouseleave(function(){
        $(this).find('i').hide();
    });


    $(document).on('click','.profileimagesf i',function(){
        var removeId= $(this).attr('data-id');
        console.log(removeId);
    });
  $('#updateimagefileupload'+id+""+id).bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });

    $('#updateimagefileupload'+id+""+id).fileupload({
        url: '/tourguide/updatefileuploads/'+id,
        dataType: 'json',
        autoUpload: true,
        // singleFileUploads:false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 2000000,
        
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
                    location.reload();
         }
            }


    }).on('fileuploadadd', function (e, data) {
                  console.log(data);  
        data.context = $('<div class="imagePreview"/>').appendTo('#ufiles'+id);
        $.each(data.files, function (index, file) {
            var node = $('<p />')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        console.log(data);
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


    }


    function editpost(id){
    var uploadid = '#updatevfileupload'+id;
    console.log(uploadid);
  $('#updatevfileupload'+id).bind('change', function (e) {
        var f;
        for(var i in e.target.files){
            if(i == "length"){
                break;
            }
            f = e.target.files[i]; 
            selectedFiles.push(f);
        }
    });

    $('#updatevfileupload'+id).fileupload({
        url: '/tourguide/updatevfileuploads',
        dataType: 'json',
        autoUpload: false,
        // singleFileUploads:false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 2000000,
        
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 500,
        previewMaxHeight: 500,
        previewCrop: true,
        success: function(e ,data) {
                if (e.success) {
                    location.reload();
         }
            }


    }).on('fileuploadadd', function (e, data) {
                  console.log(data);  
        data.context = $('<div class="imagePreview"/>').appendTo('#ufiles'+id);
        $.each(data.files, function (index, file) {
            var node = $('<p />')
                    .append($('<span class="imagePreviewname"/>').text(file.name));
                    
            if (!index) {
                // node
                //     .append('<br>')
                    //.append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        console.log(data);
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


    }

    function updatelinkPost(postId){
         var title = $('input[name=links]').val();
        
        $.ajax({
        url: "/tourguide/updateImageText",
        type:"get",
        data:{title : title,postId :postId},
        success: function(retJson){
             if (retJson.success == true) {
                location.reload();
         }
       
        }
    });
    }

    



    var jobListPages = 1;
    var loadedJobs = 0;
    $(document).ready(function(){
        $("#loadMore").click(function(){
            getJobs('new');
        }).click();
        $("#filter").change(function(){
           var filter = $(this).val();
                if (filter == 'old') {
                    $("#new").removeClass("active");
                    $("#old").addClass("active");
                }else{
                    $("#old").removeClass("active");
                    $("#new").addClass("active");
                }
                console.log($(this).val());
                getJobs(filter);
                        });
    });

    function getJobs(gdata){
        
        $.ajax({
        url: "/tourguide/getjobs/" + jobListPages,
        type: 'GET',
        dataType: 'JSON',
        data:{filter:gdata},
        success: function(data ) {
            console.log(data);
            loadedJobs += data.per_page;
            jobListPages = data.current_page + 1;
            var template = _.template($("#getjobListTmpl").html());
            template = template({data:data.data});
            $("#getjobList").append(template);
            if(loadedJobs >= data.total){
                $("#loadMore").hide();
            }
        }
    });
    }

    function removePostimg(id,bzId,path,name){
     
     console.log('hi');
     console.log(path);
        $.ajax({
        url: "/tourguide/info/removePostimg",
        type:"get",
        data:{remove_id : id,
              attachment_id : bzId,paths : path,filename : name},
        success: function(retJson){
             if (retJson) {
              location.reload();
         }
       
        }
    });
    }

     function removePostvideo(id,bzId,path,name){
     
    
     console.log(path);
        $.ajax({
        url: "/tourguide/info/removePostvideo",
        type:"get",
        data:{remove_id : id,
              attachment_id : bzId,paths : path,filename : name},
        success: function(retJson){
             if (retJson) {
              location.reload();
         }
       
        }
    });
    }

        $(function() {
          $.ajax({
        url: "/tourguide/progress",
        type:"get",
        success: function(retJson){
          console.log('prog');
            var data = JSON.parse(retJson);

          console.log(data.jobSuccess);

          var progwidth = data.jobSuccess;
          var temprog = progwidth/3;
  // $('#progress_bar2 .ui-progress .ui-label').hide();
  // Set initial value
  // $('#progress_bar2 .ui-progress').css('width', '7%');

  // Simulate some progress
  // $('#progress_bar2 .ui-progress').animateProgress(progwidth, function() {
  //   $(this).animateProgress(temprog, function() {
  //     setTimeout(function() {
  //       $('#progress_bar2 .ui-progress').animateProgress(progwidth, function() {
  //         $('#main_content').slideDown();
  //         $('#fork_me').fadeIn();
  //       });
  //     }, 2000);
  //   });
  // });
       
        }
    });
  // Hide the label at start
  
});

    //      var jobListPage = 1;
    //      var jobListPages = 1;
    // var loadedJobs = 0;
    // var loadedJob = 0;
    // $(document).ready(function(){
    //     $("#loadMore").click(function(){
    //         getJobs('new');
    //     }).click();
    //     $("#oldloadMore").click(function(){
    //         $('#filter')
    //         oldgetJobs();
    //     }).click();
    //     $("#filter").change(function(){
    //         $("#loadMore").hide();
    //        var filter = $(this).val();
    //         oldgetJobs(filter);
    //         console.log($(this).val());
    //     });
    // });
    

    // function getJobs(){
        
    //     $.ajax({
    //     url: "/vendor/getjobs/" + jobListPages,
    //     type: 'GET',
    //     dataType: 'JSON',
    //     success: function(data ) {
    //         console.log(data);
    //         loadedJobs += data.per_page;
    //         jobListPages = data.current_page + 1;
    //         var template = _.template($("#getjobListTmpl").html());
    //         template = template({data:data.data});
    //         $("#getjobList").append(template);
    //         if(loadedJobs >= data.total){
    //             $("#loadMore").hide();
    //         }
    //     }
    // });
    // }
    //  function oldgetJobs(gdata){
        
    //     $.ajax({
    //     url: "/vendor/getjobs/" + jobListPage,
    //     type: 'GET',
    //     dataType: 'JSON',
    //    data:{filter:gdata},
    //     success: function(data ) {
    //         var templates = _.template($("#loadMoreTmpl").html());
    //         $("#loadMores").append(templates);
    //         console.log(data);
    //         loadedJob += data.per_page;
    //         jobListPage = data.current_page + 1;
    //         var template = _.template($("#getjobListTmpl").html());
    //         template = template({data:data.data});
    //         $("#getjobList").html('');
    //         $("#getjobList").append(template);
    //         if(loadedJob >= data.total){
    //             $("#oldloadMore").hide();
    //         }
    //     }
    // });
    // }



    
       
    