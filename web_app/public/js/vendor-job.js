
    var jobListPage = 1;
    var loadedJobs = 0;
    $(document).ready(function(){
        $("#show-more").click(function(){
            getJobs();
        }).click();
    });
    

    function getJobs(){
        $.ajax({
        url: "/vendor/jobs/" + jobListPage,
        type: 'GET',
        dataType: 'JSON',
        success: function(data ) {
            loadedJobs += data.per_page;
            jobListPage = data.current_page + 1;
            var template = _.template($("#jobListTmpl").html());
            template = template({data:data.data});
            $("#jobList tbody").append(template);
            if(loadedJobs >= data.total){
                $("#show-more").hide();
            }
        }
    });
    }
    
        
    function declined(job_id,e)
    {
     var teamid = $(e).attr('data-id');
    $.ajax({
        url: "/vendor/invites/declined",
        type:"get",
        data:{declined_id : job_id,teamId : teamid},
        success: function(retJson){
            console.log(retJson);
             if (retJson.success) {
                    location.reload();
         }

            
        }
    });
}