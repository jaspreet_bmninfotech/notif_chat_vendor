
$(function() {

  // basic use comes with defaults values
  $(".skill").starRating({
    initialRating: 4.0,
    starSize: 25
  });
  $(".quality").starRating({
    initialRating: 4.0,
    starSize: 25
  });
  $(".availability").starRating({
    initialRating: 4.0,
    starSize: 25
  });
  $(".deadlines").starRating({
    initialRating: 4.0,
    starSize: 25
  });
  $(".communication").starRating({
    initialRating: 4.0,
    starSize: 25
  });
  $(".cooperation").starRating({
    initialRating: 4.0,
    starSize: 25
  });
  $(document).ready(function(){
var skillhiddenid_val = $("#skillhiddenid").val();
  
var total = parseFloat(skillhiddenid_val);
  $('#total').empty();
  $('#total').append(total);

$(".jq-stars").on('click', function(){
  var skill_valid = $(".skill input").last().val();
  
  if (skill_valid) {
      var skill_val = skill_valid;
  }else{
    var skill_val = 0;
  }
  var quality_valid = $(".quality input").last().val();
  if (quality_valid) {
      var quality_val = quality_valid;
  }else{
    var quality_val = 0;
  }
  var availability_valid = $(".availability input").last().val();
  if (availability_valid) {
      var availability_val = availability_valid;
  }else{
    var availability_val = 0;
  }
  var deadlines_valid = $(".deadlines input").last().val();
  if (deadlines_valid) {
      var deadlines_val = deadlines_valid;
  }else{
    var deadlines_val = 0;
  }
  var communication_valid = $(".communication input").last().val();
  if (communication_valid) {
      var communication_val = communication_valid;
  }else{
    var communication_val = 0;
  }
  var cooperation_valid = $(".cooperation input").last().val();
  if (cooperation_valid) {
      var cooperation_val = cooperation_valid;
  }else{
    var cooperation_val = 0;
  }
  var total = parseFloat(skill_val) + parseFloat(quality_val) + parseFloat(availability_val) + parseFloat(deadlines_val) + parseFloat(communication_val) + parseFloat(cooperation_val);
  console.log(skill_val);
  $('#total').empty();
  $('#total').append(total);
});
});
});
