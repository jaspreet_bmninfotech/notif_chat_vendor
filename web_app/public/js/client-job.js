function close_job(job_id)
{
    swal({
            title: 'Are you sure?',
            text: "It will be deleted permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            showLoaderOnConfirm: true,
            
            preConfirm: function() {
              return new Promise(function(resolve) {
                   
                 $.ajax({
                    url: '/client/job/close',
                    type: 'GET',
                    data: 'id='+job_id,
                    dataType: 'json'
                 })
         
                 .done(function(response){
                    swal('Deleted!', response.message, response.status);
                     setTimeout(function() {
                    window.location.href = "/client/job/all";
                    }, 1000);
                   
                 })
                 .fail(function(){
                    swal('Oops...', 'Something went wrong !', 'error');
                 });
              });
            },
            allowOutsideClick: false              
        });    
}