isValidEmail = false;
      isValidUsername = false;
      isValidPassword =false;
      isValidConfirmPassword =false;
      $(document).ready(function () {
        $('#checkuser').hide();
        $("#checkemail").hide();
        $("#checkpass").hide();
        $("#checkcpass").hide();
        $("#username").focusout(function (e) {
          check_username();
        });
        $("#email").focusout(function (e) {
          e.preventDefault();
          check_email();
        });
        $("#password").focusout(function (e) {
          check_password();
        });
        $("#cpassword").focusout(function (e) {
          check_confirmpassword();
        });
      });
      function check_username()
      {
        var pattern = /^[a-zA-Z]*$/;
          var username = $("#username").val();

          if (pattern.test(username) && username !== '' && username.length > 5 ) {
             $('#checkuser').hide();
            $.ajax({
              type: "GET",
              dataType: "json",
              url: "/auth/checkusername",
              data: "username=" + username,
              success: function (response) {
                if (response.code == 0) {
                  isValidUsername = false;
                  
                    $('#checkuser').html('<font color="red">' +
                    'The username  <b>Already</b> exist.</font>');
                     $('#checkuser').show();
                } else {
                  isValidUsername = true;
                  // $("#username").removeClass('object_ok'); // if necessary
                  // $("#username").addClass("object_error");
                  $('#checkuser').html('<font color="red">' + response.message + '</font>');
                  $('#checkuser').hide();
                }
              }
            });
          } else {
            isValidUsername = false;
            $("#checkuser").html('<font color="red">' +
              'The username should be minimum 6 characters.</font>');
            $('#checkuser').show();
          }
          if(username.length > 5 && !pattern.test(username))
          {
             isValidUsername = false;
            $("#checkuser").html('<font color="red">' +
              'The username should be only characters.</font>');
            $('#checkuser').show();
          }

      }
      function check_email()
      {
        var email = $("#email").val();
          
          if (validateEmail(email)) {
            $('#checkemail').hide();
         
            $.ajax({
              type: "GET",
              dataType: "json",
              url: "/auth/email/emailcheck",
              data: "email=" + email,
              success: function (response) {
                if (response.code == 0) {
                  isValidEmail = false;
                  // $("#checkemail").removeClass('object_error'); // if necessary
                  // $("#email").addClass("object_ok");
                  $('#checkemail').html('<font color="red">' +
                    'This email  <strong>Already</strong> exist.</font>');
                  $('#checkemail').show();
                } else {
                  isValidEmail = true;
                  $('#checkemail').html('<font color="red">' + response.message + '</font>');
                  $('#checkemail').show();
                }
                console.log(response.code);
              }
            });
          } else if(!validateEmail(email)){
            isValidEmail = false;
            $("#checkemail").html('<font color="red">'+'Invalid email address.</font>');
            $('#checkemail').show(); 
          }
      }
      function check_password()
      {
        var pass=$("#password").val();
        
          if(pass.length < 6)
          {
            isValidPassword=false;
            $("#checkpass").html('<font color="red">'+'Please Enter Strong Password.</font>');
            $("#checkpass").show();
          }
          else 
          {
            isValidPassword=true;
            $("#checkpass").hide();
          }
      }
      function check_confirmpassword()
      {
        var password=$("#password").val();
        var confirmpassword=$("#cpassword").val();
         if(password!=confirmpassword)
          {
            isValidConfirmPassword=false;
            $("#checkcpass").html('<font color="red">'+'Passwords did not matched.</font>');
              $("#checkcpass").show();
          }
          else
          {
            isValidConfirmPassword=true;
            $("#checkcpass").hide();
          }
      }
      function validateEmail(email) {
       var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return emailReg.test(email);
    }; 
      function ValidationEvent() {
        if(!isValidPassword || !isValidConfirmPassword || !isValidEmail || !isValidUsername)
        {
            return false;
        } else {
          return true;
        }

        // if (!isValidEmail || !isValidUsername) {
        //   return false;
        // } else {
        //   return true;
        // }
        return true;
      }
      function RegisterForm()
      {
      isValidEmail = false;
      isValidUsername = false;
      isValidPassword =false;
      isValidConfirmPassword =false;

      check_username();
      check_email();
      check_password();
      check_confirmpassword();
      if (isValidEmail===false && isValidUsername===false && isValidPassword===false && isValidConfirmPassword===false){
        return true;
      }else
      {
        return false;
      }
      }