
    $(document).ready(function(){
        
    });
    
    function quickJobDetails(jobId){
        $.ajax({
        url: "/vendor/jobs/quick-view-jobs/" + jobId,
        type: 'GET',
        dataType: 'JSON',
        success: function(data) {
            $("#quickjobdetailsdata").empty();
             var template = _.template($("#quickjobdetailsTmpl").html());
            template = template({data:data});
            $("#quickjobdetailsdata").append(template);
            $('.modal-header .pull-right').remove();
            $('.modal-header').append('<a target="_blank" href="/vendor/job/'+jobId+'/details" class="pull-right">Open in new window</a>');
        }
    });
    }
        
    