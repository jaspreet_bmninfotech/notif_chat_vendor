function getstate(e ,callback=false)
{

  var countryId =$(e).children(':selected').data('id');
  var stateid = $('#stateedit').val();

  $.ajax({
    url: "/getstate/"+countryId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#state').empty();
          $('#state').append('<option value="">select</option>');
          $.each(data,function(index,stateObj){
            $('#state').append('<option value="'+stateObj.id+'" data-id="'+stateObj.id+'">'+stateObj.name+'</option>');
          });
           if(stateid){
                $('#state').val($('#stateedit').val());
                $('#stateedit').val("");
            }
          if(typeof(callback) == 'function'){
            callback();
          } 

        }
  });

}
function getcity(es , callback =false)
{
  var selectedstateId=$(es).val();
  var city =$('#cityedit').val();
  $.ajax({
    url: "/getcity/"+selectedstateId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
         
          $('#city').empty();
          $('#city').append('<option value="">select</option>');
           
          $.each(data,function(index,cityObj){
           $('#city').append('<option value="'+cityObj.id+'" data-id="'+cityObj.id+'" >'+cityObj.name+'</option>');
        });
          if(city != ""){
            $('#city').val(city);
            $('#cityedit').val("");
          }
          if(typeof(callback) == 'function'){
            callback();
          }   
        }
  });

}

$(document).ready(function(){
if($('#stateedit').val() != ""){
       getstate($("#country")[0], function(){
        getcity($("#state")[0]);
       });

  // if($('#cityedit').val() != ""){
  //      getcity($("#state")[0]);
  //   }
  }
});