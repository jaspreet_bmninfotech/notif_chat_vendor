if($('#teammember tbody tr').length == 0){
    $('#teammember tbody').append('<tr class="bottom-border"><td valign="top" colspan="5" class="text-center" style="text-align:center">No data available in table</td></tr>');
}
if($('#teammember tbody tr').length == 5){
    $('#editteam').parents('.vfform').hide();
}

$("input#typeahead").typeahead({
    onSelect: function(item) {
    },
    ajax: {
        url: "/vendor/team/autoshowvendor",
        timeout: 500,
        displayField: "username",
        triggerLength: 2,
        method: "get",
        loadingClass: "loading-circle",
        preDispatch: function (query) {
            // showLoadingMask(true);
            return {
                search: query
            }
        },
        preProcess: function (response) {
            // showLoadingMask(false);
            if (response.success === false) {
                // Hide the list, there was some error
                return false;
            }
            if (response.success === true) {
                
                // Hide the list, there was some error
                return response.data;
             
           }
        }
    }
});


function declined(member_id)
{
    $.ajax({
        url: "/vendor/team/declined",
        type:"get",
        data:{declined_id : member_id},
        success: function(retJson){
            console.log(retJson);
             if (retJson.success) {
                    location.reload();
         }
            
        }
    });
}
function accept(member_id)
{
    $.ajax({
        url: "/vendor/team/accept",
        type:"get",
        data:{accepted_id : member_id},
        success: function(retJson){
             if (retJson.success) {
                    location.reload();
         }
            
        }
    });
}
function deleteteammember(team_id)
{
event.preventDefault();   
    $.ajax({
        url: "/vendor/team/delete",
        type:"get",
        data:{member_id : team_id},
        success: function(retJson){
            $('#team'+team_id).remove();
                var trcount=$('#teammember tbody tr').length;
               location.reload();
                if(trcount<1)
                {
                    $('#teammember tbody').append('<tr class="bottom-border"><td valign="top" colspan="5" class="text-center">No data available in table</td></tr>');
                }
         } 
    });
}
$('#searchteam').click(function(e)
{
    $('.search_team_result').html( '' );
    
    e.preventDefault();
    var _token = $("input[name='_token']").val();
    var name = $('input[name=cname]').val();
    var countryId = $('#country').children(':selected').data('id');
    var stateId = $('#fstate').val();
    var cityId = $('#fcity').val();
        var dataArray = { name : name,countryId : countryId,stateId : stateId,cityId : cityId };
        // console.log(dataArray);
        var dataPost = {};
        $.each(dataArray , function(k , e){
            if(e != ''){
                dataPost[k] = e;
            }
        });
        $.ajax({
                type: 'POST',
                url: '/client/team/searchteam',
                dataType: "json",
                data :{ '_token' : _token , 'data' : dataPost},
                success: function(data) {
                    console.log(data);
                if(data.success)
                {  
                    $.each(data.success , function(key , val){

                        $('.appended_team').find('.teamname > .name').html('<a href="" data-id="'+val.id+'">'+val.name+'</a>');
                        $('.appended_team').find('.invite-team').attr('data-id',val.id);
                         // $('.invite-team').attr('data-id',val.id);
                        var date= val.created_at;
                            var d=new Date(date.split("/").reverse().join("-"));
                            var dd=d.getDate();
                            var mm=d.getMonth()+1;
                            var yy=d.getFullYear();
                            var newdate=mm+"/"+dd+"/"+yy;
                        $('.appended_team').find('.teamname > .created_at').html('Created at :'+" "+ newdate);
                        $('.appended_team').find('.teamname > .tmember').html('Team Member :'+" "+ val.member);

                        $('.appended_team ').find('#appended_teams').html('<img src="/'+val.path+'" class="img-responsive img-circle">');
                        var dataAppend = $('.appended_team').html();
                        console.log(dataAppend);
                        $('.search_team_result').append(dataAppend);
                         $('.teamerror').html('');
                        $('.search_team_result').find('.new_team_append').removeClass('new_team_append');
                    });
                }else if(data.error){
                        $('.teamerrorshow').css('display','block');
                         $('.teamerror').html(data.error);
                    }
                }
        });
});
function initiate_invite(e)
{  
  var team_id=  $(e).attr('data-id');  
    $.ajax({
        url: "/client/team/details",
        dataType: 'json',
        method: 'GET',
        data :{t_id : team_id},
        success: function(retJson){
            console.log(retJson);
            $(".invitation_rcvr_name").html(retJson.data.tname);
            $(".invitation_uname").html('Admin : '+" "+ retJson.data.firstName+' '+retJson.data.lastName);
            $(".invitation_rcvr_id").val(retJson.data.tid);
        }
    });

    $('#invitation_modal').modal('show');
}
function send_invitation()
{
    var team_id= $('.invite-team').attr('data-id');
    if ($("textarea[name='invitation_message']").val() != '')
    {
        $.ajax({
            url: "/client/team/invite-to-team",
            headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
            data: {'form_data': $("#send_invitation_form").serialize(),'id':team_id},
            dataType: 'json',
            method: 'POST',
            success: function(retJson){
                alert(retJson.message);
                if (retJson.status)
                {
                    $('#invitation_modal').modal('hide');
                }
            }
        });
    }
    else
    {
        alert('Please add your custom message to post a job.');
    }
}