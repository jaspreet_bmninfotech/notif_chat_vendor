
  $( function() {
    $( "#sortable" ).sortable({
    	revert       : true,
       connectWith  : ".sortable",
       stop         : function(event,ui){
        callback(event, ui);
       }
    });
    var callback = function (event, ui) {
        var ids = [];
        $('#sortable').find('li').each(function () {
            ids.push($(this).data('id'));
        })
    }
    $( "#sortable" ).disableSelection();
  } );
// function getcat(e)
// {
//   var categoryId =$(e).children(':selected').val();
//   $.ajax({
//     url: "/admin/getcategory",
//     type: "GET",
//         dataType: "json",
//         data :{Id : categoryId},
//         success: function(data) 
//         {
// 	         if(data.name != "")
// 	     	{
// 	     		 $('#sortable').append('<li class="ui-state-default" value="'+data.id+'">'+data.name+'</li>');	
// 	     	}
//         }
//   });

// }


var categoryId="";
$(document).ready(function(){
var count= $('#sortable li').length;
    $('select[name=choosecat]').change(function(){
        categoryId = $(this).val();
        var options = this.getElementsByTagName("option");
        var optionHTML = options[this.selectedIndex].innerHTML;  
        var existingValues = $('input[name=homecategory]').val();
        var newvalue = existingValues+'-'+categoryId;
        $('input[name=homecategory]').val(newvalue);
       //  var n=$('#sortable li').length;
       //  var t= count+n;
       // if(t<=5)
       // {
        $('#sortable').append('<li class="ui-state-default" value="'+categoryId+'"><i data-id="'+categoryId+'" class="fa fa-times remove_icon" onclick="removecategory('+categoryId+',this)"></i>'+optionHTML+'</li>');
       // }
       // elseif(t=5)
       // {
       //   $('#sortable').find('li').last().remove();
       // }
	});
});
$(document).ready(function() {
    $(document).on('mouseenter','.ui-state-default',function(){
        $(this).find('i').show();
    });
     $(document).on('mouseleave','.ui-state-default',function(){
        $(this).find('i').hide();
    });
});
function removecategory(id,e){
        $.ajax({
        url: "/admin/setting/removecategory",
        type:"get",
        data:{r_id : id},
        success: function(retJson){
            if (retJson) {
                console.log(e);
              $(e).parents('.ui-state-default').remove();
              location.reload();
            }
        }
    });
}