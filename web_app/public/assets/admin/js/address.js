function getstate(e ,callback=false)
{
  var countryId =$(e).children(':selected').data('id');
  var stateid = $('#stateedit').val();
  
  $.ajax({
    url: "/admin/getstate/"+countryId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#state').empty();
          $('#state').append('<option value="">select</option>');
          $.each(data,function(index,stateObj){
            $('#state').append('<option value="'+stateObj.id+'" data-id="'+stateObj.id+'">'+stateObj.name+'</option>');
          });
           if(stateid){
                $('#state').val($('#stateedit').val());
                $('#stateedit').val("");
            }
          if(typeof(callback) == 'function'){
            callback();
          } 

        }
  });
}
function getcity(es , callback =false)
{
  var selectedstateId=$(es).val();
  var city =$('#cityedit').val();
  $.ajax({
    url: "/admin/getcity/"+selectedstateId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
          console.log(data);
          $('#city').empty();
          $('#city').append('<option value="">select</option>');
           
          $.each(data,function(index,cityObj){
           $('#city').append('<option value="'+cityObj.id+'" data-id="'+cityObj.id+'" >'+cityObj.name+'</option>');
        });
          if(city != ""){
            $('#city').val(city);
            $('#cityedit').val("");
          }
          if(typeof(callback) == 'function'){
            callback();
          }   
        }
  });

}
function addstate(e ,callback=false)
{
  var newcountryId =$(e).children(':selected').data('id');
  var newstateid = $('#stateedit').val();
  
  $.ajax({
    url: "/admin/getstate/"+newcountryId,
    type: "GET",
        dataType: "json",
        success: function(data) 
        {
          $('#state').empty();
          $('#state').append('<option value="">select</option>');
          $.each(data,function(index,stateObj){
            $('#state').append('<option value="'+stateObj.id+'" data-id="'+stateObj.id+'">'+stateObj.name+'</option>');
          });
           if(stateid){
                $('#state').val($('#stateedit').val());
                $('#stateedit').val("");
            }
          if(typeof(callback) == 'function'){
            callback();
          } 

        }
  });
}
if($('#stateedit').val() != ""){
       getstate($("#country")[0], function(){
        getcity($("#state")[0]);
       });
  }

// $(document).ready(function()
// {
//   $('#tabstate').hide();
//   $('#tabcity').hide();
//   function emptyvalues()
//   {
//     $('.addaddress input').val('');
//     $('.addaddress select option').prop('selected', function() {
//           return this.defaultSelected;
//     });
//   }
//   $('#addcountry').click(function()
//   {
//     emptyvalues();
//       $('#tabcountry').show();
//       $('#tabstate').hide();
//     $('#tabcity').hide();

//   });
//   $('#addstate').click(function()
//   {
//     emptyvalues();
//     $('#tabcountry').hide();
//     $('#tabstate').show();
//     $('#tabcity').hide();
   

//   });
//   $('#addcity').click(function()
//   {
//     $('#tabcountry').hide();
//     $('#tabstate').hide();
//     $('#tabcity').show();
//    emptyvalues();
//   });
// });
$(function(){
   $(document).on('click','.saveaddress',function(e){
   e.preventDefault();
    $( '#newcountry-error' ).html( "" );
     $( '#newsortName-error' ).html( "" );
     $( '#newphoneCode-error' ).html( "" );
    var newData= [];
    return new Promise((resolve , reject) => {
      $('.addaddress input , .addaddress select').each(function(data){
        if($(this).val()!='')
        { 

          var key = $(this).attr('name');
         
          var value = $(this).val();
          if(value != "[object Object]"){
            newData.push({key:key, value:value});
             //newData[key] = value;
          }
        }
      });
      resolve(newData);

    }).then((newdatas) => {
    var token = $('input[name=_token]').val();
      $.ajax({
        url: "/admin/address/add",
        type: "POST",
          data :{ datas: newdatas,_token :token},
            success: function(data) 
            {
                if(data.errors) {
                    if(data.errors.newcountry){
                        $( '#newcountry-error' ).html( data.errors.newcountry[0] );
                    }
                    if(data.errors.newsortName){
                        $( '#newsortName-error' ).html( data.errors.newsortName[0] );
                    }
                    if(data.errors.newphoneCode){
                        $( '#newphoneCode-error' ).html( data.errors.newphoneCode[0] );
                    }   
                }
               if (data.success){
                location.reload();
                }
            }
      });    
    });
    });
   $(document).on('click','.savestate',function(e)
   {
    $('#newstate-error').html( "" );
    var cid=$('input[name=cid]').val();
    var token = $('input[name=_token]').val();
    var state= $('input[name=newstate]').val();

           $.ajax({
         url: "/admin/address/addstate",
         type:"post",
         data:{Id : cid,name:state,_token : token},
         success: function(data){
             if(data.errors) {
                    if(data.errors.newstate){
                        $( '#newstate-error' ).html( data.errors.newstate[0] );
                    }
                }
              if (data.success){
               location.reload();
          }
       
         }
        });
    });
});
var conid = '';
$(document).on('click', '.edit-modal', function() {
  conid = $(this).attr('data-id');
  $('#getcountry').val($(this).data('name'));
  $('#getsortName').val($(this).data('stname'));
  $('#getphoneCode').val($(this).data('code'));
  // $('#editModal').modal('show');
});

$('.modal-footer').on('click', '.editcountry', function() {
     $( '#getcountry-error' ).html( "" );
     $( '#getsortName-error' ).html( "" );
     $( '#getphoneCode-error' ).html( "" );
    var id = conid;
    $.ajax({
        type: 'post',
        url: '/admin/address/updatecountry',
        data: {
                'id':id,
                'getcountry'    : $("#getcountry").val(),
                'getsortName'   : $('#getsortName').val(),
                'getphoneCode'  : $('#getphoneCode').val()
        },
        success: function(data) {

             if(data.errors) {
                    if(data.errors.getcountry){
                        $('#getcountry-error').html( data.errors.getcountry[0] );
                    }
                    if(data.errors.getsortName){
                        $('#getsortName-error').html( data.errors.getsortName[0] );
                    }
                    if(data.errors.getphoneCode){
                        $('#getphoneCode-error').html( data.errors.getphoneCode[0] );
                    }   
                }
            if (data.success){
           location.reload();
        }
      }
    });
});
//     var did="";
// $(document).on('click', '.delete-modal', function() {
//         $('.dname').html($(this).data('name'));
//     });
$(document).on('click', '.deletecountry', function(e) {
  e.preventDefault();
       id= $(this).attr('data-id');
         swal({
           title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
         })
        .then(function(isConfirm){
          console.log(isConfirm);
          if (Object.keys(isConfirm)[0] == "dismiss") {
             swal("Cancelled", "Your imaginary file is safe :)", "error");
          } else {
              $.ajax({
                  type: 'get',
                  url: '/admin/address/deletecountry',
                  data: {
                      'id': id
                  },
                   success: function () {
                      swal("Done!","It was succesfully deleted!","success");
                      $('#country'+id).remove();
                      var trcount=$('.countrylist tbody tr').length;
                      if(trcount<1)
                      {
                          $('.countrylist tbody').append('<tr class="odd"><td valign="top" colspan="3" class="dataTables_empty">No data available in table</td></tr>');
                      }
                    }
              });   
          } 
        })  
      });
var stid = '';
$(document).on('click', '.state-editmodal', function() {
  stid = $(this).attr('data-id');
  
  $('#editstate').val($(this).data('name'));
  
});
$('.modal-footer').on('click', '.editstate', function() {
     $('#editstate-error').html( "" );
    var id = stid;
    $.ajax({
        type: 'post',
        url: '/admin/address/updatestate',
        data: {
              'id':id,
              'name': $("#editstate").val()
        },
        success: function(data) {
            if(data.errors) {
                    if(data.errors.editstate){
                        $( '#editstate-error' ).html( data.errors.editstate[0]);
                    }
                }
            if (data.success){
           location.reload();
        }
      }
    });
});
//   var dstateid="";
// $(document).on('click', '.delete-statemodal', function() {
//        dstateid= $(this).attr('data-id');
//         $('.dstate').html($(this).data('name'));
//     });
$(document).on('click', '.deletestate', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
       
         swal({
           title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
         })
        .then(function(isConfirm){
        
          if (Object.keys(isConfirm)[0] == "dismiss") {
             swal("Cancelled", "Your imaginary file is safe :)", "error");
          } else {
              $.ajax({
                  type: 'get',
                  url: '/admin/address/deletestate',
                  data: {
                      'id': id
                  },
                  success: function(data) {
                    swal("Done!","It was succesfully deleted!","success");
                       $('#state'+id).remove();
                      var trcount=$('.state tbody tr').length;
                      if(trcount<1)
                      {
                          $('.state tbody').append('<tr class="odd"><td valign="top" colspan="3" class="dataTables_empty">No data available in table</td></tr>');
                      }
                     
                  }
              });
          } 
        })  
});
$(document).on('click','.savecity',function(e)
   {
    $('#newcity-error').html( "" );
    var cityid=$('input[name=cityid]').val();
    var city= $('input[name=newcity]').val();
           $.ajax({
         url: "/admin/address/addcity",
         type:"post",
         data:{Id : cityid,name:city},
         success: function(data){
            if(data.errors) {
                    if(data.errors.newcity){
                        $('#newcity-error').html( data.errors.newcity[0]);
                    }
                }
              if(data.success){
               location.reload();
          }
       
         }
        });
});
var ctid = '';
$(document).on('click', '.city-editmodal', function() {
  ctid = $(this).attr('data-id');
  $('#editcity').val($(this).data('name'));
  
});
$('.modal-footer').on('click', '.editcity', function() {
    $('#editcity-error').html( "" );
    var id = ctid;
    $.ajax({
        type: 'post',
        url: '/admin/address/updatecity',
        data: {
              'id':id,
            'name': $("#editcity").val()
        },
        success: function(data) {
            if(data.errors) {
                    if(data.errors.editcity){
                        $('#editcity-error').html( data.errors.editcity[0]);
                    }
                }
            if (data.success){
           location.reload();
        }
      }
    });
});
//   var dcityid="";
// $(document).on('click', '.delete-citymodal', function() {
//        dcityid= $(this).attr('data-id');
//         $('.dcity').html($(this).data('name'));
// });
$(document).on('click', '.deletecity', function(e) {
   e.preventDefault();
         var id = $(this).attr('data-id');
         swal({
           title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
         })
        .then(function(isConfirm){
          
          if (Object.keys(isConfirm)[0] == "dismiss") {
             swal("Cancelled", "Your imaginary file is safe :)", "error");
          } else {
            $.ajax({
                type: 'get',
                url: '/admin/address/deletecity',
                data: {
                    'id': id
                },
                success: function(data) {
                   swal("Done!","It was succesfully deleted!","success");
                    $('#city'+id).remove();
                    var trcount=$('.city tbody tr').length;
                   
                    if(trcount<1)
                    {
                        $('.city tbody').append('<tr class="odd"><td valign="top" colspan="3" class="dataTables_empty">No data available in table</td></tr>');
                    }
                   
                }
            });
          } 
        })  
});

