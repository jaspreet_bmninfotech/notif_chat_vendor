// $(document).on('click','.savecategory',function(e)
//    {
//     e.preventDefault();
//     $('#categoryName-error').html( "" );
//     $('#description-error').html( "" );
//     var cname=$('input[name=categoryName]').val();
//     var token = $('input[name=_token]').val();
//     var cdes= $('textarea[name=description]').val();
//     var form_data = new FormData('form');
//     console.log('categoryfile');
//     console.log(form_data);
//     var catfile = categoryfile;   
//    $.ajax({
//         url: "/admin/category/add",
//         type:"post",
//         data:{name : cname,description : cdes,_token:token},
//         success: function(data){
//             console.log(data);
//             if(data.errors) {
//                     if(data.errors.name){
//                         $('#categoryName-error' ).html( data.errors.name[0] );
//                     }
//             }
//             if (data.success){
//               $.ajax({
//                 url: "/admin/category/icon",
//                 type:"get",
//                 data:form_data,
//                 contentType:false,
//                 cache:false,
//                 processData:false,
//                 success: function(data){
//                     location.reload();
//                 }
//               });

//         }
//     }
// });
// });
 
 document.getElementById('catfiles').onchange = function(e){
    var path = URL.createObjectURL(event.target.files[0]);
    $('.catfilesap').append('<img src="'+path+'" width="100%">');
    if (e.target.files.length < 6) {
         for (var i = 0; i < e.target.files.length;i++) {
            if (e.target.files[i].size <= 2000000) {
          // $.each(e.target.files, function (index, file) {
          var image = e.target.files[i];
           }else{
              alert('File size should be 2mb');
            } 
         }
     }else{
      alert('Maximum five files are allowed');
    }
    
 };

$(document).on('click','.saveSubCategory',function(e){
    $('#subname-error').html( "" );
    var subCatId=$('input[name=subId]').val();
    var token = $('input[name=_token]').val();
    var subCatName= $('input[name=subname]').val();
    var subCatDesc= $('textarea[name=subcat_description]').val();
    $.ajax({
        url: "/admin/category/addsubcat",
        type:"post",
        data:{Id : subCatId, subname : subCatName, subdesc : subCatDesc, _token:token},
        success: function(data){
            console.log(data);
             if(data.errors) {
                     if(data.errors.name){
                        $( '#subname-error' ).html( data.errors.name[0] );
                    }
                }
              if (data.success){
                location.reload();
                }
       
         }
    });
});
$(document).on('click','.saveSubSubCategory',function(e){
    $('#sub_subname_error').html( "" );
    var scatId  =$('input[name=sub_subId]').val();
    var token = $('input[name=_token]').val();   
    var scatName= $('input[name=sub_subname]').val();
    var scatDesc= $('textarea[name=sub_subdesc]').val();
    console.log("{{route('category.subsubcategory.add')}}");
    $.ajax({
        url: "/admin/category/sub",
        type:"post",
        data:{Id : scatId, subname : scatName, subdesc : scatDesc, _token:token},
        success: function(data){
            console.log(data);
             if(data.errors) {
                    if(data.errors.name){
                        $( '#sub_subname_error' ).html( data.errors.name[0] );
                    }
                }
              if(data.success){
                 location.reload();
                }
       
         }

    });
});
var catid = '';
$(document).on('click', '.edit-modal', function(e) {
    $( '#getCatName-error' ).html( "" );
     $( '#getCatDesc-error' ).html( "" );
  catid = $(this).attr('data-id');
  $('#getCatName').val($(this).data('name'));
  $('#getCatDesc').val($(this).data('desc'));
});
$('.modal-footer').on('click', '.editcategory', function() {
     $( '#getCatName-error' ).html( "" );
     $( '#getCatDesc-error' ).html( "" );
    var id = catid;
    $.ajax({
        type: 'post',
        url: '/admin/category/edit',
        data: {
                'id':id,
                'getcatname'    : $("#getCatName").val(),
                'getcatdesc'  : $('#getCatDesc').val(),
                '_token'       :$('input[name=_token]').val()
        },
        success: function(data) {
             if(data.errors) {
                    if(data.errors.getcatname){
                        $('#getCatName-error').html( data.errors.getcatname );
                    }
                }
            if (data.success){
                location.reload();
        }
      }
    });
});
var subcatid = '';
$(document).on('click', '.edit-subModal', function(e) {
    $( '#getsubCatName-error' ).html( "" );
     $( '#getsubCatDesc-error' ).html( "" );
  subcatid = $(this).attr('data-id');
  $('#getsubCatName').val($(this).data('name'));
  $('#getsubCatDesc').val($(this).data('desc'));
});
$('.modal-footer').on('click', '.editsubcategory', function() {
     $( '#getsubCatName-error' ).html( "" );
     $( '#getsubCatDesc-error' ).html( "" );
    var id = subcatid;
    $.ajax({
        type: 'post',
        url: '/admin/category/subcat/edit',
        data: {
                'id':id,
                'name'    :     $('#getsubCatName').val(),
                'description'  : $('#getsubCatDesc').val(),
                '_token'       :$('input[name=_token]').val()
        },
        success: function(data) {
             if(data.errors) {
                    if(data.errors.name){
                        $('#getsubCatName-error').html( data.errors.name );
                    }
                }
            if (data.success){
                location.reload();
        }
      }
    });
});
var nextsubcatid = '';
$(document).on('click', '.edit-nextsubModal', function(e) {
    $( '#getnextCatName-error' ).html( "" );
     $( '#getnextCatDesc-error' ).html( "" );
  nextsubcatid = $(this).attr('data-id');
  $('#getnextCatName').val($(this).data('name'));
  $('#getnextCatDesc').val($(this).data('desc'));
});
$('.modal-footer').on('click', '.editnextsubcategory', function() {
     $( '#getnextCatName-error' ).html( "" );
     $( '#getnextCatDesc-error' ).html( "" );
    var id = nextsubcatid;
    $.ajax({
        type: 'post',
        url: '/admin/category/subcat/edit',
        data: {
                'id':id,
                'name'    :     $('#getnextCatName').val(),
                'description'  : $('#getnextCatDesc').val(),
                '_token'       :$('input[name=_token]').val()
        },
        success: function(data) {
             if(data.errors) {
                    if(data.errors.name){
                        $('#getnextCatName-error').html( data.errors.name );
                    }
                }
            if (data.success){
                location.reload();
        }
      }
    });
});